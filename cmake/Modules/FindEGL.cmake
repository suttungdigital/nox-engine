#
# Try to find EGL library and include path.
# Once done this will define
#
# EGL_FOUND
# EGL_INCLUDE_DIRS
# EGL_LIBRARIES

find_path(EGL_INCLUDE_DIR EGL/egl.h)
find_library(EGL_LIBRARY NAMES EGL)

if (EGL_INCLUDE_DIR)
	set(EGL_INCLUDE_DIRS ${EGL_INCLUDE_DIR})
endif ()

if (EGL_LIBRARY)
	set(EGL_LIBRARIES ${EGL_LIBRARY})
endif ()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(EGL DEFAULT_MSG EGL_LIBRARIES EGL_INCLUDE_DIRS)
