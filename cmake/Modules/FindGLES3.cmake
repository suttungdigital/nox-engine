#
# Try to find GLES3 library and include path.
# Once done this will define
#
# GLES3_FOUND
# GLES3_INCLUDE_DIRS
# GLES3_LIBRARIES

find_path(GLES3_INCLUDE_DIR GLES3/gl3.h)
find_library(GLES3_LIBRARY NAMES GLESv3)

if (GLES3_INCLUDE_DIR)
	set(GLES3_INCLUDE_DIRS ${GLES3_INCLUDE_DIR})
endif ()

if (GLES3_LIBRARY)
	set(GLES3_LIBRARIES ${GLES3_LIBRARY})
endif ()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GLES3 DEFAULT_MSG GLES3_LIBRARIES GLES3_INCLUDE_DIRS)
