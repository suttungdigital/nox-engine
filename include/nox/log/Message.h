/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOG_MESSAGE_H_
#define NOX_LOG_MESSAGE_H_

#include <nox/common/api.h>
#include <nox/util/Mask.h>

#include <string>

namespace nox { namespace log {

class NOX_API Message
{
public:
	enum class Level
	{
		INFO = 1 << 0,
		VERBOSE = 1 << 2,
		WARNING = 1 << 3,
		ERROR = 1 << 4,
		FATAL = 1 << 5,
		DEBUG_ = 1 << 6,
	};

	static util::Mask<Level> allLevels();

	Message(const Level level, const std::string& message, const std::string& loggerName);

	const std::string& getMessage() const;
	const Level& getLogLevel() const;
	const std::string getLogLevelString() const;
	const std::string& getLoggerName() const;

private:
	Level level;
	std::string message;
	std::string loggerName;
};

} }

#endif
