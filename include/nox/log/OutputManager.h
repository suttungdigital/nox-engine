/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOG_OUTPUTMANAGER_H_
#define NOX_LOG_OUTPUTMANAGER_H_

#include <nox/common/api.h>

#include <vector>
#include <memory>

namespace nox { namespace log {

class Output;
class Message;

class NOX_API OutputManager
{
public:
	OutputManager() = default;

	OutputManager(const OutputManager& other) = delete;
	OutputManager& operator=(const OutputManager& other) = delete;

	void log(const Message& message);

	void addLogOutput(std::unique_ptr<Output> output);
	bool removeLogOutput(const Output* output);

private:
	std::vector<std::unique_ptr<Output>> logOutputs;
};

} }

#endif
