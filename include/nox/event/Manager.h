/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_EVENT_MANAGER_H_
#define NOX_EVENT_MANAGER_H_

#include <nox/event/IBroadcaster.h>
#include <nox/util/thread/ThreadSafeMap.h>
#include <array>
#include <queue>
#include <vector>
#include <atomic>
#include <condition_variable>

namespace nox { namespace event {

class IListener;

/**
 * Queues and broadcast events to listeners.
 * The listeners will get the events through the
 * IEventListener::onEvent interface.
 * The event queue is double buffered, so that events queued
 * while broadcasting will be broadcasted with the
 * next broadcast.
 */
class Manager final: public IBroadcaster
{
public:
	static const Event::IdType BROADCAST_COMPLETE_EVENT;

	/**
	 * Create an EventManager.
	 * With each broadcast the active queue will be broadcasted
	 * and the next queue will be activated for the next broadcast.
	 */
	Manager();

	void addListener(IListener* listener, const Event::IdType& type, const std::string& name) override;
	void removeListener(IListener* listener, const Event::IdType& type) override;
	void queueEvent(std::shared_ptr<Event> event) override;
	void queueEvent(Event::IdType eventId) override;
	void triggerEvent(const std::shared_ptr<Event>& event) override;
	void triggerEvent(Event::IdType eventId) override;

	/**
	 * Broadcast all events queued by queueEvent call.
	 */
	void broadcastEvents();

	/**
	 * Clear all events currently queued.
	 */
	void clearEventQueue();

	/**
	 * Check if the manager has any queued events that have not been broadcasted.
	 */
	bool hasQueuedEvents() const;

private:
	struct ListenerData
	{
		explicit ListenerData(IListener* listener, const std::string& name):
			listener(listener),
			name(name)
		{}

		IListener* listener;
		std::string name;
	};

	class ListenerHandle
	{
	public:
		explicit ListenerHandle(IListener* listener, const std::string& name);
		explicit ListenerHandle(const ListenerData& data);

		void broadcastEvent(const std::shared_ptr<Event>& event);
		void invalidate();

		bool handlesListener(const IListener* listener) const;
		bool isValid() const;

	private:
		IListener* listener;
		std::string name;
	};

	struct ListenerList
	{
		ListenerList():
			listenerRemoved(false),
			occupied(false)
		{}

		std::vector<ListenerHandle> listeners;

		std::recursive_mutex listMutex;

		bool listenerRemoved;
		std::queue<ListenerData> additionQueue;

		bool occupied;
	};

	using EventQueue = std::queue<std::shared_ptr<Event>>;

	void broadcastEvent(const std::shared_ptr<Event>& event);

	static const std::size_t NUM_QUEUES = 2;

	std::array<EventQueue, NUM_QUEUES> eventQueues;
	unsigned int activeQueueIndex;
	mutable std::mutex eventQueueMutex;

	thread::ThreadSafeMap<Event::IdType, ListenerList> eventListeners;
};

} }

#endif
