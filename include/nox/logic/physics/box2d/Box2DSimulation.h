/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_BOX2DSIMULATION_H_
#define NOX_LOGIC_PHYSICS_BOX2DSIMULATION_H_

#include <nox/log/Logger.h>
#include <nox/app/graphics/2d/GeometrySet.h>
#include <nox/logic/actor/Identifier.h>
#include <nox/event/ListenerManager.h>
#include <nox/event/IListener.h>
#include <nox/logic/physics/Simulation.h>
#include <nox/util/Booker.h>
#include <nox/util/KeyBooker.h>
#include <nox/util/math/Box.h>
#include <nox/util/thread/ThreadBarrier.h>

#include <memory>
#include <unordered_map>
#include <set>
#include <queue>
#include <mutex>
#include <thread>
#include <functional>
#include <glm/glm.hpp>
#include <Box2D/Box2D.h>

namespace nox
{

namespace app
{
namespace graphics
{

class Light;

}
}

namespace logic
{

class IContext;

namespace physics
{

//#define TIME_LIGHT_CALCULATION


/**
 *  Handles physics simulation using the Box2D engine.
 */
class Box2DSimulation: public Simulation, public event::IListener, public b2DestructionListener
{
public:
	/**
	 *  Initiates the Box2D simulator.
	 */
	Box2DSimulation(IContext* logicContext);

	virtual ~Box2DSimulation();

	void setLogger(log::Logger logger);

	void setUniversalGravity(const glm::vec2& gravityForce) override;

	void onUpdate(const Duration& deltaTime) override;
	void onSyncState(const float updateAlpha) override;
	void updateLighting() override;
	void reset() override;

	void createActorBody(actor::Actor* actor, const BodyDefinition& bodyDefinition) override;
	void removeActorBody(const actor::Identifier& actor) override;
	int addActorSensor(const actor::Identifier& actor, const BodyShape& sensorShape, SensorCallback callback) override;
	void removeActorSensor(int sensorId) override;

	int createWeldJoint(const WeldJointDefinition& jointDef) override;
	int createRevoluteJoint(const RevoluteJointDefinition& jointDef) override;
	int createPrismaticJoint(const PrismaticJointDefinition& jointDef) override;

	void setPrismaticMotorSpeed(const int jointId, const float motorSpeed) override;
	void setPrismaticLimits(const int jointId, const float lower, const float upper) override;
	void setCollisionCategory(actor::Actor* actor, const std::string& collisionCategory) override;
	void addCollisionExclusions(actor::Actor* actor, const std::vector<std::string>& excludedCategories) override;
	void removeCollisionExclusions(actor::Actor* actor, const std::vector<std::string>& excludedCategories) override;
	float getPrismaticJointTranslation(const int jointId) const override;

	int createTargetJoint(const TargetJointDefinition& definition) override;
	void setTargetJointTargetPosition(const int jointId, const glm::vec2& target) override;

	bool removeJoint(int jointId) override;

	void setActorAngularVelocity(const actor::Identifier& actorId, const float velocity) override;
	void setActorPosition(const actor::Identifier& actorId, const glm::vec2& actorPosition) override;
	void moveActorTo(const actor::Identifier& actorId, const glm::vec2& actorPosition) override;
	void setActorVelocity(const actor::Identifier& actorId, const glm::vec2& actorVelocity) override;
	void setActorGravityMultiplier(const actor::Identifier& actorId, const float gravityMultiplier) override;
	void setActorLinearDampening(const actor::Identifier& actorId, const float linearDampening) override;
	void setActorAngularDampening(const actor::Identifier& actorId, const float angularDampening) override;
	void setActorRotation(const actor::Identifier& actorId, const float actorRotation) override;
	void rotateActorTo(const actor::Identifier& actorId, const float actorRotation) override;
	void setFixedRotation(const actor::Identifier& actor, bool status) override;
	void setRestitution(const actor::Identifier& actorId,  const float actorRestitution) override;
	void setActorBodyShape(const actor::Identifier& actor, const BodyShape& shape) override;
	void setActorContactCallback(const actor::Identifier& actor, SensorCallback callback) override;
	void activateActorBody(const actor::Identifier& actorId) override;
	void deactivateActorBody(const actor::Identifier& actorId) override;
	void setSensor(const actor::Identifier& actor, bool state) override;

	void applyForce(const actor::Identifier& actor, const glm::vec2& force) override;
	void applyForce(const actor::Identifier& actor, const glm::vec2& force, const glm::vec2& position) override;
	void applyTorque(const actor::Identifier& actor, const float torque) override;
	void applyImpulse(const actor::Identifier& actor, const glm::vec2& impulse) override;

	void setActorCollisionCallback(const actor::Identifier& actor, CollisionCallback callBack) override;
	void removeActorCollisionCallback(const actor::Identifier& actor) override;

	void setSticky(const actor::Identifier& actor, float stickiness) override;
	void unsetSticky(const actor::Identifier& actor) override;

	void setActorFriendGroup(const actor::Identifier& actor, const actor::Identifier& actorToBefriend) override;

	void setActorAwakeState(const actor::Identifier& actorId, bool state) override;

	glm::vec2 getVelocity(const actor::Identifier& actor) const override;
	glm::vec2 getPostition(const actor::Identifier& actorId) const override;
	float getRotation(const actor::Identifier& actorId) const override;
	float getAngularVelocity(const actor::Identifier& actor) const override;
	void setMode(SimulationMode mode) override;
	float getMassOfActor(const actor::Identifier& actor) override;
	glm::vec2 getActorCenterOfMass(const actor::Identifier& actorId) const override;
	bool hasFixedRotation(const actor::Identifier& actor) const override;
	glm::vec2 getGravitationalPullOnActor(const actor::Identifier& actorId) const override;
	glm::vec2 getStrongestGravitationalPullOnActor(const actor::Identifier& actorId) const override;
	std::vector<Contact> getActorContacts(const actor::Identifier& actorId) const override;

	void addLight(std::shared_ptr<app::graphics::Light> light) override;
	void removeLight(std::shared_ptr<app::graphics::Light> light) override;

	GravitationalForce* createGravitationalForce() override;
	bool removeGravitationalForce(const GravitationalForce* force) override;

	bool checkForIntersectionBetween(const glm::vec2& point1, const glm::vec2& point2) override;
	bool checkForStaticIntersectionBetween(const glm::vec2& point1, const glm::vec2& point2) override;
	float checkForIntersectionAndGetMassOfObjectsBetween(const glm::vec2& point1, const glm::vec2& point2, const std::unordered_set<actor::Identifier>* actorsIgnored = nullptr) override;
	float getMassOfObjectOverlappingPoint(const glm::vec2& point, const std::unordered_set<actor::Identifier>* actorsIgnored = nullptr) const override;
	bool isActorOverlappingActor(const actor::Identifier& actorId) override;
	bool isActorOverlappingAnotherActor(const actor::Identifier& firstActorId, const actor::Identifier& secondActorId) override;
	std::vector<actor::Actor*> getActorsOverlapping(const actor::Identifier& actorId) override;
	RayIntersection findFirstRayIntersection(const glm::vec2& rayStart, const glm::vec2& rayEnd, bool includeInactive = false) override;
	RayIntersection findFirstStaticRayIntersection(const glm::vec2& rayStart, const glm::vec2& rayEnd, bool includeInactive = false) override;
	float findStrongestGravitationalPullDirectionFromPoint(const glm::vec2& point, const float pointMass) const override;
	glm::vec2 findGravitationalForceFromPoint(const glm::vec2& point, const float pointMass) const override;

	bool isPointInsideFixture(const glm::vec2& point) const override;
	bool isPointShadowed(const glm::vec2& lightPoint, const glm::vec2& pointToCheck) const override;

	float findLengthOfLineBeforeHitObject(const glm::vec2& lineStart, const::glm::vec2& lineEnd, std::vector<actor::Identifier> ignoreActorId, const util::Mask<PhysicalBodyType>& bodyType) const override;

	actor::Actor* findActorIntersectingPoint(const glm::vec2& point, const util::Mask<PhysicalBodyType>& type, const std::vector<actor::Identifier>& ignoreActorId = std::vector<actor::Identifier>()) const override;
	std::vector<actor::Actor*> findActorsIntersectingAabb(const math::Box<glm::vec2>& aabbRect, const util::Mask<BodyState>& bodiesToFind) const override;
	std::vector<actor::Actor*> findEmptyActorsWithinAabb(const math::Box<glm::vec2>& aabbRect, const util::Mask<BodyState>& bodiesToFind) const override;
	std::vector<actor::Actor*> findActorsIntersectingShape(const BodyShape& shape, const glm::vec2& position, const float rotation) const override;
	bool actorAabbIntersectsAabb(const actor::Identifier& actorId, const math::Box<glm::vec2>& aabb) const override;

	void applyExplosiveImpulseInRadius(const glm::vec2& position, const float radius, const float impulse) override;

	void setWorld(std::unique_ptr<b2World> world);

	bool shouldIgnoreBodyFromLight(const b2Body* body) const;

	float getMassOfStrongestGravityPullBody(const actor::Identifier& actorId) override;

	float getDistanceToStrongestGravityPullBody(const actor::Identifier& actorId) override;

protected:
	/**
	 *  Calculates the visibility of a light.
	 *  Eg. where the light should cast light.
	 */
	void calculateVisibility(std::shared_ptr<app::graphics::Light>& light);

private:
	struct BodyData
	{
		BodyData():
			actor(nullptr),
			depth(0),
			previousPosition(0.0f, 0.0f),
			previousAngle(0.0f),
			categoryBits(1),
			maskBits(0xFFFF)
		{}

		actor::Actor* actor;		//!< Pointer to actor.
		std::string bodyName;	//!< Name of the actor's body.
		unsigned int depth; 	//!< Depth of the actor in the world.

		glm::vec2 gravitationalPull;
		glm::vec2 strongestGravitationalPull;

		b2Vec2 previousPosition;
		float32 previousAngle;

		uint16 categoryBits;
		uint16 maskBits;
	};

	struct FixtureData
	{
		int sensorCallbackId = -1;
	};

	struct ActorData
	{
		ActorData():
			actor(nullptr),
			mainBody(nullptr)
		{}

		actor::Actor* actor;
		b2Body* mainBody;
	};

	struct JointData
	{
		JointData():
		jointMapId(-1)
		{}

		int jointMapId;
	};

	/**
	 *  Used to check for points hitting bodies.
	 */
	class BodyQueryCallback: public b2QueryCallback
	{
	public:
		/**
		 *  Creates a new QueryCallback at a position.
		 *  @param point The position of the mouseclick
		 */
		BodyQueryCallback(const b2Vec2& point);

		/**
		 *  Takes a fixture and checks whether the mouse clicked on it.
		 *  @param fixture The fixture that is checked.
		 */
		bool ReportFixture(b2Fixture* fixture) override;

		b2Vec2 point;
		b2Fixture* fixture;
		std::vector<b2Body*> bodiesIgnored;
	};

	/**
	 *  Used to check for points hitting dynamic bodies.
	 */
	class DynamicBodyQueryCallback: public b2QueryCallback
	{
	public:
		/**
		 *  Creates a new QueryCallback at a position.
		 *  @param point The position of the mouseclick
		 */
		DynamicBodyQueryCallback(const b2Vec2& point);

		/**
		 *  Takes a fixture and checks whether the mouse clicked on it.
		 *  @param fixture The fixture that is checked.
		 */
		bool ReportFixture(b2Fixture* fixture) override;

		b2Vec2 point;
		b2Fixture* fixture;
		std::vector<b2Body*> bodiesIgnored;
	};

	/**
	 *  Used to check for points hitting static bodies.
	 */
	class StaticBodyQueryCallback: public b2QueryCallback
	{
	public:
		/**
		 *  Creates a new QueryCallback at a position.
		 *  @param point The position of the mouseclick
		 */
		StaticBodyQueryCallback(const b2Vec2& point);

		/**
		 *  Takes a fixture and checks whether the mouse clicked on it.
		 *  @param fixture The fixture that is checked.
		 */
		bool ReportFixture(b2Fixture* fixture) override;

		b2Vec2 point;
		b2Fixture* fixture;
		std::vector<b2Body*> bodiesIgnored;
	};

	/**
	 *  Used to check for overlapping bodies.
	 */
	class BodyOverlapsFixtureQuery: public b2QueryCallback
	{
	public:

		BodyOverlapsFixtureQuery(b2Body* body);

		/**
		*  Takes a fixture and checks if it overlaps.
		*  @param fixture The fixture that is checked.
		*  @return contiuneQuery true/false
		*/
		bool ReportFixture(b2Fixture* fixture) override;

		std::vector<b2Fixture*> fixtures;
		bool overlapping;
	};

	/**
	 *  Used to get overlapping bodies.
	 */
	class BodiesOverlappingBodyQuery : public b2QueryCallback
	{
	public:

		BodiesOverlappingBodyQuery(b2Body* body);

		/**
		*  Takes a fixture and get body(is) if it overlaps.
		*  @param fixture The fixture that is checked.
		*  @return contiuneQuery true/false
		*/
		bool ReportFixture(b2Fixture* fixture) override;

		std::set<b2Body*> overlappingBodies;

		bool overlapping;

	private:
		std::vector<b2Fixture*> fixtures;
	};

	class PointInsidePolygonCallBack : public b2QueryCallback
	{
	public:
		PointInsidePolygonCallBack(const b2Vec2& point);
		bool ReportFixture(b2Fixture* fixture) override;

		bool isInside;
		b2Vec2 point;
		float massOfObject;
		const Box2DSimulation* physics;
		const std::unordered_set<actor::Identifier>* actorsIgnored;
	};

	/**
	 *  Class used to check for intersection in a line.
	 *  Returns 0 at the first intersection.
	 */
	class LineIntersection : public b2RayCastCallback
	{
	public:
		float32 ReportFixture(b2Fixture* fixture,
				      const b2Vec2& point,
				      const b2Vec2& normal,
				      float32 fraction) override;

		bool intersectionFound;
		Box2DSimulation* physics;

	};

	class LineIntersectionTotalMass : public b2RayCastCallback
	{
	public:
		float32 ReportFixture(b2Fixture* fixture,
				      const b2Vec2& point,
				      const b2Vec2& normal,
				      float32 fraction) override;

		Box2DSimulation* physics;
		float totalMass;
		const std::unordered_set<actor::Identifier>* actorsIgnored;
	};

	class LineIntersectionStatic : public b2RayCastCallback
	{
	public:
		float32 ReportFixture(b2Fixture* fixture,
				      const b2Vec2& point,
				      const b2Vec2& normal,
				      float32 fraction) override;

		bool intersectionFound;
	};

	class ShadowedPointRayCastCallback : public b2RayCastCallback
	{
	public:
		ShadowedPointRayCastCallback();
		float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction) override;

		bool inShadow;
		const Box2DSimulation* physics;
	};

	class ClosestPointRayCastCallback : public b2RayCastCallback
	{
	public:
		ClosestPointRayCastCallback();
		float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction) override;

		b2Vec2 point;
		b2Vec2 normal;
		float32 fraction;
		b2Fixture* fixture;

		const Box2DSimulation* physics;
	};

	class ClosestStaticPointRayCastCallback : public b2RayCastCallback
	{
	public:
		ClosestStaticPointRayCastCallback();
		float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction) override;

		b2Vec2 point;
		b2Vec2 normal;
		float32 fraction;
		b2Fixture* fixture;

		const Box2DSimulation* physics;
	};

	class ClosestPointOfType : public b2RayCastCallback
	{
	public:
		ClosestPointOfType();
		float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction) override;

		b2Vec2 point;
		float32 fraction;
		std::vector<b2Body*> bodiesIgnored;

		unsigned char maskMap[3];
		util::Mask<PhysicalBodyType> mask;
	};

	/**
	 *	The Contact Listener allows us to override what happens when certain
	 *	Objects collide in the world.
	 */
	class ContactHandler: public b2ContactListener
	{
	public:
		ContactHandler();
		void init(const Box2DSimulation* physics, b2World* physicsWorld);

		void addStickyBody(const b2Body* body, const float forceNeededToStick);
		void removeStickyBody(const b2Body* body);

		void setCollisionCallback(const b2Body* body, const CollisionCallback& callback);
		void removeCollisionCallback(const b2Body* body);

		void setSensorCallback(const int sensorId, const SensorCallback& callback);
		void removeSensorCallback(const int sensorId);

		void handleContactTasks();
		void clear();

		/**
		 * If we have established a new contact
		 * @param contact the contact that is established
		 */
		void BeginContact(b2Contact* contact) override;

		/**
		 * If a contact ceases to exist.
		 * @param contact the contact
		 */
		void EndContact(b2Contact* contact) override;

		void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) override;
		void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) override;

	private:
		struct Collision
		{
			CollisionCallback collisionCallback;
			CollisionData collisionData;
			const b2Body* collisionBody;
		};

		struct BodiesToStick
		{
			b2Body* body1;
			b2Body* body2;
		};

		struct SensorTrigger
		{
			SensorCallback callback;
			glm::vec2 contactPoint;
			actor::Identifier sensorActorId;
			unsigned int contactId;
			int sensorId = -1;
			bool gainedContact;
		};

		const Box2DSimulation* physics;
		b2World* physicsWorld;
		util::Booker<unsigned int, b2Contact*> booker;

		std::unordered_map<const b2Contact*, unsigned int> contactMap;
		std::unordered_map<const b2Body*, CollisionCallback> collisionCallbackMap;
		std::unordered_map<const b2Body*, float> stickyBodiesMap;
		std::unordered_map<int, SensorCallback> sensorCallBackMap;

		std::vector<Collision> collisions;
		std::vector<BodiesToStick> stickyJointsToCreate;
		std::vector<SensorTrigger> triggeredSensors;

		std::mutex callbackMutex;

		unsigned int unbookId;
		bool unbook;
	};

	void onEvent(const std::shared_ptr<event::Event>& event) override;

	void multithreadedLightCalculation();

	/**
	* Add a body to the rendering of the physics.
	*/
	void addBodyToDebugRender(const b2Body* body);

	void removeBodyFromDebugRenderer(const b2Body* body);

	void addFixtureToDebugRenderer(const b2Fixture* fixture);

	void removeFixtureFromDebugRenderer(const b2Fixture* fixture);

	/**
	 * Remove a body from the world and all mappings.
	 */
	void removeBody(b2Body* body);

	void removeJointData(b2Joint* joint);

	/**
	 * Get the color to use for debug rendering.
	 * Depends on the state of the body.
	 * @param body Body to get color for.
	 * @return The color to use for debug rendering.
	 */
	glm::vec4 getBodyDebugColor(const b2Body* body) const;

	/**
	 * Get the color to use for debug rendering.
	 * The color is combined with the result of getBodyDebugColor on fixture's body.
	 */
	glm::vec4 getFixtureDebugColor(const b2Fixture* fixture) const;

	/**
	 * Sync the world bodies with the debug rendering.
	 * This function is expensive as it will rewrite all vertices
	 * for all fixtures.
	 */
	void syncDebugRendering();

	/**
	 * Sync the debug renering for one body.
	 * @param body Body to sync.
	 */
	void syncDebugBodyRendering(b2Body* body);

	/**
	 * Sync a Box2D fixture with a Polygon used for rendering.
	 * @param shapeFixture Fixture to sync.
	 * @param geometry Polygon to sync with.
	 */
	void syncDebugPolygonRendering(const b2Fixture* shapeFixture, app::graphics::Polygon* geometry, app::graphics::LineLoop* edges);

	void syncDebugCircleRendering(const b2Fixture* fixture, app::graphics::Polygon* geometry, app::graphics::LineLoop* edges, app::graphics::Line* angleLine);

	/**
	 * Sync the state of an actor with a body.
	 * Information as position and rotation will be synced.
	 * @param actor Actor to sync.
	 * @param body Body to sync with.
	 */
	void syncActorTransform(actor::Actor* actor, b2Body* body);

	/**
	 * Add a polygon shape to a body.
	 * Several fixtures might be created.
	 * @param body Body to add the polygon to.
	 * @param fixtureDef Definition used for fixture creation.
	 * @param polygon Polygon to create shape from.
	 * @param[out] createdFixtures The fixtures created, if not nullptr.
	 */
	void addPolygonShape(b2Body* body, b2FixtureDef fixtureDef, const util::BoostPolygon& polygon, std::vector<b2Fixture*>* createdFixtures = nullptr);

	/**
	 * Add a circle shape to a body.
	 * @param body Body to add the circle to.
	 * @param fixtureDef Definition used for fixture creation.
	 * @param radius Radius for the circle.
	 * @return Fixture created for the circle.
	 */
	b2Fixture* addCircleShape(b2Body* body, b2FixtureDef fixtureDef, float radius, const glm::vec2& position);

	/**
	 * Add a rectangle shape to a body.
	 * @param body Body to add the rectangle to.
	 * @param fixtureDef Definition used for fixture creation.
	 * @param box To set lower and upper bound.
	 */
	void addRectangleShape(b2Body* body, b2FixtureDef fixtureDef, math::Box<glm::vec2> box);

	/**
	 * Add a shape to a body.
	 * Several fixtures might be created.
	 * @param body Body to add the shape to.
	 * @param shape Shape to add.
	 * @param fixtureDef Definition used for fixture creation.
	 * @param[out] createdFixtures The fixtures created, if not nullptr
	 */
	void addBodyShape(b2Body* body, const BodyShape& shape, b2FixtureDef fixtureDef, std::vector<b2Fixture*>* createdFixtures = nullptr);

	/**
	 * Set the shape of body.
	 * The body's fixtures will be deleted, and new ones created for the shape.
	 * @param body Body to set shape on.
	 * @param shape Shape to set on body.
	 */
	void setBodyShape(b2Body* body, const BodyShape& shape);

	/**
	 * Get the actor for a body.
	 * @param body Pointer to body to get actor for.
	 * @return The actor body found.
	 */
	actor::Actor* getActor(const b2Body* body) const;

	actor::Identifier getActorId(const b2Body* body) const;

	/**
	 * Get the body for an actor.
	 * @param actorBody ID of actor to get body for.
	 * @return Pointer to body of actor, or nullptr if actor with bodyName is not in physics.
	 */
	b2Body* findBody(const actor::Identifier& actorBody) const;

	/**
	 * Set the body as active.
	 * The body will be added to the world collision system.
	 * The body will be removed from the inactive dynamic tree.
	 * Does nothing if body already is active.
	 *
	 * @param body Pointer to body to activate.
	 */
	void activateBody(b2Body* body);

	/**
	 * Set the body as inactive.
	 * The body will be removed from the world collision system.
	 * The body will be added to the inactive dynamic tree.
	 * Does nothing if body already is inactive.
	 *
	 * @param body Pointer to body to deactivate.
	 */
	void deactivateBody(b2Body* body);

	void addToInactiveTree(b2Body* body);
	void removeFromInactiveTree(const b2Fixture* fixture);
	void removeFromInactiveTree(const b2Body* body);

	void updateInactiveBodyAabb(const b2Body* body, const b2Vec2& relativePosition);

	int16 getOrCreateActorFriendGroupIndex(const actor::Identifier& actor);
	int16 getMappedActorFriendGroupIndex(const actor::Identifier& actor);
	void applyActorGroupIndex(const actor::Identifier& actorId, const int16 groupIndex);

	void setBodyPosition(b2Body* body, const b2Vec2& actorPosition);
	void setBodyAngle(b2Body* body, const float angle);

	void SayGoodbye(b2Fixture* fixture) override;
	void SayGoodbye(b2Joint* joint) override;

	mutable log::Logger log;

	event::ListenerManager listener;

	IContext* logicContext;

	//! The physical world used by Box2D to simulate the objects.
	std::unique_ptr<b2World> world;

	//! Maps actor ID's to data defined by ActorData.
	std::unordered_map<actor::Identifier, ActorData> actorMap;

	//! Lights active in the world.
	std::vector<std::shared_ptr<app::graphics::Light>> lights;

	std::vector<b2Body*> dynamicShadowcastingBodies;

	/*
	 *  The number of iterations to be done on the physics system.
	 */
	int32 velocityIterations;
	int32 positionIterations;

	/*
	 *  The colors used for the different states and types of the objects.
	 */
	glm::vec3 staticColor;
	glm::vec3 kinematicColor;
	glm::vec3 dynamicColor;
	glm::vec4 edgeColor;
	glm::vec4 angleColor;

	/*
	 *  As long as we don't change the number of segments per circle we
	 *  don't need to recalculate the theta, cosine or sine each time.
	 *  For that reason, they are calculated at init time.
	 */
	unsigned int circleNumberOfSegments;
	float circleTheta;
	float circleCosine;
	float circleSine;

	/*
	 *  Used by box2D to detect mouse clicks and create a mouse joint between
	 *  objects that are clicked.
	 */
	b2Body* groundBody;
	b2MouseJoint* mouseJoint;
	float mouseScrollScaler;
	float mouseMaxForce;

	int rayMinimumNumber;
	float rayMaxFraction;
	float rayTheta;
	float rayLengthExtra;

	/**
	 * The depth of the lights in the world.
	 * Objects with a depth less than this will not cast a shadow.
	 */
	unsigned int lightDepth;

	//! Lights ready to be processed by the light calculation
	std::queue<std::shared_ptr<app::graphics::Light>> lightQueue;

	//! Used when retrieving a light from the queue.
	std::mutex lightQueueMutex;

	//! The barrier that causes wait before the light areas are ready to be processed.
	std::unique_ptr<thread::ThreadBarrier> lightAreaProcessingBarrier;

	//! The barrier that causes the main to wait for the calculation to finish.
	std::unique_ptr<thread::ThreadBarrier> lightAreaInsertBarrier;

	//! The worker threads used to calculate light.
	std::vector<std::thread> workers;

	//! Set to true when lighting threads should quit.
	bool quitThreads;

	std::unordered_map<const b2Fixture*, app::graphics::Polygon*> debugPolygonMap;
	std::unordered_map<const b2Fixture*, app::graphics::LineLoop*> debugShapeEdgeMap;
	std::unordered_map<const b2Fixture*, app::graphics::Line*> debugCircleLineMap;

//	std::unordered_map<const b2Body*, LineLoop*> debugBodyMassCenter;

	std::shared_ptr<app::graphics::GeometrySet> polygonRenderSet;
	std::shared_ptr<app::graphics::GeometrySet> lineRenderSet;

	util::Booker<int, b2Fixture*> sensorFixtures;

	util::Booker<int, b2Joint*> joints;

	std::vector<std::unique_ptr<GravitationalForce>> gravitationalForces;

	SimulationMode activeMode;

	glm::vec2 visibilitySize;

	//! Handles different things on contact.
	ContactHandler contactHandler;

	//! Must be locked when something can interfere with the update step (body deletion, etc.)
	std::mutex physicsUpdateMutex;

	//! Must be locked when something in the world is modified or accessed (body deletion, iterating, accessing, etc.)
	mutable std::mutex worldModifyMutex;

	//! Dynamic tree that contains all the inactive bodies in the world.
	std::unique_ptr<b2DynamicTree> inactiveBodyDynamicTree;

	//! The proxy for each fixture in inactiveBodyDynamicTree.
	std::unordered_map<const b2Fixture*, int32> inactiveBodyProxies;

	//! Must be locked when dealing with the inactive bodies in the world (inactiveBodyDynamicTree).
	mutable std::mutex inactiveBodyMutex;

	bool debugRenderingEnabled;

	util::KeyBooker<int16, util::Decrementor<int16>> friendGroupIndexBooker;
	std::unordered_map<actor::Identifier, int16> friendGroupIndexMap;

	std::map<std::string, uint16> bodyCategoryMap;
	uint16 nextCategoryBit;

#ifdef TIME_LIGHT_CALCULATION
	std::chrono::high_resolution_clock::duration lightCalculationTime;
	unsigned int frameCount = 0;
	const unsigned int NUM_AVERAGING_FRAMES = 60;
	std::mutex lightTimingCalculationMutex;
#endif
};

} } }

#endif
