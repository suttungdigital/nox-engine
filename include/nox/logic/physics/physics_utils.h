/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_PHYSICSUTILS_H_
#define NOX_LOGIC_PHYSICS_PHYSICSUTILS_H_

#include <json/json.h>
#include <glm/glm.hpp>
#include <nox/util/boost_utils.h>
#include <nox/util/math/Box.h>
#include <nox/util/Mask.h>
#include <nox/logic/actor/Identifier.h>

namespace nox { namespace logic { namespace physics {

enum class PhysicalBodyType
{
	STATIC = (1 << 0),
	KINEMATIC = (1 << 1),
	DYNAMIC = (1 << 2)
};

enum class BodyState
{
	ACTIVE = (1 << 0),
	INACTIVE = (1 << 1)
};

enum class ShapeType
{
	CIRCLE,
	POLYGON,
	RECTANGLE,
	NONE
};

struct BodyShape
{
	BodyShape():
		circleRadius(0.0f),
		type(ShapeType::NONE)
	{}

	math::Box<glm::vec2> box;
	glm::vec2 position;
	util::BoostMultiPolygon polygons;
	float circleRadius;
	ShapeType type;
};

struct BodyDefinition
{
	BodyDefinition():
		linearVelocity(glm::vec2(0.0f)),
		angularVelocity(0.0f),
		angle(0.0f),
		angularDamping(0.0f),
		linearDamping(0.0f),
		density(0.0f),
		friction(0.2f),
		restitution(0.0),
		bullet(false),
		sensor(false),
		depth(9),
		bodyType(PhysicalBodyType::DYNAMIC),
		fixedRotation(false),
		gravityMultiplier(1.0f),
		active(true),
		collisionCategory("")
	{}

	glm::vec2 position;
	glm::vec2 linearVelocity;
	float angularVelocity;

	float angle;

	float angularDamping;
	float linearDamping;
	float density;
	float friction;
	float restitution;
	bool bullet;
	bool sensor;
	bool allowSleep = true;

	unsigned int depth;

	PhysicalBodyType bodyType;
	bool fixedRotation;

	BodyShape shape;
	float gravityMultiplier;

	bool active;

	actor::Identifier friendActorId;
	std::string collisionCategory;
	std::vector<std::string> maskCategories;
};

util::Mask<PhysicalBodyType> staticAndKinematicBodyTypes();
util::Mask<PhysicalBodyType> staticAndDynamicBodyTypes();
util::Mask<PhysicalBodyType> kinematicAndDynamicBodyTypes();
util::Mask<PhysicalBodyType> allBodyTypes();

util::Mask<BodyState> allBodyStates();

/**
 * Parse body shape data from a JSON value.
 *
 * # JSON Shape Properties
 * - __type__:string - What type of shape is it. Can be "circle", "box", "polygon", and "none". A "none" shape will have no shape.
 *
 * ## Circle Shape Properties
 * - __radius__:real - The radius of the circle. Default 1.
 *
 * ## Box Shape Properties
 * - __lowerBound__:vec2 - The bottom left corner of the box. Default vec2(0, 0).
 * - __upperBound__:vec2 - The top right corner of the box. Default vec2(1, 1).
 *
 * ## Polygon Shape Properties
 * - __vertexList__:array[vec2] - Vertices defining the polygon, in counter-clockwise order. May be concave. Must have at leat three.
 *
 * @param shapeJson JSON value to parse.
 * @param[out] shape Shape to output parsed data to.
 * @return If parsing succeeded or failed.
 */
bool parseBodyShapeJson(const Json::Value& shapeJson, BodyShape& shape);

BodyShape transformShape(const BodyShape& shape, const glm::vec2& position, const glm::vec2& scale, float rotation);

/**
 * Create a physics shape that is formed as a box.
 * The box will be positioned around [0,0].
 * @param size The size of the box.
 */
BodyShape makeBoxShape(const glm::vec2& size);

} } }

#endif
