/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_VIEW_H_
#define NOX_LOGIC_VIEW_H_

#include <nox/common/api.h>
#include <nox/common/types.h>
#include <string>
#include <json/value.h>

namespace nox { namespace logic
{

class IContext;

namespace actor
{

class Actor;

}

class NOX_API View
{
public:
	View();
	View(View&&);

	virtual ~View();

	View& operator=(View&&);

	actor::Actor* getControlledActor() const;
	void setControlledActor(actor::Actor* actor);

	virtual bool initialize(IContext* context);
	virtual void destroy();
	virtual void update(const Duration& deltaTime);

	virtual std::string getTypeString() const = 0;
	virtual void serialize(Json::Value& viewJson) const;

private:
	virtual void onControlledActorChanged(actor::Actor* controlledActor);

	actor::Actor* controlledActor;
};

} }

#endif
