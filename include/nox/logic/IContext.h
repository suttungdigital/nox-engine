/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_ICONTEXT_H_
#define NOX_LOGIC_ICONTEXT_H_

#include <nox/common/api.h>
#include <memory>

namespace nox
{

namespace log
{

class Logger;

}

namespace event {

class IBroadcaster;

}

namespace app
{

class IContext;

namespace resource
{

class IResourceAccess;

}

namespace storage
{

class IDataStorage;

}

}

namespace logic
{

class View;

namespace actor
{

class Identifier;

}

namespace physics
{

class Simulation;

}

namespace world {
class Manager;
}

/**
 * This interface class gives other parts of the program access to
 * the logic and its functionality.
 */
class NOX_API IContext
{
public:
	virtual ~IContext();

	/**
	 * Pause the logic.
	 * All systems except the events and rendering system will be paused.
	 * @param pause Pause or not.
	 */
	virtual void pause(bool pause) = 0;

	/**
	 * Check if the logic is paused.
	 */
	virtual bool isPaused() const = 0;

	virtual const View* findControllingView(const actor::Identifier& actorId) const = 0;

	virtual event::IBroadcaster* getEventBroadcaster() = 0;
	virtual physics::Simulation* getPhysics() = 0;
	virtual world::Manager* getWorldManager() = 0;

	virtual app::storage::IDataStorage* getDataStorage() = 0;
	virtual app::resource::IResourceAccess* getResourceAccess() = 0;
	virtual log::Logger createLogger() = 0;
};

}
}

#endif
