/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_GRAPHICS_ACTORLIGHT_H_
#define NOX_LOGIC_GRAPHICS_ACTORLIGHT_H_

#include <nox/logic/actor/Component.h>
#include <nox/app/graphics/2d/GeometrySet.h>
#include <nox/app/graphics/2d/Light.h>
#include <nox/app/graphics/2d/LightRenderNode.h>

#include <glm/glm.hpp>

namespace nox { namespace logic
{

namespace actor
{

class Transform;

}

namespace physics
{

class Simulation;

}

namespace graphics
{

/**
 * Renders a series of lights relative to the actor.
 */
class ActorLight: public actor::Component
{
public:
	static const IdType NAME;

	~ActorLight();

	const ActorLight::IdType& getName() const override;
	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;
	void onActivate() override;
	void onDeactivate() override;

	/**
	 * Sets the direction that all the lights cast.
	 * @param direction Direction in radian.
	 */
	void setCastDirection(float direction);

	/**
	 * Sets the range of a specific light.
	 * @param lightName the name of the light to set the range for.
	 * @param range Range in meters.
	 */
	void setLightRange(const std::string& lightName, float range);

	/**
	 * Get the range of a light.
	 * @param lightName the name of the light to get the range from.
	 * @return The range of the light.
	 */
	float getLightRange(const std::string& lightName) const;

	/**
	 * Disable all the lights.
	 */
	void disableLights();

	/**
	 * Enable all the lights.
	 */
	void enableLights();

	/**
	 * Disable a specified light by name.
	 * Will disable all lights that have this name.
	 * @param lightName The name we are looking for.
	 */
	void disableLight(const std::string& lightName);

	/**
	 * Enable a specified light by name.
	 * Will enable all lights that have this name.
	 * @param lightName The name we are looking for.
	 */
	void enableLight(const std::string& lightName);

	/**
	 * Check if a light is enabled.
	 * @param lightName Name of light to check.
	 */
	bool lightIsEnabled(const std::string& lightName) const;

private:
	void generateLightGeometry(const std::shared_ptr<app::graphics::Light>& light);
	void updateLightGeometry(const std::shared_ptr<app::graphics::Light>& light);

	void broadcastLightCreation(const std::shared_ptr<app::graphics::LightRenderNode>& lightNode);
	void broadcastLightRemoval(const std::shared_ptr<app::graphics::LightRenderNode>& lightNode);

	void enableLight(const std::shared_ptr<app::graphics::LightRenderNode>& lightNode);
	void disableLight(const std::shared_ptr<app::graphics::LightRenderNode>& lightNode);

	std::vector<std::shared_ptr<app::graphics::LightRenderNode>> lightNodes;

	physics::Simulation* physics;
	actor::Transform* transformComponent;
};

} } }

#endif
