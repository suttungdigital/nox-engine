/*
 * NOX Engine
 *
 * Copyright (c) 2015-2016 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_ACTOR_ACTOR_H_
#define NOX_LOGIC_ACTOR_ACTOR_H_

#include <nox/common/types.h>
#include <nox/logic/actor/Component.h>
#include <nox/logic/actor/Identifier.h>
#include <nox/log/Logger.h>

#include <unordered_map>
#include <memory>
#include <queue>
#include <iterator>
#include <mutex>

namespace nox { namespace logic
{

class IContext;

namespace actor
{

/**
 * An object in the game.
 *
 * An actor is only a container for functionality, which are described by ActorComponents.
 *
 * An actor has a unique ID.
 *
 * An actor can have multiple child actors that the parent owns. The children are managed by the parent, and many methods
 * called on the parent will also be called on the child. Examples are update(), destroy(), activate() and serialize().
 *
 * Actors go through a specific flow of states which are handled by the world::Manager. Component's will go through the same
 * state cycle as the Actor. Actor children only go through the destroy<->create cycle, and have to manually be activated.
 *
 * Allowed state changes:
 * - destroyed (default) -> created (create(), Component::onCreate())
 * - created -> destroyed (destroy(), Component::onDestroy())
 * - created -> active (activate(), Component::onActivate())
 * - active -> created (deactivate(), Component::onDeactivate())
 */
class Actor
{
public:
	struct Child
	{
		Child(const std::string& name, Actor* actor) :
			name(name),
			actor(actor)
		{
		}

		std::string name;
		Actor* actor;
	};

	/**
	 * An active state that the actor is forced into.
	 */
	enum class ForcedActiveState
	{
		ACTIVE,
		INACTIVE,
		NONE
	};

	/**
	 * Construct an actor.
	 *
	 * @warning You won't usually construct an Actor manually. Use the Factory.
	 *
	 * @param id Unique ID for the actor.
	 * @param name Name of the actor.
	 */
	Actor(Identifier id, const std::string& name);

	/**
	 * Construct an actor.
	 * See Actor(Identifier, const std::string&) for more parameter info.
	 * @param logger Logger to using for logging info.
	 */
	Actor(Identifier id, const std::string& name, log::Logger logger);

	/**
	 * Construct an actor.
	 * See Actor(Identifier, const std::string&) for more parameter info.
	 * @param definitionName Name of the definition that the actor was created from. This can be empty if the actor was created dynamically and not from a definition at all.
	 * @param extendedFrom Actor definition names that the actor was extended from. First definition is the top of the hierarchy. Last definition is the bottom of the hierarchy.
	 */
	Actor(Identifier id, const std::string& name, const std::string& definitionName, const std::vector<std::string>& extendedFrom);

	/**
	 * Construct an actor.
	 * See Actor(Identifier, const std::string&, const std::string&, const std::vector<std::string>&) for more parameter info.
	 * @param logger Logger to using for logging info.
	 */
	Actor(Identifier id, const std::string& name, const std::string& definitionName, const std::vector<std::string>& extendedFrom, log::Logger logger);

	Actor(const Actor&) = delete;
	Actor& operator=(const Actor&) = delete;

	/**
	 * Set the name of the actor.
	 */
	void setName(const std::string& name);

	/**
	 * Get the unique ID of this Actor instance.
	 */
	const Identifier& getId() const;

	/**
	 * Get the name of the actor.
	 * This name is not unique and can even be empty.
	 * @return The name of the actor.
	 */
	const std::string& getName() const;

	/**
	 * Get the name of the definition that this actor was created from.
	 */
	const std::string& getDefinitionName() const;

	/**
	 * Check if the actor is extended from a definition.
	 * This checks all the way to the bottom of its extend hierarchy.
	 * @param definitionName Definition name to check.
	 * @return true if it is extended from the definition, otherwise false.
	 */
	bool isExtendedFrom(const std::string& definitionName) const;

	/**
	 * Get the definition hierarchy that this was extended from.
	 * First definition is the top of the hierarchy.
	 */
	const std::vector<std::string>& getDefinitionsExtendedFrom() const;

	/**
	 * Create the Actor.
	 * A created Actor will be prepared for use in the world, but will not be simulated until
	 * activate() is called.
	 *
	 * A created Actor must be destroyed with destroy() before the object is destructed.
	 *
	 * @return if it was successfully created.
	 */
	bool create();

	/**
	 * Activate the Actor.
	 * An active Actor is actively simulated in the world. It will be updated and handle events.
	 *
	 * This only has an effect if the Actor is created.
	 */
	void activate();

	/**
	 * Deactivate the Actor.
	 * An inactive Actor is not simulated in the world. It will not be updated or handle events.
	 *
	 * This only has an effect if the Actor is activated.
	 */
	void deactivate();

	/**
	 * Destroy the Actor.
	 * A destroyed actor will not be prepared for used in the world.
	 *
	 * This only has an effect if the Actor is created and inactive.
	 *
	 * It should be called before the object is destructed.
	 *
	 * @return If it was successfully destroyed.
	 */
	bool destroy();

	/**
	 * Force the actor to be inactive or active.
	 *
	 * If an actor is forced to active or inactive, it will override the normal state flow for the actor.
	 * This also affects all children and components.
	 */
	void forceActiveState(const ForcedActiveState state);

	/**
	 * Make the Actor a trivial actor.
	 *
	 * A trivial Actor has little importance to the world and will be removed
	 * before any non-trivial Actors.
	 *
	 * An actor is by default non-trivial.
	 *
	 * @param trivial Trivial or not.
	 */
	void makeTrivial(const bool trivial);

	/**
	 * Check if actor is created.
	 */
	bool isCreated() const;

	/**
	 * Check if actor is active.
	 */
	bool isActive() const;

	/**
	 * Check if actor is forced to an active state.
	 */
	bool hasForcedActiveState() const;

	/**
	 * Check if the Actor is trivial.
	 * See setTrivial(const bool)
	 */
	bool isTrivial() const;

	/**
	 * Called whenever the actor should update its components' state.
	 * @param deltaTime The time since last update.
	 */
	void update(const Duration& deltaTime);

	/**
	 * Add a component to the actor.
	 *
	 * This function will fail if the Actor already has the same component type.
	 * In this case, the component will be destroyed, and false will be returned..
	 *
	 * @param component The component to add.
	 * @return If the component was added or not.
	 */
	bool addComponent(std::unique_ptr<Component> component);

	/**
	 * Find an ActorComponent of a specific type identified by the name.
	 *
	 * The ComponentType must be derived from ActorComponent and have the static const std::string member NAME.
	 *
	 * ComponentType::NAME will be used to identify which component to find.
	 *
	 * @tparam ComponentType The type of component to find.
	 * @return Pointer to the component of type ComponentType, or nullptr if not found.
	 */
	template<class ComponentType>
	ComponentType* findComponent() const;

	/**
	 * Find an ActorComponent of a specific type identified by the provided name.
	 *
	 * Because this function does not use a template to identify the type, the
	 * component cannot be casted.
	 *
	 * The name will be looked up against ComponentType::NAME
	 *
	 * It is recommended to use the template version whenever possible, to not
	 * be required to do a type-cast.
	 *
	 * @param name The name of component to find.
	 * @return Pointer to the component of type ComponentType, or nullptr if not found.
	 */
	Component* findComponent(const Component::IdType& name) const;

	/**
	 * Broadcast an event to all the components of this Actor.
	 *
	 * Used by other components to send messages locally through this Actor.
	 *
	 * If actor hasn't finished its onPostCreation(), the event will be queued until then.
	 *
	 * @param event Event to broadcast.
	 */
	void broadCastComponentEvent(const std::shared_ptr<event::Event>& event);

	/**
	 * Attach an actor as a child of this.
	 *
	 * This actor will take ownership and manage the child actor.
	 * If an actor with the same name already exists as a child, this actor won't be added, and the ownership won't be taken.
	 *
	 * @param actor Actor to attach as a child.
	 * @param actorName Name of the child actor. Used to map child actors (used by e.g. findChildActor() and detachChildActor()).
	 * @return false if a child actor with the same name exists, otherwise true.
	 */
	bool attachChildActor(std::unique_ptr<Actor> actor, const std::string& actorName);

	/**
	 * Detach a child actor.
	 *
	 * Releases the ownership and management of the child actor. The caller receives
	 * the new ownership and must ensure all external mappings are properly removed before
	 * letting the actor be destroyed.
	 *
	 * @param actorName Name of the child actor to detach.
	 * @return Child actor detached.
	 */
	std::unique_ptr<Actor> detachChildActor(const std::string& actorName);

	/**
	 * Detach all of the child actors.
	 *
	 * Releases the ownership and management of all of the child actors. The caller receives
	 * the new ownership and must ensure all external mappings are properly removed before
	 * letting the actors be destroyed.
	 *
	 * The returned vector will be empty if the actor does not have any child actors.
	 *
	 * @return Vector of all the detached child actors.
	 */
	std::vector<std::unique_ptr<Actor>> detachAllChildActors();

	/**
	 * Find a child actor recursively with a name.
	 *
	 * Child names can be chained to find grandchildren and so on. Each child is chained
	 * with a dot ('.'). Example: "child.grandchild.greatgrandchild".
	 *
	 * @param actorName Name of the child actor.
	 * @return Pointer to the actor found, or nullptr if not found.
	 */
	Actor* findChildActor(const std::string& actorName) const;

	/**
	 * Find all the children belonging to this actor and their children recursively through the whole child directory.
	 * @warning Do not assume that the order of the returned actors has any meaning, it might be changed later.
	 * @return All of the children recursively belonging to this actor.
	 */
	std::vector<Child> findChildrenRecursively() const;

	/**
	 * Check if this actor is the parent to another actor.
	 * @param childActor Actor to check.
	 * @return true if this is the actor's parent, otherwise false.
	 */
	bool isParentTo(const Actor* childActor) const;

	/**
	 * Check if the Actor has a parent (it is attached as a child actor to another Actor).
	 */
	bool hasParent() const;

	/**
	 * Write the actor with all its properties and components to the provided json object.
	 * @param jsonObject Object to write data to.
	 * @param extendDefinition If true, the definition name of this actor will be written rather than the
	 * definition of the actor that this was extended from. Normally used for saving the actor state.
	 */
	void serialize(Json::Value& jsonObject, const bool extendDefinition = false) const;

private:
	std::vector<Actor::Child> findChildrenRecursively(const std::string& childName) const;
	void broadcastQueuedComponentEvents();

	log::Logger log;

	std::mutex stateMutex;

	std::vector<std::unique_ptr<Actor>> childActors;
	std::unordered_map<std::string, Actor*> childActorMap;

	Actor* parentActor;

	std::vector<std::unique_ptr<Component>> components;
	std::unordered_map<std::string, Component*> componentMap;

	std::queue<std::shared_ptr<event::Event>> queuedComponentEvents;
	std::mutex queuedEventMutex;

	std::string name;
	std::string definitionName;
	std::vector<std::string> extendedFrom;
	Identifier id;
	bool trivial;

	bool created;
	bool active;
	ForcedActiveState forcedActiveState;
};


inline const Identifier& Actor::getId() const
{
	return this->id;
}

inline const std::string& Actor::getName() const
{
	return this->name;
}

inline const std::string& Actor::getDefinitionName() const
{
	return this->definitionName;
}

template<class ComponentType>
inline ComponentType* Actor::findComponent() const
{
	auto componentIt = this->componentMap.find(ComponentType::NAME);
	if (componentIt == this->componentMap.end())
	{
		return nullptr;
	}

	return static_cast<ComponentType*>(componentIt->second);
}

inline bool Actor::isActive() const
{
	if (this->parentActor != nullptr && this->parentActor->isActive() == false)
	{
		return false;
	}
	else if (this->forcedActiveState == ForcedActiveState::ACTIVE)
	{
		return true;
	}
	else if (this->forcedActiveState == ForcedActiveState::INACTIVE)
	{
		return false;
	}
	else
	{
		return this->active;
	}
}

inline bool Actor::isCreated() const
{
	if (this->parentActor != nullptr && this->parentActor->isCreated() == false)
	{
		return false;
	}
	else if (this->isActive() == true)
	{
		return true;
	}
	else
	{
		return this->created;
	}
}

inline bool Actor::hasForcedActiveState() const
{
	return this->forcedActiveState != ForcedActiveState::NONE;
}

inline bool Actor::hasParent() const
{
	return this->parentActor != nullptr;
}

inline bool Actor::isTrivial() const
{
	return this->trivial;
}

} } }

#endif
