/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_ANDROID_FILEOUTPUTBUFFER_H_
#define NOX_ANDROID_FILEOUTPUTBUFFER_H_

#include <streambuf>
#include <vector>
#include <jni.h>

namespace nox { namespace android {

class FileOutputBuffer final: public std::streambuf
{
public:
	FileOutputBuffer(JNIEnv* javaEnv, jobject fileOutputStreamJava, const std::size_t bufferSize);
	FileOutputBuffer(JNIEnv* javaEnv, jobject fileOutputStreamJava);

	~FileOutputBuffer();

private:
	int_type overflow(int_type ch) override;
	int sync() override;

	void flush();
	void resetPointers();

	std::vector<char> buffer;

	JNIEnv* javaEnv;
	jclass fileOutputStreamClass;
	jmethodID writeMethodId;

	jobject fileOutputStreamJava;
	jbyteArray bufferJava;
};

} }

#endif
