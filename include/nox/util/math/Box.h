/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_MATH_BOX_H_
#define NOX_MATH_BOX_H_

#include <array>
#include <glm/gtc/matrix_transform.hpp>

namespace nox { namespace math
{

template<class VecType>
class Box
{
public:
	typedef typename VecType::value_type ValueType;

	Box();

	Box(const VecType& lowerBound, const VecType& upperBound);

	void setLowerBound(const VecType& point);
	void setUpperBound(const VecType& point);

	bool intersects(const VecType& point) const;
	bool intersects(const Box<VecType>& otherBox) const;

	ValueType getWidth() const;
	ValueType getHeight() const;
	VecType getDimensions() const;

	const VecType& getLowerLeft() const;
	const VecType& getUpperRight() const;
	VecType getLowerRight() const;
	VecType getUpperLeft() const;

	ValueType getLeft() const;
	ValueType getBottom() const;
	ValueType getRight() const;
	ValueType getTop() const;

	Box<VecType> findTransformedBoxEnvelope(const glm::vec2& translation, const float rotation) const;
	VecType getCenter() const;
	ValueType getPerimeter() const;

	VecType lowerBound;
	VecType upperBound;
};

template<class VecType>
Box<VecType>::Box()
{
}

template<class VecType>
Box<VecType>::Box(const VecType& lowerBound, const VecType& upperBound):
	lowerBound(lowerBound.x, lowerBound.y),
	upperBound(upperBound.x, upperBound.y)
{
}

template<class VecType>
inline bool Box<VecType>::intersects(const VecType& point) const
{
	if (point.x < this->lowerBound.x)
	{
		return false;
	}

	if (point.x >= this->upperBound.x)
	{
		return false;
	}

	if (point.y < this->lowerBound.y)
	{
		return false;
	}

	if (point.y >= this->upperBound.y)
	{
		return false;
	}

	return true;
}

template<class VecType>
inline bool Box<VecType>::intersects(const Box<VecType>& otherBox) const
{
	if (otherBox.getLeft() > this->getRight())
	{
		return false;
	}

	if (otherBox.getRight() < this->getLeft())
	{
		return false;
	}

	if (otherBox.getBottom() > this->getTop())
	{
		return false;
	}

	if (otherBox.getTop() < this->getBottom())
	{
		return false;
	}

	return true;
}

template<class VecType>
inline typename Box<VecType>::ValueType Box<VecType>::getWidth() const
{
	return this->upperBound.x - this->lowerBound.x;
}

template<class VecType>
inline typename Box<VecType>::ValueType Box<VecType>::getHeight() const
{
	return this->upperBound.y - this->lowerBound.y;
}

template<class VecType>
inline void Box<VecType>::setLowerBound(const VecType& point)
{
	this->lowerBound = point;
}

template<class VecType>
inline void Box<VecType>::setUpperBound(const VecType& point)
{
	this->upperBound = point;
}

template<class VecType>
inline const VecType& Box<VecType>::getLowerLeft() const
{
	return this->lowerBound;
}

template<class VecType>
inline const VecType& Box<VecType>::getUpperRight() const
{
	return this->upperBound;
}

template<class VecType>
inline VecType Box<VecType>::getLowerRight() const
{
	return VecType(this->getRight(), this->getBottom());
}

template<class VecType>
inline VecType Box<VecType>::getUpperLeft() const
{
	return VecType(this->getLeft(), this->getTop());
}

template<class VecType>
inline VecType Box<VecType>::getDimensions() const
{
	return VecType(this->upperBound - this->lowerBound);
}

template<class VecType>
Box<VecType> Box<VecType>::findTransformedBoxEnvelope(const glm::vec2& translation, const float rotation) const
{
	Box<glm::vec2> localArea(glm::vec2(this->lowerBound), glm::vec2(this->upperBound));

	glm::mat4 areaRotationMatrix;
	areaRotationMatrix = glm::translate(areaRotationMatrix, glm::vec3(translation, 0.0f));
	areaRotationMatrix = glm::rotate(areaRotationMatrix, rotation, glm::vec3(0.0f, 0.0f, 1.0f));

	std::array<glm::vec4, 4> rotatedAreaCorners;
	rotatedAreaCorners[0] = areaRotationMatrix * glm::vec4(localArea.getLowerLeft(), 0.0f, 1.0f);
	rotatedAreaCorners[1] = areaRotationMatrix * glm::vec4(localArea.getLowerRight(), 0.0f, 1.0f);
	rotatedAreaCorners[2] = areaRotationMatrix * glm::vec4(localArea.getUpperRight(), 0.0f, 1.0f);
	rotatedAreaCorners[3] = areaRotationMatrix * glm::vec4(localArea.getUpperLeft(), 0.0f, 1.0f);

	Box<glm::vec2> worldAreaEnvelope;

	for (const glm::vec4& rotatedCorner : rotatedAreaCorners)
	{
		if (rotatedCorner.x < worldAreaEnvelope.lowerBound.x)
		{
			worldAreaEnvelope.lowerBound.x = rotatedCorner.x;
		}
		else if (rotatedCorner.x > worldAreaEnvelope.upperBound.x)
		{
			worldAreaEnvelope.upperBound.x = rotatedCorner.x;
		}

		if (rotatedCorner.y < worldAreaEnvelope.lowerBound.y)
		{
			worldAreaEnvelope.lowerBound.y = rotatedCorner.y;
		}
		else if (rotatedCorner.y > worldAreaEnvelope.upperBound.y)
		{
			worldAreaEnvelope.upperBound.y = rotatedCorner.y;
		}
	}

	return Box<VecType>(VecType(worldAreaEnvelope.lowerBound), VecType(worldAreaEnvelope.upperBound));
}

template<class VecType>
inline VecType Box<VecType>::getCenter() const
{
	return 0.5f * (lowerBound + upperBound);
}

template<class VecType>
inline typename Box<VecType>::ValueType Box<VecType>::getLeft() const
{
	return this->lowerBound.x;
}

template<class VecType>
inline typename Box<VecType>::ValueType Box<VecType>::getBottom() const
{
	return this->lowerBound.y;
}

template<class VecType>
inline typename Box<VecType>::ValueType Box<VecType>::getRight() const
{
	return this->upperBound.x;
}

template<class VecType>
inline typename Box<VecType>::ValueType Box<VecType>::getTop() const
{
	return this->upperBound.y;
}

template<class VecType>
inline typename Box<VecType>::ValueType Box<VecType>::getPerimeter() const
{
	return Box<VecType>::ValueType(2.0) * (this->getWidth() + this->getHeight());
}

} }

#endif
