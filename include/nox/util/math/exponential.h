/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_MATH_EXPONENTIAL_H_
#define NOX_MATH_EXPONENTIAL_H_

#include <glm/common.hpp>
#include <glm/exponential.hpp>
#include <limits>
#include <type_traits>

namespace nox { namespace math
{

/**
 * Calculate the power of two for an integral type.
 * Efficient function using only one left bit shift.
 *
 * @warning The result is undefined if power<=0.
 *
 * @param power The power (2^power).
 * @tparam IntType Integral type to use.
 * @return The resulting number.
 */
template<typename IntType>
IntType powerOfTwo(IntType power);

/**
 * Find the lowest integral number larger or equal to
 * number (parameter) that is a power of two.
 */
template<typename IntType, typename NumberType>
IntType upperPowerOfTwo(NumberType number);

template<typename IntType>
inline IntType powerOfTwo(IntType power)
{
	static_assert(std::numeric_limits<IntType>::is_integer, "IntType powerOfTwo(IntType) only takes an integer as parameter.");

	return (IntType(1) << power);
}

template<typename IntType, typename NumberType>
IntType upperPowerOfTwo(NumberType number)
{
	static_assert(std::numeric_limits<IntType>::is_integer, "IntType must be of integer type");
	static_assert(std::is_arithmetic<NumberType>::value, "NumberType must be of arithmetic type.");

	return powerOfTwo(static_cast<IntType>(glm::ceil(glm::log(number) / glm::log(2.0))));
}

} }

#endif
