/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_UTIL_MASK_H_
#define NOX_UTIL_MASK_H_

#include <type_traits>
#include <utility>
#include <initializer_list>

namespace nox { namespace util
{

template<class MaskType, typename UnderlyingType = typename std::underlying_type<MaskType>::type>
class Mask
{
public:
	Mask();

	Mask(const MaskType mask);

	Mask(const std::initializer_list<Mask>& masks);

	Mask(const Mask& other);

	Mask& operator=(const Mask& other);

	Mask& operator|=(const Mask& other);

	Mask& operator&=(const Mask& other);

	Mask& operator^=(const Mask& other);

	Mask operator|(const Mask& other) const;

	Mask operator&(const Mask& other) const;

	Mask operator^(const Mask& other) const;

	Mask operator~() const;

	bool operator==(const Mask& other) const;

	bool operator!=(const Mask& other) const;

	explicit operator bool() const;

	bool operator!() const;

	void add(const Mask& mask);

	void remove(const Mask& mask);

	void reset();

	bool matches(const Mask& other) const;

	bool isNone() const;

	Mask invert() const;

	MaskType getRawMask() const;

private:
	MaskType mask;
};


template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType>::Mask():
	mask(MaskType(0x0))
{
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType>::Mask(const MaskType mask):
	mask(mask)
{
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType>::Mask(const std::initializer_list<Mask>& masks):
	Mask()
{
	for (const Mask<MaskType, UnderlyingType>& mask : masks)
	{
		*this = *this | mask;
	}
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType>::Mask(const Mask& other):
	mask(other.mask)
{
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType>& Mask<MaskType, UnderlyingType>::operator=(const Mask<MaskType, UnderlyingType>& other)
{
	this->mask = other.mask;
	return *this;
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType>& Mask<MaskType, UnderlyingType>::operator|=(const Mask& other)
{
	*this = *this | other;
	return *this;
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType>& Mask<MaskType, UnderlyingType>::operator&=(const Mask<MaskType, UnderlyingType>& other)
{
	*this = *this & other;
	return *this;
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType>& Mask<MaskType, UnderlyingType>::operator^=(const Mask<MaskType, UnderlyingType>& other)
{
	*this = *this ^ other;
	return *this;
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType> Mask<MaskType, UnderlyingType>::operator|(const Mask& other) const
{
	return Mask(MaskType(UnderlyingType(this->mask) | UnderlyingType(other.mask)));
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType> Mask<MaskType, UnderlyingType>::operator&(const Mask& other) const
{
	return Mask(MaskType(UnderlyingType(this->mask) & UnderlyingType(other.mask)));
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType> Mask<MaskType, UnderlyingType>::operator^(const Mask& other) const
{
	return Mask(MaskType(UnderlyingType(this->mask) ^ UnderlyingType(other.mask)));
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType> Mask<MaskType, UnderlyingType>::operator~() const
{
	return Mask(MaskType(~UnderlyingType(this->mask)));
}

template<class MaskType, typename UnderlyingType>
bool Mask<MaskType, UnderlyingType>::operator==(const Mask<MaskType, UnderlyingType>& other) const
{
	return (this->mask == other.mask);
}

template<class MaskType, typename UnderlyingType>
bool Mask<MaskType, UnderlyingType>::operator!=(const Mask<MaskType, UnderlyingType>& other) const
{
	return (this->mask != other.mask);
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType>::operator bool() const
{
	return !this->isNone();
}

template<class MaskType, typename UnderlyingType>
bool Mask<MaskType, UnderlyingType>::operator!() const
{
	return this->isNone();
}

template<class MaskType, typename UnderlyingType>
void Mask<MaskType, UnderlyingType>::add(const Mask<MaskType, UnderlyingType>& mask)
{
	*this |= mask;
}

template<class MaskType, typename UnderlyingType>
void Mask<MaskType, UnderlyingType>::remove(const Mask<MaskType, UnderlyingType>& mask)
{
	*this &= ~mask;
}

template<class MaskType, typename UnderlyingType>
bool Mask<MaskType, UnderlyingType>::matches(const Mask<MaskType, UnderlyingType>& other) const
{
	return !(*this & other).isNone();
}

template<class MaskType, typename UnderlyingType>
Mask<MaskType, UnderlyingType> Mask<MaskType, UnderlyingType>::invert() const
{
	return ~*this;
}

template<class MaskType, typename UnderlyingType>
bool Mask<MaskType, UnderlyingType>::isNone() const
{
	return (this->mask == MaskType(0x0));
}

template<class MaskType, typename UnderlyingType>
void Mask<MaskType, UnderlyingType>::reset()
{
	this->mask = MaskType(0x0);
}

template<class MaskType, typename UnderlyingType>
MaskType Mask<MaskType, UnderlyingType>::getRawMask() const
{
	return this->mask;
}

} }

#endif
