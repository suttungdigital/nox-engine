/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_APPLICATION_H_
#define NOX_APP_APPLICATION_H_

#include "IContext.h"
#include <nox/log/Logger.h>
#include <nox/common/api.h>
#include <nox/common/types.h>
#include <nox/util/process/Manager.h>

#include <chrono>
#include <string>
#include <memory>

namespace nox {

namespace log
{

class OutputManager;

}

namespace app
{

namespace resource
{

class Cache;

}

namespace audio
{

class System;

}

class ApplicationProcess;

class NOX_API Application: public IContext
{
public:
	explicit Application(const std::string& applicationName, const std::string& companyName);

	Application(Application&& other);
	Application& operator=(Application&& other);

	virtual ~Application();

	resource::IResourceAccess* getResourceAccess() const override;
	storage::IDataStorage* getDataStorage() const override;
	audio::System* getAudioSystem() const override;
	log::Logger createLogger() override;

	void quitApplication() override;

	void setIdle(const bool idle);
	void setIdleSleepTime(const Duration& sleepTime);
	void setTpsUpdateInterval(const Duration& updateInterval);

	void setResourceCache(std::unique_ptr<resource::Cache> cache);
	void setAudioSystem(std::unique_ptr<audio::System> audioSystem);
	void setDataStorage(std::unique_ptr<storage::IDataStorage> storage);
	void addProcess(std::unique_ptr<ApplicationProcess> process);

	const std::string& getName() const;
	const std::string& getOrganizationName() const;
	float getTps() const;

	/**
	 * Initialize the application.
	 * If the initialization fails, the application
	 * will return false. If false is returned, the application
	 * should not be executed, rather it should immediately be shut down with shutdown().
	 *
	 * @param argc the number of arguments passed from main.
	 * @param argv the argument list from main.
	 * @return false if initialization failed, otherwise true.
	 */
	bool init(int argc, char* argv[]);

	/**
	 * Shutdown the application after being initialized.
	 * If init() has been called, shutdown() should always be called before
	 * stopping the code execution.
	 */
	void shutdown();

	/**
	 * Handles the execution of the application.
	 * @return Exit value of the application, interpreted as a program main exit value.
	 */
	int execute();

private:
	std::unique_ptr<log::OutputManager> logOutputManager;
	std::unique_ptr<resource::Cache> resourceCache;
	std::unique_ptr<storage::IDataStorage> dataStorage;
	std::unique_ptr<audio::System> audioSystem;

	virtual bool onInit();
	virtual void onDestroy();
	virtual void onUpdate(const Duration& deltaTime);

	std::string applicationName;
	std::string organizationName;
	log::Logger applicationLog;
	process::Manager processManager;

	bool running;
	bool idle;

	Duration tpsUpdateInterval;
	Duration idleSleepTime;

	float tps;
};

} }

#endif
