/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_UTIL_H_
#define NOX_APP_RESOURCE_UTIL_H_

#include <json/value.h>
#include <map>
#include <vector>

namespace nox {

namespace log {
class Logger;
}

namespace app {

namespace resource {

class File;
class Provider;

/**
 * Default filename for the JSON list of resource in a resource directory.
 * Should be used rather than a hardcoded path when parsing this file.
 */
extern const std::string RESOURCE_LIST_FILENAME;

/**
 * Parse the list of resources from a JSON value and return a map that maps from
 * directory path to a list of files in that directory.
 *
 * The JSON value must be an object where each member is the full path, relative to
 * the resource directory, to a file in the resource directory. Each of these must be a string
 * with the value "file" if it is a regular file, or an object if it is a directory. Each directory
 * object must have the same format as the resource directory (root object). This goes recursively.
 *
 * @param jsonList JSON value with list of resources.
 * @param logger log::Logger to use for logging messages (e.g. errors).
 */
std::map<std::string, std::vector<File>> parseResourceList(const Json::Value& jsonList, log::Logger& logger);

/**
 * Parse the list of resources from a raw buffer and return a map that maps from
 * directory path to a list of files in that directory.
 *
 * See std::map<std::string, std::vector<File>> parseResourceList(const Json::Value&, log::Logger&)
 *
 * @param rawList Raw buffer with the plain text JSON data for the list.
 * @param logger log::Logger to use for logging messages (e.g. errors).
 */
std::map<std::string, std::vector<File>> parseResourceList(const std::vector<char>& rawList, log::Logger& logger);

/**
 * Parse the list of resources from a Provider and return a map that maps from
 * directory path to a list of files in that directory.
 *
 * See std::map<std::string, std::vector<File>> parseResourceList(const Json::Value&, log::Logger&)
 *
 * @param resourceProvider Provider that will provide the list. Should be the one calling this function.
 * @param logger log::Logger to use for logging messages (e.g. errors).
 */
std::map<std::string, std::vector<File>> parseResourceList(Provider& resourceProvider, log::Logger& logger);


}}}

#endif
