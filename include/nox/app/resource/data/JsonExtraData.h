/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_JSONEXTRADATA_H_
#define NOX_APP_RESOURCE_JSONEXTRADATA_H_

#include <nox/common/api.h>

#include <json/value.h>
#include <nox/app/resource/data/IExtraData.h>
#include <nox/log/Logger.h>

namespace nox { namespace app { namespace resource
{

/**
 * Provides access to the json data loaded from the resource loader.
 */
class NOX_API JsonExtraData: public IExtraData
{
public:
	JsonExtraData();
	~JsonExtraData();

	/**
	 * Parse the json file from a buffer.
	 * @param buffer Pointer to buffer.
	 * @param bufferSize Size of the buffer.
	 * @return If succeeded or not.
	 */
	bool parseJson(const char* buffer, std::size_t bufferSize, log::Logger& errorLog);

	/**
	 * Get the root value of the json resource.
	 * @return Reference to the root value.
	 */
	const Json::Value& getRootValue() const;

private:
	Json::Value rootValue;
};

inline const Json::Value& JsonExtraData::getRootValue() const
{
	return this->rootValue;
}

} } }

#endif
