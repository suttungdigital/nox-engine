/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_PROVIDER_H_
#define NOX_APP_RESOURCE_PROVIDER_H_

#include <nox/common/api.h>
#include <nox/common/compat.h>
#include <nox/app/resource/File.h>
#include <nox/app/resource/Descriptor.h>

#include <string>
#include <vector>

namespace nox { namespace app { namespace resource
{

/**
 * Provides access to resources from a repository.
 */
class NOX_API Provider
{
public:
	virtual ~Provider();

	/**
	 * Open the provider for use.
	 * @pre The provider must have been initialized to its repository.
	 * @post The provider can be used to access resources.
	 * @return true if successfully opened, otherwise false.
	 */
	virtual bool open() = 0;

	/**
	 * List all the files in a directory (non-recursive).
	 *
	 * @param directory Directory to list files from.
	 * @return A list of all files in that directory.
	 */
	virtual std::vector<File> listFiles(const File& directory) = 0;

	/**
	 * Read data from a resource and return the raw data.
	 *
	 * @param descriptor Descriptor of resource to read.
	 * @return Data read as a char vector.
	 */
	virtual std::vector<char> readRawResource(const Descriptor& descriptor) = 0;

	/**
	 * Estimate the number of resources available from this provider.
	 *
	 * @warning This is an estimate, and might be completely wrong. Providers are
	 * not even required to implement this, and might just return 0.
	 *
	 * @return An estimate of the number of resources available.
	 */
	virtual std::size_t estimateNumResources() const;
};

}
} }

#endif
