/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_LRUCACHE_H_
#define NOX_APP_RESOURCE_LRUCACHE_H_

#include "Cache.h"
#include "../IHandleDestructionListener.h"
#include <nox/common/api.h>
#include <nox/log/Logger.h>

#include <list>
#include <map>
#include <mutex>

namespace nox { namespace app { namespace resource
{

/**
 * Loads and caches resources with a limit, using a least recently used algorithm.
 *
 * The cache will cache resources as they are loaded. It has a limit, specified by the
 * constructor, that if exceeded it will start dropping resources from the cache.
 */
class NOX_API LruCache: public Cache, public IHandleDestructionListener
{
public:
	/**
	 * Construct a cache.
	 *
	 * @param sizeInMb The maximum size of the cache.
	 */
	LruCache(const unsigned int sizeInMb);

	~LruCache();

	void setLogger(log::Logger log);

	bool addProvider(std::unique_ptr<Provider> provider) override;
	void addLoader(std::unique_ptr<ILoader> loader) override;
	std::size_t preload(const std::string& pattern, PreloadCallback progressCallback) override;
	void flush() override;

	std::shared_ptr<Handle> getHandle(const Descriptor& resource) override;
	std::vector<Descriptor> getResourcesRecursivelyInDirectory(const std::string& directoryPath) const override;

	void onResourceHandleDestroyed(const std::size_t size) override;

private:
	/**
	 * Find a cached resource.
	 * @param resource Resource to find.
	 * @return Handle to resource found, or nullptr if not found/cached.
	 */
	std::shared_ptr<Handle> findCachedResource(const Descriptor& resource);

	/**
	 * Handle that a ResourceHandle has been accessed.
	 *
	 * Moves the handle to the front of the LRU queue.
	 *
	 * @param handle Handle accessed.
	 */
	void onCachedHandleAccessed(std::shared_ptr<Handle> handle);

	/**
	 * Load a resource from the ResourceProvicer into the cache..
	 * @param resource Resource to load.
	 * @return Handle to the resource loaded.
	 */
	std::shared_ptr<Handle> loadResource(const Descriptor& resource);

	/**
	 * Drop a ResourceHandle from the cache.
	 * @param handle Handle to drop.
	 */
	void dropHandle(std::shared_ptr<Handle> handle);

	/**
	 * Make room for a size in bytes.
	 *
	 * Frees resources from the cache until it has enough room if it is possible to make enough room.
	 * @param size Size to make room for.
	 * @return true if the size is free to use, otherwise false.
	 */
	bool makeRoom(const std::size_t size);

	/**
	 * Removes the least recently used (oldest resource) and updates the cache members.
	 */
	void freeOneResource();

	bool reserveSpace(const std::size_t space);

	typedef std::list<std::shared_ptr<Handle>> ResourceHandleList;
	typedef std::map<std::string, std::shared_ptr<Handle>> ResourceHandleMap;
	typedef std::list<std::unique_ptr<ILoader>> ResourceLoaders;

	mutable log::Logger log;

	ResourceHandleMap resourceMap;
	ResourceLoaders resourceLoaders;

	ResourceHandleList leastRecentlyUsed;

	std::vector<std::unique_ptr<Provider>> providerList;

	std::mutex handleMutex;

	const std::size_t cacheSize;
	std::size_t allocatedMemory;
};

} } }

#endif
