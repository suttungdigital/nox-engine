/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_PLAYBACKPROCESS_H_
#define NOX_APP_AUDIO_PLAYBACKPROCESS_H_

#include <nox/util/process/Implementation.h>
#include <nox/log/Logger.h>

namespace nox { namespace app
{

namespace resource
{

class Handle;

}

namespace audio
{

class Buffer;
class System;

/**
 * Process for playing a sound.
 */
class PlaybackProcess : public process::Implementation
{
public:
	/**
	 * Create a playback process.
	 * @param soundResource Handle to the resource to play.
	 * @param audioSystem System to play the sound on.
	 * @param loop If the sound should loop or not.
	 */
	PlaybackProcess(const std::shared_ptr<resource::Handle>& soundResource, System* audioSystem, bool loop = false);

	virtual ~PlaybackProcess();

	void setLogger(log::Logger log);

	void onInit() override;
	void onUpdate(const Duration& deltaTime) override;
	void onSuccess() override;
	void onFail() override;
	void onAbort() override;
	
	void setVolume(float vol);
	float getVolume() const;

private:
	void stopAudio();
	void releaseAudioData();

	log::Logger log;

	std::shared_ptr<resource::Handle> resourceHandle;
	Buffer* audioBuffer;
	System* audio;

	bool looping;
};

}
} }

#endif
