/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_TILEDTEXTURERENDERER_H_
#define NOX_APP_GRAPHICS_TILEDTEXTURERENDERER_H_

#include "OpenGlRenderer.h"
#include <nox/app/graphics/GlVertexModel.h>

namespace nox { namespace app
{
namespace graphics
{

class TiledTextureRenderer
{
public:
	TiledTextureRenderer();
	virtual ~TiledTextureRenderer();

	virtual void setup(RenderData& renderData);
	void render(RenderData& renderData, const glm::mat4& viewProjectionMatrix);

	void updateTextureQuads(const std::vector<TextureQuad::RenderQuad>* quads);

protected:
	void setupTerrainRendering(RenderData& renderData);

	void renderTexture(RenderData& renderData);

	ShaderInfo shaderInfo;

	GlIndexedVertexModel renderModel;
	GlPackedVertexAttributes* renderAttributes;

	const std::vector<TextureQuad::RenderQuad>* textureQuads;
	std::vector<GLushort> indices;

	bool quadDataChanged;

	const std::size_t INDICES_PER_QUAD = 6;
};

}
} }

#endif
