/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_TILEDTEXTUREGENERATOR_H_
#define NOX_APP_GRAPHICS_TILEDTEXTUREGENERATOR_H_

#include <vector>
#include <string>
#include <nox/app/graphics/opengl_include.h>

#include <nox/app/graphics/TextureQuad.h>

namespace nox { namespace app { namespace graphics {

class TextureManager;
class TiledTextureRenderer;

class TiledTextureGenerator
{
public:
	TiledTextureGenerator(const TextureManager& textureManager, TiledTextureRenderer* renderer);
	
	virtual ~TiledTextureGenerator();

	bool addTextureTile(const std::string& textureName);

	void setTileSize(const glm::vec2& size);

	void setTileColor(const glm::vec4& color);

	/**
	 * Set how much extrude the tiles have.
	 *
	 * Each tile will be cropped to 100-extrudePercentage percentage of its original size and be centered.
	 * This is to avoid interpolating colors between the tile color and black on the tile borders when using
	 * a texture atlas.
	 *
	 * By default this is 0.
	 *
	 * @warning The textures should be modified to have the specified extrude percentage.
	 */
	void setTileExtrudePercentage(const float extrudePercentage);

	/**
	 * If there is the need, updates the texture generated for the world.
	 * @param currentPosition the position in the world that the center is at.
	 * @param currentCoverage the total width and height the area will cover in meters.
	 */
	virtual void onUpdate(glm::vec2 currentPosition, glm::vec2 currentCoverage);

	/**
	 * Set a value to multiply the light color with.
	 *
	 * A value of 0 will make light not affect the tile at all. A value of 1
	 * will make it completely affected.
	 *
	 * Default is 1.
	 */
	void setTileLightMultiplier(const float multiplier);

	/**
	 * Set how much light to emit.
	 *
	 * A value of 0 will not emit any light. A value of 1 will emit the largest amount
	 * of light.
	 *
	 * Default 0.
	 */
	void setTileEmissiveLight(const float emissiveLight);
	
protected:
	void setTileRenderer(TiledTextureRenderer* renderer);
	bool needToUpdateTexture(glm::vec2 lowerLeftBounds, glm::vec2 upperRightBounds);
	void generateTexture();

private:
	const TextureManager& textureManager;

	TiledTextureRenderer* tileRenderer;

	std::vector<TextureQuad> textures;
	glm::vec2 quadSize;
	glm::vec4 tileColor;

	std::vector<TextureQuad::RenderQuad> textureQuads;
	std::vector<GLuint> indices;

	glm::ivec2 currentMinTiles;
	glm::ivec2 currentMaxTiles;
	
	float lightMultiplier;
	float emissiveLight;

	float textureExtrudeMultiplier;
};

}
} }

#endif
