/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_SPRITERENDERNODE_H_
#define NOX_APP_GRAPHICS_SPRITERENDERNODE_H_

#include "SceneGraphNode.h"
#include <nox/app/graphics/TextureQuad.h>
#include <nox/app/graphics/TextureRenderer.h>

#include <string>
#include <unordered_map>

namespace nox { namespace app
{
namespace graphics
{

/**
 *  Gets the quad containing information about the transformed objects
 *  vertices and puts it in the correct level and textureAtlas in the array.
 */
class SpriteRenderNode : public SceneGraphNode
{
public:
	SpriteRenderNode();

	/**
	 * Sets and, if necessary, updates the renderlevel of the spriteRenderNode.
	 * @param level the new level to set for the sprite.
	 */
	void setRenderLevel(const unsigned int level);
		
	void addSprite(const std::string& spriteName);
	void setActiveSprite(const std::string& spriteName);

	/**
	* Set the center of the sprite.
	* The center is a point within a square between 0 and 1
	* where 0 is left and bottom, and 1 is top and right.
	* E.g. set it to (0.5, 0.5) to have the center in the center
	* of the sprite.
	* @param center A vector representing the center.
	*/
	void setCenter(const glm::vec2& center);

	/**
	*  Set the color on the actor.
	*  @param color to be set.
	*/
	void setColor(const glm::vec4& color);

	void setEmissiveLight(float emissive);

	void setLightMultiplier(float multiplier);


private:
	void onNodeEnter(TextureRenderer& renderData, glm::mat4x4& modelMatrix) override;

	void onAttachToRenderer(IRenderer* renderer) override;

	void onDetachedFromRenderer() override;

	unsigned int levelID;

	std::string activeSpriteName;
	std::vector<std::string> spriteNames;
	std::unordered_map<std::string, TextureQuad> spriteMap;
	glm::vec2 center;
	glm::vec4 color;
	float lightMultiplier;
	float emissiveLight;

	bool needUpdate;

	TextureRenderer::DataHandle spriteDataHandle;
	glm::mat4 previousMatrix;
};

}
} }

#endif
