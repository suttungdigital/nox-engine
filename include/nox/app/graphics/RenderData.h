/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_RENDERDATA_H_
#define NOX_APP_GRAPHICS_RENDERDATA_H_

#include <nox/app/graphics/opengl_utils.h>
#include <nox/app/graphics/opengl_include.h>
#include <nox/app/graphics/GlContextState.h>
#include <map>

namespace nox { namespace app
{
namespace graphics
{

class TextureManager;

/**
 * Data used for the OpenGL rendering.
 * All the currently bound variables must be changed when a new buffer is
 * bound in OpenGL.
 */
class RenderData
{
public:
	RenderData(GlContextState* glState, const TextureManager* textureManager);
	RenderData();

#if NOX_OPENGL_SUPPORT_VAO
	GLuint bindVertexArray(GLuint vao);
#endif
	GLuint bindBuffer(GLenum target, GLuint vbo);
	GLuint bindShaderProgram(GLuint shaderProgram);
	GLuint bindShaderProgram(const GlslProgram& shaderProgram);
	GLuint bindTexture(GLenum target, GLuint texture);

	void setStencilFailOperation(GLenum operation);
	void setStencilMask(GLuint mask);
	void setStencilFunc(GLenum func = GL_ALWAYS, GLint ref = 0, GLuint mask = 0xFF);

	void enable(GLenum state);
	void disable(GLenum state);

	GlContextState* getState();
	GlVersion getGlVersion() const;
	GLuint getBoundVao() const;
	GLuint getBoundVbo(GLenum target) const;
	GLuint getBoundShaderProgram() const;
	GLuint getBoundTexture(GLenum target) const;

	const TextureManager* getTextureManager() const;

private:
	GlContextState* glState;
	const TextureManager* textureManager;
};

}
} }

#endif
