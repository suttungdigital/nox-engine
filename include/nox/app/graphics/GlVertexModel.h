/* NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_GLVERTEXMODEL_H_
#define NOX_APP_GRAPHICS_GLVERTEXMODEL_H_

#include "GlVertexModelStorage.h"
#include "platform.h"

namespace nox { namespace app { namespace graphics {

#if NOX_OPENGL_SUPPORT_VAO
class GlVertexModelStorageVao;
using GlDefaultVertexModelStorage = GlVertexModelStorageVao;
#else
using GlDefaultVertexModelStorage = GlVertexModelStorage;
#endif

class GlVertexModel
{
public:
	GlVertexModel();
	GlVertexModel(GlContextState* glState);
	GlVertexModel(GlContextState* glState, std::unique_ptr<GlVertexModelStorage> modelStorage);

	virtual ~GlVertexModel();

	GlVertexModel(GlVertexModel&& other);
	GlVertexModel& operator=(GlVertexModel&& other);

	GlVertexModel(const GlVertexModel&) = delete;
	GlVertexModel& operator=(const GlVertexModel&) = delete;

	virtual void drawRange(const GLenum drawMode, const std::size_t firstVertex, const std::size_t numVertices);

	void bind();
	void draw(const GLenum drawMode);

	GlPackedVertexAttributes* createPackedVertexAttributes(const GLenum bufferUsage, const GlPackedVertexAttributeDef& packDef);

	void setNumVertices(const std::size_t num);

	std::size_t getNumVertices() const;

protected:
	GlVertexModelStorage* getStorage();

private:
	GlContextState* glState;

	std::size_t numVertices;
	std::unique_ptr<GlVertexModelStorage> storage;
};

} } }

#endif
