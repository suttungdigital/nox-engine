/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @file opengl_utils.h
 * @brief OpenGL utility functions.
 */

#ifndef NOX_APP_GRAPHICS_OPENGLUTILS_H_
#define NOX_APP_GRAPHICS_OPENGLUTILS_H_

#include <nox/app/resource/IResourceAccess.h>
#include <nox/app/graphics/opengl_include.h>

#include <string>
#include <fstream>
#include <vector>
#include <cstdint>

namespace nox { namespace app
{
namespace graphics
{

class GlslProgram;
class GlContextState;

class GlslShader
{
public:
	struct VariableLocation
	{
		std::string variableName;
		std::size_t location;
	};

	void create(const GLenum type);
	bool compile(const std::string& source);
	bool compile(const char* source);
	void addVariableLocation(const VariableLocation& location);

#if NOX_OPENGL_SUPPORT_FRAGDATALOCATION
	void bindFragDataLocations(const GlslProgram& program) const;
#else
	[[noreturn]]
	void bindFragDataLocations(const GlslProgram& program) const;
#endif

	void destroy();

	GLuint getId() const;
	bool isCreated() const;
	bool isCompiled() const;
	std::string getCompileLog() const;

private:
	GLuint id = 0;
	bool compiled = false;
	std::vector<VariableLocation> variableLocationList;
};

class GlslProgram
{
public:
	void create();
	bool link(const GLuint vert, const GLuint frag);
	bool link(const GlslShader& vert, const GlslShader& frag);
	void destroy();

	GLuint getId() const;
	bool isCreated() const;
	bool isLinked() const;
	std::string getLinkLog() const;

private:
	GLuint id = 0;
	bool linked = false;
};

struct GlslVertexAndFragmentShader
{
	bool isValid() const;

	GlslShader vertex;
	GlslShader fragment;
};

struct GlVersion
{
public:
	enum class Type
	{
		DESKTOP, //!< Standard desktop OpenGL.
		ES //!< OpenGL ES.
	};

	GlVersion(const std::uint8_t major, const std::uint8_t minor, const Type type);

	std::uint8_t getMajor() const;
	std::uint8_t getMinor() const;

	bool isDesktop() const;
	bool isEs() const;

private:
	std::uint8_t glMajor;
	std::uint8_t glMinor;
	Type type;
};

/**
 * Manages the lifetime of an OpenGL framebuffer object (FBO) and its
 * renderbuffers.
 *
 * When destroyed, the framebuffer and renderbuffers will be deleted.
 */
class FrameBuffer
{
public:
	FrameBuffer();
	FrameBuffer(const GLuint name, const std::vector<GLuint>& renderBuffers);

	FrameBuffer(const FrameBuffer& other) = delete;
	FrameBuffer& operator=(const FrameBuffer& other) = delete;

	FrameBuffer(FrameBuffer&& other);
	FrameBuffer& operator=(FrameBuffer&& other);

	~FrameBuffer();

	GLuint getId() const;

	void bind(const GLenum target);

	static FrameBuffer getScreenBuffer();

private:
	GLuint bufferName = 0;
	std::vector<GLuint> renderBuffernNames;
};

std::size_t getMaxGlColorAttachments();

bool supportGlMultisampling();
bool supportGlVao();
bool supportGlMultiColorAttachments();
bool supportGlMapBuffer();
bool supportGlRenderbufferRgba8();
bool supportGlFragDataLocation();

/**
 * Create and compile shader from the content in the provided string.
 * @param shaderContent The shader code to compile.
 * @param shaderType The shader type to compile.
 * @return The ID of the created shader. 0 if failed.
 */
GlslShader createShader(const std::string& shaderContent, const GLenum shaderType, const std::vector<GlslShader::VariableLocation>& variableLocations = {});

/**
 * Create and compile shader from the content in the provided file stream.
 * @param shaderFile File stream with code to compile.
 * @param shaderType The shader type to compile.
 * @return The ID of the created shader. 0 if failed.
 */
GlslShader createShader(std::istream& shaderFile, const GLenum shaderType, const std::vector<GlslShader::VariableLocation>& variableLocations = {});

/**
 * Generate best possible GLSL version string from the comment and current GL version.
 * The string will be of the format "#version xxx\\n".
 * @param requiredVersionString The string for the version required ("xxx")
 * @param glVersion The current GL version.
 * @return The GLSL version string.
 */
std::string generateGlslVersionString(const std::string& requiredVersionString, const GlVersion glVersion);

/**
 * Create and compile shader from the content in the provided file stream and automatically find version.
 * Does the same as createShader(std::fstream& shaderFile, GLenum shaderType)
 * but will automatically find the best GLSL version from the comment "//? required xxx"
 * @param shaderFile File stream with code to compile.
 * @param shaderType The shader type to compile.
 * @param glVersion The current GL version.
 * @return The ID of the created shader. 0 if failed.
 */
GlslShader createShaderAutoVersion(std::istream& shaderFile, const GLenum shaderType, const GlVersion glVersion);

GlslShader createShaderAutoVersion(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType, const GlVersion glVersion, std::string& error);

GlslVertexAndFragmentShader createVertexAndFragmentShaderAutoVersion(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GlVersion glVersion, std::string& error);

/**
 * Link shader program from vertex and fragment shaders.
 * @param shaderProgram Program to link to. Must be created beforehand and not be 0.
 * @param vertexShader Vertex shader to link.
 * @param fragmentShader Fragment shader to link.
 * @return true if linking succeeded, otherwise false.
 */
bool linkShaderProgram(const GLuint shaderProgram, const GLuint vertexShader, const GLuint fragmentShader);

/**
 * Link shader program from vertex and fragment shaders.
 * @param shaderProgram Program to link to. Must be created beforehand and not be 0.
 * @param vertexShader Vertex shader to link.
 * @param fragmentShader Fragment shader to link.
 * @return true if linking succeeded, otherwise false.
 */
bool linkShaderProgram(const GLuint shaderProgram, const GlslShader& vertexShader, const GlslShader& fragmentShader);

/**
 * Check the linking status of program and log errors.
 * @param program Program to check.
 * @return true if OK, false if not.
 */
bool checkLinkStatus(const GLuint program);

/**
 * Check compile status of shader and log errors.
 * @param shaderId Shader to check.
 * @return true if OK, false if not.
 */
bool checkCompileStatus(const GLuint shaderId);

std::string getShaderCompileLog(const GLuint shaderId);

template<typename T>
void fillQuadIndexVector(std::vector<T>& indexVector, const typename std::vector<T>::size_type numQuads, const typename std::vector<T>::size_type startQuadNum);

template<typename T>
void resizeQuadIndexVector(std::vector<T>& indexVector, const typename std::vector<T>::size_type newSize);

GLuint createTexture(
	GlContextState& state,
	const GLsizei textureWidth,
	const GLsizei textureHeight,
	const GLint internalFormat = GL_RGBA,
	const GLenum format = GL_RGBA,
	const GLint filtering = GL_LINEAR
);

FrameBuffer createFrameBufferObject(GLsizei FBOWidth, GLsizei FBOHeight, const std::vector<GLuint>& textures = std::vector<GLuint>(), bool hasDepthAndStencilBuffer = false);

#if NOX_OPENGL_SUPPORT_MULTISAMPLE
FrameBuffer createMultisampleFrameBufferObject(const GLsizei FBOWidth, const GLsizei FBOHeight, const std::vector<GLuint>& textures = std::vector<GLuint>(), const GLsizei samples = 2, const bool hasDepthAndStencilBuffer = false);
#endif

/**
 * Deletes a framebuffer and sets the name to 0, indicating that it is
 * not longer referring to a valid buffer.
 *
 * This only deletes the framebuffer is the name is not 0.
 */
void deleteFramebufferIfValid(GLuint& name);

/**
 * Deletes a texture and sets the name to 0, indicating that it is
 * not longer referring to a valid texture.
 *
 * This only deletes the texture is the name is not 0.
 */
void deleteTextureIfValid(GLuint& name);

template<typename T>
void resizeQuadIndexVector(std::vector<T>& indexVector, const typename std::vector<T>::size_type newSize)
{
	const auto INDICES_PER_QUAD = typename std::vector<T>::size_type(6);
	const auto previousSize = indexVector.size() / INDICES_PER_QUAD;

	if (newSize < previousSize)
	{
		auto eraseStartIt = indexVector.begin();
		std::advance(eraseStartIt, newSize * INDICES_PER_QUAD);

		indexVector.erase(eraseStartIt, indexVector.end());
	}
	else if (newSize > previousSize)
	{
		const auto extraSize = newSize - previousSize;

		indexVector.resize(indexVector.size() + (extraSize * INDICES_PER_QUAD));

		fillQuadIndexVector(indexVector, extraSize, previousSize);
	}
}

template<typename T>
void fillQuadIndexVector(std::vector<T>& indexVector, const typename std::vector<T>::size_type numQuads, const typename std::vector<T>::size_type startQuadNum)
{
	using SizeType = typename std::vector<T>::size_type;

	const auto INDICES_PER_QUAD = SizeType(6);

	assert(startQuadNum * INDICES_PER_QUAD + numQuads * INDICES_PER_QUAD <= indexVector.size());

	auto indexStart = T(0);

	if (startQuadNum > 0)
	{
		indexStart = static_cast<T>(indexVector[startQuadNum * INDICES_PER_QUAD - 1] + 1);
	}

	for (SizeType i = 0; i < numQuads; i++)
	{
		const auto QUAD_INDEX_START = (startQuadNum * INDICES_PER_QUAD) + (i * INDICES_PER_QUAD);

		indexVector[QUAD_INDEX_START] = static_cast<T>(indexStart + 0);
		indexVector[QUAD_INDEX_START + 1u] = static_cast<T>(indexStart + 1);
		indexVector[QUAD_INDEX_START + 2u] = static_cast<T>(indexStart + 2);

		indexVector[QUAD_INDEX_START + 3u] = static_cast<T>(indexStart + 0);
		indexVector[QUAD_INDEX_START + 4u] = static_cast<T>(indexStart + 2);
		indexVector[QUAD_INDEX_START + 5u] = static_cast<T>(indexStart + 3);

		indexStart = static_cast<T>(indexStart + 4);
	}
}

}
} }

#endif
