#ifndef NOX_APP_GRAPHICS_OPENGLINCLUDE_H_
#define NOX_APP_GRAPHICS_OPENGLINCLUDE_H_

#include "platform.h"

#if NOX_OPENGL_GLEW
#  include <GL/glew.h>
#elif NOX_OPENGL_ES
#  if NOX_OPENGL_VERSION_MAJOR == 2
#    include <GLES2/gl2.h>
#  endif
#  if NOX_OPENGL_VERSION_MAJOR == 3
#    include <GLES3/gl3.h>
#  endif
#endif

#if NOX_OPENGL_ES && NOX_OPENGL_VERSION_MAJOR < 3
#define glDrawBuffers(a, b) assert(false && "glDrawBuffers not supported")
#define GL_RGBA8 0
#endif

#if NOX_OPENGL_ES
#define glBindFragDataLocation(a, b, c) assert(false && "glBindFragDataLocation not supported")
#endif

#endif
