#ifndef NOX_APP_GRAPHICS_PLAFORM_H_
#define NOX_APP_GRAPHICS_PLAFORM_H_

#include <nox/common/platform.h>

/**
 * @file platform.h
 *
 * Defines several macros for identifying the graphics platform if not already defined.
 */

#if !defined(NOX_OPENGL_DESKTOP)
#  if !NOX_PLATFORM_MOBILE && !NOX_OPENGL_ES
#    define NOX_OPENGL_DESKTOP 1
#  else
#    define NOX_OPENGL_DESKTOP 0
#  endif
#endif

#if !defined(NOX_OPENGL_ES)
#  if NOX_PLATFORM_MOBILE && !NOX_OPENGL_DESKTOP
#    define NOX_OPENGL_ES 1
#  else
#    define NOX_OPENGL_ES 0
#  endif
#endif

#if !defined(NOX_OPENGL_GLEW)
#  if NOX_OPENGL_DESKTOP
#    define NOX_OPENGL_GLEW 1
#  else
#    define NOX_OPENGL_GLEW 0
#  endif
#endif

#if !defined(NOX_OPENGL_VERSION_MAJOR) || !defined(NOX_OPENGL_VERSION_MINOR)
#  if NOX_OPENGL_DESKROP
#    define NOX_OPENGL_VERSION_MAJOR 3
#    define NOX_OPENGL_VERSION_MINOR 0
#  elif NOX_OPENGL_ES
#    define NOX_OPENGL_VERSION_MAJOR 2
#    define NOX_OPENGL_VERSION_MINOR 0
#  endif
#endif

#if !defined(NOX_OPENGL_SUPPORT_MULTISAMPLE)
#  if NOX_OPENGL_DESKTOP// || (NOX_OPENGL_ES && NOX_OPENGL_VERSION_MAJOR >= 3) // GL_MULTISAMPLE not defined
#    define NOX_OPENGL_SUPPORT_MULTISAMPLE 1
#  else
#    define NOX_OPENGL_SUPPORT_MULTISAMPLE 0
#  endif
#endif

#if !defined(NOX_OPENGL_SUPPORT_VAO)
#  if NOX_OPENGL_DESKTOP || (NOX_OPENGL_ES && NOX_OPENGL_VERSION_MAJOR >= 3)
#    define NOX_OPENGL_SUPPORT_VAO 1
#  else
#    define NOX_OPENGL_SUPPORT_VAO 0
#  endif
#endif

#if !defined(NOX_OPENGL_SUPPORT_MAPBUFFER)
#  if NOX_OPENGL_DESKTOP || (NOX_OPENGL_ES && NOX_OPENGL_VERSION_MAJOR >= 3)
#    define NOX_OPENGL_SUPPORT_MAPBUFFER 1
#  else
#    define NOX_OPENGL_SUPPORT_MAPBUFFER 0
#  endif
#endif

#if !defined(NOX_OPENGL_SUPPORT_MULTICOLORATTACHMENTS)
#  if NOX_OPENGL_DESKTOP || (NOX_OPENGL_ES && NOX_OPENGL_VERSION_MAJOR >= 3)
#    define NOX_OPENGL_SUPPORT_MULTICOLORATTACHMENTS 1
#  else
#    define NOX_OPENGL_SUPPORT_MULTICOLORATTACHMENTS 0
#  endif
#endif

#if !defined(NOX_OPENGL_SUPPORT_FRAGDATALOCATION)
#  if (NOX_OPENGL_DESKTOP && NOX_OPENGL_VERSION_MAJOR >= 3) || (NOX_OPENGL_ES && NOX_OPENGL_VERSION_MAJOR >= 3)
#    define NOX_OPENGL_SUPPORT_FRAGDATALOCATION 1
#  else
#    define NOX_OPENGL_SUPPORT_FRAGDATALOCATION 0
#  endif
#endif

#endif
