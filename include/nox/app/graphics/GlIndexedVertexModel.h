/* NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_GLINDEXEDVERTEXMODEL_H_
#define NOX_APP_GRAPHICS_GLINDEXEDVERTEXMODEL_H_

#include "GlDataType.h"
#include "GlVertexModel.h"

namespace nox { namespace app { namespace graphics {

class GlIndexedVertexModel final: public GlVertexModel
{
public:
	GlIndexedVertexModel();
	GlIndexedVertexModel(GlContextState* glState, const GLenum elementArrayBufferUsage, const GlDataType elementDataType);
	GlIndexedVertexModel(GlContextState* glState, std::unique_ptr<GlVertexModelStorage> modelStorage, const GLenum elementArrayBufferUsage, const GlDataType elementDataType);

	~GlIndexedVertexModel();

	GlIndexedVertexModel(GlIndexedVertexModel&& other);
	GlIndexedVertexModel& operator=(GlIndexedVertexModel&& other);

	GlIndexedVertexModel(const GlIndexedVertexModel&) = delete;
	GlIndexedVertexModel& operator=(const GlIndexedVertexModel&) = delete;

	virtual void drawRange(const GLenum drawMode, const std::size_t firstVertex, const std::size_t numVertices) override;

	GlBuffer& getElementArrayBuffer();

private:
	GlDataType elementDataType;
};

} } }

#endif
