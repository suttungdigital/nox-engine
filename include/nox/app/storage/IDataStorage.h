/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_STORAGE_IDATASTORAGE_H_
#define NOX_APP_STORAGE_IDATASTORAGE_H_

#include <nox/common/compat.h>

#include <istream>
#include <ostream>
#include <memory>
#include <string>
#include <exception>

namespace nox { namespace app { namespace storage
{

/**
 * Provides access to a readable and writable storage directory.
 *
 * The storage only requires implementations to support UTF-8 encoded paths,
 * so using some other encoding is highly unsafe! The basic 7-bit ASCII is UTF-8
 * compatible, so 7-bit ASCII strings will work (this includes C++ string literals).
 * It will NOT work if you include extended ASCII characters.
 */
class IDataStorage
{
public:
	class FileOpenFailedException: public std::exception
	{
	public:
		const char* what() const NOX_NOEXCEPT override;
	};

	virtual ~IDataStorage();

	/**
	 * Check if a file exists in the storage.
	 *
	 * @param filePath Path to file to be checked.
	 * @return If the file exists, or not.
	 */
	virtual bool fileExists(const std::string& filePath) const = 0;

	/**
	 * Open a file that can be read from through a stream.
	 *
	 * @param filePath Path to file to be opened.
	 * @return Readable input stream, or nullptr if failed opening.
	 */
	virtual std::unique_ptr<std::istream> openReadableFile(const std::string& filePath) = 0;

	/**
	 * Open a file that can be written to through a stream.
	 *
	 * If the filePath does not exists, the whole path, including the file, will be created.
	 * Because of this it is safe to assume that the returned value always will be valid as
	 * long as the storage has been successfully initialized. If it still fails a
	 * FileOpenFailedException will the thrown.
	 *
	 * @param filePath Path to file to be opened.
	 * @param append If the stream should append to existing content, or overwrite it.
	 * @throws FileOpenFailedException If the file could not be opened.
	 * @return Writable output stream, or nullptr if failed opening.
	 */
	virtual std::unique_ptr<std::ostream> openWritableFile(const std::string& filePath, bool append = false) = 0;

	/**
	 * Read file content from a path.
	 *
	 * @param filePath Path to file to be read from.
	 * @return Content read. Empty if file was not found.
	 */
	virtual std::string readFileContent(const std::string& filePath) = 0;

	/**
	 * Write content to a file.
	 *
	 * If the filePath does not exists, the whole path, including the file, will be created.
	 *
	 * @param filePath Path to file to be written to.
	 * @param content Content to write to the file.
	 * @param append If it should append the new content to existing content, or overwrite it.
	 */
	virtual void writeFileContent(const std::string& filePath, const std::string& content, bool append = false) = 0;

	/**
	 * Remove a file (including directories) from the storage.
	 *
	 * @warning If filePath is a directory it will recursively delete all files in the directory.
	 * @param filePath Path to file to remove.
	 */
	virtual void removeFile(const std::string& filePath) = 0;
};

} } }


#endif
