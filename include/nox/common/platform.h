/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_PLATFORM_H_
#define NOX_PLATFORM_H_

#define NOX_OS_ANDROID 0
#define NOX_OS_LINUX 0
#define NOX_OS_APPLE 0
#define NOX_OS_APPLE_IPHONE 0
#define NOX_OS_APPLE_IOS 0
#define NOX_OS_APPLE_MAC 0
#define NOX_OS_WINDOWS 0
#define NOX_OS_UNKNOWN 0
#define NOX_ENV_UNIX 0
#define NOX_ENV_WINDOWS 0
#define NOX_ENV_UNKNOWN 0
#define NOX_PLATFORM_MOBILE 0
#define NOX_PLATFORM_PC 0

#if defined(__ANDROID__)
	#undef NOX_OS_ANDROID
	#define NOX_OS_ANDROID 1
#elif defined(__linux__)
	#undef NOX_OS_LINUX
	#define NOX_OS_LINUX 1
#elif defined(__APPLE__)
	#undef NOX_OS_APPLE
	#define NOX_OS_APPLE 1

	#include "TargetConditionals.h"

	#if TARGET_OS_IPHONE
		#undef NOX_OS_APPLE_IPHONE
		#define NOX_OS_APPLE_IPHONE 1
		#undef NOX_OS_APPLE_IOS
		#define NOX_OS_APPLE_IOS 1
	#elif TARGET_OS_MAC
		#undef NOX_OS_APPLE_MAC
		#define NOX_OS_APPLE_MAC 1
	#endif
#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
	#undef NOX_OS_WINDOWS
	#define NOX_OS_WINDOWS 1
#else
	#undef NOX_OS_UNKNOWN
	#define NOX_OS_UNKNOWN 1
#endif

#if NOX_OS_WINDOWS
	#undef NOX_ENV_WINDOWS
	#define NOX_ENV_WINDOWS 1
#elif defined(__unix__) || NOX_OS_LINUX || NOX_OS_APPLE
	#undef NOX_ENV_UNIX
	#define NOX_ENV_UNIX 1
#else
	#undef NOX_ENV_UNKNOWN
	#define NOX_ENV_UNKNOWN 1
#endif

#if NOX_OS_ANDROID || NOX_OS_APPLE_IPHONE
	#undef NOX_PLATFORM_MOBILE
	#define NOX_PLATFORM_MOBILE 1
#else
	#undef NOX_PLATFORM_PC
	#define NOX_PLATFORM_PC 1
#endif

#endif
