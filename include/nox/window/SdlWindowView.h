/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_WINDOW_SDLWINDOWVIEW_H_
#define NOX_WINDOW_SDLWINDOWVIEW_H_

#include <nox/logic/View.h>
#include <nox/log/Logger.h>

#include <SDL_video.h>
#include <SDL_events.h>
#include <glm/vec2.hpp>
#include <string>

namespace nox
{

namespace app
{

class IContext;

}

namespace window
{

class NOX_API SdlWindowView: public logic::View
{
public:
	/**
	 * Different fullscreen modes.
	 * Default mode is DESKTOP.
	 */
	enum class Fullscreen
	{
		/**
		 * Real fullscreen with a video mode change.
		 * The current window resolution will be stretched over the whole fullscreen desktop. To have full resolution
		 * with real fullscreen, the resolution has to be set manually.
		 */
		REAL,

		/**
		 * Desktop fullscreen that doesn't do a video mode chage.
		 * This will resize the window to fill the whole screen and remove the borders.
		 */
		DESKTOP
	};

	SdlWindowView(app::IContext* applicationContext, const std::string& windowTitle, const bool enableOpenGl);
	SdlWindowView(SdlWindowView&&);

	virtual ~SdlWindowView();

	SdlWindowView& operator=(SdlWindowView&&);

	virtual std::string getTypeString() const override;

	virtual bool initialize(logic::IContext* context) override;
	void destroy() override final;

	void setWindowSize(const glm::uvec2& size);
	const glm::uvec2& getWindowSize() const;

	/**
	 * Set which mode to use for fullscreen.
	 *
	 * This will take effect immediately if fullscreen is enabled.
	 *
	 * See Fullscreen for information about the various modes.
	 */
	void setFullscreenMode(const Fullscreen fullscreenMode);

	/**
	 * Enable fullscreen mode if not already enabled.
	 */
	void enableFullscreen();

	/**
	 * Disable fullscreen mode if not already disabled.
	 */
	void disableFullscreen();

	/**
	 * Check if the window is in fullscreen mode.
	 */
	bool isFullscreen() const;

	glm::uvec2 getDisplayResolution() const;

	virtual void onSdlEvent(const SDL_Event& event);

private:
	void update(const Duration& deltaTime) override final;

	virtual bool onWindowCreated(SDL_Window* window);
	virtual void onWindowSizeChanged(const glm::uvec2& size);
	virtual void onMousePress(const SDL_MouseButtonEvent& event);
	virtual void onMouseRelease(const SDL_MouseButtonEvent& event);
	virtual void onMouseMove(const SDL_MouseMotionEvent& event);
	virtual void onMouseScroll(const SDL_MouseWheelEvent& event);
	virtual void onKeyPress(const SDL_KeyboardEvent& event);
	virtual void onKeyRelease(const SDL_KeyboardEvent& event);
	virtual void onJoyDeviceAdded(const SDL_JoyDeviceEvent& event);
	virtual void onJoyDeviceRemoved(const SDL_JoyDeviceEvent& event);
	virtual void onJoyAxisMotion(const SDL_JoyAxisEvent& event);

	virtual void onUpdate(const Duration& deltaTime);
	virtual void onDestroy();

	app::IContext* applicationContext;

	mutable log::Logger log;

	std::string windowTitle;
	glm::uvec2 windowSize;
	bool enableOpengl;
	Fullscreen fullscreenMode;
	bool fullscreen;

	SDL_Window* window;
	SDL_GLContext glContext;
};

}
}

#endif
