# README
[The NOX Engine.](https://bitbucket.org/suttungdigital/nox-engine/)

Sample program using the engine is found [here](https://bitbucket.org/suttungdigital/nox-engine-sample).

## Building
This section describes how to set up a build environment and build the engine library for Linux, OS X and Windows.

### Supported Compilers
Because of its heavy use of new ISO C++ (currently C++11 and C++14) features, only the latest stable versions of compilers are supported.

Officially these compilers (and most likely later versions) are supported:

* [GCC](https://gcc.gnu.org/) 5
* [LLVM Clang](http://clang.llvm.org/) 3.7
* Apple Clang 6
* Microsoft Visual C++ 14 2015

Other compilers should work as long as they support the latest ISO C++ features.

### Dependencies
#### Tools
* [CMake](http://www.cmake.org/) (>=2.8.12)
* [Git](https://git-scm.com/)

#### Libraries
* OpenGL (commonly already installed, otherwise look up your system doc)
* [GLEW](http://glew.sourceforge.net/)
* [Boost](http://www.boost.org/) (Geometry, Locale and Filesystem libraries)

### Preparing System
#### Linux
Install CMake through your system's package manager.

##### Targetting Linux
Install libraries listed above through your system's package manager (or build them yourself).

##### Targetting Android
1. Download and extract the latest version of the Boost library (you can NOT use a system version).
2. Install the Java SDK through your package manager.
3. Download and extract [the Android SDK Tools](https://developer.android.com/sdk/index.html#Other).
4. Start the `android` program located in the `tools` directory in the extracted Android SDK Tools.
5. Install the latest stable Android SDK version from it (the default selected packages should suffice).
6. Download and extract [the Android NDK](https://developer.android.com/ndk/downloads/index.html).

#### OS X
Install CMake from [their website](http://www.cmake.org/).

##### Targetting OS X
Install the libraries listed above, except OpenGL as it is included with the system.
To easily accomplish this you can use [Homebrew](http://brew.sh/).

##### Targetting Android
TODO

#### Windows
Install CMake from [their website](http://www.cmake.org/).

##### Targetting Windows
You can either download and build all the libraries yourself, or use our precompiled collection [located here](https://bitbucket.org/suttungdigital/windows-libraries) (recommended).

###### Using the Precompiled Library Collection
Clone the repository [located at this link](https://bitbucket.org/suttungdigital/windows-libraries) and run the `create_msvc_usr.ps1` script (right click -> Run in PowerShell).
You might need to allow running scripts on your system.
To do this, open a PowerShell terminal and run `Set-ExecutionPolicy Unrestricted -Scope CurrentUser`.

When the script is done, there will be a new `usr` directory next to the script.
Set the the environment variable `CMAKE_PREFIX_PATH` to point to this.
Also set `PATH` to point to `usr\bin\x64` and `usr\bin\x86` to be able to use both the 64-bit and 32-bit dll's.

###### Other Options
In case you don't want to use our precompiled libraries, you can build your own or use other precompiled versions (we won't describe how, that is up to you).

You need to set up CMake on your system to find the libraries.
To do this, set the environment variable `CMAKE_PREFIX_PATH` to point to the library directory. You can also set `CMAKE_PREFIX_PATH` only for NOX Engine when configuring the CMake build.

The library directory should have one `include` directory with all the header files, and one `lib` directory with all the lib files.

To easily run applications using the libraries, set the directory of the dll files to the `PATH` environment variable.

##### Targetting Android
If you already have Android Studio installed, you can (probably) skip step 2-4.

1. Download and extract the latest version of the Boost library. Remember where you extracted this.
2. Download and install [the Java SDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html).
3. Add a new environment variable called JAVA_HOME. Set this to point at the Java SDK. The Java SDK is installed located in `C:\Program Files\Java`, you will find the sdk directory there.
4. Download and install [the Android SDK Tools](https://developer.android.com/sdk/index.html#Other).
5. Start the Android SDK Manager (there's a check box you can tick at the end of the install).
6. Install the latest stable Android SDK version from it (the default selected packages should suffice).
7. Add the Android SDK tools directory (containing android.bat) to your PATH environment variable. The Android SDK Tools is usually installed in `%USERPROFILE%\AppData\Local\Android`, you will find the tools directory there.
8. Download and extract [the Android NDK](https://developer.android.com/ndk/downloads/index.html). Remember where you extracted this.
9. Download and extract [Ninja](https://github.com/martine/ninja/releases).
10. Add the extracted ninja directory (containing ninja.exe) to the PATH environment variable.

### Getting the Sources
We recommend using a directory structure where you have a directory for the project, e.g. named `nox-engine`, and two subdirectories called `source` and `build`.
For this guide, we assume you use this structure.

To get the sources:

1. `git clone <nox-engine-url> source`
2. `cd source`
3. `git submodule update --init --recursive`

Replace <nox-engine-url> with the URL to the NOX Engine git repository.

You need to use the `git submodule update` command, otherwise the third party libraries won't be available.

After pulling a new version down, you should run `git submodule update --init --recursive` again, so that the third party libraries are properly checked out.

### Building
To configure the project, run `cmake ../source` from the build directory, or use the CMake GUI tool and set the proper directories.

If you want to configure any options, add `-D<option>` to the `cmake` command.
Use `cat CMakeCache.txt | grep -B1 NOX` after you have run `cmake` once to list all available options.
If you're using the CMake GUI, you can edit the options from there.
Example use of option:
`cmake -DNOX_BUILD_TEST=OFF ../source`.

After pulling down a new version of the source, you should run the `cmake` command over again.

You can generate different types of projects, see http://www.cmake.org/cmake/help/v3.1/manual/cmake-generators.7.html for which and how.
Below are basic instructions for the various platforms.

#### Linux
After configuring with CMake, run `make` in the build directory, or open your generated project.
Use `make -j<number-of-threads>` to build with multiple threads.

#### OS X
If you want to use Xcode, enable the Xcode generator by adding `-GXcode` to the `cmake` command.
Example: `cmake -GXcode ../source`.
From the GUI you can select to use Xcode when configuring.

After configuring with CMake, run `make` in the build directory, or use your generated project.
Use `make -j<number-of-threads>` to build with multiple threads.

#### Windows
##### Targetting Windows
If you want to use Visual Studio, enable the Visual Studio generator by adding `-G"Visual Studio 12 2013"` to the `cmake` command.
From the GUI you can select to use Visual Studio when configuring.
To create a 64-bit project, replace `-G"Visual Studio 12 2013"` with `-G"Visual Studio 12 2013 Win64"`.
Example: `cmake -G"Visual Studio 12 2013" ..\source`.

If you didn't set the `CMAKE_PREFIX_PATH` environment variable (see Windows under Preparing System), add `-DCMAKE_PREFIX_PATH=\path\to\your\librarydir` to the `cmake` command, or set the variable in the GUI.
Example: `cmake -DCMAKE_PREFIX_PATH=\path\to\your\librarydir ..\source`

After configuring with CMake, run `make` in the build directory, or use your generated project/solution.

##### Targetting Android
You need to use the Ninja generator, the Android Toolchain and set the Boost header directory.
Below you can see guides for both CMake GUI and CLI.

###### Configure with CMake GUI
CMake 3.7 or newer is required for this to work.

Add an entry with the name `CMAKE_SYSTEM_NAME`. Set the type to `STRING`.
Set the value to `Android`.

Add an entry with the name `ANDROID_NDK_TOOLCHAIN_VERSION`. Set the type to `STRING`.
Set the value to `Clang`.

Add an entry with the name `CMAKE_SYSTEM_VERSION`. Set the type to `STRING`.
Set the value to `9`.

Add an entry with the name `CMAKE_ANDROID_ARCH_ABI`. Set the type to `STRING`.
Set the value to `armeabi-v7a`.

Add an entry with the name `CMAKE_ANDROID_NDK`. Set the type to `PATH`.
Set the value to the extracted Android NDK directory.

Add an entry with the name `BOOST_INCLUDEDIR`. Set the type to `PATH`.
Set the value to the extracted Boost directory.

For more options, see https://cmake.org/cmake/help/v3.7/manual/cmake-toolchains.7.html#cross-compiling-for-android-with-the-ndk.

Press configure and select Ninja as the generator.

###### Building
After successfully configuring with CMake, open a command window in your build directory.
Write `ninja -j<number-of-jobs>` to initiate the build.
Replace <nubmer-of-jobs> with the number of jobs you want to do in parallel.
If successful (which it should be), you will find an apk file in `apk\bin` below the project build directory.

## Using the Engine
The easiest way to use the engine is to set up your own CMake project, adding the engine as a git submodule, including it in your CMake project with `add_subdirectory` and linking with the `target_link_libraries` command.
See [NOX Engine Sample](https://bitbucket.org/suttungdigital/nox-engine-sample) for an example project using the engine as a subproject.

## Documentation
You can generate html documentation by using [Doxygen](http://www.stack.nl/~dimitri/doxygen/) in the `docs` directory.

## License
NOX Engine is licensed under the MIT license, basically allowing you to use it however you want as long as you keep the license note.
See below for the license.

```
Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```
