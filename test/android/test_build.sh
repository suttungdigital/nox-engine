builddir=$1

if [ ! -f $builddir/test/android/apk/NoxTest/build/outputs/apk/NoxTest-debug.apk ]; then
	echo "NoxTest APK file was not generated"
	exit 1
fi

echo "Android tests succeeded"
exit 0
