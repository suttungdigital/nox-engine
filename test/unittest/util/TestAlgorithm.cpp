/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <vector>
#include <list>
#include <nox/util/algorithm.h>

TEST(TestAlgorithm, FindInVector)
{
	const auto vector = std::vector<int>{1, 2, 3, 4, 5};

	EXPECT_EQ(vector.end(), nox::util::find(vector, 0));
	EXPECT_EQ(1, *nox::util::find(vector, 1));
	EXPECT_EQ(2, *nox::util::find(vector, 2));
	EXPECT_EQ(3, *nox::util::find(vector, 3));
	EXPECT_EQ(4, *nox::util::find(vector, 4));
	EXPECT_EQ(5, *nox::util::find(vector, 5));
	EXPECT_EQ(vector.end(), nox::util::find(vector, 6));
}

TEST(TestAlgorithm, FindInList)
{
	const auto list = std::list<int>{1, 2, 3, 4, 5};

	EXPECT_EQ(list.end(), nox::util::find(list, 0));
	EXPECT_EQ(1, *nox::util::find(list, 1));
	EXPECT_EQ(2, *nox::util::find(list, 2));
	EXPECT_EQ(3, *nox::util::find(list, 3));
	EXPECT_EQ(4, *nox::util::find(list, 4));
	EXPECT_EQ(5, *nox::util::find(list, 5));
	EXPECT_EQ(list.end(), nox::util::find(list, 6));
}

TEST(TestAlgorithm, ContainsInVector)
{
	const auto vector = std::vector<int>{1, 2, 3, 4, 5};

	EXPECT_FALSE(nox::util::contains(vector, 0));
	EXPECT_TRUE(nox::util::contains(vector, 1));
	EXPECT_TRUE(nox::util::contains(vector, 2));
	EXPECT_TRUE(nox::util::contains(vector, 3));
	EXPECT_TRUE(nox::util::contains(vector, 4));
	EXPECT_TRUE(nox::util::contains(vector, 5));
	EXPECT_FALSE(nox::util::contains(vector, 6));
}
