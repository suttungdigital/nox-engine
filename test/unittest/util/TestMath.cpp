/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <memory>
#include <string>
#include <random>
#include <glm/gtc/constants.hpp>
#include <nox/util/math/angle.h>
#include <nox/util/math/exponential.h>

TEST(TestMath, ClampAngle)
{
	const float angleStep = glm::pi<float>() / 100.0f;
	const float angleMin = -glm::pi<float>() * 100.0f;
	const float angleMax = -angleMin;
	float angle = angleMin;

	while (angle < angleMax)
	{
		const float clampedAngle = nox::math::clampAngle(angle);
		EXPECT_GT(clampedAngle, -glm::pi<float>());
		EXPECT_LE(clampedAngle, glm::pi<float>());

		angle += angleStep;
	}
}

TEST(TestMath, ClampAnglePositive)
{
	const float angleStep = glm::pi<float>() / 100.0f;
	const float angleMin = -glm::pi<float>() * 100.0f;
	const float angleMax = glm::pi<float>() * 100.0f;
	float angle = angleMin;

	while (angle < angleMax)
	{
		const float clampedAngle = nox::math::clampAnglePositive(angle);
		EXPECT_GE(clampedAngle, 0.0f);

		angle += angleStep;
	}
}

TEST(TestMath, AngleDifference)
{
	EXPECT_FLOAT_EQ(glm::half_pi<float>(), nox::math::calculateAngleDifference(0.0f, glm::half_pi<float>()));
	EXPECT_FLOAT_EQ(-glm::half_pi<float>(), nox::math::calculateAngleDifference(glm::half_pi<float>(), 0.0f));

	EXPECT_FLOAT_EQ(-glm::pi<float>(), nox::math::calculateAngleDifference(0.0f, glm::pi<float>()));
	EXPECT_FLOAT_EQ(-glm::pi<float>(), nox::math::calculateAngleDifference(glm::pi<float>(), 0.0f));

	EXPECT_FLOAT_EQ(-glm::half_pi<float>(), nox::math::calculateAngleDifference(0.0f, glm::pi<float>() * 1.5f));
	EXPECT_FLOAT_EQ(glm::half_pi<float>(), nox::math::calculateAngleDifference(glm::pi<float>() * 1.5f, 0.0f));

	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(0.0f, glm::pi<float>() * 2.0f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(glm::pi<float>() * 2.0f, 0.0f));

	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(0.0f, 0.0f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(10.0f, 10.0f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(999.0f, 999.0f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(983457.234f, 983457.234f));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(glm::pi<float>(), glm::pi<float>()));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(-glm::pi<float>(), glm::pi<float>()));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(-glm::pi<float>(), -glm::pi<float>()));
	EXPECT_FLOAT_EQ(0.0f, nox::math::calculateAngleDifference(glm::pi<float>(), -glm::pi<float>()));
	EXPECT_NEAR(0.0f, nox::math::calculateAngleDifference(glm::pi<float>(), -glm::pi<float>() * 17.0f), 1.0e-5f);
	EXPECT_NEAR(0.0f, nox::math::calculateAngleDifference(glm::pi<float>() * 3.0f, -glm::pi<float>() * 9.0f), 1.0e-5f);
	EXPECT_NEAR(0.0f, nox::math::calculateAngleDifference(glm::pi<float>() * 2.0f, -glm::pi<float>() * 4.0f), 1.0e-5f);

	std::random_device randomDevice;
	std::minstd_rand randomEngine(randomDevice());
	std::uniform_real_distribution<float> angleDist(-glm::pi<float>() * 100.0f, glm::pi<float>() * 100.0f);
	const unsigned int numTests = 1000;

	for (unsigned int i = 0; i < numTests; i++)
	{
		const float angleA = angleDist(randomEngine);
		const float angleB = angleDist(randomEngine);

		const float difference = nox::math::calculateAngleDifference(angleA, angleB);

		EXPECT_GT(difference, -glm::pi<float>());
		EXPECT_LE(difference, glm::pi<float>());

		const float expectedDifference = nox::math::clampAngle(angleB - angleA);

		EXPECT_FLOAT_EQ(expectedDifference, difference);
	}
}

TEST(TestMath, AngleWithinCone)
{
	EXPECT_TRUE(nox::math::angleIsWithinCone(glm::half_pi<float>(), 0.0f, glm::pi<float>()));
	EXPECT_TRUE(nox::math::angleIsWithinCone(glm::pi<float>(), 0.0f, glm::pi<float>() * 2.0f));
	EXPECT_TRUE(nox::math::angleIsWithinCone(glm::pi<float>() * 1.5f, glm::pi<float>(), glm::pi<float>() * 2.0f));

	EXPECT_FALSE(nox::math::angleIsWithinCone(glm::pi<float>() * 1.5f, 0.0f, glm::pi<float>()));

	EXPECT_TRUE(nox::math::angleIsWithinCone(-glm::pi<float>() * 1.5f,  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
	EXPECT_TRUE(nox::math::angleIsWithinCone(-glm::pi<float>() * 1.7f,  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
	EXPECT_TRUE(nox::math::angleIsWithinCone(-glm::pi<float>() * 1.3f,  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
	EXPECT_FALSE(nox::math::angleIsWithinCone(-glm::pi<float>(),  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
	EXPECT_FALSE(nox::math::angleIsWithinCone(-glm::pi<float>() * 1.8f,  glm::pi<float>() / 4.0f,  glm::pi<float>() - glm::pi<float>() / 4.0f));
}

TEST(TestMath, PowerOfTwo)
{
	EXPECT_EQ(1, nox::math::powerOfTwo(0));
	EXPECT_EQ(2, nox::math::powerOfTwo(1));
	EXPECT_EQ(4, nox::math::powerOfTwo(2));
	EXPECT_EQ(1024, nox::math::powerOfTwo(10));
	EXPECT_EQ(1u, nox::math::powerOfTwo(0u));
	EXPECT_EQ(2u, nox::math::powerOfTwo(1u));
	EXPECT_EQ(4u, nox::math::powerOfTwo(2u));
	EXPECT_EQ(1024u, nox::math::powerOfTwo(10u));
	EXPECT_EQ(2147483648ul, nox::math::powerOfTwo(31ul));
}

TEST(TestMath, UpperPowerOfTwo)
{
	EXPECT_EQ(1, nox::math::upperPowerOfTwo<int>(1));
	EXPECT_EQ(2, nox::math::upperPowerOfTwo<int>(1.5f));
	EXPECT_EQ(2048, nox::math::upperPowerOfTwo<int>(1678));
	EXPECT_EQ(2147483648ul, nox::math::upperPowerOfTwo<unsigned long int>(2147483640ul));
}
