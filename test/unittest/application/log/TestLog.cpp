/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/log/OutputManager.h>
#include <nox/log/OutputStream.h>
#include <nox/log/Logger.h>

#include <gtest/gtest.h>
#include <glm/vec2.hpp>
#include <glm/gtx/io.hpp>
#include <memory>
#include <string>

class TestLog: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
		std::unique_ptr<nox::log::OutputStream> logOutput(new nox::log::OutputStream());
		this->logOutputStream = logOutput.get();

		logOutput->addOutputStream(&this->logStream);
		this->logManager.addLogOutput(std::move(logOutput));
	}

	virtual void TearDown()
	{
	}

	nox::log::OutputManager logManager;
	nox::log::OutputStream* logOutputStream;
	std::ostringstream logStream;
};

TEST_F(TestLog, TestRawOutput)
{
	nox::log::Logger log;
	log.setOutputManager(&this->logManager);
	log.info().raw("Streamed data");

	EXPECT_EQ("Streamed data\n", this->logStream.str());
}

TEST_F(TestLog, TestFormattedOutput)
{
	nox::log::Logger log;
	log.setOutputManager(&this->logManager);
	log.info().format("Streamed %i %s", 5, "data");

	EXPECT_EQ("Streamed 5 data\n", this->logStream.str());
}

TEST_F(TestLog, TestFormattedLog)
{
	this->logOutputStream->setOutputFormat("[${loglevel}][${loggername}] ${message}");

	nox::log::Logger log;
	log.setOutputManager(&this->logManager);
	log.info().raw("Streamed data");

	EXPECT_EQ("[info][] Streamed data\n", this->logStream.str());

	this->logStream.str(std::string());
	log.setName("Logger");
	log.info().raw("Streamed data");

	EXPECT_EQ("[info][Logger] Streamed data\n", this->logStream.str());
}

TEST_F(TestLog, TestLogLevelNone)
{
	this->logOutputStream->disableLogLevel(nox::log::Message::allLevels());

	nox::log::Logger log;
	log.setOutputManager(&this->logManager);

	log.info().raw("i");
	log.verbose().raw("v");
	log.warning().raw("w");
	log.error().raw("e");
	log.fatal().raw("f");
	log.debug().raw("d");

	EXPECT_EQ("", this->logStream.str());
}

TEST_F(TestLog, TestLogLevelAll)
{
	this->logOutputStream->enableLogLevel(nox::log::Message::allLevels());

	nox::log::Logger log;
	log.setOutputManager(&this->logManager);

	log.info().raw("i");
	log.verbose().raw("v");
	log.warning().raw("w");
	log.error().raw("e");
	log.fatal().raw("f");
	log.debug().raw("d");

	EXPECT_EQ("i\nv\nw\ne\nf\nd\n", this->logStream.str());
}

TEST_F(TestLog, TestLogLevelSingle)
{
	nox::log::Logger log;
	log.setOutputManager(&this->logManager);

	this->logOutputStream->disableLogLevel(nox::log::Message::allLevels());
	this->logOutputStream->enableLogLevel(nox::log::Message::Level::INFO);
	log.info().raw("i");
	log.verbose().raw("v");
	log.warning().raw("w");
	log.error().raw("e");
	log.fatal().raw("f");
	log.debug().raw("d");
	EXPECT_EQ("i\n", this->logStream.str());
	this->logStream.str("");

	this->logOutputStream->disableLogLevel(nox::log::Message::allLevels());
	this->logOutputStream->enableLogLevel(nox::log::Message::Level::VERBOSE);
	log.info().raw("i");
	log.verbose().raw("v");
	log.warning().raw("w");
	log.error().raw("e");
	log.fatal().raw("f");
	log.debug().raw("d");
	EXPECT_EQ("v\n", this->logStream.str());
	this->logStream.str("");

	this->logOutputStream->disableLogLevel(nox::log::Message::allLevels());
	this->logOutputStream->enableLogLevel(nox::log::Message::Level::WARNING);
	log.info().raw("i");
	log.verbose().raw("v");
	log.warning().raw("w");
	log.error().raw("e");
	log.fatal().raw("f");
	log.debug().raw("d");
	EXPECT_EQ("w\n", this->logStream.str());
	this->logStream.str("");

	this->logOutputStream->disableLogLevel(nox::log::Message::allLevels());
	this->logOutputStream->enableLogLevel(nox::log::Message::Level::ERROR);
	log.info().raw("i");
	log.verbose().raw("v");
	log.warning().raw("w");
	log.error().raw("e");
	log.fatal().raw("f");
	log.debug().raw("d");
	EXPECT_EQ("e\n", this->logStream.str());
	this->logStream.str("");

	this->logOutputStream->disableLogLevel(nox::log::Message::allLevels());
	this->logOutputStream->enableLogLevel(nox::log::Message::Level::FATAL);
	log.info().raw("i");
	log.verbose().raw("v");
	log.warning().raw("w");
	log.error().raw("e");
	log.fatal().raw("f");
	log.debug().raw("d");
	EXPECT_EQ("f\n", this->logStream.str());
	this->logStream.str("");

	this->logOutputStream->disableLogLevel(nox::log::Message::allLevels());
	this->logOutputStream->enableLogLevel(nox::log::Message::Level::DEBUG_);
	log.info().raw("i");
	log.verbose().raw("v");
	log.warning().raw("w");
	log.error().raw("e");
	log.fatal().raw("f");
	log.debug().raw("d");
	EXPECT_EQ("d\n", this->logStream.str());
}

TEST_F(TestLog, TestPFormattedOutput)
{
	nox::log::Logger log;
	log.setOutputManager(&this->logManager);
	log.info().pformat("Streamed {} {} {} {}", 5, "c-string", std::string("std-string"), glm::ivec2(2, 3));

	EXPECT_EQ("Streamed 5 c-string std-string [        2,        3]\n", this->logStream.str());
}
