/*
 * NOX Engine
 *
 * Copyright (c) 2016 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <nox/logic/Logic.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/Factory.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/world/Manager.h>

class TestActorPhysics: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
		this->logic = std::make_unique<nox::logic::Logic>();

		auto physics = std::make_unique<nox::logic::physics::Box2DSimulation>(this->logic.get());
		this->logic->setPhysics(std::move(physics));

		this->factory = std::make_unique<nox::logic::actor::Factory>(this->logic.get());
		this->actor = this->factory->createActor();

		auto transform = std::make_unique<nox::logic::actor::Transform>();
		transform->setContext(this->logic.get());
		transform->initialize({});
		this->actor->addComponent(std::move(transform));
	}

	virtual void TearDown()
	{
		this->actor->deactivate();
		this->actor->destroy();
		this->factory.reset();
		this->logic.reset();
	}

	std::unique_ptr<nox::logic::Logic> logic;
	std::unique_ptr<nox::logic::actor::Factory> factory;
	std::unique_ptr<nox::logic::actor::Actor> actor;
};

TEST_F(TestActorPhysics, TestGetBodySizeCircle)
{
	auto physicsJson = Json::Value{};
	auto& shapeJson = physicsJson["shape"];
	shapeJson["type"] = "circle";
	shapeJson["radius"] = 2.0f;

	auto physics = std::make_unique<nox::logic::physics::ActorPhysics>();
	physics->setContext(this->logic.get());
	ASSERT_TRUE(physics->initialize(physicsJson));
	this->actor->addComponent(std::move(physics));

	auto physicsComp = this->actor->findComponent<nox::logic::physics::ActorPhysics>();
	EXPECT_FLOAT_EQ(4.0f, physicsComp->getBodyWidth());
	EXPECT_FLOAT_EQ(4.0f, physicsComp->getBodyHeight());
}

TEST_F(TestActorPhysics, TestGetBodySizeRectangle)
{
	auto physicsJson = Json::Value{};
	auto& shapeJson = physicsJson["shape"];
	shapeJson["type"] = "box";
	auto& lowerBoundJson = shapeJson["lowerBound"];
	lowerBoundJson[0] = -1.0f;
	lowerBoundJson[1] = -1.0f;
	auto& upperBoundJson = shapeJson["upperBound"];
	upperBoundJson[0] = 1.0f;
	upperBoundJson[1] = 2.0f;

	auto physics = std::make_unique<nox::logic::physics::ActorPhysics>();
	physics->setContext(this->logic.get());
	ASSERT_TRUE(physics->initialize(physicsJson));
	this->actor->addComponent(std::move(physics));

	auto physicsComp = this->actor->findComponent<nox::logic::physics::ActorPhysics>();
	EXPECT_FLOAT_EQ(2.0f, physicsComp->getBodyWidth());
	EXPECT_FLOAT_EQ(3.0f, physicsComp->getBodyHeight());
}

TEST_F(TestActorPhysics, TestGetBodySizeNone)
{
	auto physicsJson = Json::Value{};
	auto& shapeJson = physicsJson["shape"];
	shapeJson["type"] = "none";

	auto physics = std::make_unique<nox::logic::physics::ActorPhysics>();
	physics->setContext(this->logic.get());
	ASSERT_TRUE(physics->initialize(physicsJson));
	this->actor->addComponent(std::move(physics));

	auto physicsComp = this->actor->findComponent<nox::logic::physics::ActorPhysics>();
	EXPECT_FLOAT_EQ(0.0f, physicsComp->getBodyWidth());
	EXPECT_FLOAT_EQ(0.0f, physicsComp->getBodyHeight());
}

TEST_F(TestActorPhysics, TestGetBodySizePolygon)
{
	auto physicsJson = Json::Value{};
	auto& shapeJson = physicsJson["shape"];
	shapeJson["type"] = "polygon";
	auto& vertexListJson = shapeJson["vertexList"];
	vertexListJson[0][0] = -1.0f;
	vertexListJson[0][1] = 0.0f;
	vertexListJson[1][0] = 0.0f;
	vertexListJson[1][1] = -1.0f;
	vertexListJson[2][0] = 1.0f;
	vertexListJson[2][1] = 0.0f;
	vertexListJson[3][0] = 0.0f;
	vertexListJson[3][1] = 2.0f;

	auto physics = std::make_unique<nox::logic::physics::ActorPhysics>();
	physics->setContext(this->logic.get());
	ASSERT_TRUE(physics->initialize(physicsJson));
	this->actor->addComponent(std::move(physics));

	auto physicsComp = this->actor->findComponent<nox::logic::physics::ActorPhysics>();
	EXPECT_FLOAT_EQ(2.0f, physicsComp->getBodyWidth());
	EXPECT_FLOAT_EQ(3.0f, physicsComp->getBodyHeight());
}
