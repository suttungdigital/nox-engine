/*
 * NOX Engine
 *
 * Copyright (c) 2016 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <nox/logic/actor/Actor.h>

class ChildManager : public nox::logic::actor::Component
{
public:
	const static IdType NAME;
	nox::logic::actor::Actor* child = nullptr;

	const IdType& getName() const override
	{
		return NAME;
	}

	bool initialize(const Json::Value&) override
	{
		return true;
	}

	void serialize(Json::Value&) override
	{
	}

	void onCreate() override
	{
		auto child = std::make_unique<nox::logic::actor::Actor>(nox::logic::actor::Identifier{1}, "Child");
		this->child = child.get();
		this->getOwner()->attachChildActor(std::move(child), "Child");
	}

	void onActivate() override
	{
		this->child->activate();
	}
};

const ChildManager::IdType ChildManager::NAME = "ChildManager";

TEST(TestActor, DestroyChildren)
{
	auto actor = std::make_unique<nox::logic::actor::Actor>(nox::logic::actor::Identifier{0}, "");

	auto childMngr = std::make_unique<ChildManager>();
	auto childMngrPtr = childMngr.get();
	actor->addComponent(std::move(childMngr));

	actor->create();
	ASSERT_FALSE(childMngrPtr->child == nullptr);
	EXPECT_TRUE(childMngrPtr->child->isCreated());
	EXPECT_FALSE(childMngrPtr->child->isActive());

	actor->activate();
	EXPECT_TRUE(childMngrPtr->child->isCreated());
	EXPECT_TRUE(childMngrPtr->child->isActive());

	actor->deactivate();
	ASSERT_TRUE(actor->destroy());
	EXPECT_FALSE(childMngrPtr->child->isCreated());
	EXPECT_FALSE(childMngrPtr->child->isActive());
}
