# Release Notes

## 0.1.0
**IMPORTANT**: This release will depend on Boost.Locale library.

* Fix issue with non-ASCII characters in paths not working in storage subsystem.
It will now require UTF8-encoded characters.
* Minor code enhancements.

## 0.0.0
This is the very first release of NOX Engine, providing the most important functionality to simulate and render a world.
Some subsystems and functionality are missing, like AI.
Some interfaces are also not 100% finished and will most likely go through some more refactoring.
But overall the API should be pretty stable with this release.
