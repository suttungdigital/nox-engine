FROM debian:stretch

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		software-properties-common \
		git \
		gcc \
		clang \
		openjdk-8-jdk \
		make \
		libboost-filesystem-dev \
		libboost-locale-dev \
		libncurses5 \
		libglu1-mesa-dev \
		libgl1-mesa-dev \
		wget \
		bzip2 \
		unzip \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

ARG ASDK_TOOLS_VER=3859397
ARG ASDK_BT_VER=26.0.1
ARG ASDK_API=26

ARG ANDK_VER=r15c

ARG BOOST_MAJOR=1
ARG BOOST_MINOR=65
ARG BOOST_PATCH=1

ARG CMAKE_MAJOR=3
ARG CMAKE_MINOR=9
ARG CMAKE_PATCH=2

RUN wget https://cmake.org/files/v${CMAKE_MAJOR}.${CMAKE_MINOR}/cmake-${CMAKE_MAJOR}.${CMAKE_MINOR}.${CMAKE_PATCH}.tar.gz \
	&& tar -xzf cmake-${CMAKE_MAJOR}.${CMAKE_MINOR}.${CMAKE_PATCH}.tar.gz \
	&& rm cmake-${CMAKE_MAJOR}.${CMAKE_MINOR}.${CMAKE_PATCH}.tar.gz \
	&& cd cmake-${CMAKE_MAJOR}.${CMAKE_MINOR}.${CMAKE_PATCH} \
	&& ./bootstrap --prefix=/usr/local \
	&& make \
	&& make install \
	&& cd .. \
	&& rm -r cmake-${CMAKE_MAJOR}.${CMAKE_MINOR}.${CMAKE_PATCH}

ENV PATH=/nox/android-sdk/tools:/nox/android-sdk/platform-tools:/usr/local/bin:$PATH \
	ANDROID_NDK=/nox/android-ndk \
	ANDROID_HOME=/nox/android-sdk

RUN mkdir /nox

RUN cd /nox \
	&& wget "http://downloads.sourceforge.net/project/boost/boost/$BOOST_MAJOR.$BOOST_MINOR.$BOOST_PATCH/boost_${BOOST_MAJOR}_${BOOST_MINOR}_${BOOST_PATCH}.tar.bz2" \
	&& tar -xjf boost_${BOOST_MAJOR}_${BOOST_MINOR}_${BOOST_PATCH}.tar.bz2 boost_${BOOST_MAJOR}_${BOOST_MINOR}_${BOOST_PATCH}/boost \
	&& mv boost_${BOOST_MAJOR}_${BOOST_MINOR}_${BOOST_PATCH} boost \
	&& rm boost_${BOOST_MAJOR}_${BOOST_MINOR}_${BOOST_PATCH}.tar.bz2

RUN cd /nox \
	&& wget "https://dl.google.com/android/repository/sdk-tools-linux-${ASDK_TOOLS_VER}.zip" \
	&& unzip sdk-tools-linux-${ASDK_TOOLS_VER}.zip \
	&& mkdir android-sdk \
	&& mv tools android-sdk/ \
	&& rm sdk-tools-linux-${ASDK_TOOLS_VER}.zip \
	&& mkdir ~/.android && touch ~/.android/repositories.cfg \
	&& yes | ./android-sdk/tools/bin/sdkmanager --update \
	&& ./android-sdk/tools/bin/sdkmanager tools "platform-tools" "platforms;android-${ASDK_API}" "build-tools;${ASDK_BT_VER}" "extras;google;m2repository"

RUN cd /nox \
	&& wget "http://dl.google.com/android/repository/android-ndk-${ANDK_VER}-linux-x86_64.zip" \
	&& unzip android-ndk-${ANDK_VER}-linux-x86_64.zip -x \
		"android-ndk-${ANDK_VER}/platforms/android-9*" \
		"android-ndk-${ANDK_VER}/platforms/android-12*" \
		"android-ndk-${ANDK_VER}/platforms/android-13*" \
		"android-ndk-${ANDK_VER}/platforms/android-15*" \
		"android-ndk-${ANDK_VER}/platforms/android-16*" \
		"android-ndk-${ANDK_VER}/platforms/android-17*" \
		"android-ndk-${ANDK_VER}/platforms/android-18*" \
		"android-ndk-${ANDK_VER}/platforms/android-19*"\
		"android-ndk-${ANDK_VER}/platforms/android-2*"\
		"android-ndk-${ANDK_VER}/toolchains/x86*"\
		"android-ndk-${ANDK_VER}/toolchains/aarch*" \
		"android-ndk-${ANDK_VER}/toolchains/mips*" \
		"android-ndk-${ANDK_VER}/toolchains/renderscript*" \
		"android-ndk-${ANDK_VER}/sources/cxx-stl/system*" \
		"android-ndk-${ANDK_VER}/sources/cxx-stl/stlport*" \
		"android-ndk-${ANDK_VER}/sources/cxx-stl/gabi++*" \
		"android-ndk-${ANDK_VER}/sources/cxx-stl/gnu-libstdc++*" \
		"android-ndk-${ANDK_VER}/sources/cxx-stl/llvm-libc++/libs/arm64-v8a*" \
		"android-ndk-${ANDK_VER}/sources/cxx-stl/llvm-libc++/libs/armeabi/*" \
		"android-ndk-${ANDK_VER}/sources/cxx-stl/llvm-libc++/libs/mips*" \
		"android-ndk-${ANDK_VER}/sources/cxx-stl/llvm-libc++/libs/x86*" \
		"android-ndk-${ANDK_VER}/sources/third_party*" \
		"android-ndk-${ANDK_VER}/prebuilt*" \
		"android-ndk-${ANDK_VER}/simpleperf*" \
	&& mv android-ndk-${ANDK_VER} android-ndk \
	&& rm android-ndk-${ANDK_VER}-linux-x86_64.zip
