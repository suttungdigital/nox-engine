iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex
choco install -y cmake.portable git
git clone https://bitbucket.org/suttungdigital/windows-libraries
cd windows-libraries
.\create_msvc140_usr.ps1
$usr = [System.IO.Path]::GetFullPath((Join-Path (pwd) usr))
[Environment]::SetEnvironmentVariable("CMAKE_PREFIX_PATH", "$usr", "User")
$bin64 = Join-Path $usr bin\x64
$bin32 = Join-Path $usr bin\x86
$path = [Environment]::GetEnvironmentVariable("Path", "User")
[Environment]::SetEnvironmentVariable("Path", "$bin64;$bin32;$path", "User")
