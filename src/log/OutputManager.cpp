/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/log/OutputManager.h>
#include <nox/log/Output.h>

#include <algorithm>

namespace nox { namespace log {

void OutputManager::log(const Message& message)
{
	for (const std::unique_ptr<Output>& logOutput : this->logOutputs)
	{
		logOutput->log(message);
	}
}

void OutputManager::addLogOutput(std::unique_ptr<Output> output)
{
	this->logOutputs.push_back(std::move(output));
}

bool OutputManager::removeLogOutput(const Output* output)
{
	auto outputIt = std::find_if(
			this->logOutputs.begin(),
			this->logOutputs.end(),
			[output] (const std::unique_ptr<Output>& storedOutput)
			{
				return (storedOutput.get() == output);
			}
	);

	if (outputIt != this->logOutputs.end())
	{
		this->logOutputs.erase(outputIt);
		return true;
	}
	else
	{
		return false;
	}
}

} }
