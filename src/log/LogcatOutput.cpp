/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/log/LogcatOutput.h>
#include <android/log.h>

namespace nox { namespace log {

void LogcatOutput::outputMessage(const Message& message)
{
	const auto tag = "NOX";
	const auto text = this->getFormattedMessage(message);
	int priority = ANDROID_LOG_UNKNOWN;

	switch (message.getLogLevel())
	{
	case Message::Level::INFO:
		priority = ANDROID_LOG_INFO;
		break;
	case Message::Level::VERBOSE:
		priority = ANDROID_LOG_VERBOSE;
		break;
	case Message::Level::WARNING:
		priority = ANDROID_LOG_WARN;
		break;
	case Message::Level::DEBUG_:
		priority = ANDROID_LOG_DEBUG;
		break;
	case Message::Level::ERROR:
		priority = ANDROID_LOG_ERROR;
		break;
	case Message::Level::FATAL:
		priority = ANDROID_LOG_FATAL;
		break;
	}

	__android_log_write(priority, tag, text.c_str());
}

} }
