/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/log/Logger.h>
#include <nox/log/OutputManager.h>
#include <nox/log/Message.h>

namespace nox { namespace log {

Logger::Logger():
	outputManager(nullptr)
{
}

Logger::Logger(OutputManager* manager):
	outputManager(manager)
{
}

Logger::Logger(const std::string& name, OutputManager* manager):
	name(name),
	outputManager(manager)
{
}

Logger::Logger(Logger&& other):
	name(std::move(other.name)),
	outputManager(other.outputManager)
{
	other.outputManager = nullptr;
}

Logger& Logger::operator=(Logger&& other)
{
	this->name = std::move(other.name);
	this->outputManager = other.outputManager;

	other.outputManager = nullptr;

	return *this;
}

void Logger::setName(const std::string& name)
{
	this->name = name;
}

void Logger::setOutputManager(OutputManager* manager)
{
	this->outputManager = manager;
}

Logger::Output Logger::info()
{
	return Output(this->name, this->outputManager, Message::Level::INFO);
}

Logger::Output Logger::verbose()
{
	return Output(this->name, this->outputManager, Message::Level::VERBOSE);
}

Logger::Output Logger::warning()
{
	return Output(this->name, this->outputManager, Message::Level::WARNING);
}

Logger::Output Logger::error()
{
	return Output(this->name, this->outputManager, Message::Level::ERROR);
}

Logger::Output Logger::fatal()
{
	return Output(this->name, this->outputManager, Message::Level::FATAL);
}

Logger::Output Logger::debug()
{
	return Output(this->name, this->outputManager, Message::Level::DEBUG_);
}


const std::size_t Logger::Output::MAX_FORMAT_MESSAGE_SIZE = 8 * 1024;

Logger::Output::Output(const std::string& loggerName, OutputManager* manager, const Message::Level logLevel):
	loggerName(loggerName),
	outputManager(manager),
	logLevel(logLevel)
{
}

void Logger::Output::raw(const std::string& string)
{
	if (this->outputManager != nullptr)
	{
		this->outputManager->log(Message(this->logLevel, string, this->loggerName));
	}
}

} }
