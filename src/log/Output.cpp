/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/log/Message.h>
#include <nox/log/Output.h>

#include <fmt/format.h>

namespace nox { namespace log {

Output::Output():
	outputFormat("${message}"),
	levelMask({Message::Level::ERROR, Message::Level::FATAL, Message::Level::WARNING, Message::Level::INFO})
{
	this->constructionTimePoint = std::chrono::system_clock::now();
}

Output::~Output()
{
}

void Output::setOutputFormat(const std::string& format)
{
	this->outputFormat = format;
}

void Output::enableLogLevel(const util::Mask<Message::Level>& level)
{
	this->levelMask.add(level);
}

void Output::disableLogLevel(const util::Mask<Message::Level>& level)
{
	this->levelMask.remove(level);
}

void Output::log(const Message& message)
{
	if (this->levelMask.matches(message.getLogLevel()))
	{
		this->outputMessage(message);
	}
}

std::string Output::getFormattedMessage(const Message& message)
{
	std::string formattedMessage;
	std::string::size_type formatIndex = 0;

	while (formatIndex < this->outputFormat.size())
	{
		if (this->outputFormat[formatIndex] == '$')
		{
			const std::string::size_type nextIndex = formatIndex + 1;

			if (this->outputFormat[nextIndex] == '$')
			{
				formattedMessage += '$';
			}
			else
			{
				const std::string::size_type formatStart = this->outputFormat.find_first_of('{', nextIndex) + 1;

				if (formatStart < this->outputFormat.size())
				{
					const std::string::size_type formatEnd = this->outputFormat.find_first_of('}', formatStart);
					const std::string format = this->outputFormat.substr(formatStart, formatEnd - formatStart);

					if (formatEnd < this->outputFormat.size())
					{
						formattedMessage += this->getFormattedString(message, format);
						formatIndex = formatEnd;
					}
				}
			}
		}
		else
		{
			formattedMessage += this->outputFormat[formatIndex];
		}

		formatIndex++;
	}

	return formattedMessage;
}

std::string Output::getFormattedString(const Message& message, const std::string& format) const
{
	std::string formattedMessage;

	if (format == "message")
	{
		formattedMessage = message.getMessage();
	}
	else if (format == "loglevel")
	{
		formattedMessage = message.getLogLevelString();
	}
	else if (format == "loggername" && message.getLoggerName().empty() == false)
	{
		formattedMessage = message.getLoggerName();
	}
	else if (format == "timestamp")
	{
		const auto now = std::chrono::system_clock::now();
		const auto timeSinceConstruction = now - this->constructionTimePoint;
		const auto durationMs = std::chrono::duration_cast<std::chrono::milliseconds>(timeSinceConstruction);
		formattedMessage = fmt::format("{}", durationMs.count());
	}

	return formattedMessage;
}

} }
