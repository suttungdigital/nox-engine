/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/event/IBroadcaster.h>
#include <nox/event/ListenerManager.h>
#include <algorithm>
#include <cassert>

namespace nox { namespace event {

ListenerManager::ListenerManager(const std::string& name):
	name(name),
	listening(false),
	eventBroadcaster(nullptr),
	listener(nullptr)
{
}

ListenerManager::~ListenerManager()
{
	if (this->listening == true)
	{
		for (const Event::IdType& type : this->eventTypes)
		{
			this->eventBroadcaster->removeListener(this->listener, type);
		}
	}
}

ListenerManager::ListenerManager(ListenerManager&& other) NOX_NOEXCEPT:
	name(other.name),
	listening(other.listening),
	eventBroadcaster(other.eventBroadcaster),
	listener(other.listener),
	eventTypes(std::move(other.eventTypes))
{
	other.eventBroadcaster = nullptr;
	other.listener = nullptr;
	other.listening = false;
}

ListenerManager& ListenerManager::operator=(ListenerManager&& other) NOX_NOEXCEPT
{
	this->name = other.name;
	this->listening = other.listening;
	this->eventBroadcaster = other.eventBroadcaster;
	this->listener = other.listener;
	this->eventTypes = std::move(other.eventTypes);

	other.eventBroadcaster = nullptr;
	other.listener = nullptr;
	other.listening = false;

	return *this;
}

void ListenerManager::setup(IListener* listener, IBroadcaster* broadcaster)
{
	this->listener = listener;
	this->eventBroadcaster = broadcaster;

	assert(this->listener != nullptr);
	assert(this->eventBroadcaster != nullptr);
}

void ListenerManager::setup(IListener* listener, IBroadcaster* broadcaster, StartListening_t)
{
	this->setup(listener, broadcaster);
	this->startListening();
}

void ListenerManager::addEventTypeToListenFor(const Event::IdType& type)
{
	this->eventTypes.push_back(type);

	if (this->listening == true)
	{
		this->eventBroadcaster->addListener(this->listener, type, this->name);
	}
}

void ListenerManager::removeEventTypeToListenFor(const Event::IdType& type)
{
	auto typeIt = std::find(this->eventTypes.begin(), this->eventTypes.end(), type);

	if (typeIt != this->eventTypes.end())
	{
		if (this->listening == true)
		{
			this->eventBroadcaster->removeListener(this->listener, type);
		}

		this->eventTypes.erase(typeIt);
	}
}

void ListenerManager::startListening()
{
	if (this->listening == false)
	{
		assert(this->listener != nullptr);
		assert(this->eventBroadcaster != nullptr);

		for (const Event::IdType& eventType : this->eventTypes)
		{
			this->eventBroadcaster->addListener(this->listener, eventType, this->name);
		}

		this->listening = true;
	}
}

void ListenerManager::stopListening()
{
	if (this->listening == true)
	{
		assert(this->listener != nullptr);
		assert(this->eventBroadcaster != nullptr);

		for (const Event::IdType& eventType : this->eventTypes)
		{
			this->eventBroadcaster->removeListener(this->listener, eventType);
		}

		this->listening = false;
	}
}

} }
