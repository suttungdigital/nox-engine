/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/actor/ActorGravitation.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/IContext.h>
#include <nox/logic/physics/Simulation.h>
#include <cassert>

namespace nox { namespace logic { namespace physics
{

const ActorGravitation::IdType ActorGravitation::NAME = "Gravitation";

const ActorGravitation::IdType& ActorGravitation::getName() const
{
	return NAME;
}

bool ActorGravitation::initialize(const Json::Value& componentJsonObject)
{
	this->mass = componentJsonObject.get("mass", 0.0f).asFloat();

	if (this->mass == 0.0f)
	{
		this->getLog().warning().raw("Mass is zero, meaning no gravitational force is applied.");
	}

	const Json::Value& distanceLimitJson = componentJsonObject["distanceLimit"];

	if (distanceLimitJson.isNull() == false)
	{
		const Json::Value& minLimitJson = distanceLimitJson["min"];
		const Json::Value& maxLimitJson = distanceLimitJson["max"];

		if (minLimitJson.isNull() == false)
		{
			const float distance = minLimitJson.get("distance", 0.0f).asFloat();
			const std::string typeString = minLimitJson.get("type", "clamp").asString();
			GravitationalForce::DistanceLimit::Type limitType = GravitationalForce::DistanceLimit::Type::CLAMP;

			if (typeString == "clamp")
			{
				limitType = GravitationalForce::DistanceLimit::Type::CLAMP;
			}
			else if (typeString == "border")
			{
				limitType = GravitationalForce::DistanceLimit::Type::BORDER;
			}

			this->forceDistanceLimit.setMinLimitOn(distance, limitType);
		}

		if (maxLimitJson.isNull() == false)
		{
			const float distance = maxLimitJson.get("distance", 0.0f).asFloat();
			const std::string typeString = maxLimitJson.get("type", "clamp").asString();
			GravitationalForce::DistanceLimit::Type limitType = GravitationalForce::DistanceLimit::Type::CLAMP;

			if (typeString == "clamp")
			{
				limitType = GravitationalForce::DistanceLimit::Type::CLAMP;
			}
			else if (typeString == "border")
			{
				limitType = GravitationalForce::DistanceLimit::Type::BORDER;
			}

			this->forceDistanceLimit.setMaxLimitOn(distance, limitType);
		}
	}

	return true;
}

void ActorGravitation::onCreate()
{
	this->force = this->getLogicContext()->getPhysics()->createGravitationalForce();
	assert(this->force != nullptr);

	this->force->setMass(this->mass);
	this->force->setDistanceLimit(this->forceDistanceLimit);
}

void ActorGravitation::onDestroy()
{
	this->getLogicContext()->getPhysics()->removeGravitationalForce(this->force);
}

void ActorGravitation::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (event->isType(actor::TransformChange::ID))
	{
		auto transformEvent = static_cast<actor::TransformChange*>(event.get());
		this->force->setPosition(transformEvent->getPosition());
	}
}

void ActorGravitation::serialize(Json::Value& componentObject)
{
	componentObject["mass"] = this->force->getMass();
}


} } }
