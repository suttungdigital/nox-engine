/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/box2d/Box2DSimulation.h>

#include <nox/logic/physics/box2d/box2d_utils.h>
#include <nox/logic/physics/GravitationalForce.h>
#include <nox/logic/physics/physics_utils.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/graphics/event/DebugGeometryChange.h>
#include <nox/logic/graphics/event/DebugRenderingEnabled.h>
#include <nox/event/IBroadcaster.h>
#include <nox/logic/IContext.h>
#include <nox/app/IContext.h>
#include <nox/app/graphics/2d/Geometry.h>
#include <nox/app/graphics/2d/IRenderer.h>
#include <nox/app/graphics/2d/Light.h>
#include <nox/util/boost_utils.h>
#include <nox/util/chrono_utils.h>
#include <nox/util/algorithm.h>
#include <nox/util/math/vector_math.h>
#include <nox/util/math/geometry.h>
#include <nox/util/math/angle.h>
#include <nox/util/math/Circle.h>

#include <poly2tri.h>
#include <Box2D/Common/b2Settings.h>

namespace nox { namespace logic { namespace physics
{

typedef std::vector<p2t::Point*> P2tPolygon;

/**
 * Query callback class used to get all the bodies within an AABB.
 */
class AabbBodyQuery: public b2QueryCallback
{
public:
	bool ReportFixture(b2Fixture* fixture) override;

	//! The bodies found.
	std::unordered_set<b2Body*> bodiesWithin;
};
class AabbFixtureQuery: public b2QueryCallback
{
public:
	bool ReportFixture(b2Fixture* fixture) override;

	std::vector<b2Fixture*> fixturesWithin;
};

/**
 * Query callback class used to get all the bodies within an AABB in a b2DynamicTree.
 */
class DynamicTreeAabbBodyQuery
{
public:
	DynamicTreeAabbBodyQuery(b2DynamicTree* dynamicTree):
		dynamicTree(dynamicTree)
	{}

	bool QueryCallback(int32 proxy)
	{
		assert(this->dynamicTree != nullptr);

		b2Fixture* fixture = static_cast<b2Fixture*>(this->dynamicTree->GetUserData(proxy));
		assert(fixture != nullptr && fixture->GetBody() != nullptr);

		this->bodiesWithin.insert(fixture->GetBody());

		return true;
	}

	//! The bodies found.
	std::unordered_set<b2Body*> bodiesWithin;

private:
	b2DynamicTree* dynamicTree;
};

class DynamicTreeClosestRayIntersection
{
public:
	DynamicTreeClosestRayIntersection(b2DynamicTree* dynamicTree):
		point(0.0f, 0.0f),
		normal(0.0f, 0.0f),
		fraction(1.0f),
		fixture(nullptr),
		dynamicTree(dynamicTree)
	{}

	float32 RayCastCallback(const b2RayCastInput& input, int32 proxy);

	void enableBodyType(const b2BodyType type);
	void enableBodyTypes(const std::vector<b2BodyType>& types);
	void enableAllBodyTypes();

	b2Vec2 point;
	b2Vec2 normal;
	float32 fraction;
	b2Fixture* fixture;

private:
	b2DynamicTree* dynamicTree;
	std::vector<b2BodyType> acceptedTypes;
};


struct PolygonVertex
{
	PolygonVertex():
		angle(0.0f),
		fraction(1.0f),
		fixture(nullptr),
		isMin(false),
		isMax(false)
	{}

	PolygonVertex(float angle, const b2Vec2& position, bool isMin, bool isMax, const b2Fixture* fixture, float fraction):
		position(position),
		angle(angle),
		fraction(fraction),
		fixture(fixture),
		isMin(isMin),
		isMax(isMax)
	{}

	b2Vec2 position;
	float angle;
	float fraction;
	const b2Fixture* fixture;
	bool isMin;
	bool isMax;
};

/**
 *  Does the callback that gets called by Box2D when
 */
class LightRayCastCallBack: public b2RayCastCallback
{
public:
	float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction);

	float32 closestFraction;
	PolygonVertex* currenRayTarget;
	Box2DSimulation* physics;
};

std::vector<b2PolygonShape> createPolygonShape(const util::BoostPolygon& polygon);
std::vector<b2PolygonShape> createPolygonShape(const std::vector<util::BoostPolygon>& polygonList);
b2CircleShape createCircleShape(const float radius, const glm::vec2& position);
b2PolygonShape createRectangleShape(const math::Box<glm::vec2>& rectangle);

const float SMALL_ANGLE = 0.0001f;

Box2DSimulation::Box2DSimulation(IContext* logicContext):
	listener("Box2DPhysics"),
	lightDepth(6),
	activeMode(SimulationMode::SIMULATED),
	debugRenderingEnabled(false)
{
	this->log.setName("Box2dSimulation");

	this->logicContext = logicContext;

	this->listener.setup(this, this->logicContext->getEventBroadcaster(), event::ListenerManager::StartListening_t());
	this->listener.addEventTypeToListenFor(graphics::DebugRenderingEnabled::ID);

	this->staticColor = {1.0f, 0.0f, 0.0f};
	this->kinematicColor = {0.0f, 1.0f, 0.0f};
	this->dynamicColor = {0.0f, 0.0f, 1.0f};

	this->angleColor = glm::vec4(0.0f, 0.0f, 0.5f, 0.4f);
	this->edgeColor = glm::vec4(0.0f, 1.0f, 1.0f, 0.7f);

	this->circleNumberOfSegments = 20;
	this->circleTheta = math::fullCircle<float>() / static_cast<float>(this->circleNumberOfSegments);
	this->circleCosine = glm::cos(this->circleTheta);
	this->circleSine = glm::sin(this->circleTheta);

	this->velocityIterations = 6;
	this->positionIterations = 6;

	this->mouseJoint = nullptr;
	this->mouseScrollScaler = 0.01f;
	this->mouseMaxForce = 500.0f;

	//TODO: Might need to adjust this to prevent light artifacts..
	this->rayLengthExtra = 0.0f;
	this->rayMinimumNumber = 20;

	this->rayMaxFraction = 1.0;
	this->rayTheta = 2 * glm::pi<float>() / float(this->rayMinimumNumber);

	this->quitThreads = false;

	unsigned int numberOfThreads = std::thread::hardware_concurrency();

	if (numberOfThreads == 0)
	{
		numberOfThreads = 1;      //If the hardware concurrency function above returns 0 (can't find any)
	}                                   //  we need to make at least one thread.

	this->lightAreaInsertBarrier = std::unique_ptr<thread::ThreadBarrier>(new thread::ThreadBarrier(numberOfThreads + 1));
	this->lightAreaProcessingBarrier = std::unique_ptr<thread::ThreadBarrier>(new thread::ThreadBarrier(numberOfThreads + 1));

	for (unsigned int i = 0; i < numberOfThreads; i++)
	{
		this->workers.push_back(std::thread(&Box2DSimulation::multithreadedLightCalculation, this));
	}

	this->polygonRenderSet = std::make_shared<app::graphics::GeometrySet>(app::graphics::GeometryPrimitive::TRIANGLE, app::graphics::GeometrySet::DataMode::REQUEST);
	this->lineRenderSet = std::make_shared<app::graphics::GeometrySet>(app::graphics::GeometryPrimitive::LINE, app::graphics::GeometrySet::DataMode::REQUEST);

	auto broadcaster = this->logicContext->getEventBroadcaster();
	broadcaster->queueEvent(std::make_shared<graphics::DebugGeometryChange>(graphics::DebugGeometryChange::Action::ADD, this->polygonRenderSet));
	broadcaster->queueEvent(std::make_shared<graphics::DebugGeometryChange>(graphics::DebugGeometryChange::Action::ADD, this->lineRenderSet));

	this->world = std::unique_ptr<b2World>(new b2World(b2Vec2(0.0f, 0.0f)));
	this->world->SetContactListener(&this->contactHandler);
	this->world->SetDestructionListener(this);

	b2BodyDef groundBodyDef;
	this->groundBody = this->world->CreateBody(&groundBodyDef);

	this->inactiveBodyDynamicTree = std::unique_ptr<b2DynamicTree>(new b2DynamicTree());
	this->contactHandler.init(this, this->world.get());

	this->bodyCategoryMap[""] = 0x0001;
	this->nextCategoryBit = 0x0002;

	// Book the first group index (0) as we want start at -1.
	this->friendGroupIndexBooker.book();
}

Box2DSimulation::~Box2DSimulation()
{
	this->reset();

	this->lightAreaProcessingBarrier->wait();
	this->quitThreads = true;
	this->lightAreaInsertBarrier->wait();

	for (auto &thread : this->workers)
	{
		if (thread.joinable())
		{
			thread.join();
		}
	}

	this->listener.stopListening();
}

void Box2DSimulation::setLogger(log::Logger logger)
{
	this->log = std::move(logger);
	this->log.setName("Box2DPhysics");
}

void Box2DSimulation::onUpdate(const Duration& deltaTime)
{
	if (this->activeMode == SimulationMode::SIMULATED)
	{
		// Store the current state as the previous state for each body.
		for (b2Body* body = this->world->GetBodyList(); body != nullptr; body = body->GetNext())
		{
			if (body->IsActive() == true && body->IsAwake() == true)
			{
				BodyData* bodyData = static_cast<BodyData*>(body->GetUserData());

				if (bodyData != nullptr)
				{
					bodyData->previousPosition = body->GetPosition();
					bodyData->previousAngle = body->GetAngle();
				}
			}
		}

		std::unique_lock<std::mutex> worldLock(this->worldModifyMutex);

		const glm::vec2 initialPull = math::convertVec2<glm::vec2>(this->world->GetGravity());

		for (b2Body* body = this->world->GetBodyList(); body != nullptr; body = body->GetNext())
		{
			if (body->GetType() == b2_dynamicBody)
			{
				BodyData* bodyData = static_cast<BodyData*>(body->GetUserData());
				bodyData->gravitationalPull = initialPull * body->GetGravityScale();
				bodyData->strongestGravitationalPull = bodyData->gravitationalPull;
			}
		}

		for (const std::unique_ptr<GravitationalForce>& gravitation : this->gravitationalForces)
		{
			for (b2Body* body = this->world->GetBodyList(); body != nullptr; body = body->GetNext())
			{
				if (body->GetType() == b2_dynamicBody)
				{
					const GravitationalForce::Body gravitationBody(math::convertVec2<glm::vec2>(body->GetPosition()), body->GetMass());
					const glm::vec2 gravitationalForce = gravitation->calculateForceOnBody(gravitationBody, 1.0f) * body->GetGravityScale();

					BodyData* bodyData = static_cast<BodyData*>(body->GetUserData());
					bodyData->gravitationalPull += gravitationalForce;
					bodyData->strongestGravitationalPull = glm::max(gravitationalForce, bodyData->strongestGravitationalPull);

					body->ApplyForceToCenter(math::convertVec2<b2Vec2>(gravitationalForce), false);
				}
			}
		}

		worldLock.unlock();

		std::unique_lock<std::mutex> updateLock(this->physicsUpdateMutex);
		this->world->Step(util::durationToSeconds<float32>(deltaTime), this->velocityIterations, this->positionIterations);
		updateLock.unlock();

		this->contactHandler.handleContactTasks();
	}
}

void Box2DSimulation::onSyncState(const float updateAlpha)
{
	std::unique_lock<std::mutex> worldLock(this->worldModifyMutex);

	std::vector<actor::Transform*> transforms;
	const auto alphaRemaining = 1.0f - updateAlpha;

	for (const auto& actorData : this->actorMap)
	{
		b2Body* body = actorData.second.mainBody;

		if (body != nullptr && body->IsActive() == true)
		{
			actor::Actor* actor = actorData.second.actor;

			if (actor != nullptr && actor->isActive() == true)
			{
				auto transformComponent = actor->findComponent<actor::Transform>();

				if (transformComponent != nullptr)
				{
					const BodyData* bodyData = static_cast<BodyData*>(body->GetUserData());

					// Without alpha.
//					const glm::vec2 bodyAlphaPosition = convertVec2<glm::vec2>(body->GetPosition());
//					const float bodyAlphaAngle = body->GetAngle();

					const glm::vec2 bodyAlphaPosition = math::convertVec2<glm::vec2>(updateAlpha * body->GetPosition() + alphaRemaining * bodyData->previousPosition);
					const float bodyAlphaAngle = updateAlpha * body->GetAngle() + alphaRemaining * bodyData->previousAngle;

					bool changed = false;

					if (bodyAlphaPosition != transformComponent->getPosition())
					{
						transformComponent->setPosition(bodyAlphaPosition, actor::Transform::NoBroadcast_t());
						changed = true;
					}

					if (bodyAlphaAngle != transformComponent->getRotation())
					{
						transformComponent->setRotation(bodyAlphaAngle, actor::Transform::NoBroadcast_t());
						changed = true;
					}

					if (changed == true && this->activeMode == SimulationMode::SIMULATED)
					{
						transforms.push_back(transformComponent);
					}
				}
			}
		}
	}

	worldLock.unlock();

	for (auto transform : transforms)
	{
		transform->broadcastTransformChange();
	}

	if (this->debugRenderingEnabled == true)
	{
		this->syncDebugRendering();
	}
}

void Box2DSimulation::updateLighting()
{
#ifdef TIME_LIGHT_CALCULATION
	if (this->frameCount >= NUM_AVERAGING_FRAMES)
	{
		auto nanosecondsTotal =std::chrono::duration_cast<std::chrono::nanoseconds>(this->lightCalculationTime).count();
		auto nanosecondsPerFrame = nanosecondsTotal / this->frameCount;

		auto microsecondsTotal = std::chrono::duration_cast<std::chrono::microseconds>(this->lightCalculationTime).count();
		auto microsecondsPerFrame = microsecondsTotal / this->frameCount;

		auto millisecondsTotal = std::chrono::duration_cast<std::chrono::milliseconds>(this->lightCalculationTime).count();
		auto millisecondsPerFrame = millisecondsTotal / this->frameCount;

		this->log.debug().format("\nLight calculation used %i nanoseconds per frame.", nanosecondsPerFrame);
		this->log.debug().format("Light calculation used %i microseconds per frame.", microsecondsPerFrame);
		this->log.debug().format("Light calculation used %i milliseconds per frame.", millisecondsPerFrame);

		this->frameCount = 0;
		this->lightCalculationTime = std::chrono::high_resolution_clock::duration(0);
	}
#endif

	for (auto& light : this->lights)
	{
		if (light->isEnabled() == true && light->castShadows == true && light->getRange() > 0.0f)
		{
			this->lightQueue.push(light);
		}
	}

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
	lightAreaProcessingBarrier->wait();
	lightAreaInsertBarrier->wait();

#ifdef TIME_LIGHT_CALCULATION
	this->frameCount++;
#endif
}

void Box2DSimulation::createActorBody(actor::Actor* actor, const BodyDefinition& bodyDefinition)
{
	if (this->actorMap.find(actor->getId()) != this->actorMap.end())
	{
		this->log.error().raw("Failed creating actor body. Actor already added to physics.");
		return;
	}

	b2BodyDef box2dBodyDef;
	box2dBodyDef.angularDamping = bodyDefinition.angularDamping;
	box2dBodyDef.linearDamping = bodyDefinition.linearDamping;
	box2dBodyDef.linearVelocity = math::convertVec2<b2Vec2>(bodyDefinition.linearVelocity);
	box2dBodyDef.angularVelocity = bodyDefinition.angularVelocity;
	box2dBodyDef.position = b2Vec2(bodyDefinition.position.x, bodyDefinition.position.y);
	box2dBodyDef.angle = bodyDefinition.angle;
	box2dBodyDef.gravityScale = bodyDefinition.gravityMultiplier;
	box2dBodyDef.active = bodyDefinition.active;
	box2dBodyDef.bullet = bodyDefinition.bullet;
	box2dBodyDef.allowSleep = bodyDefinition.allowSleep;

	if (bodyDefinition.bodyType == PhysicalBodyType::DYNAMIC)
	{
		box2dBodyDef.type = b2_dynamicBody;
	}

	if (bodyDefinition.bodyType == PhysicalBodyType::STATIC)
	{
		box2dBodyDef.type = b2_staticBody;
	}

	if (bodyDefinition.bodyType == PhysicalBodyType::KINEMATIC)
	{
		box2dBodyDef.type = b2_kinematicBody;
	}

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	ActorData& actorData = this->actorMap[actor->getId()];
	actorData.actor = actor;

	assert(actor != nullptr);
	BodyData* bodyData = new BodyData();
	bodyData->actor = actorData.actor;
	bodyData->depth = bodyDefinition.depth;
	bodyData->gravitationalPull = math::convertVec2<glm::vec2>(this->world->GetGravity());
	bodyData->previousPosition = box2dBodyDef.position;
	bodyData->previousAngle = box2dBodyDef.angle;

	auto categoryIt = this->bodyCategoryMap.find(bodyDefinition.collisionCategory);

	if (categoryIt != this->bodyCategoryMap.end())
	{
		bodyData->categoryBits = categoryIt->second;
	}
	else if (this->nextCategoryBit != 0x0)
	{
		bodyData->categoryBits = this->nextCategoryBit;

		this->bodyCategoryMap.insert({ bodyDefinition.collisionCategory, this->nextCategoryBit });
		this->nextCategoryBit = static_cast<uint16>(this->nextCategoryBit << 1);
	}
	else
	{
		this->log.error().format("Could not add collision category \"%s\": The number of categories exceeds the limit of 16.", bodyDefinition.collisionCategory.c_str());
	}

	for (const auto& maskCategory : bodyDefinition.maskCategories)
	{
		auto maskCategoryIt = this->bodyCategoryMap.find(maskCategory);

		if (maskCategoryIt != this->bodyCategoryMap.end())
		{
			bodyData->maskBits = static_cast<uint16>(bodyData->maskBits & ~(maskCategoryIt->second));
		}
		else if (this->nextCategoryBit != 0x0)
		{
			bodyData->maskBits = static_cast<uint16>(bodyData->maskBits & ~(this->nextCategoryBit));

			this->bodyCategoryMap.insert({ maskCategory, this->nextCategoryBit });
			this->nextCategoryBit = static_cast<uint16>(this->nextCategoryBit << 1);
		}
		else
		{
			this->log.error().format("Could not add collision category \"%s\": The number of categories exceeds the limit of 16.", maskCategory.c_str());
		}
	}

	box2dBodyDef.userData = static_cast<void*>(bodyData);

	std::unique_lock<std::mutex> updateLock(this->physicsUpdateMutex);

	b2Body* body = this->world->CreateBody(&box2dBodyDef);
	body->SetFixedRotation(bodyDefinition.fixedRotation);

	updateLock.unlock();

	if (bodyDefinition.depth >= this->lightDepth && bodyDefinition.bodyType == PhysicalBodyType::DYNAMIC)
	{
		this->dynamicShadowcastingBodies.push_back(body);
	}

	if (bodyDefinition.shape.type != ShapeType::NONE)
	{
		b2FixtureDef fixtureDef;
		fixtureDef.density = bodyDefinition.density;
		fixtureDef.friction = bodyDefinition.friction;
		fixtureDef.isSensor = bodyDefinition.sensor;
		fixtureDef.restitution = bodyDefinition.restitution;

		if (bodyDefinition.friendActorId.isValid())
		{
			fixtureDef.filter.groupIndex = this->getOrCreateActorFriendGroupIndex(bodyDefinition.friendActorId);
		}
		else
		{
			fixtureDef.filter.groupIndex = this->getMappedActorFriendGroupIndex(actor->getId());
		}

		this->addBodyShape(body, bodyDefinition.shape, fixtureDef);
	}

	actorData.mainBody = body;
	actorData.actor = actor;

	this->addBodyToDebugRender(body);
	this->syncDebugBodyRendering(body);

	if (body->IsActive() == false)
	{
		this->deactivateBody(body);
	}
}

int16 Box2DSimulation::getOrCreateActorFriendGroupIndex(const actor::Identifier& actorId)
{
	if (actorId.isValid() == false)
	{
		return 0;
	}

	const auto friendGroupIndexIt = this->friendGroupIndexMap.find(actorId);

	if (friendGroupIndexIt != this->friendGroupIndexMap.end())
	{
		return friendGroupIndexIt->second;
	}
	else
	{
		const auto friendGroupIndex = this->friendGroupIndexBooker.book();

		this->friendGroupIndexMap[actorId] = friendGroupIndex;
		this->applyActorGroupIndex(actorId, friendGroupIndex);

		return friendGroupIndex;
	}
}

int16 Box2DSimulation::getMappedActorFriendGroupIndex(const actor::Identifier& actorId)
{
	if (actorId.isValid() == false)
	{
		return 0;
	}

	const auto friendGroupIndexIt = this->friendGroupIndexMap.find(actorId);

	if (friendGroupIndexIt != this->friendGroupIndexMap.end())
	{
		return friendGroupIndexIt->second;
	}
	else
	{
		return 0;
	}
}

void Box2DSimulation::applyActorGroupIndex(const actor::Identifier& actorId, const int16 groupIndex)
{
	this->friendGroupIndexMap[actorId] = groupIndex;

	auto body = this->findBody(actorId);

	if (body != nullptr)
	{
		for (b2Fixture* fixture = body->GetFixtureList(); fixture; fixture = fixture->GetNext())
		{
			b2Filter collisionFilter = fixture->GetFilterData();
			collisionFilter.groupIndex = groupIndex;
			fixture->SetFilterData(collisionFilter);
		}
	}
}

void Box2DSimulation::removeActorBody(const actor::Identifier& actor)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actor);

	if (body != nullptr)
	{
		this->removeBody(body);
		this->actorMap.erase(actor);

		auto groupIndex = this->getMappedActorFriendGroupIndex(actor);
		if (groupIndex != 0)
		{
			this->friendGroupIndexBooker.unbook(groupIndex);
		}

		this->friendGroupIndexMap.erase(actor);
	}
}

void Box2DSimulation::setActorCollisionCallback(const actor::Identifier& actor, CollisionCallback callBack)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	const b2Body* actorBody = this->findBody(actor);

	if (actorBody != nullptr)
	{
		this->contactHandler.setCollisionCallback(actorBody, callBack);
	}
}

void Box2DSimulation::removeActorCollisionCallback(const actor::Identifier& actor)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	const b2Body* actorBody = this->findBody(actor);

	if (actorBody != nullptr)
	{
		this->contactHandler.removeCollisionCallback(actorBody);
	}
}


void Box2DSimulation::setFixedRotation(const actor::Identifier& actor, bool status)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actor);
	if (body != nullptr)
	{
		std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);

		body->SetFixedRotation(status);
	}
	else
	{
		this->log.warning().raw("Box2DPhysics::setFixedRotation: Body was not found.");
	}
}

void Box2DSimulation::setRestitution(const actor::Identifier& actorId, const float actorRestitution)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actorId);

	if (body != nullptr)
	{
		for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
		{
			fixture->SetRestitution(actorRestitution);
		}
	}
}

Box2DSimulation::BodyQueryCallback::BodyQueryCallback(const b2Vec2& point)
{
	this->point = point;
	this->fixture = nullptr;
}

bool Box2DSimulation::BodyQueryCallback::ReportFixture(b2Fixture* fixture)
{
	b2Body* body = fixture->GetBody();


	auto bodtItr = std::find(this->bodiesIgnored.begin(), this->bodiesIgnored.end(), body);
	if (bodtItr == this->bodiesIgnored.end())
	{
		bool inside = fixture->TestPoint(this->point);
		if (inside == true)
		{
			this->fixture = fixture;
			return false;
		}
	}

	return true;
}

Box2DSimulation::DynamicBodyQueryCallback::DynamicBodyQueryCallback(const b2Vec2& point)
{
	this->point = point;
	this->fixture = nullptr;
}

bool Box2DSimulation::DynamicBodyQueryCallback::ReportFixture(b2Fixture* fixture)
{
	b2Body* body = fixture->GetBody();
	if (body->GetType() == b2_dynamicBody)
	{
		auto bodtItr = std::find(this->bodiesIgnored.begin(), this->bodiesIgnored.end(), body);

		if (bodtItr == this->bodiesIgnored.end())
		{
			bool inside = fixture->TestPoint(this->point);
			if (inside == true)
			{
				this->fixture = fixture;
				return false;
			}
		}
	}
	return true;
}

Box2DSimulation::StaticBodyQueryCallback::StaticBodyQueryCallback(const b2Vec2& point)
{
	this->point = point;
	this->fixture = nullptr;
}

bool Box2DSimulation::StaticBodyQueryCallback::ReportFixture(b2Fixture* fixture)
{
	b2Body* body = fixture->GetBody();
	if (body->GetType() == b2_staticBody)
	{
		auto bodtItr = std::find(this->bodiesIgnored.begin(), this->bodiesIgnored.end(), body);

		if (bodtItr == this->bodiesIgnored.end())
		{
			bool inside = fixture->TestPoint(this->point);
			if (inside == true)
			{
				this->fixture = fixture;
				return false;
			}
		}
	}
	return true;
}

Box2DSimulation::BodyOverlapsFixtureQuery::BodyOverlapsFixtureQuery(b2Body* body)
{
	this->overlapping = false;

	for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		this->fixtures.push_back(fixture);
	}
}

bool Box2DSimulation::BodyOverlapsFixtureQuery::ReportFixture(b2Fixture* fixture)
{
	unsigned int fixtureNumber = 0;
	while (this->overlapping == false && fixtureNumber < this->fixtures.size())
	{
		if (this->fixtures[fixtureNumber] != fixture)
		{
			b2Shape* shape = fixtures[fixtureNumber]->GetShape();
			b2Shape* otherShape = fixture->GetShape();
			const b2Transform& transfrom = fixtures[fixtureNumber]->GetBody()->GetTransform();
			const b2Transform& otherTransfrom = fixture->GetBody()->GetTransform();

			this->overlapping = b2TestOverlap(shape, 0, otherShape, 0, transfrom, otherTransfrom);
		}
		fixtureNumber++;
	}
	const bool continueQuery = !this->overlapping;

	return continueQuery;
}

Box2DSimulation::BodiesOverlappingBodyQuery::BodiesOverlappingBodyQuery(b2Body* body)
{
	this->overlapping = false;

	for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		this->fixtures.push_back(fixture);
	}
}

bool Box2DSimulation::BodiesOverlappingBodyQuery::ReportFixture(b2Fixture* fixture)
{
	unsigned int fixtureNumber = 0;
	while (this->overlapping == false && fixtureNumber < this->fixtures.size())
	{
		if (this->fixtures[fixtureNumber] != fixture)
		{
			b2Shape* shape = fixtures[fixtureNumber]->GetShape();
			b2Shape* otherShape = fixture->GetShape();
			const b2Transform& transfrom = fixtures[fixtureNumber]->GetBody()->GetTransform();
			const b2Transform& otherTransfrom = fixture->GetBody()->GetTransform();

			this->overlapping = b2TestOverlap(shape, 0, otherShape, 0, transfrom, otherTransfrom);

			if (this->overlapping == true)
			{
				this->overlappingBodies.insert(fixture->GetBody());
			}
		}
		fixtureNumber++;
	}

	return true;
}

Box2DSimulation::PointInsidePolygonCallBack::PointInsidePolygonCallBack(const b2Vec2& point):
	actorsIgnored(nullptr)
{
	this->point = point;
	this->isInside = false;
	this->physics = nullptr;
	this->massOfObject = 0.0f;
}

bool Box2DSimulation::PointInsidePolygonCallBack::ReportFixture(b2Fixture *fixture)
{
	bool inside = fixture->TestPoint(this->point);
	if (inside)
	{
		this->isInside = true;

		if (this->actorsIgnored != nullptr)
		{
			const actor::Identifier actorHit = this->physics->getActorId(fixture->GetBody());

			if (actorHit != actor::Identifier() && this->actorsIgnored->find(actorHit) != this->actorsIgnored->end())
			{
				this->isInside = false;
			}
		}

		if (this->isInside == true)
		{
			this->massOfObject = fixture->GetBody()->GetMass();
		}

		return false;
	}

	return true;
}


float32 LightRayCastCallBack::ReportFixture(b2Fixture* fixture, const b2Vec2& /*point*/, const b2Vec2& /*normal*/, float32 fraction)
{
	if (this->currenRayTarget->isMin == true || this->currenRayTarget->isMax == true)
	{
		if (this->currenRayTarget->fixture == fixture)
		{
			return -1;
		}
	}

	if (fixture->IsSensor() == true)
	{
		return -1;
	}

	b2Body* hitBody = fixture->GetBody();
	if (this->physics->shouldIgnoreBodyFromLight(hitBody) == true)
	{
		return -1;
	}

	this->closestFraction = fraction;
	return fraction;
}

float32 Box2DSimulation::LineIntersection::ReportFixture(b2Fixture* /*fixture*/,
                                                      const b2Vec2& /*point*/,
                                                      const b2Vec2& /*normal*/,
                                                      float32 /*fraction*/)
{
	this->intersectionFound = true;
	return 0;
}


float32 Box2DSimulation::LineIntersectionTotalMass::ReportFixture(b2Fixture* fixture,
							       const b2Vec2& /*point*/,
							       const b2Vec2& /*normal*/,
							       float32 /*fraction*/)
{
	b2Body* hitBody = fixture->GetBody();

	bool hit = true;

	if (this->actorsIgnored != nullptr)
	{
		const actor::Identifier actorHit = this->physics->getActorId(fixture->GetBody());

		if (actorHit != actor::Identifier() && this->actorsIgnored->find(actorHit) != this->actorsIgnored->end())
		{
			hit = false;
		}
	}

	if (hit == true)
	{
		this->totalMass += hitBody->GetMass();
	}

	return 1.0f;
}


float32 Box2DSimulation::LineIntersectionStatic::ReportFixture(b2Fixture* fixture,
							    const b2Vec2& /*point*/,
							    const b2Vec2& /*normal*/,
							    float32 /*fraction*/)
{
	if (fixture->GetBody()->GetType() != b2_staticBody)
	{
		return -1;
	}

	this->intersectionFound = true;
	return 0;
}

Box2DSimulation::ContactHandler::ContactHandler():
	physics(nullptr),
	physicsWorld(nullptr),
	unbookId(0),
	unbook(false)
{
}

void Box2DSimulation::ContactHandler::init(const Box2DSimulation* physics, b2World* physicsWorld)
{
	this->physics = physics;
	this->physicsWorld = physicsWorld;
}

void Box2DSimulation::ContactHandler::BeginContact(b2Contact* contact)
{
	const b2Fixture* fixtureA = contact->GetFixtureA();
	const b2Fixture* fixtureB = contact->GetFixtureB();

	const b2Body* bodyA = fixtureA->GetBody();
	const b2Body* bodyB = fixtureB->GetBody();

	const auto fixtureDataA = static_cast<FixtureData*>(fixtureA->GetUserData());
	const auto fixtureDataB = static_cast<FixtureData*>(fixtureB->GetUserData());

	const auto sensorIdA = fixtureDataA->sensorCallbackId;
	const auto sensorIdB = fixtureDataB->sensorCallbackId;

	unsigned int contactId = booker.book(contact);
	this->contactMap[contact] = contactId;

	b2WorldManifold worldManifold;
	contact->GetWorldManifold(&worldManifold);

	auto sensorIt = this->sensorCallBackMap.find(sensorIdA);
	if (sensorIt != this->sensorCallBackMap.end())
	{
		SensorTrigger trigger;
		trigger.callback = sensorIt->second;
		trigger.sensorActorId = this->physics->getActorId(bodyB);
		trigger.gainedContact = true;
		trigger.contactId = contactId;
		trigger.contactPoint = math::convertVec2<glm::vec2>(worldManifold.points[0]);
		trigger.sensorId = sensorIdA;
		this->triggeredSensors.push_back(trigger);
	}

	sensorIt = this->sensorCallBackMap.find(sensorIdB);
	if (sensorIt != this->sensorCallBackMap.end())
	{
		SensorTrigger trigger;
		trigger.callback = sensorIt->second;
		trigger.sensorActorId = this->physics->getActorId(bodyA);
		trigger.gainedContact = true;
		trigger.contactId = contactId;
		trigger.contactPoint = math::convertVec2<glm::vec2>(worldManifold.points[0]);
		trigger.sensorId = sensorIdB;
		this->triggeredSensors.push_back(trigger);
	}
}

void Box2DSimulation::ContactHandler::EndContact(b2Contact* contact) {

	const b2Fixture* fixtureA = contact->GetFixtureA();
	const b2Fixture* fixtureB = contact->GetFixtureB();

	const b2Body* bodyA = fixtureA->GetBody();
	const b2Body* bodyB = fixtureB->GetBody();

	const auto fixtureDataA = static_cast<FixtureData*>(fixtureA->GetUserData());
	const auto fixtureDataB = static_cast<FixtureData*>(fixtureB->GetUserData());

	const auto sensorIdA = fixtureDataA->sensorCallbackId;
	const auto sensorIdB = fixtureDataB->sensorCallbackId;

	unsigned int contactId = this->contactMap[contact];

	b2WorldManifold worldManifold;
	contact->GetWorldManifold(&worldManifold);

	auto sensorIt = this->sensorCallBackMap.find(sensorIdA);
	if (sensorIt != this->sensorCallBackMap.end())
	{
		SensorTrigger trigger;
		trigger.callback = sensorIt->second;
		trigger.sensorActorId = this->physics->getActorId(bodyB);
		trigger.gainedContact = false;
		trigger.contactId = contactId;
		trigger.contactPoint = math::convertVec2<glm::vec2>(worldManifold.points[0]);
		trigger.sensorId = sensorIdA;
		this->triggeredSensors.push_back(trigger);
	}

	sensorIt = this->sensorCallBackMap.find(sensorIdB);
	if (sensorIt != this->sensorCallBackMap.end())
	{
		SensorTrigger trigger;
		trigger.callback = sensorIt->second;
		trigger.sensorActorId = this->physics->getActorId(bodyA);
		trigger.gainedContact = false;
		trigger.contactId = contactId;
		trigger.contactPoint = math::convertVec2<glm::vec2>(worldManifold.points[0]);
		trigger.sensorId = sensorIdB;
		this->triggeredSensors.push_back(trigger);
	}
	this->unbook = true;
	this->unbookId = contactId;
}

void Box2DSimulation::ContactHandler::PreSolve(b2Contact* contact, const b2Manifold* /*oldManifold*/)
{
	if (contact->IsEnabled())
	{
		auto bodyA = contact->GetFixtureA()->GetBody();
		auto bodyB = contact->GetFixtureB()->GetBody();

		std::array<b2Body*, 2> bodies{{bodyA, bodyB}};

		for (std::size_t i = 0; i < bodies.size(); ++i)
		{
			auto body = bodies[i];

			auto callbackIt = this->collisionCallbackMap.find(body);
			if (callbackIt != this->collisionCallbackMap.end())
			{
				auto otherBody = bodies[(i + 1) % bodies.size()];

				BodyData* bodyData = static_cast<BodyData*>(otherBody->GetUserData());
				auto actorId = bodyData->actor->getId();

				Collision collision;
				collision.collisionCallback = callbackIt->second;
				collision.collisionBody = body;
				collision.collisionData.collidingActorId = actorId;
				collision.collisionData.positionAtCollision = math::convertVec2<glm::vec2>(body->GetPosition());

				this->collisions.push_back(collision);
			}
		}
	}
}

void Box2DSimulation::ContactHandler::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
	b2Body* bodyA = contact->GetFixtureA()->GetBody();
	b2Body* bodyB = contact->GetFixtureB()->GetBody();

	const b2Manifold* contacManifold = contact->GetManifold();

	b2WorldManifold worldManifold;
	contact->GetWorldManifold(&worldManifold);

	auto fixtureIt = this->stickyBodiesMap.find(bodyA);
	if (fixtureIt != this->stickyBodiesMap.end())
	{
		float impulseToStick = fixtureIt->second;
		impulseToStick *= bodyA->GetMass();

		float totalImpulseValue = 0.0f;
		for (int i = 0; i < impulse->count; i++)
		{
			totalImpulseValue += impulse->normalImpulses[i];
		}

		if (totalImpulseValue > impulseToStick)
		{
			this->stickyJointsToCreate.push_back(BodiesToStick{bodyA,bodyB});
		}
	}

	fixtureIt = this->stickyBodiesMap.find(bodyB);
	if (fixtureIt != this->stickyBodiesMap.end())
	{
		float impulseToStick = fixtureIt->second;
		impulseToStick *= bodyB->GetMass();

		float totalImpulseValue = 0.0f;
		for (int i = 0; i < impulse->count; i++)
		{
			totalImpulseValue += impulse->normalImpulses[i];
		}

		if (totalImpulseValue > impulseToStick)
		{
			this->stickyJointsToCreate.push_back(BodiesToStick{bodyB,bodyA});
		}
	}

	std::array<b2Body*, 2> bodies{{bodyA, bodyB}};
	const auto contactNormal = math::convertVec2<glm::vec2>(worldManifold.normal);

	for (std::size_t i = 0; i < bodies.size(); ++i)
	{
		auto body = bodies[i];

		auto collisionIt = std::find_if(
			this->collisions.begin(),
			this->collisions.end(),
			[body](const Collision& collision)
			{
				return collision.collisionBody == body;
			});

		if (collisionIt != this->collisions.end())
		{
			float totalImpulseValue = 0.0f;
			for (int i = 0; i < impulse->count; i++)
			{
				totalImpulseValue += impulse->normalImpulses[i];
			}

			Collision& collision = *collisionIt;
			collision.collisionData.collisionImpulse = totalImpulseValue;
			collision.collisionData.collisionPoint = math::convertVec2<glm::vec2>(worldManifold.points[0]);

			/*
			 * The normal goes from Body A to Body B, so the normal have to be inverted
			 * to be correct for Body A as we want it to always point toward itself.
			 */
			if (i == 0)
			{
				collision.collisionData.collisionSurfaceNormal = -contactNormal;
			}
			else
			{
				collision.collisionData.collisionSurfaceNormal = contactNormal;
			}

			if (contacManifold->pointCount == 2)
			{
				collision.collisionData.collisionPoint += (math::convertVec2<glm::vec2>(worldManifold.points[1]) - math::convertVec2<glm::vec2>(worldManifold.points[0])) / 2.0f;
			}
		}
	}
}

void Box2DSimulation::ContactHandler::addStickyBody(const b2Body* body, const float forceNeededToStick)
{
	std::lock_guard<std::mutex>(this->callbackMutex);
	this->stickyBodiesMap[body] = forceNeededToStick;
}

void Box2DSimulation::ContactHandler::removeStickyBody(const b2Body* body)
{
	std::lock_guard<std::mutex>(this->callbackMutex);
	this->stickyBodiesMap.erase(body);

	util::removeFastIf(
		this->stickyJointsToCreate,
		[body](const BodiesToStick& stickPair)
		{
			return stickPair.body1 == body || stickPair.body2 == body;
		});
}

void Box2DSimulation::ContactHandler::setCollisionCallback(const b2Body* body, const CollisionCallback& callback)
{
	std::lock_guard<std::mutex>(this->callbackMutex);
	this->collisionCallbackMap[body] = callback;
}

void Box2DSimulation::ContactHandler::removeCollisionCallback(const b2Body* body)
{
	std::lock_guard<std::mutex>(this->callbackMutex);
	this->collisionCallbackMap.erase(body);

	util::removeFastIf(
		this->collisions,
		[body](const Collision& collision)
		{
			return collision.collisionBody == body;
		});
}

void Box2DSimulation::ContactHandler::setSensorCallback(const int sensorId, const SensorCallback& callback)
{
	std::lock_guard<std::mutex>(this->callbackMutex);
	this->sensorCallBackMap[sensorId] = callback;
}

void Box2DSimulation::ContactHandler::removeSensorCallback(const int sensorId)
{
	std::lock_guard<std::mutex>(this->callbackMutex);
	this->sensorCallBackMap.erase(sensorId);

	util::removeFastIf(
		this->triggeredSensors,
		[sensorId](const SensorTrigger& trigger)
		{
			return trigger.sensorId == sensorId;
		});
}

void Box2DSimulation::ContactHandler::handleContactTasks()
{
	std::lock_guard<std::mutex>(this->callbackMutex);

	for (const BodiesToStick& bodies : this->stickyJointsToCreate)
	{
		b2Vec2 worldCoordsAnchorPoint = bodies.body1->GetWorldPoint( b2Vec2(0.6f, 0) );

		b2WeldJointDef weldJointDef;
		weldJointDef.bodyA = bodies.body2;
		weldJointDef.bodyB = bodies.body1;
		weldJointDef.localAnchorA = weldJointDef.bodyA->GetLocalPoint(worldCoordsAnchorPoint);
		weldJointDef.localAnchorB = weldJointDef.bodyB->GetLocalPoint(worldCoordsAnchorPoint);
		weldJointDef.referenceAngle = weldJointDef.bodyB->GetAngle() - weldJointDef.bodyA->GetAngle();

		this->physicsWorld->CreateJoint(&weldJointDef);
	}

	for (const Collision& collision : this->collisions)
	{
		collision.collisionCallback(collision.collisionData);
	}

	for (const SensorTrigger& trigger : this->triggeredSensors)
	{
		trigger.callback(trigger.gainedContact, trigger.sensorActorId, trigger.contactId, trigger.contactPoint);
	}

	if (this->unbook == true)
	{
		this->booker.unbook(this->unbookId);
		this->unbook = false;
	}

	this->stickyJointsToCreate.clear();
	this->collisions.clear();
	this->triggeredSensors.clear();
}

void Box2DSimulation::ContactHandler::clear()
{
	std::lock_guard<std::mutex> lock(this->callbackMutex);

	this->collisionCallbackMap.clear();
	this->sensorCallBackMap.clear();
	this->stickyBodiesMap.clear();

	this->collisions.clear();
	this->triggeredSensors.clear();
	this->stickyJointsToCreate.clear();
}

void Box2DSimulation::setSticky(const actor::Identifier& actor, float stickiness)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actor);

	if (body != nullptr)
	{
		float forceNeededToStick = (stickiness - 1.0f) * -1.0f;
		forceNeededToStick *= 30.0f;

		this->contactHandler.addStickyBody(body, forceNeededToStick);
	}
}

void Box2DSimulation::multithreadedLightCalculation()
{
	while (!this->quitThreads)
	{
		bool empty = false;

		this->lightAreaProcessingBarrier->wait();

		while (!empty)
		{
			std::shared_ptr<app::graphics::Light> light;

			std::unique_lock<std::mutex> lightLock(this->lightQueueMutex);

			if (this->lightQueue.empty() == false)
			{
				light = this->lightQueue.front();
				lightQueue.pop();
			}
			else
			{
				empty = true;
			}

			lightLock.unlock();

			if (empty == false)
			{
				math::Circle lightCircle;
				lightCircle.radius = light->getRange();
				lightCircle.position = light->getPosition();

				auto shadowCastingBodyIt = this->dynamicShadowcastingBodies.begin();

				while (light->needsGeometryUpdate() == false && shadowCastingBodyIt != this->dynamicShadowcastingBodies.end())
				{
					auto body = *shadowCastingBodyIt;
					const auto bodyData = static_cast<BodyData*>(body->GetUserData());

					if (bodyData != nullptr)
					{
						if (isEqual(body->GetPosition(), bodyData->previousPosition) == false || body->GetAngle() != bodyData->previousAngle)
						{
							auto fixture = body->GetFixtureList();

							while (light->needsGeometryUpdate() == false && fixture != nullptr)
							{
								math::Box<glm::vec2> fixtureBox;
								const b2AABB& fixtureAabb = fixture->GetAABB(0);
								fixtureBox.lowerBound = math::convertVec2<glm::vec2>(fixtureAabb.lowerBound);
								fixtureBox.upperBound = math::convertVec2<glm::vec2>(fixtureAabb.upperBound);

								// TODO: This can be improved by doing a proper circle/aabb check and
								// doing a cone/aabb check for cone lights.
								if (math::simpleCircleAabbRangeCheck(lightCircle, fixtureBox))
								{
									light->setGeometryOutdated();
								}

								fixture = fixture->GetNext();
							}
						}
					}

					++shadowCastingBodyIt;
				}

				if (light->needsGeometryUpdate())
				{
					this->calculateVisibility(light);
					light->setGeometryUpdated();
				}
			}
		}

		this->lightAreaInsertBarrier->wait();
	}
}


void Box2DSimulation::calculateVisibility(std::shared_ptr<app::graphics::Light>& light)
{
#ifdef TIME_LIGHT_CALCULATION
	auto startTime = std::chrono::high_resolution_clock::now();
#endif

	const b2Vec2 lightCenter = b2Vec2(light->getPosition().x, light->getPosition().y);
	std::list<PolygonVertex> rayTargets;

	float minAngle = light->getCastDirection() - light->coneAngleRadian / 2;
	float maxAngle = light->getCastDirection() + light->coneAngleRadian / 2;

	AabbFixtureQuery fixtureQuery;
	b2AABB lightRangeAabb;
	lightRangeAabb.lowerBound.Set(light->getPosition().x - light->getRange(), light->getPosition().y - light->getRange());
	lightRangeAabb.upperBound.Set(light->getPosition().x + light->getRange(), light->getPosition().y + light->getRange());

	this->world->QueryAABB(&fixtureQuery, lightRangeAabb);

	for (const b2Fixture* fixture : fixtureQuery.fixturesWithin)
	{
		const b2Body* fixtureBody = fixture->GetBody();

		if (fixture->IsSensor() == false && this->shouldIgnoreBodyFromLight(fixtureBody) == false)
		{
			//If the light is under a body that is not on the ignore list, there is no light.
			if (fixture->TestPoint(lightCenter))
			{
				light->lightArea.clear();
				return;
			}

			const b2Shape::Type shapeType = fixture->GetType();

			if (shapeType == b2Shape::e_circle)
			{
				//FIXME: DO NOTHING FOR CIRCLES ATM.
			}
			else if (shapeType == b2Shape::e_polygon)
			{
				auto polygonShape = static_cast<const b2PolygonShape*>(fixture->GetShape());
				const int numberOfVertices = polygonShape->GetVertexCount();

				PolygonVertex fixtureVertices[b2_maxPolygonVertices];
				size_t vertexIndex = 0;
				bool fixtureHasVertexInRange = false;

				for (int i = 0; i < numberOfVertices; i++)
				{
					const b2Vec2 position = fixtureBody->GetWorldPoint(polygonShape->GetVertex(i));
					const b2Vec2 distance = position - lightCenter;

					if (distance.Length() < light->getRange() + this->rayLengthExtra)
					{
						fixtureHasVertexInRange = true;
					}

					float angle = glm::atan(position.y - lightCenter.y, position.x - lightCenter.x);

					if (light->isRadialLight)
					{
						angle = math::clampAnglePositive(angle);
					}

					fixtureVertices[vertexIndex].angle = angle;
					fixtureVertices[vertexIndex].position = position;
					fixtureVertices[vertexIndex].fixture = fixture;
					fixtureVertices[vertexIndex].fraction = distance.Length() / light->getRange();

					vertexIndex++;
				}

				const size_t numVertices = vertexIndex;

				if (fixtureHasVertexInRange == true && numVertices > 2)
				{
					unsigned int minAxisIndex = 0;
					unsigned int maxAxisIndex = 0;
					float maxDifference = 0.0f;
					unsigned int indexOne = 0;
					unsigned int indexTwo = 0;

					b2Vec2 axisOne = fixtureBody->GetWorldPoint(polygonShape->m_centroid) - lightCenter;
					axisOne.Normalize();
					axisOne.x = axisOne.y;
					axisOne.y = -axisOne.x;

					/*
					 *  We find the minimum and maximum visible on each fixture.
					 *  this is used to cast a ray beyond the point as well as one to it.
					 */
					for (unsigned int i = 0; i < numVertices; i++)
					{
						for (unsigned int j = 0; j < numVertices; j++)
						{
							if (j != i)
							{
								const float difference = glm::abs(math::calculateAngleDifference(fixtureVertices[i].angle, fixtureVertices[j].angle));

								if (difference > maxDifference)
								{
									maxDifference = difference;
									indexOne = i;
									indexTwo = j;
								}
							}
						}
					}

					if (math::calculateAngleDifference(fixtureVertices[indexOne].angle, fixtureVertices[indexTwo].angle) < 0)
					{
						maxAxisIndex = indexOne;
						minAxisIndex = indexTwo;
					}
					else
					{
						minAxisIndex = indexOne;
						maxAxisIndex = indexTwo;
					}

					fixtureVertices[minAxisIndex].isMin = true;
					glm::vec2 minPosition(fixtureVertices[minAxisIndex].position.x, fixtureVertices[minAxisIndex].position.y);
					glm::vec2 minRotatedTarget(math::rotateVectorAroundPoint(minPosition, light->getPosition(), -SMALL_ANGLE));
					fixtureVertices[minAxisIndex].position = b2Vec2(minRotatedTarget.x, minRotatedTarget.y);

					fixtureVertices[minAxisIndex].angle -= SMALL_ANGLE;

					fixtureVertices[maxAxisIndex].isMax = true;
					glm::vec2 maxPosition(fixtureVertices[maxAxisIndex].position.x,fixtureVertices[maxAxisIndex].position.y);
					glm::vec2 maxRotatedTarget(math::rotateVectorAroundPoint(maxPosition, light->getPosition(), SMALL_ANGLE));
					fixtureVertices[maxAxisIndex].position = b2Vec2(maxRotatedTarget.x, maxRotatedTarget.y);
					fixtureVertices[maxAxisIndex].angle += SMALL_ANGLE;

					for (size_t i = 0; i < numVertices; i++)
					{
						rayTargets.push_back(fixtureVertices[i]);
					}
				}
			}
		}
	}

	/*
	 *  If the light is a radial light. (360 degrees) we add a ray at some interval
	 *  to make sure we cast enough rays to simulate a circle.
	 */
	if (light->isRadialLight)
	{
		for (int i = 0; i < this->rayMinimumNumber; i++)
		{
			const auto angle = math::clampAnglePositive(rayTheta * static_cast<float>(i));

			const b2Vec2 position = b2Vec2(
					glm::cos(angle) * (light->getRange() + this->rayLengthExtra) + lightCenter.x,
					glm::sin(angle) * (light->getRange() + this->rayLengthExtra) + lightCenter.y);

			rayTargets.push_back(PolygonVertex(angle, position, false, false, nullptr, 1.0f));
		}
	}
	else    // THIS IS A CONE-LIGHT
	{
		// Problem with the cone light? This originally used a function called "setAngleBetween". See the git log.
		minAngle = math::clampAngle(minAngle);
		maxAngle = math::clampAngle(maxAngle);

		/*
		 *  for the case where the angle maxAngle is lower than the min anlge, (when looking in the -PI direction
		 *  we need have a special case when removing the targets that are not valid.
		 */
		if (maxAngle < minAngle)
		{
			/*
			 *  First we the angles that are between the min and PI.
			 */
			float currentAngle = minAngle;
			while (currentAngle < glm::pi<float>())
			{
				const b2Vec2 position = b2Vec2(
						glm::cos(currentAngle) * (light->getRange() + this->rayLengthExtra) + lightCenter.x,
						glm::sin(currentAngle) * (light->getRange() + this->rayLengthExtra) + lightCenter.y);

				rayTargets.push_back(PolygonVertex(currentAngle, position, false, false, nullptr, 1.0f));

				currentAngle += rayTheta * (light->coneAngleRadian / (2 * glm::pi<float>()));
			}

			/*
			 *  Then we add the angles that are between -PI and maxAngle.
			 */
			currentAngle = -glm::pi<float>();
			while (currentAngle < maxAngle)
			{
				b2Vec2 position = b2Vec2(glm::cos(currentAngle) * (light->getRange() + this->rayLengthExtra),
							glm::sin(currentAngle)  * (light->getRange() + this->rayLengthExtra)) + lightCenter;
				rayTargets.push_back(PolygonVertex(currentAngle, position, false, false, nullptr, 1.0f));

				currentAngle += rayTheta * (light->coneAngleRadian/(2.0f * glm::pi<float>()));
			}

			/*
			 *  We then iterate through all the targets and remove all of those that are not in the conelight's range.
			 */
			auto it = rayTargets.begin();
			while (it != rayTargets.end())
			{
				if (it->angle > maxAngle && it->angle < minAngle)
				{
					it = rayTargets.erase(it);
					continue;
				}

				if (it->angle < maxAngle)   //  For sorting purposes (rendering order) we need to sort them
				{                           //  to make sure the max rays are after the min rays we simply
					it->angle += 2.0f * glm::pi<float>();  //  add a revolution to the ones below.
				}

				it++;
			}

			/*
			 *  We add the min and max angle as targets.
			 */
			maxAngle += 2.0f * glm::pi<float>();
			b2Vec2 position = b2Vec2(glm::cos(minAngle) * (light->getRange() + this->rayLengthExtra), glm::sin(minAngle) * (light->getRange() + this->rayLengthExtra)) + lightCenter;
			rayTargets.push_back(PolygonVertex(minAngle, position, false, false, nullptr, 1.0f));

			position = b2Vec2(glm::cos(maxAngle) * (light->getRange() + this->rayLengthExtra), glm::sin(maxAngle) * (light->getRange() + this->rayLengthExtra)) + lightCenter;
			rayTargets.push_back(PolygonVertex(maxAngle, position, false, false, nullptr, 1.0f));
		}
		else    //This is the non special case.
		{
			auto it = rayTargets.begin();   // We remove any targets that are not in range.
			while (it != rayTargets.end())
			{
				if (it->angle < minAngle || it->angle > maxAngle)
				{
					it = rayTargets.erase(it);
					continue;
				}

				it++;
			}

			/*
			 *  We add a number of rays to make sure we simulate a cone.
			 */
			float currentAngle = minAngle;
			while (currentAngle < maxAngle)
			{
				b2Vec2 position = b2Vec2(glm::cos(currentAngle) * (light->getRange() + this->rayLengthExtra), glm::sin(currentAngle) * (light->getRange() + this->rayLengthExtra)) + lightCenter;
				rayTargets.push_back(PolygonVertex { currentAngle, position, false, false, nullptr, 1.0f });
				currentAngle += rayTheta * (light->coneAngleRadian / (2.0f * glm::pi<float>()));
			}

			/*
			 *  We add the min and max angle as targets.
			 */
			b2Vec2 position = b2Vec2(glm::cos(minAngle) * (light->getRange() + this->rayLengthExtra), glm::sin(minAngle) * (light->getRange() + this->rayLengthExtra)) + lightCenter;
			rayTargets.push_back(PolygonVertex(minAngle, position, false, false, nullptr, 1.0f));

			position = b2Vec2(glm::cos(maxAngle) * (light->getRange() + this->rayLengthExtra), glm::sin(maxAngle) * (light->getRange() + this->rayLengthExtra)) + lightCenter;
			rayTargets.push_back(PolygonVertex(maxAngle, position, false, false, nullptr, 1.0f));
		}
	}


	/*
	*  For rendering purposes we sort the targets based on the angles.
	*/
	rayTargets.sort([](const PolygonVertex & a, const PolygonVertex & b)
		      {
			  return a.angle < b.angle;
		      });

	LightRayCastCallBack rayCastCaller;    //The object that will be used to callback when something is hit.

	const float extraColorLenghtFraction = 0.00f;

	/*
	 *  We iterate through all the targets and cast a ray towards them.
	 */
	for (auto it = rayTargets.begin(); it != rayTargets.end(); it++)
	{
		b2RayCastInput input;
		input.p1 = lightCenter;
		input.p2 = it->position;

		b2Vec2 directionVector = input.p1 - input.p2;
		float length = directionVector.Normalize();

		if (length > 0.0f)
		{
			directionVector = light->getRange() * directionVector;

			input.p2 = input.p1 - directionVector;
			input.maxFraction = rayMaxFraction;

			rayCastCaller.physics = this;
			rayCastCaller.closestFraction = 1.0;
			rayCastCaller.currenRayTarget = &*it;

			this->world->RayCast(&rayCastCaller, input.p1, input.p2);


			float actualFraction = rayCastCaller.closestFraction;

			if (actualFraction < 1.0f)
			{
				if ((actualFraction + extraColorLenghtFraction) < 1.0f)
				{
					actualFraction = actualFraction + extraColorLenghtFraction;
				}
				else
				{
					actualFraction = 1.0f;
				}

				rayCastCaller.closestFraction = actualFraction;
			}



			b2Vec2 intersectionPoint(lightCenter + actualFraction * (input.p2 - lightCenter));
			it->position = intersectionPoint;

			/*
			 *  If the ray hit anything further away than the target we check for some special cases.
			 */
			if ((lightCenter - intersectionPoint).Length() / light->getRange() > it->fraction)
			{
				/*
				 *  If the target is a min (calculated for each fixture at the start of the function)
				 *  We add a ray that goes only to the min edge,
				 *  We do the same for max.
				 */
				if (it->isMin == true)
				{
					float oldFraction = it->fraction;
					it->fraction = rayCastCaller.closestFraction;
					b2Vec2 cornerPoint = lightCenter + oldFraction * (input.p2 - lightCenter);

					auto next = it;		//std::next does not work in vs 2013...?
					next++;
					it = rayTargets.insert(next, PolygonVertex(it->angle, cornerPoint, false, false, it->fixture, oldFraction));
				}
				else if (it->isMax == true)
				{
					float oldFraction = it->fraction;
					it->fraction = rayCastCaller.closestFraction;
					b2Vec2 cornerPoint = lightCenter + oldFraction * (input.p2 - lightCenter);

					it = rayTargets.insert(it, PolygonVertex(it->angle, cornerPoint, false, false, it->fixture, oldFraction));
					it++;
				}
				else
				{
					it->fraction = actualFraction;
				}
			}
			else
			{
				it->fraction = rayCastCaller.closestFraction;
			}
		}
	}

	light->lightArea.clear();                           //We clear the previous lightAreas. THIS IS A MAJOR PERFORMANCE HIT.
							    // TODO make this more optimal.



	/*
	 *  We then move through all the rays and make areas that are safe for lightcasting.
	 *  We allso calculate the fallof on the strenght og light based on the length it was cast.
	 */
	for (auto it = rayTargets.begin(); it != rayTargets.end(); it++)
	{
		auto next = it;
		next++;
		if (next == rayTargets.end())
		{
			if (light->isRadialLight)
			{
				next = rayTargets.begin();
			}
			else
			{
				return;
			}
		}

		app::graphics::Triangle* triangle = light->lightArea.createTriangle();

		triangle->setVertexA(glm::vec2(lightCenter.x, lightCenter.y));
		triangle->setColorA(light->getColor());




		glm::vec4 endLight = light->getColor();
		float alphaSubtract = it->fraction * light->alphaFallOff;
		endLight.a -= alphaSubtract;

		triangle->setVertexB(glm::vec2(it->position.x, it->position.y));
		triangle->setColorB(endLight);




		endLight = light->getColor();
		alphaSubtract = next->fraction * light->alphaFallOff;
		endLight.a -= alphaSubtract;

		triangle->setVertexC(glm::vec2(next->position.x, next->position.y));
		triangle->setColorC(endLight);

		triangle->signalChange();
	}

#ifdef TIME_LIGHT_CALCULATION
	auto endTime = std::chrono::high_resolution_clock::now();

	std::lock_guard<std::mutex> timerLock(this->lightTimingCalculationMutex);
	this->lightCalculationTime += (endTime - startTime);
#endif
}

void Box2DSimulation::addLight(std::shared_ptr<app::graphics::Light> light)
{
	this->lights.push_back(light);
}

void Box2DSimulation::removeLight(std::shared_ptr<app::graphics::Light> light)
{
	auto it = std::find(this->lights.begin(), this->lights.end(), light);

	if (it != this->lights.end())
	{
		this->lights.erase(it);
	}
}

GravitationalForce* Box2DSimulation::createGravitationalForce()
{
	std::unique_ptr<GravitationalForce> force(new GravitationalForce());
	GravitationalForce* forcePtr = force.get();

	this->gravitationalForces.push_back(std::move(force));

	return forcePtr;
}

bool Box2DSimulation::removeGravitationalForce(const GravitationalForce* force)
{
	auto forceIt = std::find_if(
			this->gravitationalForces.begin(),
			this->gravitationalForces.end(),
			[force](const std::unique_ptr<GravitationalForce>& containedForce)
			{
				return (containedForce.get() == force);
			}
	);

	if (forceIt != this->gravitationalForces.end())
	{
		this->gravitationalForces.erase(forceIt);
		return true;
	}
	else
	{
		return false;
	}
}

void Box2DSimulation::setActorFriendGroup(const actor::Identifier& actorId, const actor::Identifier& actorToBefriend)
{
	assert(actorId.isValid());

	auto groupIndex = int16{0};

	if (actorToBefriend.isValid())
	{
		groupIndex = this->getOrCreateActorFriendGroupIndex(actorToBefriend);
	}

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	this->applyActorGroupIndex(actorId, groupIndex);
}

void Box2DSimulation::onEvent(const std::shared_ptr<event::Event> &event)
{
	if (event->isType(actor::TransformChange::ID))
	{
		auto transformEvent = static_cast<actor::TransformChange*>(event.get());

		const auto actor = transformEvent->getActor();
		assert(actor != nullptr);

		auto actorItr = this->actorMap.find(actor->getId());
		if (actorItr != this->actorMap.end())
		{
			this->setActorPosition(actor->getId(), transformEvent->getPosition());
			this->setActorRotation(actor->getId(), transformEvent->getRotation());

			b2Body* body = this->findBody(actor->getId());

			if (body != nullptr)
			{
				this->syncActorTransform(actor, body);
			}
		}
	}
	else if (event->isType(graphics::DebugRenderingEnabled::ID))
	{
		auto debugEnabled = static_cast<graphics::DebugRenderingEnabled*>(event.get());

		this->debugRenderingEnabled = debugEnabled->isTrue();
	}
}

void Box2DSimulation::applyForce(const actor::Identifier& actor, const glm::vec2& force)
{
	b2Vec2 b2Force(force.x, force.y);

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actor);
	if (body != nullptr)
	{
		std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
		body->ApplyForceToCenter(b2Force, true);
	}
	else
	{
		this->log.warning().raw("Box2DPhysics::applyForce: Body was not found (no force applied).");
	}
}

void Box2DSimulation::applyForce(const actor::Identifier& actor, const glm::vec2& force, const glm::vec2& position)
{
	const b2Vec2 b2Force(force.x, force.y);
	const b2Vec2 b2Position(position.x, position.y);

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actor);
	if (body != nullptr)
	{
		std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
		body->ApplyForce(b2Force, b2Position, true);
	}
	else
	{
		this->log.warning().raw("Box2DPhysics::applyForce: Body was not found (no force applied).");
	}
}

void Box2DSimulation::applyTorque(const actor::Identifier& actor, const float torque)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actor);
	if (body != nullptr)
	{
		std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
		body->ApplyTorque(torque, true);
	}
	else
	{
		this->log.warning().raw("Box2DPhysics::applyTorque: Body was not found (no torque applied).");
	}
}

void Box2DSimulation::applyImpulse(const actor::Identifier& actor, const glm::vec2& impulse)
{
	b2Vec2 b2Impulse(impulse.x, impulse.y);

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actor);
	if (body != nullptr)
	{
		std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
		body->ApplyLinearImpulse(b2Impulse, body->GetWorldCenter(), true);
	}
	else
	{
		this->log.warning().raw("Box2DPhysics::applyImpulse: Body was not found (no impulse applied).");
	}
}

void Box2DSimulation::syncDebugPolygonRendering(const b2Fixture* shapeFixture, app::graphics::Polygon* polygon, app::graphics::LineLoop* edge)
{
	const b2Body* shapeBody = shapeFixture->GetBody();
	auto poly = static_cast<const b2PolygonShape*>(shapeFixture->GetShape());
	const auto vertexCount = static_cast<unsigned int>(poly->GetVertexCount());

	for(unsigned int i = 0; i < vertexCount; i++)
	{
		const b2Vec2 currentVertexWorld = shapeBody->GetWorldPoint(poly->GetVertex(static_cast<int32>(i)));

		if (polygon != nullptr)
		{
			polygon->setVertex(i, glm::vec2(currentVertexWorld.x, currentVertexWorld.y));
		}

		if (edge != nullptr)
		{
			edge->setVertex(i, glm::vec2(currentVertexWorld.x, currentVertexWorld.y));
		}
	}

	if (polygon != nullptr)
	{
		polygon->setColor(this->getFixtureDebugColor(shapeFixture));
		polygon->signalChange();
	}

	if (edge != nullptr)
	{
		edge->signalChange();
	}
}

void Box2DSimulation::syncDebugCircleRendering(const b2Fixture* fixture, app::graphics::Polygon* polygon, app::graphics::LineLoop* edge, app::graphics::Line* angleLine)
{
	if (polygon != nullptr)
	{
		polygon->setColor(this->getFixtureDebugColor(fixture));
	}

	const b2Body* shapeBody = fixture->GetBody();
	auto circle = static_cast<const b2CircleShape*>(fixture->GetShape());

	const float32 radius = circle->m_radius;
	const b2Vec2 circlePosition = shapeBody->GetWorldPoint(circle->m_p);
	const glm::vec2 origin = glm::vec2(circlePosition.x, circlePosition.y);

	float x = 1.0f;
	float y = 0.0f;

	for(unsigned int i = 0; i < this->circleNumberOfSegments; i++)
	{
		const glm::vec2 point(origin.x + x * radius, origin.y + y * radius);

		if (polygon != nullptr)
		{
			polygon->setVertex(i, point);
		}

		if (edge != nullptr)
		{
			edge->setVertex(i, point);
		}

		const auto nextX = this->circleCosine * x - this->circleSine * y;
		const auto nextY = this->circleSine * x + this->circleCosine * y;

		x = nextX;
		y = nextY;
	}

	if (polygon != nullptr)
	{
		polygon->setColor(this->getFixtureDebugColor(fixture));
		polygon->signalChange();
	}

	if (edge != nullptr)
	{
		edge->signalChange();
	}

	if (angleLine != nullptr)
	{
		angleLine->setLineBegin(origin);

		const float angle = shapeBody->GetAngle();
		angleLine->setLineEnd(origin + (glm::vec2(glm::cos(angle), glm::sin(angle)) * radius));
	}
}

void Box2DSimulation::addBodyToDebugRender(const b2Body* body)
{
	for (const b2Fixture* fixture = body->GetFixtureList(); fixture; fixture = fixture->GetNext())
	{
		this->addFixtureToDebugRenderer(fixture);
	}

	if (body->GetType() == b2_dynamicBody)
	{
//		this->debugBodyMassCenter[body] = this->lineRenderSet->createLineLoop(this->circleNumberOfSegments, false);
	}
}

void Box2DSimulation::syncDebugRendering()
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	for (b2Body* body = this->world->GetBodyList(); body; body = body->GetNext())
	{
		this->syncDebugBodyRendering(body);
	}
}

void Box2DSimulation::syncDebugBodyRendering(b2Body* body)
{
	for (b2Fixture* fixture = body->GetFixtureList(); fixture; fixture = fixture->GetNext())
	{
		b2Shape* shape = fixture->GetShape();

		if (shape->GetType() == b2Shape::e_polygon)
		{
			app::graphics::Polygon* polygon = nullptr;
			app::graphics::LineLoop* edge = nullptr;

			auto polygonIt = this->debugPolygonMap.find(fixture);
			if (polygonIt != this->debugPolygonMap.end())
			{
				polygon = polygonIt->second;
			}

			auto edgesIt = this->debugShapeEdgeMap.find(fixture);
			if (edgesIt != this->debugShapeEdgeMap.end())
			{
				edge = edgesIt->second;
			}

			this->syncDebugPolygonRendering(fixture, polygon, edge);
		}
		else if (shape->GetType() == b2Shape::e_circle)
		{
			app::graphics::Polygon* polygon = nullptr;
			app::graphics::LineLoop* edge = nullptr;
			app::graphics::Line* line = nullptr;

			auto polygonIt = this->debugPolygonMap.find(fixture);
			if (polygonIt != this->debugPolygonMap.end())
			{
				polygon = polygonIt->second;
			}

			auto edgesIt = this->debugShapeEdgeMap.find(fixture);
			if (edgesIt != this->debugShapeEdgeMap.end())
			{
				edge = edgesIt->second;
			}

			auto lineIt = this->debugCircleLineMap.find(fixture);
			if (lineIt != this->debugCircleLineMap.end())
			{
				line = lineIt->second;
			}

			this->syncDebugCircleRendering(fixture, polygon, edge, line);
		}
	}

	if (body->GetType() == b2_dynamicBody)
	{
//		app::graphics::LineLoop* lineLoop = this->debugBodyMassCenter[body];
//
//		if (lineLoop != nullptr)
//		{
//
//			b2MassData massData;
//			body->GetMassData(&massData);
//
//			const glm::vec2 centerOfMass = math::convertVec2<glm::vec2>(massData.center);
//			const glm::vec2 origin = glm::rotate(centerOfMass, body->GetAngle()) + math::convertVec2<glm::vec2>(body->GetPosition());
//
//			const float32 radius = 0.1f;
//
//			float x = radius;
//			float y = 0;
//
//			for (unsigned int i = 0; i < this->circleNumberOfSegments; i++)
//			{
//				const glm::vec2 point(x + origin.x, y + origin.y);
//
//				lineLoop->setVertex(i, point);
//
//				x = this->circleCosine * x - this->circleSine * y;
//				y = this->circleSine * x + this->circleCosine * y;
//			}
//
//			lineLoop->setColor(glm::vec4(0.0f, 0.9f, 0.0f, 1.0f));
//			lineLoop->signalChange();
//		}
	}
}

glm::vec4 Box2DSimulation::getFixtureDebugColor(const b2Fixture* fixture) const
{
	auto body = fixture->GetBody();
	auto fixtureColor = getBodyDebugColor(body);

	if (fixture->IsSensor())
	{
		if (body->GetType() == b2_dynamicBody)
		{
			fixtureColor.r += 0.3f;
		}
		else if (body->GetType() == b2_kinematicBody)
		{
			fixtureColor.b += 0.3f;
		}
		else
		{
			fixtureColor.g += 0.3f;
		}
	}

	return fixtureColor;
}

glm::vec4 Box2DSimulation::getBodyDebugColor(const b2Body* body) const
{
	glm::vec3 color;

	if (body->GetType() == b2_dynamicBody)
	{
		color = this->dynamicColor;;
	}
	else if (body->GetType() == b2_kinematicBody)
	{
		color = this->kinematicColor;
	}
	else
	{
		color = this->staticColor;
	}

	if (body->IsAwake() == false)
	{
		color += 0.7f;
	}

	color = glm::clamp(color, 0.0f, 1.0f);

	auto alpha = 0.4f;

	if (body->IsActive() == false)
	{
		alpha = 0.2f;
	}

	return glm::vec4{color, alpha};
}

void Box2DSimulation::setWorld(std::unique_ptr<b2World> world)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
	std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
	this->world = std::move(world);
}

void Box2DSimulation::setUniversalGravity(const glm::vec2& gravityForce)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
	std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
	this->world->SetGravity(math::convertVec2<b2Vec2>(gravityForce));
}

glm::vec2 Box2DSimulation::getVelocity(const actor::Identifier& actor) const
{
	glm::vec2 velocity;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto bodyIt = this->actorMap.find(actor);
	if (bodyIt != this->actorMap.end() && bodyIt->second.mainBody != nullptr)
	{
		velocity.x = bodyIt->second.mainBody->GetLinearVelocity().x;
		velocity.y = bodyIt->second.mainBody->GetLinearVelocity().y;
	}
	else
	{
		this->log.warning().raw("Returning linear velocity of non-existent body.");
	}

	return velocity;
}

glm::vec2 Box2DSimulation::getPostition(const actor::Identifier& actorId) const
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto bodyIt = this->actorMap.find(actorId);
	if (bodyIt != this->actorMap.end() && bodyIt->second.mainBody != nullptr)
	{
		return math::convertVec2<glm::vec2>(bodyIt->second.mainBody->GetPosition());
	}
	else
	{
		return glm::vec2();
	}
}

float Box2DSimulation::getRotation(const actor::Identifier& actorId) const
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	const b2Body* body = this->findBody(actorId);

	if (body != nullptr)
	{
		return body->GetAngle();
	}
	else
	{
		return 0.0f;
	}
}

float Box2DSimulation::getAngularVelocity(const actor::Identifier& actor) const
{
	float velocity = 0.0f;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto bodyIt = this->actorMap.find(actor);
	if (bodyIt != this->actorMap.end() && bodyIt->second.mainBody != nullptr)
	{
		velocity = bodyIt->second.mainBody->GetAngularVelocity();
	}
	else
	{
		this->log.warning().raw("Returning angular velocity of non-existent body.");
	}

	return velocity;
}

glm::vec2 Box2DSimulation::getGravitationalPullOnActor(const actor::Identifier& actorId) const
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	const b2Body* body = this->findBody(actorId);

	if (body != nullptr)
	{
		const BodyData* data = static_cast<BodyData*>(body->GetUserData());
		return data->gravitationalPull;
	}
	else
	{
		return glm::vec2();
	}
}

glm::vec2 Box2DSimulation::getStrongestGravitationalPullOnActor(const actor::Identifier& actorId) const
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	const b2Body* body = this->findBody(actorId);

	if (body != nullptr)
	{
		const BodyData* data = static_cast<BodyData*>(body->GetUserData());
		return data->strongestGravitationalPull;
	}
	else
	{
		return glm::vec2();
	}
}

float Box2DSimulation::findStrongestGravitationalPullDirectionFromPoint(const glm::vec2& point, const float pointMass) const
{
	const b2Vec2 worldGravity = this->world->GetGravity();

	float strongestPull = worldGravity.Length();
	float strongestPullDirection = 0.0f;

	if (strongestPull != 0.0f)
	{
		strongestPullDirection = glm::atan(worldGravity.y, worldGravity.x);
	}

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	for (const std::unique_ptr<GravitationalForce>& gravitation : this->gravitationalForces)
	{
		const GravitationalForce::Body pointBody(math::convertVec2<glm::vec2>(point), pointMass);
		const glm::vec2 gravitationalForce = gravitation->calculateForceOnBody(pointBody, 1.0f);

		const float forceStrength = glm::length(gravitationalForce);

		if (forceStrength > strongestPull)
		{
			strongestPull = forceStrength;
			strongestPullDirection = glm::atan(gravitationalForce.y, gravitationalForce.x);
		}
	}

	return strongestPullDirection;
}

glm::vec2 Box2DSimulation::findGravitationalForceFromPoint(const glm::vec2& point, const float pointMass) const
{
	const b2Vec2 worldGravity = this->world->GetGravity();

	glm::vec2 gravitationalPull = math::convertVec2<glm::vec2>(worldGravity);

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	for (const std::unique_ptr<GravitationalForce>& gravitation : this->gravitationalForces)
	{
		const GravitationalForce::Body pointBody(math::convertVec2<glm::vec2>(point), pointMass);
		const glm::vec2 gravitationalForce = gravitation->calculateForceOnBody(pointBody, 1.0f);

		gravitationalPull += gravitationalForce;
	}

	return gravitationalPull;
}

actor::Actor* Box2DSimulation::getActor(const b2Body* body) const
{
	assert(body != nullptr);

	BodyData* bodyData = static_cast<BodyData*>(body->GetUserData());

	return bodyData->actor;
}

actor::Identifier Box2DSimulation::getActorId(const b2Body* body) const
{
	actor::Actor* actor = this->getActor(body);
	if (actor != nullptr)
	{
		return actor->getId();
	}
	else
	{
		return actor::Identifier();
	}
}

bool Box2DSimulation::shouldIgnoreBodyFromLight(const b2Body* body) const
{
	if (body->IsActive() == false)
	{
		return true;
	}

	BodyData* bodyData = static_cast<BodyData*>(body->GetUserData());

	const unsigned int depth = bodyData->depth;

	if (depth < this->lightDepth)
	{
		return true;
	}
	else
	{
		return false;
	}
}

float Box2DSimulation::getMassOfStrongestGravityPullBody(const actor::Identifier& actorId)
{
	float mass = 0.0f;

	auto bodyIt = this->actorMap.find(actorId);
	if (bodyIt != this->actorMap.end() && bodyIt->second.mainBody != nullptr)
	{
		b2Body* mainBody = bodyIt->second.mainBody;
		float strongestGravitationalPull = 0.0f;

		for (const std::unique_ptr<GravitationalForce>& gravitation : this->gravitationalForces)
		{
			const GravitationalForce::Body gravitationBody(math::convertVec2<glm::vec2>(mainBody->GetPosition()), mainBody->GetMass());
			const glm::vec2 gravitationalForce = gravitation->calculateForceOnBody(gravitationBody, 1.0f) * mainBody->GetGravityScale();

			float gravitationalPull = glm::length(gravitationalForce);
			if (gravitationalPull > strongestGravitationalPull)
			{
				strongestGravitationalPull = gravitationalPull;
				mass = gravitation->getMass();
			}
		}
	}

	return mass;
}

float Box2DSimulation::getDistanceToStrongestGravityPullBody(const actor::Identifier& actorId)
{
	float distance = 0.0f;

	auto bodyIt = this->actorMap.find(actorId);
	if (bodyIt != this->actorMap.end() && bodyIt->second.mainBody != nullptr)
	{
		b2Body* mainBody = bodyIt->second.mainBody;
		float strongestGravitationalPull = 0.0f;

		for (const std::unique_ptr<GravitationalForce>& gravitation : this->gravitationalForces)
		{
			glm::vec2 position = math::convertVec2<glm::vec2>(mainBody->GetPosition());

			const GravitationalForce::Body gravitationBody(position, mainBody->GetMass());
			const glm::vec2 gravitationalForce = gravitation->calculateForceOnBody(gravitationBody, 1.0f/60.0f) * mainBody->GetGravityScale();

			float gravitationalPull = glm::length(gravitationalForce);
			if (gravitationalPull > strongestGravitationalPull)
			{
				strongestGravitationalPull = gravitationalPull;
				distance = glm::distance(position, gravitation->getPosition());
			}
		}
	}

	return distance;
}

void Box2DSimulation::applyExplosiveImpulseInRadius(const glm::vec2& position, const float radius, const float impulse)
{
	const b2Vec2 explosionCenter = math::convertVec2<b2Vec2>(position);

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	for (b2Body* body = this->world->GetBodyList(); body; body = body->GetNext())
	{
		if (body->GetType() == b2_dynamicBody)
		{
			b2Vec2 direction = body->GetPosition() - explosionCenter;
			const float bodyDistance = direction.Normalize();

			if (bodyDistance < radius)
			{
				float distanceFraction = bodyDistance / radius;
				float invertedDistanceFraction = 1.0f - distanceFraction;

				float appliedImpulse = impulse * (invertedDistanceFraction * invertedDistanceFraction);

				b2Vec2 directionImpulse = b2Vec2(direction.x * appliedImpulse, direction.y * appliedImpulse);

				std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
				body->ApplyLinearImpulse(directionImpulse, body->GetWorldCenter(), true);
			}
		}
	}
}

bool Box2DSimulation::isPointShadowed(const glm::vec2& lightPoint, const glm::vec2& pointToCheck) const
{
	ShadowedPointRayCastCallback callback;
	callback.physics = this;

	if (glm::distance(lightPoint, pointToCheck) > b2_epsilon)
	{
		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
		this->world->RayCast(&callback,
				     b2Vec2(lightPoint.x, lightPoint.y),
				     b2Vec2(pointToCheck.x, pointToCheck.y));
	}

	return callback.inShadow;
}


float Box2DSimulation::findLengthOfLineBeforeHitObject(const glm::vec2& lineStart, const ::glm::vec2& lineEnd, std::vector<actor::Identifier> ignoreActorId, const util::Mask<PhysicalBodyType>& mask) const
{
	ClosestPointOfType callback;
	callback.mask = mask;

	for (const actor::Identifier& ignoreActor : ignoreActorId)
	{
		callback.bodiesIgnored.push_back(this->findBody(ignoreActor));
	}

	if (glm::distance(lineStart, lineEnd) > b2_epsilon)
	{
		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
		this->world->RayCast(&callback, b2Vec2(lineStart.x, lineStart.y), b2Vec2(lineEnd.x, lineEnd.y));

		return callback.fraction;
	}

	return 0.0f;
}

void Box2DSimulation::syncActorTransform(actor::Actor* actor, b2Body* body)
{
	auto transformComponent = actor->findComponent<actor::Transform>();

	if (transformComponent != nullptr)
	{
		const glm::vec2 bodyPosition(math::convertVec2<glm::vec2>(body->GetPosition()));
		const float bodyRotation = body->GetAngle();

		bool changed = false;

		if (bodyPosition != transformComponent->getPosition()) {
			transformComponent->setPosition(bodyPosition);
			changed = true;
		}

		if (bodyRotation != transformComponent->getRotation())
		{
			transformComponent->setRotation(bodyRotation);
			changed = true;
		}

		if (changed == true && this->activeMode == SimulationMode::SIMULATED)
		{
			transformComponent->broadcastTransformChange();
		}
	}
}



bool Box2DSimulation::checkForIntersectionBetween(const glm::vec2& point1,
					       const glm::vec2& point2)
{
	LineIntersection intersectionCheck;
	intersectionCheck.physics = this;
	intersectionCheck.intersectionFound = false;

	if (glm::distance(point1, point2) > b2_epsilon)
	{
		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
		this->world->RayCast(&intersectionCheck,
				     b2Vec2(point1.x,point1.y),
				     b2Vec2(point2.x,point2.y));
	}

	return intersectionCheck.intersectionFound;
}

bool Box2DSimulation::checkForStaticIntersectionBetween(const glm::vec2& point1, const glm::vec2& point2)
{
	LineIntersectionStatic intersectionCheck;
	intersectionCheck.intersectionFound = false;

	if (glm::distance(point1, point2) > b2_epsilon)
	{
		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
		this->world->RayCast(&intersectionCheck,
				     b2Vec2(point1.x,point1.y),
				     b2Vec2(point2.x,point2.y));
	}

	return intersectionCheck.intersectionFound;
}

float Box2DSimulation::checkForIntersectionAndGetMassOfObjectsBetween(const glm::vec2& point1, const glm::vec2& point2, const std::unordered_set<actor::Identifier>* actorsIgnored)
{
	LineIntersectionTotalMass intersectionCheck;
	intersectionCheck.physics = this;
	intersectionCheck.totalMass = 0;
	intersectionCheck.actorsIgnored = actorsIgnored;

	if (glm::distance(point1, point2) > b2_epsilon)
	{
		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
		this->world->RayCast(&intersectionCheck,
				     b2Vec2(point1.x,point1.y),
				     b2Vec2(point2.x,point2.y));
	}

	return intersectionCheck.totalMass;
}

bool Box2DSimulation::isPointInsideFixture(const glm::vec2& point) const
{
	b2Vec2 position = b2Vec2(point.x, point.y);

	b2AABB smallBoxAABB;
	b2Vec2 smallBoxDimensions;
	smallBoxDimensions.Set(0.001f, 0.001f);
	smallBoxAABB.lowerBound = position - smallBoxDimensions;
	smallBoxAABB.upperBound = position + smallBoxDimensions;

	PointInsidePolygonCallBack callback(position);
	callback.physics = this;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
	this->world->QueryAABB(&callback, smallBoxAABB);

	return callback.isInside;
}

float Box2DSimulation::getMassOfObjectOverlappingPoint(const glm::vec2& point, const std::unordered_set<actor::Identifier>* actorsIgnored) const
{
	b2Vec2 position = b2Vec2(point.x, point.y);

	b2AABB smallBoxAABB;
	b2Vec2 smallBoxDimensions;
	smallBoxDimensions.Set(0.001f, 0.001f);
	smallBoxAABB.lowerBound = position - smallBoxDimensions;
	smallBoxAABB.upperBound = position + smallBoxDimensions;

	PointInsidePolygonCallBack callback(position);
	callback.physics = this;
	callback.actorsIgnored = actorsIgnored;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
	this->world->QueryAABB(&callback, smallBoxAABB);

	return callback.massOfObject;
}

bool Box2DSimulation::isActorOverlappingActor(const actor::Identifier& actorId)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
	b2Body* body = this->findBody(actorId);

	bool overlapping = false;

	if (body != nullptr && body->IsActive() == true)
	{
		BodyOverlapsFixtureQuery callback(body);
		const b2Fixture* fixture = body->GetFixtureList();
		b2AABB bodyBox;

		if (fixture != nullptr)
		{
			bodyBox = fixture->GetAABB(0);
			fixture = fixture->GetNext();
		}

		while( fixture != nullptr)
		{
			b2AABB fixtureBox = fixture->GetAABB(0);
			bodyBox.Combine(fixtureBox);
			fixture = fixture->GetNext();
		}

		this->world->QueryAABB(&callback, bodyBox);
		overlapping = callback.overlapping;
	}
	return overlapping;
}

bool Box2DSimulation::isActorOverlappingAnotherActor(const actor::Identifier& firstActorId, const actor::Identifier& secondActorId)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
	b2Body* bodyOne = this->findBody(firstActorId);
	b2Body* bodyTwo = this->findBody(secondActorId);

	bool overlapping = false;

	for (b2Fixture* bodyOneFixture = bodyOne->GetFixtureList(); bodyOneFixture != nullptr; bodyOneFixture = bodyOneFixture->GetNext())
	{
		if (bodyOneFixture->IsSensor() == false)
		{
			for (b2Fixture* bodyTwoFixture = bodyTwo->GetFixtureList(); bodyTwoFixture != nullptr; bodyTwoFixture = bodyTwoFixture->GetNext())
			{
				if (bodyTwoFixture->IsSensor() == false)
				{
					overlapping = b2TestOverlap(bodyOneFixture->GetShape(), 0, bodyTwoFixture->GetShape(), 0, bodyOne->GetTransform(), bodyTwo->GetTransform());

					if (overlapping == true)
					{
						return overlapping;
					}
				}
			}
		}
	}
	return overlapping;
}

std::vector<actor::Actor*> Box2DSimulation::getActorsOverlapping(const actor::Identifier& actorId)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
	b2Body* body = this->findBody(actorId);

	if (body != nullptr && body->IsActive() == true)
	{
		BodiesOverlappingBodyQuery callback(body);
		const b2Fixture* fixture = body->GetFixtureList();
		b2AABB bodyBox;

		if (fixture != nullptr)
		{
			bodyBox = fixture->GetAABB(0);
			fixture = fixture->GetNext();
		}

		while (fixture != nullptr)
		{
			b2AABB fixtureBox = fixture->GetAABB(0);
			bodyBox.Combine(fixtureBox);
			fixture = fixture->GetNext();
		}

		this->world->QueryAABB(&callback, bodyBox);

		std::vector<actor::Actor*> overlappingActors;

		for (const b2Body* body : callback.overlappingBodies)
		{
			actor::Actor* actor = this->getActor(body);
			overlappingActors.push_back(actor);
		}

		return overlappingActors;

	}
	return std::vector<actor::Actor*>();
}

actor::Actor* Box2DSimulation::findActorIntersectingPoint(const glm::vec2& point, const util::Mask<PhysicalBodyType>& type, const std::vector<actor::Identifier>& ignoreActorId) const
{
	const b2Vec2 mouseClickPosition(point.x,point.y);

	b2AABB smallBoxAABB;
	b2Vec2 smallBoxDimensions;
	smallBoxDimensions.Set(0.001f, 0.001f);
	smallBoxAABB.lowerBound = mouseClickPosition - smallBoxDimensions;
	smallBoxAABB.upperBound = mouseClickPosition + smallBoxDimensions;

	if (type.matches(PhysicalBodyType::DYNAMIC) && type.matches(PhysicalBodyType::STATIC))
	{
		BodyQueryCallback callback(mouseClickPosition);

		for (const actor::Identifier& ignoreActor : ignoreActorId)
		{
			callback.bodiesIgnored.push_back(this->findBody(ignoreActor));
		}

		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
		this->world->QueryAABB(&callback, smallBoxAABB);

		if (callback.fixture != nullptr)
		{
			BodyData* bodyData = static_cast<BodyData*>(callback.fixture->GetBody()->GetUserData());
			return bodyData->actor;
		}
	}
	else if (type.matches(PhysicalBodyType::DYNAMIC))
	{
		DynamicBodyQueryCallback callback(mouseClickPosition);

		for (const actor::Identifier& ignoreActor : ignoreActorId)
		{
			callback.bodiesIgnored.push_back(this->findBody(ignoreActor));
		}

		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
		this->world->QueryAABB(&callback, smallBoxAABB);

		if (callback.fixture != nullptr)
		{
			BodyData* bodyData = static_cast<BodyData*>(callback.fixture->GetBody()->GetUserData());
			return bodyData->actor;
		}
	}
	else if (type.matches(PhysicalBodyType::STATIC))
	{
		StaticBodyQueryCallback callback(mouseClickPosition);

		for (const actor::Identifier& ignoreActor : ignoreActorId)
		{
			callback.bodiesIgnored.push_back(this->findBody(ignoreActor));
		}

		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
		this->world->QueryAABB(&callback, smallBoxAABB);

		if (callback.fixture != nullptr)
		{
			BodyData* bodyData = static_cast<BodyData*>(callback.fixture->GetBody()->GetUserData());
			return bodyData->actor;
		}
	}
	else
	{
		this->log.warning().raw("Box2DPhysics::findActorIntersecting: Kinetic bodies are not handeled.");
	}

	return nullptr;
}

float Box2DSimulation::getMassOfActor(const actor::Identifier& actor)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	const b2Body* body = this->findBody(actor);

	if (body != nullptr)
	{
		return body->GetMass();
	}

	return 0.0f;
}

void Box2DSimulation::setActorAwakeState(const actor::Identifier& actorId, bool state)
{
	auto actorItr = this->actorMap.find(actorId);

	if (actorItr != this->actorMap.end())
	{
		b2Body* mainBody = actorItr->second.mainBody;
		if (mainBody != nullptr)
		{
			mainBody->SetAwake(state);
		}
	}
}

void Box2DSimulation::setMode(SimulationMode mode)
{
	if (this->activeMode == SimulationMode::REPRESENTATIONAL && mode != SimulationMode::REPRESENTATIONAL)
	{
		this->listener.removeEventTypeToListenFor(actor::TransformChange::ID);
	}

	if (mode == SimulationMode::REPRESENTATIONAL && this->activeMode != SimulationMode::REPRESENTATIONAL)
	{
		this->listener.addEventTypeToListenFor(actor::TransformChange::ID);
	}

	this->activeMode = mode;
}

Box2DSimulation::ShadowedPointRayCastCallback::ShadowedPointRayCastCallback():
	inShadow(false),
	physics(nullptr)
{
}

float32 Box2DSimulation::ShadowedPointRayCastCallback::ReportFixture(b2Fixture* fixture, const b2Vec2& /*point*/, const b2Vec2& /*normal*/, float32 /*fraction*/)
{
	b2Body* hitBody = fixture->GetBody();
	if (this->physics->shouldIgnoreBodyFromLight(hitBody) == true)
	{
		return -1;
	}
	else
	{
		this->inShadow = true;
		return 0;
	}
}

Box2DSimulation::ClosestPointRayCastCallback::ClosestPointRayCastCallback():
	fraction(1.0f),
	fixture(nullptr),
	physics(nullptr)
{
}

float32 Box2DSimulation::ClosestPointRayCastCallback::ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction)
{
	b2Body* hitBody = fixture->GetBody();
	if (this->physics->shouldIgnoreBodyFromLight(hitBody) == true)
	{
		return -1;
	}

	if (fraction < this->fraction)
	{
		this->fraction = fraction;
		this->fixture = fixture;
		this->point = point;
		this->normal = normal;
	}

	return fraction;
}

Box2DSimulation::ClosestStaticPointRayCastCallback::ClosestStaticPointRayCastCallback():
	fraction(1.0f),
	fixture(nullptr),
	physics(nullptr)
{
}

float32 Box2DSimulation::ClosestStaticPointRayCastCallback::ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction)
{
	b2Body* hitBody = fixture->GetBody();

	if (hitBody->GetType() != b2_staticBody)
	{
		return -1;
	}

	if (this->physics->shouldIgnoreBodyFromLight(hitBody) == true)
	{
		return -1;
	}

	if (fraction < this->fraction)
	{
		this->fraction = fraction;
		this->fixture = fixture;
		this->point = point;
		this->normal = normal;
	}

	return fraction;
}

Box2DSimulation::ClosestPointOfType::ClosestPointOfType():
	fraction(1.0f)
{

}

float32 Box2DSimulation::ClosestPointOfType::ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& /*normal*/, float32 fraction)
{
	const auto bodyMask = maskFromBodyType(fixture->GetBody()->GetType());

	if (this->mask.matches(bodyMask) == false)
	{
		return -1.0f;
	}

	auto bodyIt = std::find(this->bodiesIgnored.begin(), this->bodiesIgnored.end(), fixture->GetBody());
	if (bodyIt != this->bodiesIgnored.end())
	{
		return -1.0f;
	}
	else
	{
		for (b2Body* body : this->bodiesIgnored)
		{
			for (auto ignoreFixture = body->GetFixtureList(); ignoreFixture != nullptr; ignoreFixture = ignoreFixture->GetNext())
			{
				b2ContactFilter contactFilter;

				if (contactFilter.ShouldCollide(ignoreFixture, fixture) == false)
				{
					return -1.0f;
				}
			}
		}
	}


	this->fraction = fraction;
	this->point = point;

	return fraction;
}

void Box2DSimulation::setActorAngularVelocity(const actor::Identifier& actorId, const float velocity)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto actorItr = this->actorMap.find(actorId);
	if (actorItr != this->actorMap.end())
	{
		auto mainBody = actorItr->second.mainBody;
		if (mainBody != nullptr)
		{
			std::lock_guard<std::mutex> physicsLock(this->physicsUpdateMutex);

			mainBody->SetAngularVelocity(velocity);
		}
	}
}

void Box2DSimulation::setBodyPosition(b2Body* body, const b2Vec2& position)
{
	assert(body != nullptr);

	const auto movement = - body->GetPosition();
	std::lock_guard<std::mutex> physicsLock(this->physicsUpdateMutex);

	body->SetTransform(position, body->GetAngle());

	std::lock_guard<std::mutex> inactiveTreeLock(this->inactiveBodyMutex);

	if (body->IsActive() == false)
	{
		this->updateInactiveBodyAabb(body, movement);
	}
}

void Box2DSimulation::setActorPosition(const actor::Identifier& actorId, const glm::vec2& actorPosition)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto body = this->findBody(actorId);
	assert(body != nullptr);

	const auto bodyPosition = math::convertVec2<b2Vec2>(actorPosition);

	auto bodyData = static_cast<BodyData*>(body->GetUserData());
	bodyData->previousPosition = bodyPosition; // To avoid interpolation

	this->setBodyPosition(body, bodyPosition);
}

void Box2DSimulation::moveActorTo(const actor::Identifier& actorId, const glm::vec2& actorPosition)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto body = this->findBody(actorId);
	assert(body != nullptr);

	const auto bodyPosition = math::convertVec2<b2Vec2>(actorPosition);

	auto bodyData = static_cast<BodyData*>(body->GetUserData());
	bodyData->previousPosition = body->GetPosition();

	this->setBodyPosition(body, bodyPosition);
}

void Box2DSimulation::setActorVelocity(const actor::Identifier& actorId, const glm::vec2& actorVelocity)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto actorItr = this->actorMap.find(actorId);
	if (actorItr != this->actorMap.end())
	{
		b2Body* mainBody = actorItr->second.mainBody;
		if (mainBody != nullptr)
		{

			std::unique_lock<std::mutex> physicsLock(this->physicsUpdateMutex);

			mainBody->SetLinearVelocity(b2Vec2(actorVelocity.x, actorVelocity.y));

			physicsLock.unlock();
		}
	}
}

void Box2DSimulation::setActorGravityMultiplier(const actor::Identifier& actorId, const float gravityMultiplier)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto actorItr = this->actorMap.find(actorId);
	if (actorItr != this->actorMap.end())
	{
		b2Body* mainBody = actorItr->second.mainBody;
		if (mainBody != nullptr)
		{
			std::unique_lock<std::mutex> physicsLock(this->physicsUpdateMutex);

			mainBody->SetGravityScale(gravityMultiplier);

			physicsLock.unlock();
		}
	}

}

void Box2DSimulation::setActorLinearDampening(const actor::Identifier& actorId, const float linearDampening)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto actorItr = this->actorMap.find(actorId);
	if (actorItr != this->actorMap.end())
	{
		b2Body* mainBody = actorItr->second.mainBody;
		if (mainBody != nullptr)
		{

			std::unique_lock<std::mutex> physicsLock(this->physicsUpdateMutex);

			mainBody->SetLinearDamping(linearDampening);

			physicsLock.unlock();
		}
	}
}

void Box2DSimulation::setActorAngularDampening(const actor::Identifier& actorId, const float angularDampening)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto actorItr = this->actorMap.find(actorId);
	if (actorItr != this->actorMap.end())
	{
		b2Body* mainBody = actorItr->second.mainBody;
		if (mainBody != nullptr)
		{

			std::unique_lock<std::mutex> physicsLock(this->physicsUpdateMutex);

			mainBody->SetAngularDamping(angularDampening);

			physicsLock.unlock();
		}
	}
}


void Box2DSimulation::setBodyAngle(b2Body* body, const float angle)
{
	std::lock_guard<std::mutex> physicsLock(this->physicsUpdateMutex);

	body->SetTransform(body->GetPosition(), angle);
}

void Box2DSimulation::setActorRotation(const actor::Identifier& actorId, const float actorRotation)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto body = this->findBody(actorId);
	assert(body != nullptr);

	auto bodyData = static_cast<BodyData*>(body->GetUserData());
	bodyData->previousAngle = actorRotation; // To avoid interpolation

	this->setBodyAngle(body, actorRotation);
}

void Box2DSimulation::rotateActorTo(const actor::Identifier& actorId, const float actorRotation)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	auto body = this->findBody(actorId);
	assert(body != nullptr);

	auto bodyData = static_cast<BodyData*>(body->GetUserData());
	bodyData->previousAngle = body->GetAngle();

	this->setBodyAngle(body, actorRotation);
}

void Box2DSimulation::removeBodyFromDebugRenderer(const b2Body* body)
{
	for (const b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		this->removeFixtureFromDebugRenderer(fixture);
	}

	//app::graphics::LineLoop* lineLoop = this->debugBodyMassCenter[body];

	//if (lineLoop != nullptr)
	//{
	//	this->lineRenderSet->removeGeometry(lineLoop);
	//}
}

void Box2DSimulation::addFixtureToDebugRenderer(const b2Fixture* fixture)
{
	const b2Shape* shape = fixture->GetShape();

	if (shape->GetType() == b2Shape::e_polygon)
	{
		const b2PolygonShape* poly = static_cast<const b2PolygonShape*>(shape);

		if (this->debugPolygonMap.find(fixture) == this->debugPolygonMap.end())
		{
			app::graphics::Polygon* polygon = this->polygonRenderSet->createPolygon(static_cast<unsigned int>(poly->GetVertexCount()));
			this->debugPolygonMap[fixture] = polygon;
		}

		if (this->debugShapeEdgeMap.find(fixture) == this->debugShapeEdgeMap.end())
		{
			app::graphics::LineLoop* edges = this->lineRenderSet->createLineLoop(static_cast<unsigned int>(poly->GetVertexCount()));
			this->debugShapeEdgeMap[fixture] = edges;
			edges->setColor(this->edgeColor);
		}
	}
	else if (shape->GetType() == b2Shape::e_circle)
	{
		if (this->debugPolygonMap.find(fixture) == this->debugPolygonMap.end())
		{
			app::graphics::Polygon* polygon = this->polygonRenderSet->createPolygon(this->circleNumberOfSegments);
			this->debugPolygonMap[fixture] = polygon;
		}

		if (this->debugShapeEdgeMap.find(fixture) == this->debugShapeEdgeMap.end())
		{
			app::graphics::LineLoop* edges = this->lineRenderSet->createLineLoop(this->circleNumberOfSegments);
			this->debugShapeEdgeMap[fixture] = edges;
			edges->setColor(this->edgeColor);
		}

		if (this->debugCircleLineMap.find(fixture) == this->debugCircleLineMap.end())
		{
			app::graphics::Line* line = this->lineRenderSet->createLine();
			this->debugCircleLineMap[fixture] = line;
			line->setColor(this->angleColor);
		}
	}
}


void Box2DSimulation::removeFixtureFromDebugRenderer(const b2Fixture* fixture)
{
	auto polygonIt = this->debugPolygonMap.find(fixture);
	if (polygonIt != this->debugPolygonMap.end())
	{
		this->polygonRenderSet->removeGeometry(polygonIt->second);
		this->debugPolygonMap.erase(polygonIt);
	}

	auto edgesIt = this->debugShapeEdgeMap.find(fixture);
	if (edgesIt != this->debugShapeEdgeMap.end())
	{
		this->lineRenderSet->removeGeometry(edgesIt->second);
		this->debugShapeEdgeMap.erase(edgesIt);
	}

	auto lineIt = this->debugCircleLineMap.find(fixture);
	if (lineIt != this->debugCircleLineMap.end())
	{
		this->lineRenderSet->removeGeometry(lineIt->second);
		this->debugCircleLineMap.erase(lineIt);
	}
}

void Box2DSimulation::addBodyShape(b2Body* body, const BodyShape& shape, b2FixtureDef fixtureDef, std::vector<b2Fixture*>* createdFixtures)
{
	if (body != nullptr)
	{
		BodyData* bodyData = static_cast<BodyData*>(body->GetUserData());
		assert(bodyData != nullptr);

		fixtureDef.filter.categoryBits = bodyData->categoryBits;
		fixtureDef.filter.maskBits = bodyData->maskBits;
	}

	if (shape.type == ShapeType::POLYGON)
	{
		for (const util::BoostPolygon& polygon : shape.polygons)
		{
			this->addPolygonShape(body, fixtureDef, util::transformPolygon(polygon, shape.position, 0.0f, glm::vec2(1.0f, 1.0f)), createdFixtures);
		}
	}

	if (shape.type == ShapeType::RECTANGLE)
	{
		this->addRectangleShape(body, fixtureDef, math::Box<glm::vec2>(shape.box.lowerBound + shape.position, shape.box.upperBound + shape.position));
	}
	if (shape.type == ShapeType::CIRCLE)
	{
		auto fixture = this->addCircleShape(body, fixtureDef, shape.circleRadius, shape.position);

		if (createdFixtures != nullptr)
		{
			createdFixtures->push_back(fixture);
		}
	}
}

b2Fixture* Box2DSimulation::addCircleShape(b2Body* body, b2FixtureDef fixtureDef, float radius, const glm::vec2& position)
{
	b2CircleShape circleShape = createCircleShape(radius, position);

	fixtureDef.shape = &circleShape;

	std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
	auto fixture = body->CreateFixture(&fixtureDef);
	fixture->SetUserData(static_cast<void*>(new FixtureData{}));

	return fixture;
}

void Box2DSimulation::addPolygonShape(b2Body* body, b2FixtureDef fixtureDef, const util::BoostPolygon& polygon, std::vector<b2Fixture*>* createdFixtures)
{
	const std::vector<b2PolygonShape> shapes = createPolygonShape(polygon);

	for (const b2PolygonShape& shape : shapes)
	{
		fixtureDef.shape = &shape;

		std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
		b2Fixture* fixture = body->CreateFixture(&fixtureDef);
		fixture->SetUserData(static_cast<void*>(new FixtureData{}));

		if (createdFixtures != nullptr)
		{
			createdFixtures->push_back(fixture);
		}
	}
}

void Box2DSimulation::addRectangleShape(b2Body* body, b2FixtureDef fixtureDef, math::Box<glm::vec2> box)
{
	assert(box.upperBound.x > box.lowerBound.x && box.upperBound.y > box.lowerBound.y);

	b2PolygonShape polygonShape = createRectangleShape(box);

	fixtureDef.shape = &polygonShape;

	std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
	auto fixture = body->CreateFixture(&fixtureDef);
	fixture->SetUserData(static_cast<void*>(new FixtureData{}));
}

void Box2DSimulation::setActorBodyShape(const actor::Identifier& actorId, const BodyShape& shape)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actorId);
	if (body != nullptr)
	{
		this->setBodyShape(body, shape);

		const bool ignoreBodyFromLight = this->shouldIgnoreBodyFromLight(body);

		for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
		{
			if (ignoreBodyFromLight == false)
			{
				for (auto& light : this->lights)
				{
					if (light->isEnabled() == true && light->castShadows == true && light->getRange() > 0.0f)
					{
						math::Circle lightCircle;
						lightCircle.radius = light->getRange();
						lightCircle.position = light->getPosition();

						math::Box<glm::vec2> fixtureBox;
						const b2AABB& fixtureAabb = fixture->GetAABB(0);
						fixtureBox.lowerBound = math::convertVec2<glm::vec2>(fixtureAabb.lowerBound);
						fixtureBox.upperBound = math::convertVec2<glm::vec2>(fixtureAabb.upperBound);

						if (math::simpleCircleAabbRangeCheck(lightCircle, fixtureBox))
						{
							light->setGeometryOutdated();
						}
					}
				}
			}
		}
	}
}

void Box2DSimulation::setBodyShape(b2Body* body, const BodyShape& shape)
{
	auto density = 0.0f;
	auto restituion = 0.0f;
	auto friction = 0.0f;

	std::vector<b2Fixture*> oldFixtures;

	for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		density += fixture->GetDensity();
		restituion += fixture->GetRestitution();
		friction += fixture->GetFriction();
		oldFixtures.push_back(fixture);
	}

	bool bodyIsActive = true;

	if (body->IsActive() == false)
	{
		bodyIsActive = false;
	}

	std::unique_lock<std::mutex> inactiveBodylock(this->inactiveBodyMutex);

	this->removeFromInactiveTree(body);

	inactiveBodylock.unlock();

	//FIXME: Average of density is not really correct.
	if (oldFixtures.size() > 0)
	{
		const auto numFixtures = static_cast<float>(oldFixtures.size());
		density = density / numFixtures;
		restituion = restituion / numFixtures;
		friction = friction / numFixtures;
	}
	else
	{
		density = 1.0f;
		restituion = 0.0f;
		friction = 0.2f;
	}

	for (b2Fixture* fixture : oldFixtures)
	{
		std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
		this->removeFixtureFromDebugRenderer(fixture);

		auto fixtureData = static_cast<FixtureData*>(fixture->GetUserData());
		if (fixtureData->sensorCallbackId != -1)
		{
			this->contactHandler.removeSensorCallback(fixtureData->sensorCallbackId);
			this->sensorFixtures.unbook(fixtureData->sensorCallbackId);
		}
		delete fixtureData;

		body->DestroyFixture(fixture);
	}

	b2FixtureDef fixtureDef;
	fixtureDef.density = density;
	fixtureDef.restitution = restituion;
	fixtureDef.friction = friction;

	BodyData* bodyData = static_cast<BodyData*>(body->GetUserData());
	fixtureDef.filter.groupIndex = this->getMappedActorFriendGroupIndex(bodyData->actor->getId());

	this->addBodyShape(body, shape, fixtureDef);

	inactiveBodylock.lock();

	if (bodyIsActive == false)
	{
		this->addToInactiveTree(body);
	}

	inactiveBodylock.unlock();

	this->addBodyToDebugRender(body);
	this->syncDebugBodyRendering(body);
}

void Box2DSimulation::unsetSticky(const actor::Identifier& actor)
{
	std::lock_guard<std::mutex> worldMutex(this->worldModifyMutex);

	b2Body* body = this->findBody(actor);
	if (body != nullptr)
	{
		this->contactHandler.removeStickyBody(body);
	}
}

b2Body* Box2DSimulation::findBody(const actor::Identifier& actorBody) const
{
	auto actorIt = this->actorMap.find(actorBody);
	if (actorIt != this->actorMap.end())
	{
		return actorIt->second.mainBody;
	}

	return nullptr;
}

std::vector<actor::Actor*> Box2DSimulation::findActorsIntersectingAabb(const math::Box<glm::vec2>& aabbRect, const util::Mask<BodyState>& bodiesToFind) const
{
	b2AABB aabb;
	aabb.lowerBound = math::convertVec2<b2Vec2>(aabbRect.lowerBound);
	aabb.upperBound = math::convertVec2<b2Vec2>(aabbRect.upperBound);

	std::vector<actor::Actor*> actorsWithin;

	if (bodiesToFind.matches(BodyState::ACTIVE))
	{
		AabbBodyQuery activeQuery;

		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);
		this->world->QueryAABB(&activeQuery, aabb);

		for (b2Body* body : activeQuery.bodiesWithin)
		{
			actorsWithin.push_back(this->getActor(body));
		}
	}

	if (bodiesToFind.matches(BodyState::INACTIVE))
	{
		DynamicTreeAabbBodyQuery inactiveQuery(this->inactiveBodyDynamicTree.get());

		std::lock_guard<std::mutex> inactiveTreeLock(this->inactiveBodyMutex);
		this->inactiveBodyDynamicTree->Query(&inactiveQuery, aabb);

		for (b2Body* body : inactiveQuery.bodiesWithin)
		{
			actorsWithin.push_back(this->getActor(body));
		}
	}

	return actorsWithin;
}

std::vector<actor::Actor*> Box2DSimulation::findEmptyActorsWithinAabb(const math::Box<glm::vec2>& aabbRect, const util::Mask<BodyState>& bodiesToFind) const
{
	std::vector<actor::Actor*> actorsWithin;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	for (const b2Body* body = this->world->GetBodyList(); body != nullptr; body = body->GetNext())
	{
		if (body->GetFixtureList() == nullptr && body != this->groundBody)
		{
			if ((body->IsActive() && bodiesToFind.matches(BodyState::ACTIVE)) || (body->IsActive() == false && bodiesToFind.matches(BodyState::INACTIVE)))
			{
				if (aabbRect.intersects(math::convertVec2<glm::vec2>(body->GetPosition())))
				{
					actorsWithin.push_back(this->getActor(body));
				}
			}
		}
	}

	return actorsWithin;
}

Simulation::RayIntersection Box2DSimulation::findFirstRayIntersection(const glm::vec2& rayStart, const glm::vec2& rayEnd, bool includeInactive)
{
	RayIntersection intersection;

	if (glm::distance(rayStart, rayEnd) > b2_epsilon)
	{
		const b2Vec2 startPoint = math::convertVec2<b2Vec2>(rayStart);
		const b2Vec2 endPoint = math::convertVec2<b2Vec2>(rayEnd);

		ClosestPointRayCastCallback closestActiveIntersection;
		closestActiveIntersection.physics = this;
		closestActiveIntersection.point = endPoint;

		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

		this->world->RayCast(&closestActiveIntersection, startPoint, endPoint);

		if (closestActiveIntersection.fixture != nullptr)
		{
			intersection.intersected = true;
			intersection.position = math::convertVec2<glm::vec2>(closestActiveIntersection.point);
			intersection.surfaceNormal = math::convertVec2<glm::vec2>(closestActiveIntersection.normal);
		}

		if (includeInactive == true)
		{
			DynamicTreeClosestRayIntersection closestInactiveIntersection(this->inactiveBodyDynamicTree.get());
			closestInactiveIntersection.point = endPoint;
			closestInactiveIntersection.enableAllBodyTypes();

			b2RayCastInput input;
			input.p1 = startPoint;
			input.p2 = endPoint;
			input.maxFraction = 1.0f;

			std::lock_guard<std::mutex> inactiveBodyLock(this->inactiveBodyMutex);

			this->inactiveBodyDynamicTree->RayCast(&closestInactiveIntersection, input);

			if (closestInactiveIntersection.fixture != nullptr && closestInactiveIntersection.fraction <= closestActiveIntersection.fraction)
			{
				intersection.intersected = true;
				intersection.position = math::convertVec2<glm::vec2>(closestInactiveIntersection.point);
				intersection.surfaceNormal = math::convertVec2<glm::vec2>(closestInactiveIntersection.normal);
			}
		}
	}

	return intersection;
}

void Box2DSimulation::reset()
{
	std::lock_guard<std::mutex> worldMutex(this->worldModifyMutex);

	std::vector<b2Body*> bodiesToDestroy;
	bodiesToDestroy.reserve(static_cast<std::vector<b2Body*>::size_type>(this->world->GetBodyCount()));

	for (b2Body* body = this->world->GetBodyList(); body != nullptr; body = body->GetNext())
	{
		if (body != this->groundBody)
		{
			bodiesToDestroy.push_back(body);
		}
	}

	for (b2Body* body : bodiesToDestroy)
	{
		this->removeBody(body);
	}

	std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
	std::lock_guard<std::mutex> inactiveBodylock(this->inactiveBodyMutex);

	this->inactiveBodyDynamicTree = std::unique_ptr<b2DynamicTree>(new b2DynamicTree());
	this->inactiveBodyProxies.clear();

	this->actorMap.clear();
	this->lights.clear();
	this->debugPolygonMap.clear();
	this->debugShapeEdgeMap.clear();
	this->debugCircleLineMap.clear();
	this->polygonRenderSet->clear();
	this->lineRenderSet->clear();
	this->sensorFixtures.clear();
	this->contactHandler.clear();
}

void Box2DSimulation::removeBody(b2Body* body)
{
	std::unique_lock<std::mutex> inactiveBodylock(this->inactiveBodyMutex);
	for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		auto proxyIt = this->inactiveBodyProxies.find(fixture);
		if (proxyIt != this->inactiveBodyProxies.end())
		{
			this->inactiveBodyDynamicTree->DestroyProxy(proxyIt->second);
			this->inactiveBodyProxies.erase(proxyIt);
		}
	}
	inactiveBodylock.unlock();

	this->contactHandler.removeCollisionCallback(body);
	this->contactHandler.removeStickyBody(body);

	this->removeBodyFromDebugRenderer(body);

	BodyData* bodyData = static_cast<BodyData*>(body->GetUserData());

	if (bodyData->depth >= this->lightDepth && body->GetType() == b2_dynamicBody)
	{
		util::removeFast(this->dynamicShadowcastingBodies, body);
	}

	std::unique_lock<std::mutex> updateLock(this->physicsUpdateMutex);
	this->world->DestroyBody(body);
	updateLock.unlock();

	delete bodyData;
}

void Box2DSimulation::removeJointData(b2Joint* joint)
{
	JointData* jointData = static_cast<JointData*>(joint->GetUserData());

	if (jointData != nullptr)
	{
		this->joints.unbook(jointData->jointMapId);
		delete jointData;
	}
}

void Box2DSimulation::activateBody(b2Body* body)
{
	{
		std::lock_guard<std::mutex> inactiveBodyLock(this->inactiveBodyMutex);
		this->removeFromInactiveTree(body);
	}

	std::lock_guard<std::mutex> physicsLock(this->physicsUpdateMutex);
	body->SetActive(true);
}

void Box2DSimulation::deactivateBody(b2Body* body)
{
	{
		std::lock_guard<std::mutex> inactiveBodyLock(this->inactiveBodyMutex);
		this->addToInactiveTree(body);
	}

	std::lock_guard<std::mutex> physicsLock(this->physicsUpdateMutex);
	body->SetActive(false);
}

void Box2DSimulation::addToInactiveTree(b2Body* body)
{
	for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		b2AABB shapeAabb;

		if (body->IsActive() == true)
		{
			shapeAabb = fixture->GetAABB(0);
		}
		else
		{
			fixture->GetShape()->ComputeAABB(&shapeAabb, body->GetTransform(), 0);
		}

		const int32 proxy = this->inactiveBodyDynamicTree->CreateProxy(shapeAabb, static_cast<void*>(fixture));
		this->inactiveBodyProxies[fixture] = proxy;
	}
}

void Box2DSimulation::removeFromInactiveTree(const b2Fixture* fixture)
{
	auto proxyIt = this->inactiveBodyProxies.find(fixture);
	if (proxyIt != this->inactiveBodyProxies.end())
	{
		this->inactiveBodyDynamicTree->DestroyProxy(proxyIt->second);
		this->inactiveBodyProxies.erase(proxyIt);
	}
}

void Box2DSimulation::removeFromInactiveTree(const b2Body* body)
{
	for (const b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		this->removeFromInactiveTree(fixture);
	}
}

void Box2DSimulation::updateInactiveBodyAabb(const b2Body* body, const b2Vec2& relativePosition)
{
	for (const b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		const auto proxyIt = this->inactiveBodyProxies.find(fixture);

		b2AABB shapeAabb;
		fixture->GetShape()->ComputeAABB(&shapeAabb, body->GetTransform(), 0);

		this->inactiveBodyDynamicTree->MoveProxy(proxyIt->second, shapeAabb, relativePosition);
	}
}

int Box2DSimulation::addActorSensor(const actor::Identifier& actorId, const BodyShape& sensorShape, SensorCallback callback)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* actorBody = this->findBody(actorId);
	if (actorBody == nullptr)
	{
		return -1;
	}

	b2FixtureDef sensorFixtureDef;
	sensorFixtureDef.isSensor = true;
	sensorFixtureDef.density = 0.0f;
	sensorFixtureDef.filter.groupIndex = this->getMappedActorFriendGroupIndex(actorId);

	std::vector<b2Fixture*> fixtures;

	this->addBodyShape(actorBody, sensorShape, sensorFixtureDef, &fixtures);

	const int sensorId = this->sensorFixtures.book(fixtures);
	this->contactHandler.setSensorCallback(sensorId, callback);

	for (const b2Fixture* sensorFixture : fixtures)
	{
		this->addFixtureToDebugRenderer(sensorFixture);

		auto fixtureData = static_cast<FixtureData*>(sensorFixture->GetUserData());
		fixtureData->sensorCallbackId = sensorId;
	}

	return sensorId;
}

void Box2DSimulation::removeActorSensor(int sensorId)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	this->contactHandler.removeSensorCallback(sensorId);

	std::vector<b2Fixture*> fixtureList = this->sensorFixtures.find(sensorId);
	for (b2Fixture* fixture : fixtureList)
	{
		this->removeFixtureFromDebugRenderer(fixture);

		delete static_cast<FixtureData*>(fixture->GetUserData());

		auto body = fixture->GetBody();
		if (body->IsActive() == false)
		{
			std::lock_guard<std::mutex> inactiveBodyLock(this->inactiveBodyMutex);
			this->removeFromInactiveTree(fixture);
		}
		body->DestroyFixture(fixture);
	}

	this->sensorFixtures.unbook(sensorId);
}

bool Box2DSimulation::hasFixedRotation(const actor::Identifier& actor) const
{
	b2Body* body = this->findBody(actor);

	if (body != nullptr)
	{
		return body->IsFixedRotation();
	}

	this->log.error().raw("Box2DPhysics::hasFixedRotation: Body not found.");

	return false;
}

void Box2DSimulation::activateActorBody(const actor::Identifier& actorId)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actorId);

	if (body != nullptr)
	{
		this->activateBody(body);
	}
}

void Box2DSimulation::deactivateActorBody(const actor::Identifier& actorId)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actorId);

	if (body != nullptr)
	{
		this->deactivateBody(body);
	}
}

std::vector<actor::Actor*> Box2DSimulation::findActorsIntersectingShape(const BodyShape& shape, const glm::vec2& position, const float rotation) const
{
	std::vector<actor::Actor*> actorsIntersecting;
	std::vector<std::unique_ptr<b2Shape>> shapes;

	if (shape.type == ShapeType::POLYGON)
	{
		const std::vector<b2PolygonShape> polygonShapes = createPolygonShape(util::transformPolygons(shape.polygons, shape.position, 0.0f, {1.0f, 1.0f}));

		for (const b2PolygonShape& polygon : polygonShapes)
		{
			shapes.push_back(std::unique_ptr<b2Shape>(new b2PolygonShape(polygon)));
		}
	}
	else if (shape.type == ShapeType::RECTANGLE)
	{
		shapes.push_back(std::unique_ptr<b2Shape>(new b2PolygonShape(createRectangleShape(shape.box))));
	}
	else if (shape.type == ShapeType::CIRCLE)
	{
		shapes.push_back(std::unique_ptr<b2Shape>(new b2CircleShape(createCircleShape(shape.circleRadius, shape.position))));
	}

	const b2Vec2 physicsPosition = math::convertVec2<b2Vec2>(position);

	b2Transform transform;
	transform.Set(physicsPosition, rotation);

	if (shapes.empty() == false)
	{
		b2AABB shapeAabb;

		bool first = true;

		for (const auto& shape : shapes)
		{
			for (int childIndex = 0; childIndex < shape->GetChildCount(); childIndex++)
			{
				if (first == true)
				{
					shape->ComputeAABB(&shapeAabb, transform, childIndex);
					first = false;
				}
				else
				{
					b2AABB aabb;
					shape->ComputeAABB(&aabb, transform, childIndex);
					shapeAabb.Combine(aabb);
				}
			}
		}

		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

		AabbFixtureQuery query;
		this->world->QueryAABB(&query, shapeAabb);

		for (const b2Fixture* fixture : query.fixturesWithin)
		{
			const b2Body* fixtureBody = fixture->GetBody();
			const b2Shape* fixtureShape = fixture->GetShape();
			const b2Transform fixtureTransform = fixtureBody->GetTransform();

			for (int fixtureChildIndex = 0; fixtureChildIndex < fixtureShape->GetChildCount(); fixtureChildIndex++)
			{
				for (const auto& testShape : shapes)
				{
					for (int childIndex = 0; childIndex < testShape->GetChildCount(); childIndex++)
					{
						for (const auto& testShape : shapes)
						{
							const bool overlaps = b2TestOverlap(testShape.get(), childIndex, fixtureShape, fixtureChildIndex, transform, fixtureTransform);

							if (overlaps == true)
							{
								actorsIntersecting.push_back(this->getActor(fixtureBody));
							}
						}
					}
				}
			}
		}
	}

	return actorsIntersecting;
}

void Box2DSimulation::SayGoodbye(b2Joint* joint)
{
	if (joint == this->mouseJoint)
	{
		this->mouseJoint = nullptr;
	}

	this->removeJointData(joint);
}

void Box2DSimulation::SayGoodbye(b2Fixture* fixture)
{
	this->removeFixtureFromDebugRenderer(fixture);

	const auto fixtureData = static_cast<FixtureData*>(fixture->GetUserData());
	if (fixtureData->sensorCallbackId != -1)
	{
		this->contactHandler.removeSensorCallback(fixtureData->sensorCallbackId);
		this->sensorFixtures.unbook(fixtureData->sensorCallbackId);
	}

	delete fixtureData;
}

float32 DynamicTreeClosestRayIntersection::RayCastCallback(const b2RayCastInput& input, int32 proxy)
{
        assert(this->dynamicTree != nullptr);

        const auto fixture = static_cast<b2Fixture*>(this->dynamicTree->GetUserData(proxy));
        assert(fixture != nullptr && fixture->GetBody() != nullptr);

        const b2BodyType bodyType = fixture->GetBody()->GetType();
        const auto typeComparison = std::bind(std::equal_to<b2BodyType>(), std::placeholders::_1, bodyType);

        if (std::any_of(this->acceptedTypes.begin(), this->acceptedTypes.end(), typeComparison) == true)
        {
		// TODO: Support fixtures with more indices, like chain.
		b2RayCastOutput output;
		bool hit = fixture->RayCast(&output, input, 0);

		if (hit == true)
		{
			float32 fraction = output.fraction;
			b2Vec2 point = (1.0f - fraction) * input.p1 + fraction * input.p2;

			this->fraction = output.fraction;
			this->point = point;
			this->fixture = fixture;
			this->normal = output.normal;

			return output.fraction;
		}
        }

        return input.maxFraction;
}

void DynamicTreeClosestRayIntersection::enableBodyType(const b2BodyType type)
{
	this->acceptedTypes.push_back(type);
}

void DynamicTreeClosestRayIntersection::enableBodyTypes(const std::vector<b2BodyType>& types)
{
	this->acceptedTypes = types;
}

void DynamicTreeClosestRayIntersection::enableAllBodyTypes()
{
	this->acceptedTypes = {b2_dynamicBody, b2_staticBody, b2_kinematicBody};
}

void Box2DSimulation::setActorContactCallback(const actor::Identifier& actor, SensorCallback callback)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actor);

	auto fixtures = std::vector<b2Fixture*>{};
	for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		fixtures.push_back(fixture);
	}

	const auto sensorId = this->sensorFixtures.book(fixtures);
	this->contactHandler.setSensorCallback(sensorId, callback);

	for (auto fixture : fixtures)
	{
		auto fixtureData = static_cast<FixtureData*>(fixture->GetUserData());
		fixtureData->sensorCallbackId = sensorId;
	}
}

glm::vec2 Box2DSimulation::getActorCenterOfMass(const actor::Identifier& actorId) const
{
	std::unique_lock<std::mutex> worldLock(this->worldModifyMutex);

	const b2Body* body = this->findBody(actorId);

	if (body != nullptr)
	{
		b2MassData massData;
		body->GetMassData(&massData);

		return math::convertVec2<glm::vec2>(massData.center);
	}

	return glm::vec2();
}

void Box2DSimulation::setSensor(const actor::Identifier& actor, bool state)
{
	b2Body* body = this->findBody(actor);
	if (body != nullptr)
	{
		for (b2Fixture* fixture = body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
		{
			fixture->SetSensor(state);
		}
	}
}

int Box2DSimulation::createWeldJoint(const WeldJointDefinition& jointDef)
{
	b2WeldJointDef box2dJointDef;

	box2dJointDef.localAnchorA = math::convertVec2<b2Vec2>(jointDef.firstBodyAnchor);
	box2dJointDef.localAnchorB = math::convertVec2<b2Vec2>(jointDef.secondBodyAnchor);
	box2dJointDef.collideConnected = jointDef.collideBodies;
	box2dJointDef.frequencyHz = jointDef.frequency;
	box2dJointDef.dampingRatio = jointDef.damping;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	box2dJointDef.bodyA = this->findBody(jointDef.firstBody);
	box2dJointDef.bodyB = this->findBody(jointDef.secondBody);

	if (box2dJointDef.bodyA == nullptr || box2dJointDef.bodyB == nullptr)
	{
		return -1;
	}

	std::unique_lock<std::mutex> updateLock(this->physicsUpdateMutex);
	b2Joint* joint = this->world->CreateJoint(&box2dJointDef);
	updateLock.unlock();

	if (joint == nullptr)
	{
		return -1;
	}

	const int jointId = this->joints.book(joint);

	JointData* jointData = new JointData();
	jointData->jointMapId = jointId;
	joint->SetUserData(static_cast<void*>(jointData));

	return jointId;
}

int Box2DSimulation::createRevoluteJoint(const RevoluteJointDefinition& jointDef)
{
	b2RevoluteJointDef box2dJointDef;

	box2dJointDef.localAnchorA = math::convertVec2<b2Vec2>(jointDef.firstBodyAnchor);
	box2dJointDef.localAnchorB = math::convertVec2<b2Vec2>(jointDef.secondBodyAnchor);
	box2dJointDef.collideConnected = jointDef.collideBodies;
	box2dJointDef.enableMotor = jointDef.motor;
	box2dJointDef.motorSpeed = jointDef.motorSpeed;
	box2dJointDef.maxMotorTorque = jointDef.motorMaxTorque;
	box2dJointDef.referenceAngle = jointDef.anchorAngle;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	box2dJointDef.bodyA = this->findBody(jointDef.firstBody);
	box2dJointDef.bodyB = this->findBody(jointDef.secondBody);

	if (box2dJointDef.bodyA == nullptr || box2dJointDef.bodyB == nullptr)
	{
		return -1;
	}

	std::unique_lock<std::mutex> updateLock(this->physicsUpdateMutex);
	b2Joint* joint = this->world->CreateJoint(&box2dJointDef);
	updateLock.unlock();

	if (joint == nullptr)
	{
		return -1;
	}

	const int jointId = this->joints.book(joint);

	JointData* jointData = new JointData();
	jointData->jointMapId = jointId;
	joint->SetUserData(static_cast<void*>(jointData));

	return jointId;
}

int Box2DSimulation::createPrismaticJoint(const PrismaticJointDefinition& jointDef)
{
	b2PrismaticJointDef box2dJointDef;

	box2dJointDef.localAnchorA = math::convertVec2<b2Vec2>(jointDef.firstBodyAnchor);
	box2dJointDef.localAnchorB = math::convertVec2<b2Vec2>(jointDef.secondBodyAnchor);
	box2dJointDef.collideConnected = jointDef.collideBodies;
	box2dJointDef.enableMotor = jointDef.motor;
	box2dJointDef.enableLimit = jointDef.enableLimit;
	box2dJointDef.motorSpeed = jointDef.motorSpeed;
	box2dJointDef.maxMotorForce = jointDef.maxMotorForce;
	box2dJointDef.lowerTranslation = jointDef.lowerTranslation;
	box2dJointDef.upperTranslation = jointDef.upperTranslation;
	box2dJointDef.localAxisA = math::convertVec2<b2Vec2>(jointDef.axisA);
	box2dJointDef.referenceAngle = jointDef.anchorAngle;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	box2dJointDef.bodyA = this->findBody(jointDef.firstBody);
	box2dJointDef.bodyB = this->findBody(jointDef.secondBody);

	if (box2dJointDef.bodyA == nullptr || box2dJointDef.bodyB == nullptr)
	{
		return -1;
	}

	std::unique_lock<std::mutex> updateLock(this->physicsUpdateMutex);
	b2Joint* joint = this->world->CreateJoint(&box2dJointDef);
	updateLock.unlock();

	if (joint == nullptr)
	{
		return -1;
	}

	const int jointId = this->joints.book(joint);

	JointData* jointData = new JointData();
	jointData->jointMapId = jointId;
	joint->SetUserData(static_cast<void*>(jointData));

	return jointId;

}

void Box2DSimulation::setPrismaticMotorSpeed(const int jointId, const float motorSpeed)
{
	std::vector<b2Joint*> joints = this->joints.find(jointId);

	for (b2Joint* joint : joints)
	{
		if(joint->GetType() == b2JointType::e_prismaticJoint)
		{
			b2PrismaticJoint* prismaticJoint = static_cast<b2PrismaticJoint*>(joint);
			prismaticJoint->SetMotorSpeed(motorSpeed);
		}
	}
}

void Box2DSimulation::setPrismaticLimits(const int jointId, const float lower, const float upper)
{
	std::vector<b2Joint*> joints = this->joints.find(jointId);

	for (b2Joint* joint : joints)
	{
		if (joint->GetType() == b2JointType::e_prismaticJoint)
		{
			b2PrismaticJoint* prismaticJoint = static_cast<b2PrismaticJoint*>(joint);
			prismaticJoint->SetLimits(static_cast<float32>(lower), static_cast<float32>(upper));
		}
	}
}

void Box2DSimulation::setCollisionCategory(actor::Actor* actor, const std::string& collisionCategory)
{
	auto b2body = this->findBody(actor->getId());
	auto bodyData = static_cast<BodyData*>(b2body->GetUserData());

	auto exceeds = false;

	auto categoryIt = this->bodyCategoryMap.find(collisionCategory);
	if (categoryIt != this->bodyCategoryMap.end())
	{
		bodyData->categoryBits = categoryIt->second;
	}
	else if (this->nextCategoryBit != 0x0)
	{
		bodyData->categoryBits = this->nextCategoryBit;

		this->bodyCategoryMap.insert({ collisionCategory, this->nextCategoryBit });
		this->nextCategoryBit = static_cast<uint16>(this->nextCategoryBit << 1);
		this->log.debug().format("Collision category \"%s\": Did not exist. Added to the category map.", collisionCategory.c_str());
	}
	else
	{
		exceeds = true;
		this->log.error().format("Could not add collision category \"%s\": The number of categories exceeds the limit of 16.", collisionCategory.c_str());
	}

	if (exceeds == false)
	{
		for (auto fixture = b2body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
		{
			auto fixtureFilter = fixture->GetFilterData();
			fixtureFilter.categoryBits = bodyData->categoryBits;
			fixture->SetFilterData(fixtureFilter);
		}
	}
}

void Box2DSimulation::addCollisionExclusions(actor::Actor* actor, const std::vector<std::string>& excludedCategories)
{
	auto b2body = this->findBody(actor->getId());
	auto bodyData = static_cast<BodyData*>(b2body->GetUserData());

	for (const auto& maskCategory : excludedCategories)
	{
		auto maskCategoryIt = this->bodyCategoryMap.find(maskCategory);
		auto exceeds = false;

		if (maskCategoryIt != this->bodyCategoryMap.end())
		{
			bodyData->maskBits = static_cast<uint16>(bodyData->maskBits & ~(maskCategoryIt->second));
		}
		else if (this->nextCategoryBit != 0x0)
		{
			bodyData->maskBits = static_cast<uint16>(bodyData->maskBits & ~(this->nextCategoryBit));

			this->bodyCategoryMap.insert({ maskCategory, this->nextCategoryBit });
			this->nextCategoryBit = static_cast<uint16>(this->nextCategoryBit << 1);
		}
		else
		{
			exceeds = true;
			this->log.error().format("Could not add collision category \"%s\": The number of categories exceeds the limit of 16.", maskCategory.c_str());
		}

		if (exceeds == false)
		{
			for (auto fixture = b2body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
			{
				auto fixtureFilter = fixture->GetFilterData();
				fixtureFilter.maskBits = bodyData->maskBits;
				fixture->SetFilterData(fixtureFilter);
			}
		}
	}
}

void Box2DSimulation::removeCollisionExclusions(actor::Actor * actor, const std::vector<std::string>& excludedCategories)
{
	auto b2body = this->findBody(actor->getId());
	auto bodyData = static_cast<BodyData*>(b2body->GetUserData());

	for (const auto& maskCategory : excludedCategories)
	{
		auto maskCategoryIt = this->bodyCategoryMap.find(maskCategory);
		auto exists = true;

		if (maskCategoryIt != this->bodyCategoryMap.end())
		{
			bodyData->maskBits = static_cast<uint16>(bodyData->maskBits | maskCategoryIt->second);
		}
		else
		{
			exists = false;
		}

		if (exists)
		{
			for (auto fixture = b2body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
			{
				auto fixtureFilter = fixture->GetFilterData();
				fixtureFilter.maskBits = bodyData->maskBits;
				fixture->SetFilterData(fixtureFilter);
			}
		}
	}
}

float Box2DSimulation::getPrismaticJointTranslation(const int jointId) const
{
	std::vector<b2Joint*> joints = this->joints.find(jointId);

	for (b2Joint* joint : joints)
	{
		if (joint->GetType() == b2JointType::e_prismaticJoint)
		{
			b2PrismaticJoint* prismaticJoint = static_cast<b2PrismaticJoint*>(joint);
			return static_cast<float>(prismaticJoint->GetJointTranslation());
		}
	}

	return 0.0f;
}

int Box2DSimulation::createTargetJoint(const TargetJointDefinition& definition)
{
	b2MouseJointDef jointDef;

	jointDef.target = math::convertVec2<b2Vec2>(definition.target);
	jointDef.maxForce = definition.maxForce;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	jointDef.bodyA = this->groundBody;
	jointDef.bodyB = this->findBody(definition.firstBody);

	if (jointDef.bodyA == nullptr || jointDef.bodyB == nullptr)
	{
		return -1;
	}

	std::unique_lock<std::mutex> updateLock(this->physicsUpdateMutex);
	b2Joint* joint = this->world->CreateJoint(&jointDef);
	updateLock.unlock();

	if (joint == nullptr)
	{
		return -1;
	}

	const int jointId = this->joints.book(joint);

	JointData* jointData = new JointData();
	jointData->jointMapId = jointId;
	joint->SetUserData(static_cast<void*>(jointData));

	return jointId;
}

void Box2DSimulation::setTargetJointTargetPosition(const int jointId, const glm::vec2& target)
{
	std::vector<b2Joint*> joints = this->joints.find(jointId);

	for (b2Joint* joint : joints)
	{
		if (joint->GetType() == b2JointType::e_mouseJoint)
		{
			auto targetJoint = static_cast<b2MouseJoint*>(joint);
			targetJoint->SetTarget(math::convertVec2<b2Vec2>(target));
		}
	}
}

bool Box2DSimulation::removeJoint(int jointId)
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	std::vector<b2Joint*> jointList = this->joints.find(jointId);

	if (jointList.empty() == true)
	{
		return false;
	}

	for (b2Joint* joint : jointList)
	{
		this->removeJointData(joint);

		std::lock_guard<std::mutex> updateLock(this->physicsUpdateMutex);
		this->world->DestroyJoint(joint);
	}

	return true;
}

Simulation::RayIntersection Box2DSimulation::findFirstStaticRayIntersection(const glm::vec2& rayStart, const glm::vec2& rayEnd, bool includeInactive)
{
	RayIntersection intersection;

	if (glm::distance(rayStart, rayEnd) > b2_epsilon)
	{
		const b2Vec2 startPoint = math::convertVec2<b2Vec2>(rayStart);
		const b2Vec2 endPoint = math::convertVec2<b2Vec2>(rayEnd);

		ClosestStaticPointRayCastCallback closestActivetIntersection;
		closestActivetIntersection.physics = this;
		closestActivetIntersection.point = endPoint;

		std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

		this->world->RayCast(&closestActivetIntersection, startPoint, endPoint);

		if (closestActivetIntersection.fixture != nullptr)
		{
			intersection.intersected = true;
			intersection.position = math::convertVec2<glm::vec2>(closestActivetIntersection.point);
			intersection.surfaceNormal = math::convertVec2<glm::vec2>(closestActivetIntersection.normal);
		}

		if (includeInactive == true)
		{
			DynamicTreeClosestRayIntersection closestInactiveIntersection(this->inactiveBodyDynamicTree.get());
			closestInactiveIntersection.point = endPoint;
			closestInactiveIntersection.enableBodyType(b2_staticBody);

			b2RayCastInput input;
			input.p1 = startPoint;
			input.p2 = endPoint;
			input.maxFraction = 1.0f;

			std::lock_guard<std::mutex> inactiveBodyLock(this->inactiveBodyMutex);

			this->inactiveBodyDynamicTree->RayCast(&closestInactiveIntersection, input);

			if (closestInactiveIntersection.fixture != nullptr && closestInactiveIntersection.fraction <= closestActivetIntersection.fraction)
			{
				intersection.intersected = true;
				intersection.position = math::convertVec2<glm::vec2>(closestInactiveIntersection.point);
				intersection.surfaceNormal = math::convertVec2<glm::vec2>(closestInactiveIntersection.normal);
			}
		}
	}

	return intersection;
}

bool Box2DSimulation::actorAabbIntersectsAabb(const actor::Identifier& actorId, const math::Box<glm::vec2>& aabb) const
{
	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	b2Body* body = this->findBody(actorId);

	bool inside = false;

	if (body != nullptr)
	{
		const b2Fixture* fixture = body->GetFixtureList();

		if (fixture == nullptr)
		{
			return aabb.intersects(math::convertVec2<glm::vec2>(body->GetPosition()));
		}

		b2AABB queryAabb;
		queryAabb.lowerBound = math::convertVec2<b2Vec2>(aabb.lowerBound);
		queryAabb.upperBound = math::convertVec2<b2Vec2>(aabb.upperBound);

		while (inside == false && fixture != nullptr)
		{
			const int32 numChildren = fixture->GetShape()->GetChildCount();

			for (int32 childIndex = 0; childIndex < numChildren; childIndex++)
			{
				b2AABB fixtureAabb;

				if (body->IsActive() == true)
				{
					fixtureAabb = fixture->GetAABB(childIndex);
				}
				else
				{
					fixture->GetShape()->ComputeAABB(&fixtureAabb, body->GetTransform(), childIndex);
				}

				if (b2TestOverlap(queryAabb, fixtureAabb) == true)
				{
					inside = true;
				}
			}

			fixture = fixture->GetNext();
		}
	}

	return inside;
}

std::vector<Simulation::Contact> Box2DSimulation::getActorContacts(const actor::Identifier& actorId) const
{
	std::vector<Contact> contacts;

	std::lock_guard<std::mutex> worldLock(this->worldModifyMutex);

	const b2Body* body = this->findBody(actorId);

	if (body != nullptr)
	{
		for (const b2ContactEdge* contactEdge = body->GetContactList(); contactEdge != nullptr; contactEdge = contactEdge->next)
		{
			Contact contactData;
			contactData.touchingActor = this->getActor(contactEdge->other)->getId();

			const b2Contact* contact = contactEdge->contact;
			const b2Manifold* manifold = contact->GetManifold();

			b2WorldManifold worldManifold;
			contact->GetWorldManifold(&worldManifold);

			for (int32 i = 0; i < manifold->pointCount; i++)
			{
				contactData.collisionPoints.push_back(math::convertVec2<glm::vec2>(worldManifold.points[i]));
			}

			b2Vec2 aToBVector = worldManifold.normal;

			if (contact->GetFixtureB()->GetBody() == contactEdge->other)
			{
				contactData.contactDirection = math::convertVec2<glm::vec2>(aToBVector);
			}
			else
			{
				contactData.contactDirection = math::convertVec2<glm::vec2>(-aToBVector);
			}

			contacts.push_back(contactData);
		}
	}

	return contacts;
}

std::vector<b2PolygonShape> createPolygonShape(const util::BoostPolygon& polygon)
{
	std::vector<b2PolygonShape> shapes;

	// Convex polygon without holes.
	if (polygon.outer().size() >= 4 && polygon.inners().size() == 0 && util::isConvex(polygon.outer()) == true)
	{
		std::vector<b2Vec2> polygonVertices;
		polygonVertices.reserve(polygon.outer().size() - 1);

		for (std::size_t i = 0; i < polygon.outer().size() - 1; i++)
		{
			polygonVertices.push_back(b2Vec2(polygon.outer()[i].x, polygon.outer()[i].y));
		}

		// Split the fixture so it can be handled by Box2D, which requires that there are no more than b2_maxPolygonVertices.
		std::size_t currentVertexStart = 0;
		bool shapesCreated = false;

		while (shapesCreated == false)
		{
			std::vector<b2Vec2> fixtureVertices;

			std::vector<b2Vec2>::size_type i = 1;
			while (i < b2_maxPolygonVertices && (i + currentVertexStart < polygonVertices.size()))
			{
				fixtureVertices.push_back(polygonVertices[i + currentVertexStart]);
				i++;
			}

			fixtureVertices.push_back(polygonVertices[0]);

			if (validPolygon(fixtureVertices.data(), static_cast<int32>(fixtureVertices.size())) == true)
			{

				b2PolygonShape fixtureShape;
				fixtureShape.Set(fixtureVertices.data(), static_cast<int>(fixtureVertices.size()));

				shapes.push_back(std::move(fixtureShape));
			}

			currentVertexStart += fixtureVertices.size() - 2;

			if (currentVertexStart >= polygonVertices.size() - 2)
			{
				shapesCreated = true;
			}
		}
	}
	else
	{
		P2tPolygon outerPolygon;
		std::vector<P2tPolygon> polygonHoles;

		glm::vec2 previousPoint(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());

		for (std::size_t i = 0; i < polygon.outer().size() - 1; i++)
		{
			const glm::vec2& point = polygon.outer()[i];

			if (point != previousPoint)
			{
				outerPolygon.push_back(new p2t::Point(point.x, point.y));
				previousPoint = point;
			}
		}

		if (outerPolygon.size() >= 3)
		{
			p2t::CDT cdt(outerPolygon);

			for (const util::BoostRing& innerRing : polygon.inners())
			{
				P2tPolygon polygonHole;

				for (std::size_t i = 0; i < innerRing.size() - 1; i++)
				{
					glm::vec2 point = innerRing[i];
					polygonHole.push_back(new p2t::Point(point.x, point.y));
				}

				cdt.AddHole(polygonHole);
				polygonHoles.push_back(std::move(polygonHole));
			}

			cdt.Triangulate();
			std::vector<p2t::Triangle*> triangles = cdt.GetTriangles();

			for (p2t::Triangle* triangle : triangles)
			{
				b2Vec2 vertices[3];
				vertices[0].x = static_cast<float>(triangle->GetPoint(0)->x);
				vertices[0].y = static_cast<float>(triangle->GetPoint(0)->y);
				vertices[1].x = static_cast<float>(triangle->GetPoint(1)->x);
				vertices[1].y = static_cast<float>(triangle->GetPoint(1)->y);
				vertices[2].x = static_cast<float>(triangle->GetPoint(2)->x);
				vertices[2].y = static_cast<float>(triangle->GetPoint(2)->y);

				if (validPolygon(vertices, 3) == true)
				{
					b2PolygonShape fixtureShape;
					fixtureShape.Set(vertices, 3);

					shapes.push_back(std::move(fixtureShape));
				}
			}

			for (P2tPolygon& hole : polygonHoles)
			{
				for (p2t::Point* point : hole)
				{
					delete point;
				}
			}

			for (p2t::Point* point : outerPolygon)
			{
				delete point;
			}
		}
	}

	return shapes;
}

std::vector<b2PolygonShape> createPolygonShape(const std::vector<util::BoostPolygon>& polygonList)
{
	std::vector<b2PolygonShape> shapes;

	for (const util::BoostPolygon& polygon : polygonList)
	{
		const std::vector<b2PolygonShape> polygonShapes = createPolygonShape(polygon);
		std::move(polygonShapes.begin(), polygonShapes.end(), std::back_inserter(shapes));
	}

	return shapes;
}

b2CircleShape createCircleShape(const float radius, const glm::vec2& position)
{
	b2CircleShape circleShape;
	circleShape.m_radius = radius;
	circleShape.m_p = math::convertVec2<b2Vec2>(position);

	return circleShape;
}

b2PolygonShape createRectangleShape(const math::Box<glm::vec2>& rectangle)
{
	b2PolygonShape polygonShape;
	polygonShape.SetAsBox(rectangle.getWidth() / 2.0f, rectangle.getHeight() / 2.0f, math::convertVec2<b2Vec2>(rectangle.getCenter()), 0.0f);

	return polygonShape;
}

bool AabbBodyQuery::ReportFixture(b2Fixture* fixture)
{
	this->bodiesWithin.insert(fixture->GetBody());
	return true;
}


bool AabbFixtureQuery::ReportFixture(b2Fixture* fixture)
{
	this->fixturesWithin.push_back(fixture);
	return true;
}

} } }
