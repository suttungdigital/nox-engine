/*
 * NOX Engine
 *
 * Copyright (c) 2015,2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/actor/Actor.h>
#include <nox/logic/IContext.h>
#include <glm/gtc/matrix_transform.hpp>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/event/IBroadcaster.h>
#include <nox/util/json_utils.h>

namespace nox { namespace logic { namespace actor
{

const Component::IdType Transform::NAME = "Transform";

Transform::~Transform() = default;

bool Transform::initialize(const Json::Value& componentJsonObject)
{
	this->position = util::parseJsonVec(componentJsonObject["position"], glm::vec2(0.0f, 0.0f));
	this->scale = util::parseJsonVec(componentJsonObject["scale"], glm::vec2(1.0f, 1.0f));
	this->rotation = componentJsonObject.get("rotation", 0.0f).asFloat();

	return true;
}

void Transform::onCreate()
{
	this->broadcastTransformChange();
}

glm::mat4 Transform::getTransformMatrix()
{
	glm::mat4 transform;
	transform = glm::translate(transform, glm::vec3(this->position, 0.0f));
	transform = glm::rotate(transform, this->rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	transform = glm::scale(transform, glm::vec3(this->scale, 1.0f));

	return transform;
}

const Component::IdType& Transform::getName() const
{
	return NAME;
}

void Transform::setPosition(const glm::vec2& position)
{
	this->position = position;

	this->broadcastTransformChange();
}

void Transform::setScale(const glm::vec2& scale)
{
	this->scale = scale;

	this->broadcastTransformChange();
}

void Transform::setRotation(const float rotation)
{
	this->rotation = rotation;

	this->broadcastTransformChange();
}

void Transform::setTransform(const glm::vec2& position, const float rotation, const glm::vec2& scale)
{
	this->position = position;
	this->scale = scale;
	this->rotation = rotation;

	this->broadcastTransformChange();
}

void Transform::broadcastTransformChange()
{
	assert(this->getOwner() != nullptr);

	const auto transformEvent = std::make_shared<TransformChange>(this->getOwner(), this->position, this->rotation, this->scale);

	this->getOwner()->broadCastComponentEvent(transformEvent);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(transformEvent);
}

void Transform::serialize(Json::Value& componentObject)
{
	componentObject["position"] = util::writeJsonVec(this->position);
	componentObject["scale"] = util::writeJsonVec(this->scale);
	componentObject["rotation"] = this->rotation;
}

} } }
