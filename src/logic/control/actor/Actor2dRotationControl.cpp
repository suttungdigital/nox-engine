/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/control/actor/Actor2dRotationControl.h>

#include <nox/logic/actor/Actor.h>
#include <nox/logic/physics/actor/ActorPhysics.h>

#include <cassert>

namespace nox { namespace logic { namespace control
{

const Actor2dRotationControl::IdType Actor2dRotationControl::NAME = "2dRotationControl";

const Actor2dRotationControl::IdType& Actor2dRotationControl::getName() const
{
	return NAME;
}

bool Actor2dRotationControl::initialize(const Json::Value& componentJson)
{
	if (this->ActorControl::initialize(componentJson) == false)
	{
		return false;
	}

	this->rotationForce = componentJson.get("rotationForce", 1.0f).asFloat();
	this->currentRotationDirection = 0.0f;

	return true;
}

void Actor2dRotationControl::serialize(Json::Value& componentJson)
{
	componentJson["rotationForce"] = this->rotationForce;
}

void Actor2dRotationControl::onCreate()
{
	this->ActorControl::onCreate();

	this->actorPhysics = this->getOwner()->findComponent<physics::ActorPhysics>();
	assert(this->actorPhysics != nullptr);
}

bool Actor2dRotationControl::handleControl(const std::shared_ptr<Action>& controlEvent)
{
	if (controlEvent->getControlName() == "rotate")
	{
		this->currentRotationDirection = controlEvent->getControlVector().x;

		return true;
	}

	return false;
}

void Actor2dRotationControl::onUpdate(const nox::Duration& deltaTime)
{
	this->ActorControl::onUpdate(deltaTime);

	if (this->currentRotationDirection != 0.0f)
	{
		this->actorPhysics->applyTorque(this->currentRotationDirection * this->rotationForce);
	}
}

} } }
