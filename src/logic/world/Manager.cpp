/*
 * NOX Engine
 *
 * Copyright (c) 2015-2017 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/world/Manager.h>

#include <nox/logic/IContext.h>
#include <nox/logic/View.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/event/IBroadcaster.h>
#include <nox/logic/physics/Simulation.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/world/event/ActorRemoved.h>
#include <nox/logic/world/Writer.h>
#include <nox/app/storage/IDataStorage.h>
#include <nox/util/algorithm.h>

#include <list>
#include <string>
#include <cassert>

namespace nox { namespace logic { namespace world
{

Manager::Manager(IContext* context):
	context(context),
	eventBroadcaster(context->getEventBroadcaster()),
	log(context->createLogger()),
	actorFactory(this->context),
	actorUpdateLocked(false)
{
	assert(this->context != nullptr);
	assert(this->eventBroadcaster != nullptr);

	this->log.setName("WorldManager");
}

Manager::~Manager()
{
	if (this->saveThread.joinable() == true)
	{
		this->saveThread.join();
	}
}

void Manager::update(const Duration& deltaTime)
{
	this->actorUpdateLocked = true;

	std::unique_lock<std::mutex> actorLock(this->actorStorageMutex);

	for (auto& actor : this->actors)
	{
		if (actor->isActive() == true)
		{
			actor->update(deltaTime);
		}
	}

	actorLock.unlock();

	this->actorUpdateLocked = false;

	this->handleQueuedTasks();

	this->onUpdate(deltaTime);
}

void Manager::reset()
{
	this->handleQueuedTasks();

	while (this->actorManagementQueue.isEmpty() == false)
	{
		this->actorManagementQueue.pop();
	}

	this->clearActors();
	this->actorRemovalQueue.clear();
	this->actorFactory.resetCounter();

	{
		std::lock_guard<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);
		this->nonPhysicalActorTransforms.clear();
	}

	this->onReset();
}

void Manager::saveEverything(const std::string& saveDirectory, SaveCompleteCallback completeCallback)
{
	if (this->saveThread.joinable() == true)
	{
		this->saveThread.join();
	}

	this->saveThread = std::thread(&Manager::saveWorld, this, completeCallback, saveDirectory);
}

void Manager::saveWorld(SaveCompleteCallback completeCallback, const std::string& saveDirectory)
{
	auto writer = Writer{this->context};

	auto actorPointers = std::vector<const actor::Actor*>{this->actors.size()};
	std::transform(
		this->actors.begin(),
		this->actors.end(),
		actorPointers.begin(),
		[](const auto& uniqueActor)
		{
			return uniqueActor.get();
		}
	);
	writer.addActors(actorPointers);

	auto storage = this->getLogicContext()->getDataStorage();
	auto outputStream = storage->openWritableFile(saveDirectory + "/world.json");
	writer.write(*outputStream);

	if (completeCallback)
	{
		completeCallback();
	}
}

void Manager::loadActorDefinitions(app::resource::IResourceAccess* resourceAccess, const std::string& definitionPath)
{
	this->actorFactory.loadActorDefinitions(resourceAccess, definitionPath);
}

actor::Actor* Manager::findActor(const actor::Identifier& id) const
{
	std::lock_guard<std::mutex> idMapLock(this->actorIdMutex);

	auto actorItr = this->idToActorMap.find(id);

	if (actorItr != this->idToActorMap.end())
	{
		return actorItr->second;
	}

	return nullptr;
}

std::vector<actor::Actor*> Manager::findActorsWithinRange(const glm::vec2& position, const float range) const
{
	std::vector<actor::Actor*> actorsInRange;

	std::unique_lock<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);

	for (const auto& actorTransform : this->nonPhysicalActorTransforms)
	{
		if (glm::distance(position, actorTransform->getPosition()) <= range)
		{
			actorsInRange.push_back(actorTransform->getOwner());
		}
	}

	transformLock.unlock();

	const math::Box<glm::vec2> rangeAabb(position - glm::vec2(range / 2.0f), position + glm::vec2(range / 2.0f));

	std::vector<actor::Actor*> physicsActorsWithin = this->context->getPhysics()->findActorsIntersectingAabb(rangeAabb, physics::allBodyStates());
	util::mergeInto(physicsActorsWithin, this->context->getPhysics()->findEmptyActorsWithinAabb(rangeAabb, physics::allBodyStates()));

	for (actor::Actor* actor : physicsActorsWithin)
	{
		auto actorTransform = actor->findComponent<actor::Transform>();
		assert(actorTransform != nullptr);

		if (glm::distance(position, actorTransform->getPosition()) <= range)
		{
			actorsInRange.push_back(actorTransform->getOwner());
		}
	}

	return actorsInRange;
}

std::vector<actor::Actor*> Manager::findActorsWithinAxisAlignedBox(const math::Box<glm::vec2>& box) const
{
	std::vector<actor::Actor*> actorsInRange;

	std::unique_lock<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);

	for (const auto& actorTransform : this->nonPhysicalActorTransforms)
	{
		if (box.intersects(actorTransform->getPosition()))
		{
			actorsInRange.push_back(actorTransform->getOwner());
		}
	}

	transformLock.unlock();

	std::vector<actor::Actor*> physicsActorsWithin = this->context->getPhysics()->findActorsIntersectingAabb(box, physics::allBodyStates());
	util::mergeInto(physicsActorsWithin, this->context->getPhysics()->findEmptyActorsWithinAabb(box, physics::allBodyStates()));

	actorsInRange.insert(actorsInRange.end(), physicsActorsWithin.begin(), physicsActorsWithin.end());

	return actorsInRange;
}

void Manager::handleActorManagement(actor::Actor* actor)
{
	auto actorTransform = actor->findComponent<actor::Transform>();

	if (actorTransform != nullptr)
	{
		auto actorPhysics = actor->findComponent<physics::ActorPhysics>();

		if (actorPhysics == nullptr)
		{
			std::lock_guard<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);
			this->nonPhysicalActorTransforms.push_back(actorTransform);
		}
	}

	this->onActorManaged(actor);
}

void Manager::handleActorRemoval(actor::Actor* actor)
{
	const actor::Identifier& actorId = actor->getId();

	std::lock_guard<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);

	auto actorTransformIt = std::find_if(
			this->nonPhysicalActorTransforms.begin(),
			this->nonPhysicalActorTransforms.end(),
			[actorId](actor::Transform* transform)
			{
				return actorId == transform->getOwner()->getId();
			}
	);

	if (actorTransformIt != this->nonPhysicalActorTransforms.end())
	{
		this->nonPhysicalActorTransforms.erase(actorTransformIt);
	}

	this->onActorRemoved(actor);
}

void Manager::onActorManaged(actor::Actor* actor)
{
	if (actor->hasParent() == false)
	{
		actor->create();
		actor->activate();
	}
}

void Manager::onActorRemoved(actor::Actor* /*actor*/)
{
}

std::unique_ptr<actor::Actor> Manager::createActorFromDefinitionName(const std::string& actorDefinitionName)
{
	std::vector<actor::Actor*> childActors;
	auto actor = this->actorFactory.createActor(actorDefinitionName, childActors);

	if (actor != nullptr)
	{
		this->handleActorCreation(actor.get());
	}

	return actor;
}

std::unique_ptr<actor::Actor> Manager::createActorFromSource(const Json::Value& jsonSource, const std::string& definitionName)
{
	std::vector<actor::Actor*> childActors;
	auto actor = this->actorFactory.createActor(jsonSource, definitionName, childActors);

	if (actor != nullptr)
	{
		this->handleActorCreation(actor.get());
	}

	return actor;
}

actor::Actor* Manager::manageActor(std::unique_ptr<actor::Actor> actor)
{
	actor::Actor* actorReference = actor.get();

	this->actorManagementQueue.push(std::move(actor));

	this->handleActorManagement(actorReference);

	return actorReference;
}

void Manager::manageQueuedActor(std::unique_ptr<actor::Actor> actor)
{
	actor::Actor* actorReference = actor.get();

	std::unique_lock<std::mutex> actorLock(this->actorStorageMutex);
	this->actors.push_back(std::move(actor));
	actorLock.unlock();

	this->log.verbose().format("Actor %s with id %d managed.", actorReference->getName().c_str(), actorReference->getId().getValue());
}

void Manager::removeActor(std::unique_ptr<actor::Actor> actor)
{
	std::unique_lock<std::mutex> idMapLock(this->actorIdMutex);
	this->idToActorMap.erase(actor->getId());
	idMapLock.unlock();

	actor->forceActiveState(actor::Actor::ForcedActiveState::NONE);
	actor->deactivate();
	actor->destroy();

	std::vector<std::unique_ptr<actor::Actor>> childActors = actor->detachAllChildActors();
	std::list<std::unique_ptr<actor::Actor>> detachedChildActorQueue;
	std::move(childActors.begin(), childActors.end(), std::back_inserter(detachedChildActorQueue));

	std::shared_ptr<ActorRemoved> removeEvent = std::make_shared<ActorRemoved>();
	removeEvent->setActor(std::move(actor));
	this->eventBroadcaster->queueEvent(removeEvent);

	while (detachedChildActorQueue.empty() == false)
	{
		std::unique_ptr<actor::Actor>& childActor = detachedChildActorQueue.front();

		std::vector<std::unique_ptr<actor::Actor>> childOfChildActors = childActor->detachAllChildActors();
		std::move(childOfChildActors.begin(), childOfChildActors.end(), std::back_inserter(detachedChildActorQueue));

		idMapLock.lock();
		this->idToActorMap.erase(childActor->getId());
		idMapLock.unlock();

		childActor->forceActiveState(actor::Actor::ForcedActiveState::NONE);
		childActor->deactivate();
		childActor->destroy();

		std::shared_ptr<ActorRemoved> childRemoveEvent = std::make_shared<ActorRemoved>();
		childRemoveEvent->setActor(std::move(childActor));
		this->eventBroadcaster->queueEvent(childRemoveEvent);

		detachedChildActorQueue.pop_front();
	}

	this->handleActorRemoval(removeEvent->getActor());
}

void Manager::removeActor(const actor::Identifier& actorId)
{
	this->actorRemovalQueue.push(actorId);
}

void Manager::clearActors()
{
	for (auto& actor : this->actors)
	{
		actor->forceActiveState(actor::Actor::ForcedActiveState::NONE);
		actor->deactivate();
		actor->destroy();

		this->onActorRemoved(actor.get());

		std::shared_ptr<ActorRemoved> removeEvent = std::make_shared<ActorRemoved>();
		removeEvent->setActor(std::move(actor));

		this->eventBroadcaster->queueEvent(removeEvent);

	}

	this->actors.clear();
	this->idToActorMap.clear();
}

void Manager::handleQueuedTasks()
{
	while (this->actorManagementQueue.isEmpty() == false)
	{
		auto actor = this->actorManagementQueue.pop();

		if (actor)
		{
			this->manageQueuedActor(std::move(actor));
		}
	}

	while (this->actorRemovalQueue.isEmpty() == false)
	{
		const auto actorId = this->actorRemovalQueue.pop();
		if (actorId != actor::Identifier())
		{
			std::lock_guard<std::mutex> actorLock(this->actorStorageMutex);

			auto actorItr = std::find_if(
				this->actors.begin(),
				this->actors.end(),
				[&actorId](const std::unique_ptr<actor::Actor> &actor)
				{
					return actorId == actor->getId();
				}
			);

			if (actorItr != this->actors.end())
			{
				this->removeActor(std::move(*actorItr));

				this->actors.erase(actorItr);
			}
		}
	}
}

void Manager::handleActorCreation(actor::Actor* actor)
{
	assert(actor != nullptr);

	actor::Identifier actorId = actor->getId();

	std::unique_lock<std::mutex> idMapLock(this->actorIdMutex);

	this->idToActorMap[actorId] = actor;

	const std::vector<actor::Actor::Child> children = actor->findChildrenRecursively();

	for (const actor::Actor::Child& child : children)
	{
		this->idToActorMap[child.actor->getId()] = child.actor;
	}

	idMapLock.unlock();

	auto actorCreatedEvent = std::make_shared<ActorCreated>(actor);

	this->eventBroadcaster->queueEvent(actorCreatedEvent);

	for (const actor::Actor::Child& child : children)
	{
		auto childActorCreatedEvent = std::make_shared<ActorCreated>(child.actor);

		this->eventBroadcaster->queueEvent(childActorCreatedEvent);
	}

	this->log.verbose().format("Created actor \"%s\" with id %d from definition \"%s\".", actor->getName().c_str(), actor->getId().getValue(), actor->getDefinitionName().c_str());
}

IContext* Manager::getLogicContext()
{
	return this->context;
}

void Manager::onUpdate(const nox::Duration& /*deltaTime*/)
{
}

void nox::logic::world::Manager::onReset()
{
}

} } }
