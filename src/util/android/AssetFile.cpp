#include <nox/util/android/AssetFile.h>

#include <cassert>

namespace nox { namespace android {

class AssetFile::Impl
{
public:
	Impl(const std::string& path, JNIEnv* javaEnv, jobject javaInputStream);

	jmethodID getJavaMethodId(const char* name, const char* signature);
	jint readJava(jbyteArray& bufferJava, const jint bufferOffsetJava, const jint lengthJava);

	std::string path;

	JNIEnv* javaEnv;
	jobject javaInputStream;
	jclass javaInputStreamClass;

	jmethodID readMethodId;
	jmethodID closeMethodId;
};

AssetFile::AssetFile(const std::string& path, JNIEnv* javaEnv, jobject javaInputStream):
	pimpl(std::make_unique<Impl>(path, javaEnv, javaInputStream))
{
}

AssetFile::AssetFile(const std::string& path):
	AssetFile(path, nullptr, nullptr)
{
}

AssetFile::AssetFile():
	AssetFile(std::string())
{
}

AssetFile::~AssetFile()
{
	this->close();

	if (this->pimpl->javaInputStream != nullptr)
	{
		this->pimpl->javaEnv->DeleteLocalRef(this->pimpl->javaInputStream);
	}
}

AssetFile::AssetFile(AssetFile&& other):
	pimpl(std::move(other.pimpl))
{
}

AssetFile& AssetFile::operator=(AssetFile&& other)
{
	this->pimpl = std::move(other.pimpl);

	return *this;
}

bool AssetFile::isValid() const
{
	return this->pimpl->javaInputStream != nullptr;
}

const std::string& AssetFile::getPath() const
{
	return this->pimpl->path;
}

std::size_t AssetFile::read(char buffer[], const std::size_t bufferOffset, const std::size_t length)
{
	const auto offsetJava = static_cast<jint>(bufferOffset);
	const auto lengthJava = static_cast<jint>(length);

	auto bufferJava = this->pimpl->javaEnv->NewByteArray(lengthJava);
	auto bytesReadJava = this->pimpl->javaEnv->CallIntMethod(this->pimpl->javaInputStream, this->pimpl->readMethodId, bufferJava, offsetJava, lengthJava);

	auto bytesRead = std::size_t(0);

	if (bytesReadJava > 0)
	{
		auto bufferByte = reinterpret_cast<jbyte*>(buffer);
		this->pimpl->javaEnv->GetByteArrayRegion(bufferJava, 0, bytesReadJava, bufferByte);

		bytesRead = static_cast<std::size_t>(bytesReadJava);
	}

	this->pimpl->javaEnv->DeleteLocalRef(bufferJava);

	return bytesRead;
}

jint AssetFile::Impl::readJava(jbyteArray& bufferJava, const jint bufferOffsetJava, const jint lengthJava)
{
	auto bytesReadJava = this->javaEnv->CallIntMethod(this->javaInputStream, this->readMethodId, bufferJava, bufferOffsetJava, lengthJava);

	if (bytesReadJava < 0)
	{
		bytesReadJava = 0;
	}

	return bytesReadJava;
}

std::vector<char> AssetFile::readAll()
{
	std::vector<char> data;

	static const auto bufferSize = std::size_t(16 * 1024);
	char buffer[bufferSize];

	const auto bufferSizeJava = static_cast<jint>(bufferSize);
	auto bufferJava = this->pimpl->javaEnv->NewByteArray(bufferSizeJava);

	auto finished = false;

	while (finished == false)
	{
		const auto bytesReadJava = this->pimpl->readJava(bufferJava, 0, bufferSizeJava);

		if (bytesReadJava == 0)
		{
			finished = true;
		}
		else
		{
			auto bufferByte = reinterpret_cast<jbyte*>(buffer);
			this->pimpl->javaEnv->GetByteArrayRegion(bufferJava, 0, bytesReadJava, bufferByte);

			data.insert(data.end(), std::begin(buffer), std::begin(buffer) + bytesReadJava);
		}
	}

	this->pimpl->javaEnv->DeleteLocalRef(bufferJava);

	return data;
}

void AssetFile::close()
{
	if (this->pimpl->javaInputStream != nullptr)
	{
		this->pimpl->javaEnv->CallVoidMethod(this->pimpl->javaInputStream, this->pimpl->closeMethodId);
		this->pimpl->javaEnv->DeleteLocalRef(this->pimpl->javaInputStream);
		this->pimpl->javaInputStream = nullptr;
	}
}

AssetFile::Impl::Impl(const std::string& path, JNIEnv* javaEnv, jobject javaInputStream):
	path(path),
	javaEnv(javaEnv),
	javaInputStream(javaInputStream),
	javaInputStreamClass(nullptr)
{
	if (this->javaEnv && this->javaInputStream)
	{
		this->javaInputStreamClass = javaEnv->GetObjectClass(javaInputStream);

		this->readMethodId = this->getJavaMethodId("read", "([BII)I");
		this->closeMethodId = this->getJavaMethodId("close", "()V");
	}
}

jmethodID AssetFile::Impl::getJavaMethodId(const char* name, const char* signature)
{
	return this->javaEnv->GetMethodID(this->javaInputStreamClass, name, signature);
}

} }
