/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/common/platform.h>
#include <nox/app/IContext.h>
#include <nox/app/graphics/platform.h>
#include <nox/app/platform.h>
#include <nox/window/SdlWindowView.h>

#include <SDL.h>
#include <glm/gtx/string_cast.hpp>
#include <string>
#include <cassert>

namespace nox
{
namespace window
{

SdlWindowView::~SdlWindowView() = default;
SdlWindowView::SdlWindowView(SdlWindowView&&) = default;
SdlWindowView& SdlWindowView::operator=(SdlWindowView&&) = default;

SdlWindowView::SdlWindowView(app::IContext* applicationContext, const std::string& windowTitle, const bool enableOpenGl):
	applicationContext(applicationContext),
	windowTitle(windowTitle),
	windowSize(800, 600),
	enableOpengl(enableOpenGl),
	fullscreenMode(Fullscreen::DESKTOP),
	fullscreen(false),
	window(nullptr),
	glContext(nullptr)
{
	assert(this->applicationContext != nullptr);

	this->log = this->applicationContext->createLogger();
	this->log.setName("SdlWindowView");
}

bool SdlWindowView::initialize(logic::IContext* /*context*/)
{
	/**
	 * Some SDL_GL flags have to be set before the window is created to
	 * prevent some serious issues (e.g. crashes). One known problem
	 * is SDL_GL_STENCIL_SIZE. If it is set after the window is created,
	 * the Android app will crash after being resumed from a paused state
	 * (no idea why).
	 */
	if (this->enableOpengl == true)
	{
		auto glMajor = 0;
		auto glMinor = 0;
		auto glProfile = 0;
#if NOX_OPENGL_DESKTOP && !NOX_OS_WINDOWS
		glMajor = NOX_OPENGL_VERSION_MAJOR;
		glMinor = NOX_OPENGL_VERSION_MINOR;
		glProfile = SDL_GL_CONTEXT_PROFILE_CORE;
#elif NOX_OPENGL_ES
		glMajor = NOX_OPENGL_VERSION_MAJOR;
		glMinor = NOX_OPENGL_VERSION_MINOR;
		glProfile = SDL_GL_CONTEXT_PROFILE_ES;
#endif

		if (glMajor != 0)
		{
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, glMajor);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, glMinor);
			this->log.verbose().format("Setting SDL OpenGL context version to %d.%d", glMajor, glMinor);
		}

		if (glProfile != 0)
		{
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, glProfile);

			auto profileName = std::string("unknown");
			if (glProfile == SDL_GL_CONTEXT_PROFILE_ES)
			{
				profileName = "ES";
			}
			else if (glProfile == SDL_GL_CONTEXT_PROFILE_CORE)
			{
				profileName = "core";
			}
			else if (glProfile == SDL_GL_CONTEXT_PROFILE_COMPATIBILITY)
			{
				profileName = "compatibility";
			}

			this->log.verbose().format("Setting SDL OpenGL context profile to %s", profileName.c_str());
		}

		if (app::platform::isMobile())
		{
			// Workaround for some Android devices (e.g. Samsung Galaxy S3) rendering everything with a red tint.
			// See SDL bug: https://bugzilla.libsdl.org/show_bug.cgi?id=2291#c105
			SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
			SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 6);
			SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
		}
	}

	Uint32 windowFlags = SDL_WINDOW_RESIZABLE;
	if (this->enableOpengl == true)
	{
		windowFlags |= SDL_WINDOW_OPENGL;
	}

	if (app::platform::isMobile())
	{
		this->fullscreen = true;
		this->fullscreenMode = Fullscreen::DESKTOP;
		this->windowSize = this->getDisplayResolution();
		windowFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}

	this->window = SDL_CreateWindow(
		this->windowTitle.c_str(),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		static_cast<int>(this->windowSize.x),
		static_cast<int>(this->windowSize.y),
		windowFlags);

	if (this->window == nullptr)
	{
		this->log.fatal().format("Failed creating window: %s", SDL_GetError());
		return false;
	}
	else
	{
		this->log.verbose().raw("SDL window created.");
	}

	if (this->enableOpengl == true)
	{
		this->glContext = SDL_GL_CreateContext(this->window);

		if (this->glContext == nullptr)
		{
			this->log.fatal().format("Failed creating OpenGL context: %s", SDL_GetError());
			return false;
		}
		else
		{
			this->log.verbose().raw("SDL OpenGL context created.");
		}
	}

	if (this->onWindowCreated(this->window) == false)
	{
		return false;
	}

	this->onWindowSizeChanged(this->windowSize);

	return true;
}

void SdlWindowView::destroy()
{
	this->onDestroy();

	if (this->glContext != nullptr)
	{
		SDL_GL_DeleteContext(this->glContext);
		this->glContext = nullptr;
		this->log.verbose().raw("Destroyed SDL OpenGL context.");
	}

	if (this->window != nullptr)
	{
		SDL_DestroyWindow(this->window);
		this->window = nullptr;
		this->log.verbose().raw("Destroyed SDL window.");
	}

	this->log.verbose().raw("Quit SDL video and events subsystems.");
}

void SdlWindowView::update(const Duration& deltaTime)
{
	this->onUpdate(deltaTime);
}

void SdlWindowView::setWindowSize(const glm::uvec2& size)
{
	this->windowSize = size;

	if (this->window != nullptr)
	{
		SDL_SetWindowSize(this->window, static_cast<int>(size.x), static_cast<int>(size.y));
		this->onWindowSizeChanged(this->windowSize);
	}
}

const glm::uvec2& SdlWindowView::getWindowSize() const
{
	return this->windowSize;
}

void SdlWindowView::setFullscreenMode(const Fullscreen fullscreenMode)
{
	const bool modeChanged = fullscreenMode != this->fullscreenMode;

	this->fullscreenMode = fullscreenMode;

	if (modeChanged && this->fullscreen == true)
	{
		this->fullscreen = false;
		this->enableFullscreen();
	}
}

void SdlWindowView::enableFullscreen()
{
	if (this->fullscreen == false)
	{
		this->fullscreen = true;

		Uint32 mode = 0;
		if (this->fullscreenMode == Fullscreen::REAL)
		{
			mode = SDL_WINDOW_FULLSCREEN;
		}
		else if (this->fullscreenMode == Fullscreen::DESKTOP)
		{
			mode = SDL_WINDOW_FULLSCREEN_DESKTOP;
		}

		SDL_SetWindowFullscreen(this->window, mode);
	}
}

void SdlWindowView::disableFullscreen()
{
	if (this->fullscreen == true)
	{
		this->fullscreen = false;
		SDL_SetWindowFullscreen(this->window, 0);
	}
}

bool SdlWindowView::isFullscreen() const
{
	return this->fullscreen;
}

glm::uvec2 SdlWindowView::getDisplayResolution() const
{
	SDL_DisplayMode displayMode;

	const auto displayIndex = 0;
	if (SDL_GetDesktopDisplayMode(displayIndex, &displayMode) < 0)
	{
		this->log.error().format("Could not get the desktop display mode: %s", SDL_GetError());
		return {0u, 0u};
	}
	else
	{
		assert(displayMode.w >= 0 && displayMode.h >= 0);
		auto resolution = glm::uvec2(static_cast<unsigned int>(displayMode.w), static_cast<unsigned int>(displayMode.h));
		return resolution;
	}
}

void SdlWindowView::onUpdate(const Duration& /*deltaTime*/)
{
}

void SdlWindowView::onSdlEvent(const SDL_Event& event)
{
	const auto windowId = SDL_GetWindowID(this->window);

	if (event.type == SDL_EventType::SDL_WINDOWEVENT)
	{
		if (event.window.event == SDL_WindowEventID::SDL_WINDOWEVENT_RESIZED)
		{
			if (event.window.data1 >= 0 && event.window.data2 >= 0)
			{
				this->windowSize.x = static_cast<unsigned int>(event.window.data1);
				this->windowSize.y = static_cast<unsigned int>(event.window.data2);

				this->log.debug().format("Window resized: %s", glm::to_string(this->windowSize).c_str());
				this->onWindowSizeChanged(this->windowSize);
			}
			else
			{
				this->log.error().format("Invalid window size from resize event, ignoring: (%d, %d)", event.window.data1, event.window.data2);
			}
		}
	}
	else if (event.type == SDL_EventType::SDL_MOUSEBUTTONDOWN && event.button.windowID == windowId)
	{
		this->onMousePress(event.button);
	}
	else if (event.type == SDL_EventType::SDL_MOUSEBUTTONUP && event.button.windowID == windowId)
	{
		this->onMouseRelease(event.button);
	}
	else if (event.type == SDL_EventType::SDL_MOUSEMOTION && event.motion.windowID == windowId)
	{
		this->onMouseMove(event.motion);
	}
	else if (event.type == SDL_EventType::SDL_MOUSEWHEEL && event.wheel.windowID == windowId)
	{
		this->onMouseScroll(event.wheel);
	}
	else if (event.type == SDL_EventType::SDL_KEYDOWN && event.key.windowID == windowId)
	{
		this->onKeyPress(event.key);
	}
	else if (event.type == SDL_EventType::SDL_KEYUP && event.key.windowID == windowId)
	{
		this->onKeyRelease(event.key);
	}
	else if (event.type == SDL_EventType::SDL_JOYAXISMOTION)
	{
		this->onJoyAxisMotion(event.jaxis);
	}
	else if (event.type == SDL_EventType::SDL_JOYDEVICEADDED)
	{
		this->onJoyDeviceAdded(event.jdevice);
	}
	else if (event.type == SDL_EventType::SDL_JOYDEVICEREMOVED)
	{
		this->onJoyDeviceRemoved(event.jdevice);
	}
}

void SdlWindowView::onWindowSizeChanged(const glm::uvec2& /*size*/)
{
}

void SdlWindowView::onMousePress(const SDL_MouseButtonEvent& /*event*/)
{
}

void SdlWindowView::onMouseRelease(const SDL_MouseButtonEvent& /*event*/)
{
}

void SdlWindowView::onMouseMove(const SDL_MouseMotionEvent& /*event*/)
{
}

void SdlWindowView::onKeyPress(const SDL_KeyboardEvent& /*event*/)
{
}

void SdlWindowView::onKeyRelease(const SDL_KeyboardEvent& /*event*/)
{
}

void SdlWindowView::onJoyAxisMotion(const SDL_JoyAxisEvent& /*event*/)
{
}

void SdlWindowView::onJoyDeviceAdded(const SDL_JoyDeviceEvent& /*event*/)
{
}

void SdlWindowView::onJoyDeviceRemoved(const SDL_JoyDeviceEvent& /*event*/)
{
}

void SdlWindowView::onMouseScroll(const SDL_MouseWheelEvent& /*event*/)
{
}

void SdlWindowView::onDestroy()
{
}

bool SdlWindowView::onWindowCreated(SDL_Window* /*window*/)
{
	return true;
}

std::string SdlWindowView::getTypeString() const
{
	return "SdlWindowView";
}

}
}
