/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/TextureQuad.h>

namespace nox { namespace app { namespace graphics {

GlPackedVertexAttributeDef TextureQuad::VertexAttribute::generateVertexAttributeDef(
	const GlVertexAttributeLocation positionLocation,
	const GlVertexAttributeLocation textureCoordinateLocation,
	const GlVertexAttributeLocation colorLocation,
	const GlVertexAttributeLocation luminanceLocation)
{
	return 	GlPackedVertexAttributeDef(
		sizeof(VertexAttribute),
		{
			{
				positionLocation,
				positionSize(),
				GlDataType::FLOAT,
				offsetof(VertexAttribute, position)
			},
			{
				textureCoordinateLocation,
				textureCoordinateSize(),
				GlDataType::FLOAT,
				offsetof(VertexAttribute, textureCoordinate)
			},
			{
				colorLocation,
				colorSize(),
				GlDataType::FLOAT,
				offsetof(VertexAttribute, color)
			},
			{
				luminanceLocation,
				luminanceSize(),
				GlDataType::FLOAT,
				offsetof(VertexAttribute, lightLuminance)
			}
		}
	);
}

GlPackedVertexAttributeDef TextureQuad::VertexAttribute::generateVertexAttributeDef(
	const GlVertexAttributeLocation positionLocation,
	const GlVertexAttributeLocation textureCoordinateLocation,
	const GlVertexAttributeLocation colorLocation)
{
	return generateVertexAttributeDef(
		positionLocation,
		textureCoordinateLocation,
		colorLocation,
		GlVertexAttributeLocation::Unused_t());
}

GlPackedVertexAttributeDef TextureQuad::VertexAttribute::generateVertexAttributeDef(
	const GlVertexAttributeLocation positionLocation,
	const GlVertexAttributeLocation textureCoordinateLocation)
{
	return generateVertexAttributeDef(
		positionLocation,
		textureCoordinateLocation,
		GlVertexAttributeLocation::Unused_t(),
		GlVertexAttributeLocation::Unused_t());
}

GlPackedVertexAttributeDef TextureQuad::VertexAttribute::generateVertexAttributeDef(
	const GlVertexAttributeLocation positionLocation)
{
	return generateVertexAttributeDef(
		positionLocation,
		GlVertexAttributeLocation::Unused_t(),
		GlVertexAttributeLocation::Unused_t(),
		GlVertexAttributeLocation::Unused_t());
}

TextureQuad::TextureQuad():
	atlasId(0)
{
}

TextureQuad::TextureQuad(unsigned int atlasId):
	atlasId(atlasId)
{
}

void TextureQuad::setRenderQuad(const TextureQuad::RenderQuad& quad)
{
	this->renderQuad = quad;
}

unsigned int TextureQuad::getAtlasId() const
{
	return this->atlasId;
}

const TextureQuad::RenderQuad& TextureQuad::getRenderQuad() const
{
	return this->renderQuad;
}

float TextureQuad::getWidth() const
{
	return (this->renderQuad.bottomRight.getPosition().x - this->renderQuad.bottomLeft.getPosition().x);
}

float TextureQuad::getHeight() const
{
	return (this->renderQuad.topRight.getPosition().y - this->renderQuad.bottomRight.getPosition().y);
}

TextureQuad::RenderQuad transform(const TextureQuad::RenderQuad& quad, const glm::mat4& matrix)
{
	TextureQuad::RenderQuad transformedQuad = quad;

	transformedQuad.bottomLeft.setPosition(matrix * quad.bottomLeft.getPosition());
	transformedQuad.bottomRight.setPosition(matrix * quad.bottomRight.getPosition());
	transformedQuad.topRight.setPosition(matrix * quad.topRight.getPosition());
	transformedQuad.topLeft.setPosition(matrix * quad.topLeft.getPosition());

	return transformedQuad;
}

TextureQuad::VertexAttribute::VertexAttribute():
	color(1.0),
	lightLuminance(LuminanceType(Vec4Type(1.0, 0.0, 0.0, 0.0)))
{
}

TextureQuad::RenderQuad makeRenderQuadFromBox(const math::Box<glm::vec2>& box)
{
	TextureQuad::RenderQuad quad;

	quad.bottomLeft.setPosition(box.getLowerLeft());
	quad.bottomRight.setPosition(box.getLowerRight());
	quad.topRight.setPosition(box.getUpperRight());
	quad.topLeft.setPosition(box.getUpperLeft());

	quad.bottomLeft.setTextureCoordinate({0.0f, 0.0f});
	quad.bottomRight.setTextureCoordinate({1.0f, 0.0f});
	quad.topRight.setTextureCoordinate({1.0f, 1.0f});
	quad.topLeft.setTextureCoordinate({0.0f, 1.0f});

	return quad;
}

} } }
