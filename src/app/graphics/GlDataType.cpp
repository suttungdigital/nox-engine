/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/GlDataType.h>

#include <cassert>

namespace nox { namespace app { namespace graphics {

GLenum glDataTypeToEnum(const GlDataType type)
{
	switch(type)
	{
	case GlDataType::BYTE:
		return GL_BYTE;
	case GlDataType::UNSIGNED_BYTE:
		return GL_UNSIGNED_BYTE;
	case GlDataType::SHORT:
		return GL_SHORT;
	case GlDataType::UNSIGNED_SHORT:
		return GL_UNSIGNED_SHORT;
	case GlDataType::FIXED:
		return GL_FIXED;
	case GlDataType::FLOAT:
		return GL_FLOAT;
	}

	assert(false);
	return GL_BYTE;
}

std::size_t glDataTypeBytes(const GlDataType type)
{
	switch(type)
	{
	case GlDataType::BYTE:
		return sizeof(GLbyte);
	case GlDataType::UNSIGNED_BYTE:
		return sizeof(GLubyte);
	case GlDataType::SHORT:
		return sizeof(GLshort);
	case GlDataType::UNSIGNED_SHORT:
		return sizeof(GLushort);
	case GlDataType::FIXED:
		return sizeof(GLfixed);
	case GlDataType::FLOAT:
		return sizeof(GLfloat);
	}

	assert(false);
	return 0;
}

} } }
