/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/BackgroundGradient.h>
#include <nox/app/graphics/RenderData.h>
#include <nox/app/graphics/opengl_utils.h>
#include <nox/app/graphics/TextureQuad.h>

namespace nox { namespace app
{
namespace graphics
{

struct Vertex
{
	Vertex() = default;
	Vertex(const glm::vec4& position, const glm::vec4& color):
		position(position),
		color(color)
	{}

	glm::vec4 position;
	glm::vec4 color;
};

BackgroundGradient::BackgroundGradient():
	alpha(1.0f),
	shader(0),
	dataChanged(true)
{
	this->gradientColors = {
		ColorPoint(0.0f, glm::vec3(0.0f)),
		ColorPoint(1.0f, glm::vec3(0.0f))
	};
}

void BackgroundGradient::addColorPoint(float position, const glm::vec3& color)
{
	auto pointIt = this->gradientColors.begin() + 1;
	const auto endIt = this->gradientColors.end() - 1;

	while (pointIt != endIt && pointIt->position < position)
	{
		++pointIt;
	}

	this->gradientColors.insert(pointIt, ColorPoint(position, color));

	this->dataChanged = true;
}

void BackgroundGradient::setBottomColor(const glm::vec3& color)
{
	if (this->gradientColors.front().color != color)
	{
		this->gradientColors.front().color = color;
		this->dataChanged = true;
	}
}

void BackgroundGradient::setTopColor(const glm::vec3& color)
{
	if (this->gradientColors.back().color != color)
	{
		this->gradientColors.back().color = color;
		this->dataChanged = true;
	}
}

void BackgroundGradient::setAlpha(const float alpha)
{
	if (this->alpha != alpha)
	{
		this->alpha = alpha;
		this->dataChanged = true;
	}
}

void BackgroundGradient::clearColorPoints()
{
	if (this->gradientColors.size() > 2)
	{
		this->gradientColors.erase(this->gradientColors.begin() + 1, this->gradientColors.end() - 1);
		this->dataChanged = true;
	}
}

void BackgroundGradient::setupRendering(RenderData& renderData, const GLuint shader)
{
	this->shader = shader;

	this->renderModel = GlVertexModel(renderData.getState());

	this->renderAttributes = this->renderModel.createPackedVertexAttributes(
			GL_DYNAMIC_DRAW,
			GlPackedVertexAttributeDef(
				sizeof(Vertex),
				{
					{
						0,
						static_cast<std::size_t>(decltype(Vertex::position)().length()),
						GlDataType::FLOAT,
						offsetof(Vertex, position)
					},
					{
						1,
						static_cast<std::size_t>(decltype(Vertex::color)().length()),
						GlDataType::FLOAT,
						offsetof(Vertex, color)
					}
				}
			));

	this->colorMultiplierUniform = glGetUniformLocation(this->shader, "colorMultiplier");
	assert(this->colorMultiplierUniform >= 0);

	this->colorMutiplierChanged = true;
}

void BackgroundGradient::handleIo(RenderData& renderData)
{
	std::vector<Vertex> coords(this->gradientColors.size() * 2);
	std::vector<Vertex>::size_type currentCoordIndex = 0;

	for (const ColorPoint& point : this->gradientColors)
	{
		const float height = (point.position * 2.0f) - 1.0f;
		const glm::vec4 color(point.color, alpha);

		coords[currentCoordIndex] = Vertex({-1.0f, height, 0.0f, 1.0f}, color);
		currentCoordIndex++;

		coords[currentCoordIndex] = Vertex({1.0f, height, 0.0f, 1.0f}, color);
		currentCoordIndex++;
	}

	this->renderAttributes->getBuffer().overwriteData(coords, renderData.getState());
	this->renderModel.setNumVertices(coords.size());

	this->dataChanged = false;
}

void BackgroundGradient::render(RenderData& renderData)
{
	renderData.bindShaderProgram(this->shader);

	if (this->colorMutiplierChanged == true)
	{
		glUniform1f(this->colorMultiplierUniform, this->colorMultiplier);
		this->colorMutiplierChanged = false;
	}

	this->renderModel.draw(GL_TRIANGLE_STRIP);
}

bool BackgroundGradient::needsBufferUpdate() const
{
	return this->dataChanged;
}

void BackgroundGradient::setColorMultiplier(const float multiplier)
{
	if (multiplier != this->colorMultiplier)
	{
		this->colorMultiplier = multiplier;
		this->colorMutiplierChanged = true;
	}
}

BackgroundGradient::ColorPoint::ColorPoint(const float position, const glm::vec3& color):
	position(position),
	color(color)
{
}

}
} }
