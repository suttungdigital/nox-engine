/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/SceneGraphNode.h>
#include <nox/app/graphics/RenderData.h>
#include <nox/util/algorithm.h>

namespace nox { namespace app
{
namespace graphics
{

SceneGraphNode::SceneGraphNode():
	parent(nullptr),
	currentRenderer(nullptr)
{
}

SceneGraphNode::~SceneGraphNode()
{
    this->children.clear();
}

void SceneGraphNode::addChild(const std::shared_ptr<SceneGraphNode>& child)
{
	if (this->currentRenderer != nullptr)
	{
		child->setCurrentRenderer(this->currentRenderer);
	}

	child->parent = this;

	this->children.push_back(child);
}

void SceneGraphNode::removeChild(const std::shared_ptr<SceneGraphNode>& childToBeRemoved)
{
	auto childIt = std::find(this->children.begin(), this->children.end(), childToBeRemoved);

	if (childIt != this->children.end())
	{
		(*childIt)->removeCurrentRenderer();
		(*childIt)->onDetachedFromParent();

		util::eraseFast(this->children, childIt);
	}
}

void SceneGraphNode::removeChildren()
{
	for (const auto& child : this->children)
	{
		child->removeCurrentRenderer();
		child->onDetachedFromParent();
	}

	this->children.clear();
}

void SceneGraphNode::onTraverse(TextureRenderer& renderData, glm::mat4x4& modelMatrix)
{
    this->onNodeEnter(renderData, modelMatrix);
    
    for (auto& child : this->children)
    {
        child->onTraverse(renderData, modelMatrix);
    }

    this->onNodeLeave(renderData, modelMatrix);
}

void SceneGraphNode::onAttachToRenderer(IRenderer* /*renderer*/)
{
}

void SceneGraphNode::onNodeLeave(TextureRenderer& /*renderData*/, glm::mat4x4& /*modelMatrix*/)
{
}

void SceneGraphNode::onDetachedFromRenderer()
{
}

void SceneGraphNode::setCurrentRenderer(IRenderer* renderer)
{
	if (this->currentRenderer != renderer)
	{
		this->currentRenderer = renderer;
		this->onAttachToRenderer(renderer);

		for (auto child : this->children)
		{
			child->setCurrentRenderer(renderer);
		}
	}
}

void SceneGraphNode::removeCurrentRenderer()
{
	if (this->currentRenderer != nullptr)
	{
		this->onDetachedFromRenderer();

		this->currentRenderer = nullptr;
		for (auto& child : this->children)
		{
			child->removeCurrentRenderer();
		}
	}
}


SceneGraphNode* SceneGraphNode::getParent()
{
	return this->parent;
}


void SceneGraphNode::onDetachedFromParent()
{
	this->parent = nullptr;
}

IRenderer* SceneGraphNode::getCurrentRenderer()
{
	return this->currentRenderer;
}

bool SceneGraphNode::hasNodeInTree(const std::shared_ptr<SceneGraphNode>& node) const
{
	bool nodeFound = false;
	auto childIt = this->children.begin();

	while (nodeFound == false && childIt != this->children.end())
	{
		if (*childIt == node)
		{
			nodeFound = true;
		}
		else
		{
			nodeFound = (*childIt)->hasNodeInTree(node);
		}

		childIt++;
	}

	return nodeFound;
}

}
} }
