/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/TiledTextureGenerator.h>

#include <nox/app/graphics/2d/TiledTextureRenderer.h>
#include <nox/app/graphics/TextureManager.h>

namespace nox { namespace app { namespace graphics {

TiledTextureGenerator::TiledTextureGenerator(const TextureManager& textureManager, TiledTextureRenderer* renderer) :
	textureManager(textureManager),
	tileRenderer(renderer),
	quadSize(10.0f, 10.0f),
	tileColor(1.0f),
	currentMinTiles(0, 0),
	currentMaxTiles(0, 0),
	lightMultiplier(1.0f),
	emissiveLight(0.0f),
	textureExtrudeMultiplier(1.0f)
{
}

TiledTextureGenerator::~TiledTextureGenerator()
{
}

void TiledTextureGenerator::onUpdate(glm::vec2 currentPosition, glm::vec2 currentCoverage)
{
	glm::vec2 lowerLeftBounds = currentPosition - (currentCoverage / 2.0f);
	glm::vec2 upperRightBounds = currentPosition + (currentCoverage / 2.0f);

	if (this->needToUpdateTexture(lowerLeftBounds, upperRightBounds) && this->textures.empty() == false)
	{
		this->generateTexture();
		this->tileRenderer->updateTextureQuads(&this->textureQuads);
	}
}

bool TiledTextureGenerator::needToUpdateTexture(glm::vec2 lowerLeftBounds, glm::vec2 upperRightBounds)
{
	bool needToUpdate = false;

	auto tilesThatWillFitFractionMin = glm::ivec2(lowerLeftBounds/this->quadSize);

	if (tilesThatWillFitFractionMin.x < 0)
	{
		tilesThatWillFitFractionMin.x--;
	}

	if (tilesThatWillFitFractionMin.y < 0)
	{
		tilesThatWillFitFractionMin.y--;
	}

	const auto tilesThatWillCoverAreaMin = glm::ivec2(tilesThatWillFitFractionMin.x - 1, tilesThatWillFitFractionMin.y - 1);
	if (this->currentMinTiles != tilesThatWillCoverAreaMin)
	{
		this->currentMinTiles = tilesThatWillCoverAreaMin;
		needToUpdate = true;
	}

	const auto tilesThatWillFitFractionMax = (upperRightBounds/this->quadSize);
	const auto tilesThatWillCoverAreaMax = glm::ivec2(tilesThatWillFitFractionMax.x + 3, tilesThatWillFitFractionMax.y + 3);
	if (this->currentMaxTiles != tilesThatWillCoverAreaMax)
	{
		this->currentMaxTiles = tilesThatWillCoverAreaMax;
		needToUpdate = true;
	}

	return needToUpdate;
}

void TiledTextureGenerator::generateTexture()
{
	using SizeType = std::vector<TextureQuad::RenderQuad>::size_type;

	const SizeType numTiles = static_cast<SizeType>((currentMaxTiles.x - currentMinTiles.x) * (currentMaxTiles.y - currentMinTiles.y));
	this->textureQuads.resize(numTiles);

	SizeType index = 0;

	for (int y = currentMinTiles.y; y < currentMaxTiles.y ; y++)
	{
		for (int x = currentMinTiles.x; x < currentMaxTiles.x; x++)
		{
			float actualX = static_cast<float>(x);
			float actualY = static_cast<float>(y);

			const glm::vec2 bottomLeft = glm::vec2(this->quadSize.x * actualX, this->quadSize.y * actualY);

			actualX = static_cast<float>(x + 1);
			actualY = static_cast<float>(y + 1);

			const glm::vec2 topRight = glm::vec2(this->quadSize.x * actualX, this->quadSize.y * actualY);

			TextureQuad::RenderQuad& quad = this->textureQuads[index];

			quad = textures[0].getRenderQuad();

			quad.bottomLeft.setPosition(bottomLeft);
			quad.bottomRight.setPosition({topRight.x, bottomLeft.y});
			quad.topRight.setPosition(topRight);
			quad.topLeft.setPosition({bottomLeft.x, topRight.y});

			const auto texLeft = quad.bottomLeft.getTextureCoordinate().x ;
			const auto texBottom = quad.bottomLeft.getTextureCoordinate().y;
			const auto texRight = quad.topRight.getTextureCoordinate().x;
			const auto texTop = quad.topRight.getTextureCoordinate().y;

			const auto texWidth = texRight - texLeft;
			const auto texHeight = texTop - texBottom;

			const auto texTransform = glm::vec2(-texLeft - texWidth / 2.0f, -texBottom - texHeight / 2.0f);

			const auto transTexLeft = ((texLeft + texTransform.x) / this->textureExtrudeMultiplier) - texTransform.x;
			const auto transTexBottom = ((texBottom + texTransform.y) / this->textureExtrudeMultiplier) - texTransform.y;
			const auto transTexRight = ((texRight + texTransform.x) / this->textureExtrudeMultiplier) - texTransform.x;
			const auto transTexTop = ((texTop + texTransform.y) / this->textureExtrudeMultiplier) - texTransform.y;

			quad.bottomLeft.setTextureCoordinate({transTexLeft, transTexBottom});
			quad.bottomRight.setTextureCoordinate({transTexRight, transTexBottom});
			quad.topRight.setTextureCoordinate({transTexRight, transTexTop});
			quad.topLeft.setTextureCoordinate({transTexLeft, transTexTop});

			quad.setColor(this->tileColor);

			quad.setLightMultiplier(this->lightMultiplier);
			quad.setEmissiveLight(this->emissiveLight);

			++index;
		}
	}
}

bool TiledTextureGenerator::addTextureTile(const std::string& textureName)
{
	if (this->textureManager.hasTexture(textureName))
	{
		this->textures.push_back(this->textureManager.getTexture(textureName));
		return true;
	}
	else
	{
		return false;
	}
}

void TiledTextureGenerator::setTileSize(const glm::vec2& size)
{
	this->quadSize = size;
}

void TiledTextureGenerator::setTileRenderer(TiledTextureRenderer* renderer)
{
	this->tileRenderer = renderer;
}

void TiledTextureGenerator::setTileColor(const glm::vec4& color)
{
	this->tileColor = color;
}

void TiledTextureGenerator::setTileExtrudePercentage(const float extrudePercentage)
{
	this->textureExtrudeMultiplier = 1.0f + (extrudePercentage / 100.0f);
}

void TiledTextureGenerator::setTileLightMultiplier(const float multiplier)
{
	this->lightMultiplier = multiplier;
}

void TiledTextureGenerator::setTileEmissiveLight(const float emissiveLight)
{
	this->emissiveLight = emissiveLight;
}

} } }
