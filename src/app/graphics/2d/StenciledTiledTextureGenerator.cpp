/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/StenciledTiledTextureGenerator.h>

#include <nox/app/graphics/2d/StenciledTiledTextureRenderer.h>
#include <nox/app/graphics/TextureManager.h>

namespace nox { namespace app
{
namespace graphics
{

StenciledTiledTextureGenerator::StenciledTiledTextureGenerator(const TextureManager& textureManager, StenciledTiledTextureRenderer* renderer) :
	TiledTextureGenerator(textureManager, renderer)
{
	this->stencilRenderer = renderer;
}

StenciledTiledTextureGenerator::~StenciledTiledTextureGenerator()
{
}

void StenciledTiledTextureGenerator::addStencilGeometry(const std::shared_ptr<GeometrySet>& geometry)
{
	this->stencilRenderer->addStencilGeometry(geometry);
}

void StenciledTiledTextureGenerator::removeStencilGeometry(const std::shared_ptr<GeometrySet>& geometry)
{
	this->stencilRenderer->removeStencilGeometry(geometry);
}

void StenciledTiledTextureGenerator::notifyGeometryChange()
{
	this->stencilRenderer->notifyGeometryChange();
}

}
} }
