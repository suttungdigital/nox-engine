/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/WrappedSpriteRenderNode.h>

#include <nox/app/graphics/2d/OpenGlRenderer.h>
#include <nox/util/math/vector_math.h>
#include <nox/util/math/angle.h>

#include <cassert>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

namespace nox { namespace app
{
namespace graphics
{

WrappedSpriteRenderNode::WrappedSpriteRenderNode() :
	levelID(0),
	color(1.0f, 1.0f, 1.0f, 1.0f),
	centerHeight(0.5f),
	needUpdate(false),
	leftPadding(0.0f),
	rightPadding(0.0f)
{
}

void WrappedSpriteRenderNode::onNodeEnter(TextureRenderer& renderData, glm::mat4x4& modelMatrix)
{
	if (modelMatrix != this->previousMatrix || this->needUpdate == true)
	{
		std::vector<TextureQuad::RenderQuad> transformedQuads;
		for (const TextureQuad::RenderQuad& quad : this->textureQuads)
		{
			TextureQuad::RenderQuad transformedQuad = transform(quad, modelMatrix);

			transformedQuad.setColor(this->color);

			transformedQuads.push_back(transformedQuad);
		}
		
		renderData.setData(this->spriteDataHandle, transformedQuads);

		this->previousMatrix = modelMatrix;
	}
}

void WrappedSpriteRenderNode::onAttachToRenderer(IRenderer* renderer)
{
	assert(this->localWrapPositions.size() >= 2);

	this->textureQuads.clear();

	this->addPaddingToLocalPositions();

	const TextureManager& spriteManager = renderer->getTextureManager();

	this->sprite = spriteManager.getTexture(spriteName);
	const TextureQuad& textureQuad = this->sprite;

	const float texLeft = textureQuad.getRenderQuad().bottomLeft.getTextureCoordinate().x;
	const float texBottom = textureQuad.getRenderQuad().bottomLeft.getTextureCoordinate().y;
	const float texTop = textureQuad.getRenderQuad().topLeft.getTextureCoordinate().y;

	const float textureCoordWidth = textureQuad.getRenderQuad().bottomRight.getTextureCoordinate().x - texLeft;

	const float geometryBottom = textureQuad.getRenderQuad().bottomLeft.getPosition().y;
	const float geometryTop = textureQuad.getRenderQuad().topLeft.getPosition().y;
	const float geometryHeight = geometryTop - geometryBottom;

	float totalDistance = 0.0f;

	float currentTexLeft = texLeft;


	for (size_t i = 0; i < this->localWrapPositions.size() - 1u; i++)
	{
		totalDistance += glm::distance(this->localWrapPositions[i], this->localWrapPositions[i + 1u]);
	}

	std::vector<PointData> pointsData;
	for (size_t i = 0; i < this->localWrapPositions.size(); i++)
	{
		glm::vec2 perpendicular;

		if (i == 0)
		{
			const glm::vec2 diff = this->localWrapPositions[i + 1u] - this->localWrapPositions[i];
			perpendicular = glm::vec2(-diff.y, diff.x);
		}
		else if (i == this->localWrapPositions.size() - 1u)
		{
			const glm::vec2 diff = this->localWrapPositions[i] - this->localWrapPositions[i - 1u];
			perpendicular = glm::vec2(-diff.y, diff.x);
		}
		else
		{
			const glm::vec2 previousDiff = glm::normalize(this->localWrapPositions[i] - this->localWrapPositions[i - 1u]);
			const glm::vec2 previousPerpendicular(-previousDiff.y, previousDiff.x);

			const glm::vec2 nextDiff = glm::normalize(this->localWrapPositions[i + 1u] - this->localWrapPositions[i]);
			const glm::vec2 nextPerpendicular(-nextDiff.y, nextDiff.x);

			perpendicular = (nextPerpendicular + previousPerpendicular) * 0.5f;
		}

		const float upperY = geometryHeight - (geometryHeight * this->centerHeight);
		const float bottomY = (geometryHeight - ((geometryHeight * this->centerHeight))) - geometryHeight;

		PointData pointData;
		pointData.point = this->localWrapPositions[i];

		pointData.lower = glm::vec2(0.0f, bottomY);
		pointData.upper = glm::vec2(0.0f, upperY);

		pointData.upper = glm::rotate(pointData.upper, math::vectorAngle(perpendicular) - math::quarterCircle<float>());
		pointData.lower = glm::rotate(pointData.lower, math::vectorAngle(perpendicular) - math::quarterCircle<float>());

		pointData.upper += pointData.point;
		pointData.lower += pointData.point;

		pointsData.push_back(pointData);
	}

	for (size_t i = 0; i < this->localWrapPositions.size() - 1u; i++)
	{
		TextureQuad::RenderQuad quad;

		const glm::vec2& currentPoint = this->localWrapPositions[i];
		const glm::vec2& nextPoint = this->localWrapPositions[i + 1u];

		const float texRight = currentTexLeft + glm::distance(currentPoint, nextPoint) * (textureCoordWidth / totalDistance);

		quad.bottomLeft.setTextureCoordinate({currentTexLeft, texBottom});
		quad.bottomLeft.setPosition(pointsData[i].lower);

		quad.bottomRight.setTextureCoordinate({texRight, texBottom});
		quad.bottomRight.setPosition(pointsData[i + 1u].lower);

		quad.topRight.setTextureCoordinate({texRight, texTop});
		quad.topRight.setPosition(pointsData[i + 1u].upper);

		quad.topLeft.setTextureCoordinate({currentTexLeft, texTop});
		quad.topLeft.setPosition(pointsData[i].upper);

		this->textureQuads.push_back(quad);

		currentTexLeft = texRight;
	}

	this->spriteDataHandle = renderer->getTextureRenderer().requestDataSpace(static_cast<unsigned int>(this->textureQuads.size()), this->levelID);
}

void WrappedSpriteRenderNode::setRenderLevel(const unsigned int level)
{
	this->levelID = level;

	if (this->spriteDataHandle.isValid())
	{
		this->getCurrentRenderer()->getTextureRenderer().deleteData(this->spriteDataHandle);
		this->spriteDataHandle = this->getCurrentRenderer()->getTextureRenderer().requestDataSpace(1, this->levelID);
	}
}

void WrappedSpriteRenderNode::setSprite(const std::string& spriteName)
{
	this->spriteName = spriteName;
	this->needUpdate = true;
}

void WrappedSpriteRenderNode::setCenterHeight(const float centerHeight)
{
	this->centerHeight = centerHeight;
	this->needUpdate = true;
}

void WrappedSpriteRenderNode::setColor(const glm::vec4& color)
{
	this->color = color;
	this->needUpdate = true;
}

void WrappedSpriteRenderNode::onDetachedFromRenderer()
{
	this->getCurrentRenderer()->getTextureRenderer().deleteData(this->spriteDataHandle);
}


void WrappedSpriteRenderNode::setWrapping(const std::vector<glm::vec2>& localPositions)
{
	assert(localPositions.size() >= 2);

	this->localWrapPositions = localPositions;
}

void WrappedSpriteRenderNode::setPadding(const float leftPadding, const float rightPadding)
{
	this->leftPadding = leftPadding;
	this->rightPadding = rightPadding;
}


void WrappedSpriteRenderNode::addPaddingToLocalPositions()
{
	if (this->rightPadding == 0.0f && this->leftPadding == 0.0f)
	{
		return;
	}

	const glm::vec2 firstPosition = this->localWrapPositions.front();
	const glm::vec2 lastPosition = this->localWrapPositions.back();

	glm::vec2 leftPaddingVector;
	glm::vec2 rightPaddingVector;

	if (this->localWrapPositions.size() == 2u)
	{
		leftPaddingVector = firstPosition - lastPosition;
		rightPaddingVector = lastPosition - firstPosition;
	}
	else if (this->localWrapPositions.size() == 3u)
	{
		const glm::vec2 centerPosition = this->localWrapPositions[1u];

		leftPaddingVector = firstPosition - centerPosition;
		rightPaddingVector = lastPosition - centerPosition;
	}
	else
	{
		const glm::vec2 afterFirstPosition = this->localWrapPositions[1u];
		const glm::vec2 beforeLastPosition = this->localWrapPositions[this->localWrapPositions.size() - 2u];

		leftPaddingVector = firstPosition - afterFirstPosition;
		rightPaddingVector = lastPosition - beforeLastPosition;
	}

	if (this->leftPadding != 0.0f)
	{
		const glm::vec2 leftPaddingPosition = firstPosition + (this->leftPadding * glm::normalize(leftPaddingVector));
		this->localWrapPositions.insert(this->localWrapPositions.begin(), leftPaddingPosition);
	}

	if (this->rightPadding != 0.0f)
	{
		const glm::vec2 rightPaddingPosition = lastPosition + (this->rightPadding * glm::normalize(rightPaddingVector));
		this->localWrapPositions.push_back(rightPaddingPosition);
	}
}

}
} }
