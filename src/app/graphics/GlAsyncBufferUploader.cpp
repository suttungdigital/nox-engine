/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/GlAsyncBufferUploader.h>

#include <nox/app/graphics/GlBuffer.h>

#include <cstring>

namespace nox { namespace app { namespace graphics {

GlAsyncBufferUploader::GlAsyncBufferUploader():
	GlBufferUploader()
{
}

GlAsyncBufferUploader::GlAsyncBufferUploader(nox::log::Logger log):
	GlBufferUploader(std::move(log))
{
}

GlAsyncBufferUploader::GlAsyncBufferUploader(GlAsyncBufferUploader&& other):
	GlBufferUploader(std::move(other))
{
}

GlAsyncBufferUploader& GlAsyncBufferUploader::operator=(GlAsyncBufferUploader&& other)
{
	GlBufferUploader::operator=(std::move(other));

	return *this;
}

void GlAsyncBufferUploader::resizeBuffer(GlBuffer& buffer, const std::size_t bufferSize, GlContextState* state)
{
	buffer.bind(state);
	glBufferData(buffer.getType(), static_cast<GLsizeiptr>(bufferSize), nullptr, buffer.getUsage());
}

void GlAsyncBufferUploader::uploadData(GlBuffer& buffer, const void* data, const std::size_t dataSize, GlContextState* state)
{
	this->uploadSubData(buffer, data, dataSize, 0, state);
}

void GlAsyncBufferUploader::uploadSubData(GlBuffer& buffer, const void* data, const std::size_t dataSize, const std::size_t dstStart, GlContextState* state)
{
	const auto maxTries = 2u;
	auto tries = 0u;
	auto dataIsGood = false;

	const auto target = buffer.getType();
	const auto offset = static_cast<GLintptr>(dstStart);
	const auto length = static_cast<GLsizeiptr>(dataSize);
	const auto flags = GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT | GL_MAP_INVALIDATE_RANGE_BIT;

	buffer.bind(state);

	while (dataIsGood == false && tries < maxTries)
	{
		tries++;

		auto dataStore = glMapBufferRange(target, offset, length, flags);
		assert(dataStore != nullptr);
		memcpy(dataStore, data, dataSize);
		dataIsGood = (glUnmapBuffer(target) == GL_TRUE) ? true : false;

		if (dataIsGood == false)
		{
			if (tries < maxTries)
			{
				this->getLog().debug().format("Data store needed to be reinitialized. %u tries out of %u", tries, maxTries);
			}
			else
			{
				this->getLog().warning().format("Uploading data might have failed. Tried %u times.", tries);
			}
		}
	}
}

} } }
