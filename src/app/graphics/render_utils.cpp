/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/render_utils.h>

#include <limits>
#include <array>

namespace nox { namespace app
{
namespace graphics
{

math::Box<glm::vec2> findBoundingBox(const TextureQuad::RenderQuad& renderQuad)
{
	const glm::vec2 max(std::numeric_limits<glm::vec2::value_type>::max());
	const glm::vec2 min(std::numeric_limits<glm::vec2::value_type>::lowest());

	math::Box<glm::vec2> bound(max, min);

	std::array<glm::vec2, 4> quadCorners
	{{
			glm::vec2(renderQuad.bottomLeft.getPosition()),
			glm::vec2(renderQuad.bottomRight.getPosition()),
			glm::vec2(renderQuad.topRight.getPosition()),
			glm::vec2(renderQuad.topLeft.getPosition())
	}};

	for (const glm::vec2& corner : quadCorners)
	{
		if (corner.x < bound.lowerBound.x)
		{
			bound.lowerBound.x = corner.x;
		}

		if (corner.y < bound.lowerBound.y)
		{
			bound.lowerBound.y = corner.y;
		}

		if (corner.x > bound.upperBound.x)
		{
			bound.upperBound.x = corner.x;
		}

		if (corner.y > bound.upperBound.y)
		{
			bound.upperBound.y = corner.y;
		}
	}

	return bound;
}

}
} }
