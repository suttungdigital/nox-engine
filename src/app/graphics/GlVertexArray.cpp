/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/GlVertexArray.h>

namespace nox { namespace app { namespace graphics {

GlVertexArray::GlVertexArray():
	name(0)
{
}

GlVertexArray::GlVertexArray(const GLuint name):
	name(name)
{
}

GlVertexArray::~GlVertexArray()
{
	if (this->name != 0)
	{
		glDeleteVertexArrays(1, &this->name);
	}
}

GlVertexArray::GlVertexArray(GlVertexArray&& other):
	name(other.name)
{
	other.name = 0;
}

GlVertexArray& GlVertexArray::operator=(GlVertexArray&& other)
{
	this->name = other.name;
	other.name = 0;

	return *this;
}

void GlVertexArray::bind(GlContextState* contextState)
{
	contextState->bindVertexArray(this->name);
}

GlVertexArray GlVertexArray::generate()
{
	auto name = GLuint(0);
	glGenVertexArrays(1, &name);

	return GlVertexArray(name);
}

} } }
