/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/GlVertexModel.h>
#if NOX_OPENGL_SUPPORT_VAO
#include <nox/app/graphics/GlVertexModelStorageVao.h>
#endif

#include <fmt/format.h>

namespace nox { namespace app { namespace graphics {

GlVertexModel::GlVertexModel(GlContextState* glState, std::unique_ptr<GlVertexModelStorage> modelStorage):
	glState(glState),
	numVertices(0),
	storage(std::move(modelStorage))
{
}

GlVertexModel::GlVertexModel(GlContextState* glState):
	GlVertexModel(glState, std::make_unique<GlDefaultVertexModelStorage>())
{
}

GlVertexModel::GlVertexModel():
	GlVertexModel(nullptr, nullptr)
{
}


GlVertexModel::~GlVertexModel()
{
}

GlVertexModel::GlVertexModel(GlVertexModel&& other):
	glState(other.glState),
	numVertices(other.numVertices),
	storage(std::move(other.storage))
{
}

GlVertexModel& GlVertexModel::operator=(GlVertexModel&& other)
{
	this->glState = other.glState;
	this->numVertices = other.numVertices;
	this->storage = std::move(other.storage);

	return *this;
}

void GlVertexModel::bind()
{
	this->storage->bind(this->glState);
}

void GlVertexModel::draw(const GLenum drawMode)
{
	this->drawRange(drawMode, 0, this->numVertices);
}

void GlVertexModel::drawRange(const GLenum drawMode, const std::size_t firstVertex, const std::size_t numVertices)
{
	if (firstVertex + numVertices > this->numVertices)
	{
		auto message = fmt::format("Vertex draw range [{}, {}) is out of bounds. Num vertices is {}. Remember to call GlVertexModel::setNumVertices().", firstVertex, firstVertex + numVertices, this->numVertices);
		throw std::out_of_range(message);
	}

	if (numVertices > 0)
	{
		this->bind();
		glDrawArrays(drawMode, static_cast<GLint>(firstVertex), static_cast<GLsizei>(numVertices));
	}
}

void GlVertexModel::setNumVertices(const std::size_t num)
{
	this->numVertices = num;
}

std::size_t GlVertexModel::getNumVertices() const
{
	return this->numVertices;
}

GlVertexModelStorage* GlVertexModel::getStorage()
{
	return this->storage.get();
}

GlPackedVertexAttributes* GlVertexModel::createPackedVertexAttributes(const GLenum bufferUsage, const GlPackedVertexAttributeDef& packDef)
{
	return this->storage->createPackedVertexAttributes(bufferUsage, packDef);
}

} } }
