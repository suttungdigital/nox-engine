/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/common/platform.h>
#include <nox/app/Application.h>
#include <nox/app/ApplicationProcess.h>
#include <nox/app/audio/System.h>
#include <nox/log/OutputManager.h>
#include <nox/app/resource/cache/Cache.h>
#include <nox/app/storage/IDataStorage.h>
#include <nox/util/chrono_utils.h>

#if NOX_OS_ANDROID
#include <nox/log/LogcatOutput.h>
#else
#include <nox/log/OutputStream.h>
#endif

#include <thread>
#include <iostream>

namespace nox { namespace app
{

static void parseLogLevelArgs(const std::vector<std::string>& args, log::Output& output)
{
	for (auto argIt = args.begin(); argIt != args.end(); ++argIt)
	{
		const auto& arg = *argIt;

		if (arg == "--logall")
		{
			output.enableLogLevel(log::Message::allLevels());
		}
		else if (arg == "--lognone")
		{
			output.disableLogLevel(log::Message::allLevels());
		}
		else if (arg == "--logadd" || arg == "--logrm")
		{
			auto nextArgIt = argIt + 1;

			if (nextArgIt == args.end())
			{
				throw std::runtime_error(arg + ": No level provided as argument.");
			}

			const auto& levelString = *nextArgIt;
			auto level = log::Message::Level::INFO;

			if (levelString == "info")
			{
				level = log::Message::Level::INFO;
			}
			else if (levelString == "verbose")
			{
				level = log::Message::Level::VERBOSE;
			}
			else if (levelString == "error")
			{
				level = log::Message::Level::ERROR;
			}
			else if (levelString == "fatal")
			{
				level = log::Message::Level::FATAL;
			}
			else if (levelString == "warning")
			{
				level = log::Message::Level::WARNING;
			}
			else if (levelString == "debug")
			{
				level = log::Message::Level::DEBUG_;
			}
			else
			{
				throw std::runtime_error(arg + ": Level " + levelString + " is not valid.");
			}

			if (arg == "--logadd")
			{
				output.enableLogLevel(level);
			}
			else if (arg == "--logrm")
			{
				output.disableLogLevel(level);
			}
			else
			{
				assert(false);
			}
		}
	}
}

Application::Application(const std::string& applicationName, const std::string& organizationName):
	applicationName(applicationName),
	organizationName(organizationName),
	processManager(process::Manager::UpdateMode::FIXED_DELTATIME, util::secondsToDuration<Duration>(1.0f / 60.0f)),
	running(false),
	idle(false),
	tpsUpdateInterval(std::chrono::milliseconds(1000)),
	idleSleepTime(50),
	tps(0.0f)
{
}

Application::Application(Application&& other):
	logOutputManager(std::move(other.logOutputManager)),
	resourceCache(std::move(other.resourceCache)),
	dataStorage(std::move(other.dataStorage)),
	audioSystem(std::move(other.audioSystem)),
	applicationName(std::move(other.applicationName)),
	organizationName(std::move(other.organizationName)),
	applicationLog(std::move(other.applicationLog)),
	processManager(std::move(other.processManager)),
	running(other.running),
	idle(other.idle),
	tpsUpdateInterval(other.tpsUpdateInterval),
	idleSleepTime(other.idleSleepTime),
	tps(other.tps)
{
}

Application& Application::operator=(Application&& other)
{
	this->logOutputManager = std::move(other.logOutputManager);
	this->resourceCache = std::move(other.resourceCache);
	this->dataStorage = std::move(other.dataStorage);
	this->audioSystem = std::move(other.audioSystem);
	this->applicationName = std::move(other.applicationName);
	this->organizationName = std::move(other.organizationName);
	this->applicationLog = std::move(other.applicationLog);
	this->processManager = std::move(other.processManager);
	this->running = other.running;
	this->idle = other.idle;
	this->tpsUpdateInterval = other.tpsUpdateInterval;
	this->idleSleepTime = other.idleSleepTime;
	this->tps = other.tps;

	return *this;
}

Application::~Application() = default;

bool Application::init(int argc, char* argv[])
{
	const std::vector<std::string> args(argv, argv + argc);

	this->logOutputManager = std::unique_ptr<log::OutputManager>(new log::OutputManager());

	const std::string logFormat = "[${loglevel}][${timestamp}][${loggername}] ${message}";

#if NOX_OS_ANDROID
	auto logcatOutput = std::make_unique<log::LogcatOutput>();

# ifdef NOX_DEBUG
	logcatOutput->enableLogLevel(log::Message::allLevels());
# endif

	logcatOutput->setOutputFormat(logFormat);
	this->logOutputManager->addLogOutput(std::move(logcatOutput));
#else
	std::unique_ptr<log::OutputStream> standardOutput(new log::OutputStream());
	standardOutput->setOutputFormat(logFormat);
	standardOutput->addOutputStream(&std::cout);

	parseLogLevelArgs(args, *standardOutput);

	this->logOutputManager->addLogOutput(std::move(standardOutput));
#endif

	this->applicationLog.setOutputManager(this->logOutputManager.get());
	this->applicationLog.setName("NoxApplication");

	this->applicationLog.verbose().raw("Starting initialization.");

	if (this->onInit() == false)
	{
		this->applicationLog.fatal().raw("Failed initialization.");
		return false;
	}

	this->running = true;

	this->applicationLog.info().raw("Initialized.");

	return true;
}

void Application::shutdown()
{
	this->applicationLog.verbose().raw("Shutting down.");

	this->onDestroy();

	this->processManager.abortAllProcesses();

	this->applicationLog.verbose().raw("All processes aborted.");

	if (this->audioSystem != nullptr)
	{
		this->audioSystem->shutdown();
		this->audioSystem.reset();

		this->applicationLog.verbose().raw("Audio system shut down.");
	}

	if (this->resourceCache != nullptr)
	{
		this->resourceCache.reset();

		this->applicationLog.verbose().raw("Resource cache shut down.");
	}

	this->applicationLog.info().raw("Shut down.");
}

int Application::execute()
{
	using Clock = std::chrono::high_resolution_clock;

	const Clock::duration MAX_FRAME_TIME(std::chrono::milliseconds(50));

	unsigned int frameCounter = 0;
	auto currentTime = Clock::now();
	auto nextTpsUpdateTimePoint = currentTime + this->tpsUpdateInterval;

	while (this->running == true)
	{
		if (this->idle == true)
		{
			std::this_thread::sleep_for(this->idleSleepTime);
		}

		const auto newTime = Clock::now();
		Clock::duration frameDuration = newTime - currentTime;

		if (frameDuration > MAX_FRAME_TIME && this->idle == false)
		{
			frameDuration = MAX_FRAME_TIME;
			if (this->idle == false)
			{
				this->applicationLog.warning().format("Frame time exceeded maximum of %.4f. Clamping frame time to maximum.", util::durationToSeconds<float>(MAX_FRAME_TIME));
			}
		}

		currentTime = newTime;

		if (currentTime > nextTpsUpdateTimePoint)
		{
			const auto timeSinceLastUpdate = this->tpsUpdateInterval + (currentTime - nextTpsUpdateTimePoint);
			this->tps = static_cast<float>(frameCounter) / util::durationToSeconds<float>(timeSinceLastUpdate);

			frameCounter = 0;
			nextTpsUpdateTimePoint = currentTime + tpsUpdateInterval;
		}

		this->processManager.updateProcesses(frameDuration);
		this->onUpdate(frameDuration);

		frameCounter++;
	}

	return 0;
}

void Application::setIdle(const bool idle)
{
	this->idle = idle;
}

void Application::setIdleSleepTime(const Duration& sleepTime)
{
	this->idleSleepTime = sleepTime;
}

void Application::setTpsUpdateInterval(const Duration& updateInterval)
{
	this->tpsUpdateInterval = updateInterval;
}

void Application::setDataStorage(std::unique_ptr<storage::IDataStorage> storage)
{
	this->dataStorage = std::move(storage);
}

void Application::setResourceCache(std::unique_ptr<resource::Cache> cache)
{
	this->resourceCache = std::move(cache);
}

void Application::setAudioSystem(std::unique_ptr<audio::System> audioSystem)
{
	this->audioSystem = std::move(audioSystem);
}

void Application::addProcess(std::unique_ptr<ApplicationProcess> process)
{
	process->setContext(this);
	this->processManager.startProcess(std::move(process));
}

float Application::getTps() const
{
	return this->tps;
}

const std::string& Application::getName() const
{
	return this->applicationName;
}

const std::string& Application::getOrganizationName() const
{
	return this->organizationName;
}

resource::IResourceAccess* Application::getResourceAccess() const
{
	return this->resourceCache.get();
}

storage::IDataStorage* Application::getDataStorage() const
{
	return this->dataStorage.get();
}

audio::System* Application::getAudioSystem() const
{
	return this->audioSystem.get();
}

log::Logger Application::createLogger()
{
	return log::Logger(this->logOutputManager.get());
}

void Application::quitApplication()
{
	this->running = false;
}

bool Application::onInit()
{
	return true;
}

void Application::onDestroy()
{
}

void Application::onUpdate(const Duration& /*deltaTime*/)
{
}

} }
