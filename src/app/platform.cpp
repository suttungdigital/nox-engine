/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/platform.h>
#include <nox/common/platform.h>

namespace nox { namespace app { namespace platform {

Os getOs()
{
#if NOX_OS_ANDROID
	return Os::ANDROID_;
#elif NOX_OS_LINUX
	return Os::LINUX;
#elif NOX_OS_APPLE_IOS
	return Os::IOS;
#elif NOX_OS_APPLE_MAC
	return Os::MAC;
#elif NOX_OS_WINDOWS
	return Os::WINDOWS;
#else
	return Os::UNKNOWN;
#endif
}

Env getEnv()
{
#if NOX_ENV_UNIX
	return Env::UNIX;
#elif NOX_ENV_WINDOWS
	return Env::WINDOWS;
#else
	return Env::UNKNOWN;
#endif
}

Platform getPlatform()
{
#if NOX_PLATFORM_MOBILE
	return Platform::MOBILE;
#elif NOX_PLATFORM_PC
	return Platform::PC;
#else
	return Platform::UNKNOWN;
#endif
}

bool isWindows()
{
	return getOs() == Os::WINDOWS;
}

bool isUnix()
{
	return getEnv() == Env::UNIX;
}

bool isLinux()
{
	return getOs() == Os::LINUX;
}

bool isApple()
{
	return isMac() || isIos();
}

bool isMac()
{
	return getOs() == Os::MAC;
}

bool isIos()
{
	return getOs() == Os::IOS;
}

bool isAndroid()
{
	return getOs() == Os::ANDROID_;
}

bool isPc()
{
	return getPlatform() == Platform::PC;
}

bool isMobile()
{
	return getPlatform() == Platform::MOBILE;
}

} } }
