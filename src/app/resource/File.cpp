#include <nox/app/resource/File.h>

namespace nox { namespace app { namespace resource {

File::File(const std::string& path, const Type type):
	path(path),
	type(type)
{
}

bool File::isRegular() const
{
	return this->type == Type::REGULAR;
}

bool File::isDirectory() const
{
	return this->type == Type::DIRECTORY;
}

const std::string& File::getPath() const
{
	return this->path;
}

}}}
