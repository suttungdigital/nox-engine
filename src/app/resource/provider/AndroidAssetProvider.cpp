/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/Descriptor.h>
#include <nox/app/resource/provider/AndroidAssetProvider.h>
#include <nox/app/resource/util.h>

#include <json/reader.h>
#include <android/asset_manager_jni.h>
#include <cassert>

namespace nox { namespace app { namespace resource {

AndroidAssetProvider::AndroidAssetProvider(JNIEnv* javaEnv, jobject javaAssetManager, std::string rootPath)
{
	// Remove all trailing slashes.
	while (rootPath.back() == '/' || rootPath.back() == '\\')
	{
		rootPath.erase(rootPath.size() - 1, 1);
	}

	this->rootPath = rootPath;

	this->assetManager = android::AssetManager(javaEnv, javaAssetManager);
}

void AndroidAssetProvider::setLogger(log::Logger log)
{
	this->log = std::move(log);
	this->log.setName("AndroidAssetProvider");
}

bool AndroidAssetProvider::open()
{
	this->directories = parseResourceList(*this, this->log);

	// There doesn't seem to be a way of checking if a directory exists with android.content.res.AssetManager,
	// so we always return true.
	return true;
}

std::vector<File> AndroidAssetProvider::listFiles(const File& directory)
{
	auto fileList = std::vector<File>{};

	if (this->directories.empty() == false)
	{
		this->log.debug().format("Returning pre-listed list of files in \"%s\".", directory.getPath().c_str());

		fileList = directories[directory.getPath()];
	}
	else
	{
		const auto fullDirPath = this->rootPath + "/" + directory.getPath();
		auto& directoryEntry = directories[directory.getPath()];

		this->log.debug().format("Listing files in \"%s\".", fullDirPath.c_str());

		auto androidFileList = this->assetManager.list(fullDirPath);

		for (const auto& filename : androidFileList)
		{
			const auto fullFilePath = fullDirPath + "/" + filename;
			this->log.debug().format("Looking for file \"%s\".", fullFilePath.c_str());

			auto assetFile = this->assetManager.open(fullFilePath);
			const auto relativePath = this->removeRootSegment(fullFilePath);

			if (assetFile.isValid())
			{
				this->log.debug().format("Found file \"%s\".", fullFilePath.c_str());

				fileList.push_back(File{relativePath, File::Type::REGULAR});
				directoryEntry.push_back(File{relativePath, File::Type::REGULAR});
			}
			else
			{
				this->log.debug().format("Found directory \"%s\".", fullFilePath.c_str());

				fileList.push_back(File{relativePath, File::Type::DIRECTORY});
				directoryEntry.push_back(File{relativePath, File::Type::DIRECTORY});
			}
		}
	}

	return fileList;
}

std::vector<char> AndroidAssetProvider::readRawResource(const Descriptor& descriptor)
{
	this->log.debug().format("Trying to open resource \"%s\" in \"%s\".", descriptor.getPath().c_str(), this->rootPath.c_str());

	auto filePath = this->rootPath + "/" + descriptor.getPath();
	auto assetFile = this->assetManager.open(filePath);
	std::vector<char> data;

	if (assetFile.isValid())
	{
		this->log.debug().format("Opened resource \"%s\" and reading its contents.", descriptor.getPath().c_str());
		data = assetFile.readAll();
	}
	else
	{
		this->log.debug().format("Failed opening resource \"%s\", returning empty data.", descriptor.getPath().c_str());
	}

	return data;
}

std::string AndroidAssetProvider::removeRootSegment(const std::string& path) const
{
	std::string modifiedPath;

	auto rootSegmentPos = path.find(this->rootPath);

	if (rootSegmentPos != std::string::npos)
	{
		return path.substr(rootSegmentPos + this->rootPath.size() + 1);
	}
	else
	{
		return path;
	}
}

} } }
