/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/Descriptor.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/util.h>

#include <fstream>
#include <cassert>

namespace nox { namespace app
{
namespace resource
{

BoostFilesystemProvider::BoostFilesystemProvider(std::string rootPathString):
	ignoreHiddenFiles(true)
{
	// Remove all trailing slashes.
	while (rootPathString.back() == '/' || rootPathString.back() == '\\')
	{
		rootPathString.erase(rootPathString.size() - 1, 1);
	}

	this->rootPath = rootPathString;
	this->rootPath.make_preferred();
}

void BoostFilesystemProvider::setLogger(log::Logger log)
{
	this->log = std::move(log);
	this->log.setName("BoostFilesystemProvider");
}

bool BoostFilesystemProvider::open()
{
	if (bfs::exists(this->rootPath) == false)
	{
		this->log.error().format("Failed opening provider: Root path \"%s\" does not exist.", this->rootPath.string());
		return false;
	}

	if (bfs::is_symlink(this->rootPath) == true)
	{
		this->rootPath = bfs::read_symlink(this->rootPath);
	}

	if (bfs::is_directory(this->rootPath) == false)
	{
		this->log.error().format("Failed opening provider: BoostFilesystemResourceProvider::open: Root path \"%s\" is not a directory.", this->rootPath.string());
		return false;
	}

	this->directories = parseResourceList(*this, this->log);

	return true;
}

std::vector<char> BoostFilesystemProvider::readRawResource(const Descriptor& descriptor)
{
	auto filePath = this->rootPath;
	filePath /= bfs::path(descriptor.getPath());
	filePath.make_preferred();
	auto filePathString = filePath.string();

	std::ifstream fileStream(filePathString, std::ios::binary);
	if (!fileStream)
	{
		return {};
	}

	std::vector<char> data;
	data.assign(std::istreambuf_iterator<char>(fileStream), std::istreambuf_iterator<char>());

	return data;
}

std::vector<File> BoostFilesystemProvider::listFiles(const File& directory)
{
	assert(directory.isDirectory());

	auto fileList = std::vector<File>{};

	auto dirIt = this->directories.find(directory.getPath());
	if (dirIt != this->directories.end())
	{
		this->log.debug().format("Returning pre-listed list of files in \"%s\".", directory.getPath().c_str());

		fileList = dirIt->second;
	}
	else
	{
		auto dirPath = this->rootPath;
		dirPath /= bfs::path(directory.getPath()).make_preferred();

		if (bfs::exists(dirPath) && bfs::is_directory(dirPath))
		{
			for (bfs::directory_iterator fileIt(dirPath); fileIt != bfs::directory_iterator(); ++fileIt)
			{
				const auto filePath = fileIt->path();

				if (this->ignoreHiddenFiles == false || this->fileIsHidden(filePath) == false)
				{
					const auto localFilePath = this->removeRootSegment(filePath);

					if (bfs::is_directory(filePath) == true)
					{
						fileList.push_back(File{localFilePath.generic_string(), File::Type::DIRECTORY});
					}
					else if (bfs::is_regular_file(filePath) == true)
					{
						fileList.push_back(File{localFilePath.generic_string(), File::Type::REGULAR});
					}
				}
			}
		}
	}

	return fileList;
}

bool BoostFilesystemProvider::fileIsHidden(const bfs::path& filePath) const
{
	// TODO: Should do checking for Windows as well.
	return (filePath.filename().string().front() == '.');
}

bfs::path BoostFilesystemProvider::removeRootSegment(const bfs::path& path) const
{
	bfs::path modifiedPath;

	auto pathIt = path.begin();
	bool matchesRoot = true;

	for (const bfs::path& rootPathElement : this->rootPath)
	{
		if (rootPathElement != *pathIt)
		{
			matchesRoot = false;;
		}

		pathIt++;
	}

	if (matchesRoot == false)
	{
		return path;
	}
	else
	{
		while (pathIt != path.end())
		{
			modifiedPath /= *pathIt;

			pathIt++;
		}

		return modifiedPath;
	}
}

}
} }
