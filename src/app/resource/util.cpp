/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/util.h>

#include <nox/app/resource/provider/Provider.h>
#include <nox/app/resource/File.h>
#include <nox/log/Logger.h>

#include <json/reader.h>

namespace nox { namespace app { namespace resource {

const std::string RESOURCE_LIST_FILENAME = "nox-resourcelist.json";

std::map<std::string, std::vector<File>> parseResourceList(const Json::Value& jsonList, log::Logger& log)
{
	struct EntryInfo {
		const Json::Value* value;
		std::string path;
	};

	auto directories = std::map<std::string, std::vector<File>>();

	auto valuesToParse = std::vector<EntryInfo>{ EntryInfo{&jsonList, ""} };

	while(valuesToParse.empty() == false)
	{
		const auto currentFile = valuesToParse.back();
		valuesToParse.pop_back();

		const auto& currentJsonValue = *currentFile.value;

		const auto subFileNameList = currentJsonValue.getMemberNames();
		auto& directoryFileList = directories[currentFile.path];

		log.debug().format("Getting file list for \"%s\".", currentFile.path.c_str());

		for (const auto& subFileName : subFileNameList)
		{
			log.debug().format("Checking file \"%s\".", subFileName.c_str());

			const auto& subFileValue = currentJsonValue[subFileName];

			if (subFileValue.isObject())
			{
				log.debug().raw("File is a directory.");
				directoryFileList.push_back(File{subFileName, File::Type::DIRECTORY});
				valuesToParse.push_back(EntryInfo{&subFileValue, subFileName});
			}
			else if (subFileValue.isString() && subFileValue.asString() == "file")
			{
				log.debug().raw("File is a regular file.");
				directoryFileList.push_back(File{subFileName, File::Type::REGULAR});
			}
			else
			{
				auto error = std::string("Entry in directory list is not an object or a string with value 'file'");
				log.error().raw(error);
				throw std::runtime_error(error);
			}
		}
	}

	return directories;
}

std::map<std::string, std::vector<File>> parseResourceList(const std::vector<char>& rawList, log::Logger& log)
{
	auto reader = Json::Reader();

	Json::Value jsonList;
	if (reader.parse(rawList.data(), rawList.data() + rawList.size(), jsonList, false) == true)
	{
		return parseResourceList(jsonList, log);
	}
	else
	{
		auto errors = reader.getFormattedErrorMessages();
		log.error().format("Failed parsing %s: %s", RESOURCE_LIST_FILENAME.c_str(), errors.c_str());
		return {};
	}
}

std::map<std::string, std::vector<File>> parseResourceList(Provider& resourceProvider, log::Logger& log)
{
	const auto rawList = resourceProvider.readRawResource(RESOURCE_LIST_FILENAME);

	if (rawList.empty() == false) {
		log.debug().raw("Found resource list.");

		return parseResourceList(rawList, log);
	}
	else
	{
		return {};
	}
}


}}}
