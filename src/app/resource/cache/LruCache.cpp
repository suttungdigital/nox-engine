/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/data/IExtraData.h>
#include <nox/app/resource/Handle.h>

#include "../loader/DefaultLoader.h"

#include <nox/app/resource/provider/Provider.h>
#include <nox/util/string_utils.h>

#include <cstring>
#include <cassert>
#include <queue>

namespace nox { namespace app
{
namespace resource
{

LruCache::LruCache(const unsigned int sizeInMb):
	cacheSize(sizeInMb * 1024 * 1024),
	allocatedMemory(0)
{
	this->resourceLoaders.push_back(std::unique_ptr<ILoader>(new DefualtLoader()));
}

LruCache::~LruCache()
{
	while (!this->leastRecentlyUsed.empty())
	{
		this->freeOneResource();
	}
}

void LruCache::setLogger(log::Logger log)
{
	this->log = std::move(log);
	this->log.setName("LruCache");
}

void LruCache::addLoader(std::unique_ptr<ILoader> loader)
{
	this->resourceLoaders.push_front(std::move(loader));
}

std::vector<Descriptor> LruCache::getResourcesRecursivelyInDirectory(const std::string& directoryPath) const
{
	std::vector<Descriptor> resources;

	auto providerIt = this->providerList.begin();

	while (providerIt != this->providerList.end() && resources.empty() == true)
	{
		auto& provider = *providerIt;

		auto dirsToSearch = std::vector<File>{ File(directoryPath, File::Type::DIRECTORY) };

		while (dirsToSearch.empty() == false)
		{
			auto currentDir = dirsToSearch.back();
			dirsToSearch.pop_back();

			const auto subFileList = provider->listFiles(currentDir);
			for (const auto& subFile : subFileList)
			{
				if (subFile.isDirectory())
				{
					dirsToSearch.push_back(subFile);
				}
				else if (subFile.isRegular())
				{
					this->log.debug().format("Found resource \"%s\" recursively in directory \"%s\".", subFile.getPath().c_str(), currentDir.getPath().c_str());
					resources.push_back(subFile.getPath());
				}
			}
		}

		++providerIt;
	}

	return resources;
}

std::shared_ptr<Handle> LruCache::getHandle(const Descriptor& resource)
{
	std::lock_guard<std::mutex> guard(this->handleMutex);

	std::shared_ptr<Handle> handle(this->findCachedResource(resource));

	if (handle == nullptr)
	{
		handle = this->loadResource(resource);
	}
	else
	{
		this->onCachedHandleAccessed(handle);
	}

	return handle;
}

std::shared_ptr<Handle> LruCache::loadResource(const Descriptor& resource)
{
	ILoader* loader = nullptr;
	std::shared_ptr<Handle> handle;

	bool wildcardFound = false;
	auto loaderIt = this->resourceLoaders.begin();

	while (wildcardFound == false && loaderIt != this->resourceLoaders.end())
	{
		ILoader* testLoader = loaderIt->get();
		if (util::matchWildcard(testLoader->getPattern(), resource.getPath().c_str()))
		{
			loader = testLoader;
			wildcardFound = true;
		}
		else
		{
			loaderIt++;
		}
	}

	if (!loader)
	{
		assert(loader && "Default resource loader not found");
		this->log.error().raw("Default resource loader not found.");
		return std::shared_ptr<Handle>();
	}

	std::vector<char> rawBuffer;

	auto providerIt = this->providerList.begin();
	while (providerIt != this->providerList.end() && rawBuffer.empty() == true)
	{
		auto provider = providerIt->get();
		rawBuffer = provider->readRawResource(resource);

		++providerIt;
	}

	if (rawBuffer.empty() == true)
	{
		this->log.error().format("Read 0 (zero) bytes from resource \"%s\"", resource.getPath().c_str());
		return nullptr;
	}

	if (loader->useRawFile() == true)
	{
		this->reserveSpace(rawBuffer.size());
		handle = std::make_shared<Handle>(resource, std::move(rawBuffer), nullptr);
	}
	else
	{
		auto buffer = std::vector<char>();

		std::unique_ptr<IExtraData> extraData(nullptr);
		util::Buffer<char> rawBufferRef;
		rawBufferRef.data = rawBuffer.data();
		rawBufferRef.size = rawBuffer.size();
		auto success = loader->loadResource(rawBufferRef, buffer, extraData);

		handle = std::make_shared<Handle>(resource, std::move(buffer), std::move(extraData));

		if (success == false)
		{
			this->log.error().format("Failed loading resource \"%s\"", resource.getPath().c_str());
			return nullptr;
		}
	}

	if (handle == nullptr)
	{
		handle->setDestructionListener(this);

		this->leastRecentlyUsed.push_front(handle);
		this->resourceMap[resource.getPath()] = handle;
	}

	assert(loader && "Default resource loader not found");
	return handle;
}

bool LruCache::reserveSpace(const std::size_t size)
{
	if (this->makeRoom(size) == false)
	{
		return false;
	}

	this->allocatedMemory += size;

	return true;
}

bool LruCache::makeRoom(const std::size_t size)
{
	if (size > this->cacheSize)
	{
		this->log.error().raw("No more space.");
		return false;
	}

	while (size > (this->cacheSize - this->allocatedMemory))
	{
		if (this->leastRecentlyUsed.empty())
		{
			return false;
		}

		this->freeOneResource();
	}

	return true;
}

void LruCache::freeOneResource()
{
	ResourceHandleList::iterator gonner = this->leastRecentlyUsed.end();
	gonner--;

	std::shared_ptr<Handle> handle = *gonner;

	this->leastRecentlyUsed.pop_back();
	this->resourceMap.erase(handle->getResourceDescriptor().getPath());
}

void LruCache::onCachedHandleAccessed(std::shared_ptr<Handle> handle)
{
	this->leastRecentlyUsed.remove(handle);
	this->leastRecentlyUsed.push_front(handle);
}

std::shared_ptr<Handle> LruCache::findCachedResource(const Descriptor& resource)
{
	const ResourceHandleMap::iterator it = resourceMap.find(resource.getPath());
	if (it == this->resourceMap.end())
	{
		return std::shared_ptr<Handle>();
	}

	return it->second;
}

void LruCache::dropHandle(std::shared_ptr<Handle> handle)
{
	this->leastRecentlyUsed.remove(handle);
	this->resourceMap.erase(handle->getResourceDescriptor().getPath());
}

std::size_t LruCache::preload(const std::string& pattern, std::function<void (unsigned int, bool&)> progressCallback)
{
	auto loaded = std::size_t{0};
	auto cancel = false;

	for (auto& provider : this->providerList)
	{
		const auto numResources = provider->estimateNumResources();

		auto dirsToSearch = std::vector<File>{ File("", File::Type::DIRECTORY) };

		while (dirsToSearch.empty() == false)
		{
			auto currentDir = dirsToSearch.back();
			dirsToSearch.pop_back();

			const auto subFileList = provider->listFiles(currentDir);
			for (const auto& subFile : subFileList)
			{
				if (subFile.isDirectory())
				{
					dirsToSearch.push_back(subFile);
				}
				else if (subFile.isRegular())
				{
					if (util::matchWildcard(pattern.c_str(), subFile.getPath().c_str()))
					{
						std::shared_ptr<Handle> handle = this->getHandle(subFile.getPath());
						++loaded;
					}

					if (progressCallback)
					{
						auto progress = std::size_t{0};
						if (numResources > 0)
						{
							progress = loaded * 100 / numResources;
						}

						progressCallback(static_cast<unsigned int>(progress), cancel);
					}
				}
			}
		}
	}

	return loaded;
}

void LruCache::flush(void)
{
	while (!this->leastRecentlyUsed.empty())
	{
		std::shared_ptr<Handle> handle = leastRecentlyUsed.front();
		dropHandle(handle);
		this->leastRecentlyUsed.pop_front();
	}
}

void LruCache::onResourceHandleDestroyed(const std::size_t size)
{
	this->allocatedMemory -= size;
}

bool LruCache::addProvider(std::unique_ptr<Provider> provider)
{
	if (provider != nullptr && provider->open() == true)
	{
		this->providerList.push_back(std::move(provider));
		return true;
	}
	else
	{
		return false;
	}
}

}
} }
