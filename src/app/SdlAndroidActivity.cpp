/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/SdlAndroidActivity.h>

#include <SDL_system.h>
#include <android/asset_manager_jni.h>

namespace nox { namespace app {

SdlAndroidActivity::SdlAndroidActivity():
	javaEnv(nullptr),
	javaActivityClass(nullptr),
	javaActivity(nullptr)
{
}

void SdlAndroidActivity::initialize()
{
	this->javaEnv = static_cast<JNIEnv*>(SDL_AndroidGetJNIEnv());
	this->javaActivity = static_cast<jobject>(SDL_AndroidGetActivity());
	this->javaActivityClass = this->javaEnv->GetObjectClass(this->javaActivity);
}

JNIEnv* SdlAndroidActivity::getJavaEnvironment()
{
	return this->javaEnv;
}

jmethodID SdlAndroidActivity::getJavaMethodId(const char* name, const char* signature)
{
	return this->javaEnv->GetMethodID(this->javaActivityClass, name, signature);
}

jobject SdlAndroidActivity::getJavaAssetManager()
{
	auto methodId = this->getJavaMethodId("getAssets", "()Landroid/content/res/AssetManager;");
	return this->javaEnv->CallObjectMethod(this->javaActivity, methodId);
}

jobject SdlAndroidActivity::getJavaActivity()
{
	return this->javaActivity;
}

AAssetManager* SdlAndroidActivity::getAssetManager()
{
	return AAssetManager_fromJava(this->javaEnv, this->getJavaAssetManager());
}

} }
