/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/audio/Buffer.h>
#include <nox/app/audio/PlaybackProcess.h>
#include <nox/app/audio/System.h>
#include <nox/app/resource/data/SoundExtraData.h>
#include <nox/app/resource/Handle.h>

#include <cassert>

namespace nox { namespace app
{

namespace audio
{

PlaybackProcess::PlaybackProcess(const std::shared_ptr<resource::Handle>& soundResource, System* aud, bool loop):
	resourceHandle(soundResource),
	audioBuffer(nullptr),
	audio(aud),
	looping(loop)
{

}

PlaybackProcess::~PlaybackProcess() = default;

void PlaybackProcess::setLogger(log::Logger log)
{
	this->log = std::move(log);
	this->log.setName("audio::PlaybackProcess");
}

void PlaybackProcess::onInit()
{
	if (this->resourceHandle == nullptr || this->resourceHandle->getExtraData<resource::SoundExtraData>() == nullptr)
	{
		this->failExecution();
		return;
	}
	
	Buffer* buffer = this->audio->createAudioBuffer(this->resourceHandle);

	if (buffer == nullptr)
	{
		this->log.error().raw("No buffer in SoundProcess::onInit, stopping execution.");
		this->failExecution();
		return;
	}

	this->audioBuffer = buffer;

	this->audioBuffer->enableLooping(looping);
	this->audioBuffer->playAudio();
}

void PlaybackProcess::onUpdate(const Duration& /*deltaTime*/)
{
	if (this->audioBuffer->isPlaying() == false)
	{
		this->succeedExecution();
	}
}

void PlaybackProcess::onSuccess()
{
	this->stopAudio();
	this->releaseAudioData();
}

void PlaybackProcess::onFail()
{
	this->stopAudio();
	this->releaseAudioData();
}

void PlaybackProcess::onAbort()
{
	this->stopAudio();
	this->releaseAudioData();
}

void PlaybackProcess::stopAudio()
{
	if (this->audioBuffer != nullptr)
	{
		this->audioBuffer->stopAudio();
	}
}

void PlaybackProcess::setVolume(float vol)
{
	this->audioBuffer->setVolume(vol);
}

float PlaybackProcess::getVolume() const
{
	return this->audioBuffer->getVolume();
}

void PlaybackProcess::releaseAudioData()
{
	if (this->audioBuffer)
	{
		this->audio->releaseAudioBuffer(this->audioBuffer);
	}
}

}
} }
