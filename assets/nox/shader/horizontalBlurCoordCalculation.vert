/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Derived from origianl work from this article: http://xissburg.com/faster-gaussian-blur-in-glsl/
 */

//? required 130

in vec4 vertex;
in vec4 textureCoord;

uniform float blurSize;

out vec2 fragTexCoord;
out vec2 blurTexCoords[14];

void main()
{
	gl_Position = vertex;
	fragTexCoord = textureCoord.xy;
	blurTexCoords[ 0] = fragTexCoord + vec2(7.0 * blurSize, 0.0);
	blurTexCoords[ 1] = fragTexCoord + vec2(6.0 * blurSize, 0.0);
	blurTexCoords[ 2] = fragTexCoord + vec2(5.0 * blurSize, 0.0);
	blurTexCoords[ 3] = fragTexCoord + vec2(4.0 * blurSize, 0.0);
	blurTexCoords[ 4] = fragTexCoord + vec2(3.0 * blurSize, 0.0);
	blurTexCoords[ 5] = fragTexCoord + vec2(2.0 * blurSize, 0.0);
	blurTexCoords[ 6] = fragTexCoord + vec2(blurSize, 0.0);
	blurTexCoords[ 7] = fragTexCoord + vec2(-blurSize, 0.0);
	blurTexCoords[ 8] = fragTexCoord + vec2(-2.0 * blurSize, 0.0);
	blurTexCoords[ 9] = fragTexCoord + vec2(-3.0 * blurSize, 0.0);
	blurTexCoords[10] = fragTexCoord + vec2(-4.0 * blurSize, 0.0);
	blurTexCoords[11] = fragTexCoord + vec2(-5.0 * blurSize, 0.0);
	blurTexCoords[12] = fragTexCoord + vec2(-6.0 * blurSize, 0.0);
	blurTexCoords[13] = fragTexCoord + vec2(-7.0 * blurSize, 0.0);
}
